use crate::call_stack::{CallStack, KeyStack};
use crate::core_module::UnitValue;
use crate::inc_map::IncMap;
use crate::interpreter::{InterpreterEvent, InterpreterEventSink, RuntimeError};
use crate::item_path::ItemPath;
use crate::library::{Libraries, LookupExpressionError, LookupExpressionMutError};
use crate::value::{LiteralValue, Value};
use crate::value_key::{
    AbsoluteRuntimeKey, BlockExpressionKey, FunctionCallInputKey, RelativeRuntimeKey,
    RelativeValueKey, ValueKey,
};
use crate::values::Values;
use crate::DeferredFormatter;
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt::{Display, Formatter};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Expression {
    Literal(ItemPath, LiteralValue),
    Value(KeyStack),
    FunctionCall(FunctionCall),
    Block(ExpressionBlock),
}

impl Expression {
    pub fn execute(
        &self,
        call_stack: &CallStack,
        relative_position: usize,
        values: &mut Values,
        libraries: &Libraries,
        mut event_sink: impl InterpreterEventSink,
    ) -> Result<(), RuntimeError> {
        puffin::profile_function!();
        log::trace!("Exec {call_stack}");

        let result: Result<(), RuntimeError> = try {
            match self {
                Expression::Literal(_item_path, literal_value) => {
                    let output_key = AbsoluteRuntimeKey::new(call_stack.clone());
                    let value = literal_value.clone().into_value();
                    log::trace!("Insert literal {output_key}: {value}");
                    values.insert_value(output_key, value);
                }
                Expression::Value(key_stack) => {
                    let output_key = AbsoluteRuntimeKey::new(call_stack.clone());
                    let relative_stack = call_stack
                        .subset(relative_position)
                        .ok_or_else(|| {
                            RuntimeError::callstack_position_too_long(
                                call_stack.clone(),
                                values.clone(),
                                call_stack.clone(),
                                relative_position,
                            )
                        })?
                        .with_extend(key_stack.clone());

                    let absolute_value_key = ValueKey::new_runtime_key(relative_stack);

                    log::trace!("Insert value key {output_key}: {absolute_value_key}");
                    values.insert_ref(output_key, absolute_value_key);
                }
                Expression::FunctionCall(function_call) => {
                    function_call.execute(
                        call_stack,
                        relative_position,
                        values,
                        libraries,
                        event_sink.clone(),
                    )?;
                }
                Expression::Block(expressions) => {
                    expressions.execute(
                        call_stack,
                        relative_position,
                        values,
                        libraries,
                        event_sink.clone(),
                    )?;
                }
            }
        };

        event_sink.event(InterpreterEvent {
            call_stack: call_stack.clone(),
            values: values.clone(),
        });

        result.map_err(|e| {
            e.with_callstack(call_stack.clone())
                .with_values(values.clone())
        })
    }

    pub fn format<'s>(&'s self, libraries: &'s Libraries) -> impl Display + 's {
        DeferredFormatter::new(move |f| match self {
            Expression::Literal(item_path, literal) => {
                write!(f, "Literal {} {:?}", item_path, literal)
            }
            Expression::Value(key_stack) => {
                write!(
                    f,
                    "Value keys {}",
                    key_stack.iter().map(ToString::to_string).join(" -> ")
                )
            }
            Expression::FunctionCall(function_call) => {
                write!(f, "Function Call to {}", function_call.format(libraries))
            }
            Expression::Block(block) => {
                write!(f, "Block with {} expressions", block.expressions.len())
            }
        })
    }

    pub fn lookup_expression<'a>(
        &'a self,
        call_stack: CallStack,
        position: usize,
        libraries: &'a Libraries,
    ) -> Result<&'a Expression, LookupExpressionError> {
        match self {
            Expression::Literal(_, _) | Expression::Value(_) => {
                let top_key = call_stack.key_at_position(position).ok_or_else(|| {
                    LookupExpressionError::callstack_position_too_long(call_stack.clone(), position)
                })?;

                Err(LookupExpressionError::invalid_key(
                    top_key.clone(),
                    self.clone(),
                    call_stack,
                    position,
                ))
            }
            Expression::FunctionCall(function_call) => {
                function_call.lookup_expression(call_stack, position, libraries)
            }
            Expression::Block(block) => block.lookup_expression(call_stack, position, libraries),
        }
    }

    pub fn lookup_expression_mut(
        &mut self,
        call_stack: CallStack,
        position: usize,
    ) -> Result<&mut Expression, LookupExpressionMutError> {
        match self {
            Expression::Literal(_, _) | Expression::Value(_) => {
                let top_key = call_stack.key_at_position(position).ok_or_else(|| {
                    LookupExpressionError::callstack_position_too_long(call_stack.clone(), position)
                })?;

                Err(LookupExpressionError::invalid_key(
                    top_key.clone(),
                    self.clone(),
                    call_stack,
                    position,
                )
                .into())
            }
            Expression::FunctionCall(function_call) => {
                function_call.lookup_expression_mut(call_stack, position)
            }
            Expression::Block(block) => block.lookup_expression_mut(call_stack, position),
        }
    }
}

impl Display for Expression {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Expression::Literal(item_path, value) => write!(f, "{value} ({item_path})"),
            Expression::Value(key_stack) => write!(
                f,
                "{}",
                key_stack.iter().map(ToString::to_string).join(" -> ")
            ),
            Expression::FunctionCall(function_call) => {
                writeln!(f, "{} (", function_call.function)?;

                for (input_key, input_expression) in function_call.input_values.iter() {
                    writeln!(f, "{input_key}: {input_expression}")?;
                }

                writeln!(f, ")")
            }
            Expression::Block(block) => {
                writeln!(f, "{{")?;
                for (expression_key, expression) in block.expressions.iter() {
                    let key = BlockExpressionKey { expression_key };
                    writeln!(f, "{key} = {expression}")?;
                }
                writeln!(f, "}}")
            }
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FunctionCall {
    pub function: ItemPath,
    pub input_values: HashMap<FunctionCallInputKey, Expression>,
}

impl FunctionCall {
    pub fn format<'s>(&'s self, libraries: &'s Libraries) -> impl Display + 's {
        DeferredFormatter::new(move |f| {
            if let Ok(function) = libraries.lookup_fn(&self.function) {
                write!(f, "{}: {}", self.function, function)
            } else {
                write!(f, "{}(<missing function>)", self.function)
            }
        })
    }

    fn execute(
        &self,
        call_stack: &CallStack,
        relative_position: usize,
        values: &mut Values,
        libraries: &Libraries,
        event_sink: impl InterpreterEventSink,
    ) -> Result<(), RuntimeError> {
        // Insert the values for the inputs of the function call
        for (input_key, expression) in self.input_values.iter() {
            let input_call_stack = call_stack
                .clone()
                .with_push(RelativeRuntimeKey::new_function_input(input_key.clone()));

            expression.execute(
                &input_call_stack,
                relative_position,
                values,
                libraries,
                event_sink.clone(),
            )?;
        }

        // Lookup the function in the libraries
        let function = libraries
            .lookup_fn(&self.function)
            .map_err(|e| RuntimeError::lookup(call_stack.clone(), values.clone(), e))?;

        // Call the function
        function.call(&call_stack, values, libraries, event_sink.clone())?;

        // Remove any remaining values that were inserted for the inputs to the function call
        for (input_key, _) in self.input_values.iter() {
            let input_call_stack = call_stack
                .clone()
                .with_push(RelativeRuntimeKey::new_function_input(input_key.clone()));

            values.remove(&AbsoluteRuntimeKey::new(input_call_stack));
        }

        Ok(())
    }

    pub fn lookup_expression<'a>(
        &'a self,
        call_stack: CallStack,
        position: usize,
        libraries: &'a Libraries,
    ) -> Result<&'a Expression, LookupExpressionError> {
        let top_key = call_stack.key_at_position(position).ok_or_else(|| {
            LookupExpressionError::callstack_position_too_long(call_stack.clone(), position)
        })?;

        let next_expression = match top_key {
            RelativeRuntimeKey::FunctionCallInput(input_key) => {
                let input_key = input_key.clone();
                self.input_values.get(&input_key).ok_or_else(|| {
                    LookupExpressionError::invalid_key(
                        input_key.into(),
                        Expression::FunctionCall(self.clone()),
                        call_stack.clone(),
                        position,
                    )
                })?
            }

            RelativeRuntimeKey::FunctionCallOutput => {
                let function = libraries
                    .lookup_fn(&self.function)
                    .map_err(|e| {
                        LookupExpressionError::missing_function_call(
                            e,
                            call_stack.clone(),
                            position,
                        )
                    })?
                    .user()
                    .ok_or_else(|| {
                        LookupExpressionError::native_function_call(
                            self.function.clone(),
                            call_stack.clone(),
                            position,
                        )
                    })?;

                &function.body
            }

            RelativeRuntimeKey::BlockExpression(_) => {
                return Err(LookupExpressionError::invalid_key(
                    top_key.clone(),
                    Expression::FunctionCall(self.clone()),
                    call_stack,
                    position,
                ))
            }
        };

        if position < call_stack.len() {
            next_expression.lookup_expression(call_stack, position + 1, libraries)
        } else {
            Ok(next_expression)
        }
    }

    pub fn lookup_expression_mut(
        &mut self,
        call_stack: CallStack,
        position: usize,
    ) -> Result<&mut Expression, LookupExpressionMutError> {
        let top_key = call_stack.key_at_position(position).ok_or_else(|| {
            LookupExpressionError::callstack_position_too_long(call_stack.clone(), position)
        })?;

        match top_key {
            RelativeRuntimeKey::FunctionCallInput(input_key) => {
                let input_key = input_key.clone();
                let function_call = self.clone();
                let next_expression = self.input_values.get_mut(&input_key).ok_or_else(|| {
                    LookupExpressionError::invalid_key(
                        input_key.into(),
                        Expression::FunctionCall(function_call),
                        call_stack.clone(),
                        position,
                    )
                })?;

                if position < call_stack.len() {
                    next_expression.lookup_expression_mut(call_stack, position + 1)
                } else {
                    Ok(next_expression)
                }
            }

            RelativeRuntimeKey::FunctionCallOutput => Err(
                LookupExpressionMutError::function_not_mutable(call_stack, position),
            ),

            RelativeRuntimeKey::BlockExpression(_) => Err(LookupExpressionError::invalid_key(
                top_key.clone(),
                Expression::FunctionCall(self.clone()),
                call_stack,
                position,
            )
            .into()),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ExpressionBlock {
    pub expressions: IncMap<Expression>,
}

impl ExpressionBlock {
    pub fn new() -> ExpressionBlock {
        ExpressionBlock {
            expressions: IncMap::new(),
        }
    }

    pub fn iter(&self) -> impl Iterator<Item = (BlockExpressionKey, &Expression)> {
        self.expressions
            .iter()
            .map(|(expression_key, expression)| (BlockExpressionKey { expression_key }, expression))
    }

    pub fn execute(
        &self,
        call_stack: &CallStack,
        relative_position: usize,
        values: &mut Values,
        libraries: &Libraries,
        event_sink: impl InterpreterEventSink,
    ) -> Result<(), RuntimeError> {
        let output_key = AbsoluteRuntimeKey::new(call_stack.clone());

        // Go through all the expressions in the body and call them
        for (key, expression) in self.expressions.iter() {
            let callee_call_stack = call_stack
                .clone()
                .with_push(RelativeRuntimeKey::from_expression_key(key));

            expression.execute(
                &callee_call_stack,
                relative_position,
                values,
                libraries,
                event_sink.clone(),
            )?;
        }

        if let Some(last_value_key) = self.expressions.get_last_key() {
            let last_value_stack = call_stack
                .clone()
                .with_push(RelativeRuntimeKey::from_expression_key(last_value_key));

            let last_value = values
                .remove(&AbsoluteRuntimeKey::new(last_value_stack))
                .ok_or_else(|| {
                    RuntimeError::missing_output_value(call_stack.clone(), values.clone())
                })?;

            log::trace!("Insert block expr {output_key}: {last_value}");
            values.insert(output_key, last_value);
        } else {
            log::trace!("Insert block expr {output_key}: ()");
            values.insert_value(output_key, Value::from_value(UnitValue));
        }

        for (key, _) in self.expressions.iter() {
            let callee_call_stack = call_stack
                .clone()
                .with_push(RelativeRuntimeKey::from_expression_key(key));

            values.remove(&AbsoluteRuntimeKey::new(callee_call_stack));
        }

        Ok(())
    }

    /*
    pub fn expression_output_type<'path>(
        &'path self,
        output_key: BlockExpressionKey,
        input_types: &'path IndexMap<FunctionCallInputKey, ItemPath>,
        libraries: &'path Libraries,
    ) -> Result<&'path ItemPath, RuntimeError> {
        let expression = self
            .expressions
            .get(output_key.expression_key)
            .ok_or(RuntimeError::missing_output_value(None, None))?;

        match expression {
            Expression::Literal(_type, _) => Ok(_type),
            Expression::Value(value_key) => match value_key {
                RelativeValueKey::Runtime(expression_key) => match expression_key {
                    RelativeRuntimeKey::FunctionCallInput(input_key) => input_types
                        .get(input_key)
                        .ok_or(RuntimeError::missing_input(None, None, input_key.clone())),
                    RelativeRuntimeKey::BlockExpression(output_key) => {
                        self.expression_output_type(output_key.clone(), input_types, libraries)
                    }
                },
                RelativeValueKey::Constant(_) => {
                    Err(RuntimeError::missing_output_value(None, None)) // No values here, constants should be in libraries
                }
            },
            Expression::FunctionCall(function_call) => {
                let function = libraries.lookup_fn(&function_call.function)?;
                Ok(function.output_type())
            }
            Expression::Block(block) => {
                let last_output_key = BlockExpressionKey {
                    expression_key: block
                        .expressions
                        .get_last_key()
                        .ok_or(RuntimeError::missing_output_value(None, None))?,
                };

                block.expression_output_type(last_output_key, input_types, libraries)
            }
        }
    }

    pub fn available_values<'path>(
        &'path self,
        up_to_expression_key: Option<Key<Expression>>,
        input_types: &'path IndexMap<FunctionCallInputKey, ItemPath>,
        libraries: &'path Libraries,
    ) -> Vec<(RelativeRuntimeKey, Result<&'path ItemPath, RuntimeError>)> {
        let mut value_keys = Vec::new();

        for (input_key, input_type) in input_types.iter() {
            let key = RelativeRuntimeKey::FunctionCallInput(input_key.clone());
            value_keys.push((key, Ok(input_type)));
        }

        for (expression_key, _expression) in self.expressions.iter() {
            if up_to_expression_key.is_some_and(|up_to| up_to == expression_key) {
                break;
            }

            let output_key = BlockExpressionKey { expression_key };

            let output_type = self.expression_output_type(output_key, input_types, libraries);

            value_keys.push((RelativeRuntimeKey::BlockExpression(output_key), output_type));
        }

        value_keys
    }
     */

    pub fn lookup_expression<'a>(
        &'a self,
        call_stack: CallStack,
        position: usize,
        libraries: &'a Libraries,
    ) -> Result<&'a Expression, LookupExpressionError> {
        let top_key = call_stack.key_at_position(position).ok_or_else(|| {
            LookupExpressionError::callstack_position_too_long(call_stack.clone(), position)
        })?;

        let next_expression = match top_key {
            RelativeRuntimeKey::FunctionCallInput(_) | RelativeRuntimeKey::FunctionCallOutput => {
                Err(LookupExpressionError::invalid_key(
                    top_key.clone(),
                    Expression::Block(self.clone()),
                    call_stack.clone(),
                    position,
                ))?
            }
            RelativeRuntimeKey::BlockExpression(expression_key) => self
                .expressions
                .get(expression_key.expression_key)
                .ok_or_else(|| {
                    LookupExpressionError::invalid_key(
                        top_key.clone(),
                        Expression::Block(self.clone()),
                        call_stack.clone(),
                        position,
                    )
                })?,
        };

        if position < call_stack.len() {
            next_expression.lookup_expression(call_stack, position + 1, libraries)
        } else {
            Ok(next_expression)
        }
    }

    pub fn lookup_expression_mut(
        &mut self,
        call_stack: CallStack,
        position: usize,
    ) -> Result<&mut Expression, LookupExpressionMutError> {
        let top_key = call_stack.key_at_position(position).ok_or_else(|| {
            LookupExpressionError::callstack_position_too_long(call_stack.clone(), position)
        })?;

        let next_expression = match top_key {
            RelativeRuntimeKey::FunctionCallInput(_) | RelativeRuntimeKey::FunctionCallOutput => {
                Err(LookupExpressionError::invalid_key(
                    top_key.clone(),
                    Expression::Block(self.clone()),
                    call_stack.clone(),
                    position,
                ))?
            }
            RelativeRuntimeKey::BlockExpression(expression_key) => {
                let block = self.clone();
                self.expressions
                    .get_mut(expression_key.expression_key)
                    .ok_or_else(|| {
                        LookupExpressionError::invalid_key(
                            top_key.clone(),
                            Expression::Block(block),
                            call_stack.clone(),
                            position,
                        )
                    })?
            }
        };

        if position < call_stack.len() {
            next_expression.lookup_expression_mut(call_stack, position + 1)
        } else {
            Ok(next_expression)
        }
    }
}
