#![feature(try_blocks)]
#![feature(error_generic_member_access)]
extern crate core;

use std::cell::Cell;
use std::fmt::{Display, Formatter};

pub mod call_stack;
pub mod core_module;
pub mod expression;
pub mod function;
pub mod inc_map;
pub mod interpreter;
pub mod item_path;
pub mod library;
mod library_builder;
pub mod module;
#[cfg(test)]
mod test;
pub mod type_;
pub mod value;
pub mod value_key;
pub mod values;

pub struct DeferredFormatter<F>(Cell<Option<F>>);

impl<F: FnOnce(&mut Formatter<'_>) -> std::fmt::Result> DeferredFormatter<F> {
    pub fn new(f: F) -> DeferredFormatter<F> {
        DeferredFormatter(Cell::new(Some(f)))
    }
}

impl<F: FnOnce(&mut Formatter<'_>) -> std::fmt::Result> Display for DeferredFormatter<F> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let Some(fmt_fn) = self.0.replace(None) {
            fmt_fn(f)
        } else {
            write!(f, "ERROR FORMATTING: FORMAT FUNCTION TAKEN")
        }
    }
}
