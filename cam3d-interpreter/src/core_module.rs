use crate::library::native::NativeLibrary;
use crate::library::CoreItems;
use crate::module::native::NativeModule;
use crate::type_::NativeType;
use crate::value::{LiteralValueBox, ValueBox, ValueUnboxed};
use serde::{Deserialize, Serialize};
use std::any::Any;
use std::fmt::{Debug, Display, Formatter};
use std::ops::{Add, Div, Mul, Sub};
use std::time::Duration;

pub fn core_lib() -> (NativeLibrary, CoreItems) {
    let mut library = NativeLibrary::new("core");

    let unit_type = library.insert_type(NativeType::new("()"));
    let string_type = library.insert_type(NativeType::new("String"));
    let multilinestring_type = library.insert_type(NativeType::new("MultiLineString"));

    let list_type = library.insert_type(NativeType::new_with_generics("List", ["T"]));

    let test_module = library.insert_module(NativeModule::new("test"));
    library.insert_fn1_to(
        (&test_module, "panic"),
        ("message", &string_type),
        &unit_type,
        |msg: &String| {
            panic!("{}", msg);
            UnitValue
        },
    );

    let integer_module = library.insert_module(NativeModule::new("int"));

    let integer_type = library.insert_type_to(&integer_module, NativeType::new("Integer"));

    library.insert_method2_to(
        (&integer_type, "add"),
        ("b", &integer_type),
        &integer_type,
        |a: &i32, b: &i32| i32::add(*a, *b),
    );

    library.insert_method2_to(
        (&integer_type, "sub"),
        ("b", &integer_type),
        &integer_type,
        |a: &i32, b: &i32| i32::sub(*a, *b),
    );

    library.insert_method2_to(
        (&integer_type, "mul"),
        ("b", &integer_type),
        &integer_type,
        |a: &i32, b: &i32| i32::mul(*a, *b),
    );

    library.insert_method2_to(
        (&integer_type, "div"),
        ("b", &integer_type),
        &integer_type,
        |a: &i32, b: &i32| i32::div(*a, *b),
    );

    let float_module = library.insert_module(NativeModule::new("float"));

    let float_type = library.insert_type_to(&float_module, NativeType::new("Float"));
    let float_option_type = library.insert_type_to(&float_module, NativeType::new("Option<Float>"));

    library.insert_method2_to(
        (&float_type, "add"),
        ("b", &float_type),
        &float_type,
        |a: &f32, b: &f32| f32::add(*a, *b),
    );

    library.insert_method2_to(
        (&float_type, "sub"),
        ("b", &float_type),
        &float_type,
        |a: &f32, b: &f32| f32::sub(*a, *b),
    );

    library.insert_method2_to(
        (&float_type, "mul"),
        ("b", &float_type),
        &float_type,
        |a: &f32, b: &f32| f32::mul(*a, *b),
    );

    library.insert_method2_to(
        (&float_type, "div"),
        ("b", &float_type),
        &float_type,
        |a: &f32, b: &f32| f32::div(*a, *b),
    );

    let bool_module = library.insert_module(NativeModule::new("bool"));
    let bool_type = library.insert_type_to(&bool_module, NativeType::new("bool"));

    library.insert_method1_to((&bool_type, "not"), &bool_type, |a: &bool| !a);

    library.insert_method2_to(
        (&bool_type, "and"),
        ("b", &bool_type),
        &bool_type,
        |a: &bool, b: &bool| *a && *b,
    );

    library.insert_method2_to(
        (&bool_type, "or"),
        ("b", &bool_type),
        &bool_type,
        |a: &bool, b: &bool| *a && *b,
    );

    let time_module = library.insert_module(NativeModule::new("time"));

    library.insert_fn1_to(
        (&time_module, "sleep"),
        ("s", &float_type),
        &unit_type,
        |s: &f32| {
            std::thread::sleep(Duration::from_secs_f32(*s));
            UnitValue
        },
    );

    let filesystem_module = library.insert_module(NativeModule::new("fs"));

    let path_open_type = library.insert_type_to(&filesystem_module, NativeType::new("PathOpen"));
    let path_save_type = library.insert_type_to(&filesystem_module, NativeType::new("PathSave"));

    let core_items = CoreItems {
        unit_type,
        bool_type,
        integer_type,
        float_type,
        float_option_type,
        string_type,
        newlinestring: multilinestring_type,
        path_open_type,
        path_save_type,
    };

    (library, core_items)
}

#[derive(Debug, Clone, Copy, Default, Serialize, Deserialize)]
pub struct UnitValue;

#[typetag::serde]
impl LiteralValueBox for UnitValue {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox> {
        Box::new(*self)
    }
}

impl ValueBox for UnitValue {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for UnitValue {
    fn name() -> &'static str {
        "()"
    }
}

impl Display for UnitValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str("()")
    }
}

#[typetag::serde]
impl LiteralValueBox for i32 {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox> {
        Box::new(*self)
    }
}

impl ValueBox for i32 {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for i32 {
    fn name() -> &'static str {
        "i32"
    }
}

#[typetag::serde]
impl LiteralValueBox for f32 {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox> {
        Box::new(*self)
    }
}

impl ValueBox for f32 {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for f32 {
    fn name() -> &'static str {
        "f32"
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FloatOptionValue(pub Option<f32>);

impl Display for FloatOptionValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.0 {
            Some(x) => Display::fmt(&x, f),
            None => write!(f, "--"),
        }
    }
}

#[typetag::serde]
impl LiteralValueBox for FloatOptionValue {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox> {
        Box::new(*self)
    }
}

impl ValueBox for FloatOptionValue {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for FloatOptionValue {
    fn name() -> &'static str {
        "option_f32"
    }
}

#[typetag::serde]
impl LiteralValueBox for bool {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox> {
        Box::new(*self)
    }
}

impl ValueBox for bool {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for bool {
    fn name() -> &'static str {
        "bool"
    }
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct PathValue(pub std::path::PathBuf);

impl Display for PathValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0.display())
    }
}

#[typetag::serde]
impl LiteralValueBox for PathValue {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox> {
        Box::new(*self)
    }
}

impl ValueBox for PathValue {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for PathValue {
    fn name() -> &'static str {
        "PathBuf"
    }
}

#[typetag::serde]
impl LiteralValueBox for String {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox> {
        Box::new(*self)
    }
}

impl ValueBox for String {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for String {
    fn name() -> &'static str {
        "String"
    }
}
