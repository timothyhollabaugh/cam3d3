use crate::expression::{Expression, FunctionCall};
use crate::function::FunctionRef;
use crate::item_path::ItemPath;
use crate::library::{Libraries, LookupError};
use crate::value_key::RelativeRuntimeKey;
use crate::DeferredFormatter;
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct KeyStack(Vec<RelativeRuntimeKey>);

impl KeyStack {
    pub fn new() -> KeyStack {
        KeyStack(Vec::new())
    }

    pub fn new_with_key(key: RelativeRuntimeKey) -> KeyStack {
        KeyStack(vec![key])
    }

    pub fn new_with_keys(keys: Vec<RelativeRuntimeKey>) -> KeyStack {
        KeyStack(keys)
    }

    pub fn with_push(mut self, key: RelativeRuntimeKey) -> KeyStack {
        self.0.push(key);
        self
    }

    pub fn push(&mut self, key: RelativeRuntimeKey) {
        self.0.push(key);
    }

    pub fn with_extend(mut self, keys: KeyStack) -> KeyStack {
        self.0.extend(keys.0);
        self
    }

    pub fn extend(&mut self, keys: KeyStack) {
        self.0.extend(keys.0);
    }

    pub fn iter(&self) -> impl Iterator<Item = &RelativeRuntimeKey> {
        self.0.iter()
    }

    pub fn first(&self) -> Option<&RelativeRuntimeKey> {
        self.0.first()
    }

    pub fn last(&self) -> Option<&RelativeRuntimeKey> {
        self.0.last()
    }

    pub fn pop(&mut self) -> Option<RelativeRuntimeKey> {
        self.0.pop()
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }
}

impl Display for KeyStack {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let [first, remaining @ ..] = self.0.as_slice() {
            write!(f, "{}", first)?;
            for key in remaining {
                write!(f, "-> {}", key)?;
            }

            Ok(())
        } else {
            write!(f, "<empty>")
        }
    }
}

///
/// The path to any expression that could be executed,
/// referring both to the output of the expression and where the value goes
///
/// Ex. For the following program,
///
/// ```txt
/// fn UserModule::main() -> core::int::int {
///   _fn_1 = core::int::add(3, 4);
///   _fn_2 = core::int::sub(_fn_1, 2);
///   _fn_3 = core::int::add(core::int::mul(_fn_1, _fn_2), 3);
///   _fn_4 = UserModule::double(_fn_3);
///   _fn_5 = UserModule::triple(_fn_3);
///   _fn_5
/// }
///
/// fn UserModule::double(_in_a: core::int::int) -> int {
///     _fn_1 = core::int::add(_in_a, _in_a);
///     _fn_1
/// }
///
/// fn UserModule::triple(_in_a: core::int::int) -> int {
///     _fn_1 = core::int::add(core::int::add(_in_a, _in_a), _in_a);
///     _fn_1
/// }
/// ```
///
/// These call stacks will be encountered, in order:
///
/// ```txt
/// UserModule::main
/// UserModule::main -> _fn_1
/// UserModule::main -> _fn_1 -> _in_a
/// UserModule::main -> _fn_1 -> _in_b
/// UserModule::main -> _fn_2
/// UserModule::main -> _fn_2 -> _in_a
/// UserModule::main -> _fn_2 -> _in_b
/// UserModule::main -> _fn_3
/// UserModule::main -> _fn_3 -> _in_a
/// UserModule::main -> _fn_3 -> _in_a -> _in_a
/// UserModule::main -> _fn_3 -> _in_a -> _in_b
/// UserModule::main -> _fn_3 -> _in_b
/// UserModule::main -> _fn_4 -> _in_a
/// UserModule::main -> _fn_4 -> _fn_1
/// UserModule::main -> _fn_4 -> _fn_1 -> _in_a
/// UserModule::main -> _fn_4 -> _fn_1 -> _in_b
/// UserModule::main -> _fn_4 -> _fn_2
/// UserModule::main -> _fn_5
/// UserModule::main -> _fn_5 -> _in_a
/// UserModule::main -> _fn_5 -> _fn_1
/// UserModule::main -> _fn_5 -> _fn_1 -> _in_a
/// UserModule::main -> _fn_5 -> _fn_1 -> _in_a -> _in_a
/// UserModule::main -> _fn_5 -> _fn_1 -> _in_a -> _in_b
/// UserModule::main -> _fn_5 -> _fn_1 -> _in_b
/// UserModule::main -> _fn_5 -> _fn_2
/// UserModule::main -> _fn_6
/// ```
///
#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct CallStack {
    root: ItemPath,
    stack: KeyStack,
}

impl Display for CallStack {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} -> ", self.root)?;
        write!(f, "{}", self.stack.iter().join("::"))
    }
}

#[derive(thiserror::Error, Debug)]
pub enum UserFunctionProgressError {
    #[error("{0}")]
    Lookup(
        #[from]
        #[backtrace]
        LookupError,
    ),
    #[error("Invalid function call key")]
    InvalidFunctionCallKey,

    #[error("Empty call stack")]
    EmptyCallStack,

    #[error("Not a user function")]
    NotAUserFunction,
}

impl CallStack {
    pub fn new(root: ItemPath) -> CallStack {
        CallStack {
            root,
            stack: KeyStack::new(),
        }
    }

    pub fn new_with_stack(root: ItemPath, stack: KeyStack) -> CallStack {
        CallStack { root, stack }
    }

    pub fn root(&self) -> &ItemPath {
        &self.root
    }

    pub fn stack(&self) -> &KeyStack {
        &self.stack
    }

    pub fn progress(
        &self,
        libraries: &Libraries,
    ) -> Result<(usize, usize, String), UserFunctionProgressError> {
        let FunctionRef::User(function) = libraries.lookup_fn(&self.root)? else {
            return Err(UserFunctionProgressError::NotAUserFunction);
        };

        let first_call = self
            .stack
            .0
            .first()
            .ok_or(UserFunctionProgressError::EmptyCallStack)?;

        let (pos, len) = match (first_call, &function.body) {
            (RelativeRuntimeKey::BlockExpression(expression_key), Expression::Block(block)) => {
                let position = block
                    .expressions
                    .position(expression_key.expression_key)
                    .ok_or(UserFunctionProgressError::InvalidFunctionCallKey)?;
                let len = block.expressions.len();
                (position, len)
            }
            _ => (0, 1),
        };

        Ok((pos, len, self.to_string()))
    }

    pub fn push(&mut self, key: RelativeRuntimeKey) {
        self.stack.push(key);
    }

    pub fn with_push(mut self, key: RelativeRuntimeKey) -> CallStack {
        self.push(key);
        self
    }

    pub fn pop(&mut self) -> Option<RelativeRuntimeKey> {
        self.stack.pop()
    }

    pub fn extend(&mut self, keys: KeyStack) {
        self.stack.extend(keys)
    }

    pub fn with_extend(mut self, keys: KeyStack) -> CallStack {
        CallStack {
            root: self.root,
            stack: self.stack.with_extend(keys),
        }
    }

    /// Create a new callstack that includes all of this call stack up to and
    /// including the given position.
    pub fn subset(&self, position: usize) -> Option<CallStack> {
        self.stack
            .0
            .split_at_checked(position)
            .map(|(stack, _)| CallStack {
                root: self.root.clone(),
                stack: KeyStack::new_with_keys(stack.to_vec()),
            })
    }

    /// Get the key at the given positions.
    /// Position `0` is the root function itself, so it has no key and returns None.
    /// Positions `1..self.len()` return the key at that position in the stack
    /// Positions `> self.len()` return None
    pub fn key_at_position(&self, position: usize) -> Option<&RelativeRuntimeKey> {
        if position == 0 {
            None
        } else {
            self.stack.0.get(position - 1)
        }
    }

    pub fn len(&self) -> usize {
        self.stack.len()
    }

    /// UserModule::main -> _fn_5 -> _fn_1 -> _in_a -> _in_b
    ///
    /// UserModule::main
    /// _fn_5 UserModule::triple
    /// _fn_1 core::int::add
    /// _in_a core::int::add
    /// _in_b _in_a
    pub fn format<'s>(
        &'s self,
        root_call: Option<FunctionCall>,
        libraries: &'s Libraries,
    ) -> impl Display + 's {
        DeferredFormatter::new(move |f: &mut Formatter| {
            if let Ok(first_function) = libraries.lookup_fn(&self.root) {
                write!(f, "{}", first_function)?;
                writeln!(f)?;
            } else {
                writeln!(f, "{} <missing-function>", self.root)?;
            };

            for position in 1..self.stack.len() {
                if let Some(substack) = self.subset(position) {
                    let key = substack
                        .stack
                        .last()
                        .map(|k| k.to_string())
                        .unwrap_or("--".into());

                    let expression_result = if let Some(root_call) = &root_call {
                        root_call.lookup_expression(substack, 1, libraries)
                    } else {
                        libraries.lookup_expression(substack)
                    };

                    match expression_result {
                        Ok(expression) => {
                            writeln!(f, "{}: {key}: {}", position, expression.format(libraries))?;
                        }
                        Err(e) => {
                            writeln!(f, "{}: {key}: Error: {}", position, e)?;
                            log::error!("Error formatting callstack {self}: {e:#?}");
                        }
                    }
                } else {
                    writeln!(f, "{}: <missing-expression>", position)?;
                }
            }

            Ok(())
        })
    }
}
