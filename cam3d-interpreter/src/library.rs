pub mod native;
pub mod user;

use crate::call_stack::CallStack;
use crate::expression::{Expression, ExpressionBlock};
use crate::function::user::UserFunction;
use crate::function::{FunctionMut, FunctionRef};
use crate::item_path::ItemPath;
use crate::module::{ModuleMut, ModuleRef};
use crate::type_::{TypeMut, TypeRef};
use crate::value_key::{RelativeRuntimeKey, RelativeValueKey};
use indexmap::IndexMap;
use native::NativeLibrary;
use std::backtrace::Backtrace;
use user::UserLibrary;

#[derive(Clone, Debug)]
pub struct CoreItems {
    pub unit_type: ItemPath,
    pub bool_type: ItemPath,
    pub integer_type: ItemPath,
    pub float_type: ItemPath,
    pub float_option_type: ItemPath,
    pub string_type: ItemPath,
    pub newlinestring: ItemPath,
    pub path_open_type: ItemPath,
    pub path_save_type: ItemPath,
}

#[derive(thiserror::Error, Debug)]
#[error("{kind}")]
pub struct LookupError {
    #[source]
    kind: LookupErrorKind,
    backtrace: Backtrace,
}

#[derive(thiserror::Error, Debug)]
pub enum LookupErrorKind {
    #[error("Searched the wrong library {0}")]
    WrongLibrary(String),

    #[error("Could not find library")]
    MissingLibrary,

    #[error("Could not find type: {0}")]
    MissingType(ItemPath),

    #[error("Could not find function: {0}")]
    MissingFunction(ItemPath),

    #[error("Could not find module: {0}")]
    MissingModule(ItemPath),
}

impl LookupError {
    pub fn wrong_library(l: String) -> LookupError {
        LookupError {
            kind: LookupErrorKind::WrongLibrary(l),
            backtrace: Backtrace::capture(),
        }
    }

    pub fn missing_library() -> LookupError {
        LookupError {
            kind: LookupErrorKind::MissingLibrary,
            backtrace: Backtrace::capture(),
        }
    }

    pub fn missing_type(i: ItemPath) -> LookupError {
        LookupError {
            kind: LookupErrorKind::MissingType(i),
            backtrace: Backtrace::capture(),
        }
    }

    pub fn missing_function(i: ItemPath) -> LookupError {
        LookupError {
            kind: LookupErrorKind::MissingFunction(i),
            backtrace: Backtrace::capture(),
        }
    }

    pub fn missing_module(i: ItemPath) -> LookupError {
        LookupError {
            kind: LookupErrorKind::MissingModule(i),
            backtrace: Backtrace::capture(),
        }
    }
}

#[derive(thiserror::Error, Debug)]
pub enum LookupExpressionErrorKind {
    #[error("Could not find the root function: {0}")]
    MissingRootFunction(LookupError),

    #[error("Root function is a native function: {function_path}")]
    NativeRootFunction {
        function_path: ItemPath,
        backtrace: Backtrace,
    },

    #[error("Could not find function call: {0}")]
    MissingFunctionCall(LookupError),

    #[error("Function call is a native function: {function_path}")]
    NativeFunctionCall {
        function_path: ItemPath,
        backtrace: Backtrace,
    },

    #[error("Invalid key {key} provided for expression: {expression}")]
    InvalidKey {
        key: RelativeRuntimeKey,
        expression: Expression,
        backtrace: Backtrace,
    },

    #[error("Invalid key {key} provided at root function")]
    InvalidKeyAtRoot {
        key: RelativeRuntimeKey,
        backtrace: Backtrace,
    },

    #[error("The position is longer than the callstack")]
    CallStackPositionTooLong { backtrace: Backtrace },
}

#[derive(thiserror::Error, Debug)]
#[error("Error looking up an expression: {kind}\nCallstack: {call_stack}\nposition: {position}")]
pub struct LookupExpressionError {
    kind: LookupExpressionErrorKind,
    call_stack: CallStack,
    position: usize,
}

impl LookupExpressionError {
    pub fn missing_root_function(
        lookup_error: LookupError,
        call_stack: CallStack,
    ) -> LookupExpressionError {
        LookupExpressionError {
            kind: LookupExpressionErrorKind::MissingRootFunction(lookup_error),
            call_stack,
            position: 0,
        }
    }

    pub fn native_root_function(
        function_path: ItemPath,
        call_stack: CallStack,
    ) -> LookupExpressionError {
        LookupExpressionError {
            kind: LookupExpressionErrorKind::NativeRootFunction {
                function_path,
                backtrace: Backtrace::capture(),
            },
            call_stack,
            position: 0,
        }
    }

    pub fn missing_function_call(
        lookup_error: LookupError,
        call_stack: CallStack,
        position: usize,
    ) -> LookupExpressionError {
        LookupExpressionError {
            kind: LookupExpressionErrorKind::MissingFunctionCall(lookup_error),
            call_stack,
            position,
        }
    }

    pub fn native_function_call(
        function_path: ItemPath,
        call_stack: CallStack,
        position: usize,
    ) -> LookupExpressionError {
        LookupExpressionError {
            kind: LookupExpressionErrorKind::NativeFunctionCall {
                function_path,
                backtrace: Backtrace::capture(),
            },
            call_stack,
            position,
        }
    }

    pub fn invalid_key(
        key: RelativeRuntimeKey,
        expression: Expression,
        call_stack: CallStack,
        position: usize,
    ) -> LookupExpressionError {
        LookupExpressionError {
            kind: LookupExpressionErrorKind::InvalidKey {
                key,
                expression,
                backtrace: Backtrace::capture(),
            },
            call_stack,
            position,
        }
    }

    pub fn invalid_key_at_root(
        key: RelativeRuntimeKey,
        call_stack: CallStack,
        position: usize,
    ) -> LookupExpressionError {
        LookupExpressionError {
            kind: LookupExpressionErrorKind::InvalidKeyAtRoot {
                key,
                backtrace: Backtrace::capture(),
            },
            call_stack,
            position,
        }
    }

    pub fn callstack_position_too_long(
        call_stack: CallStack,
        position: usize,
    ) -> LookupExpressionError {
        LookupExpressionError {
            kind: LookupExpressionErrorKind::CallStackPositionTooLong {
                backtrace: Backtrace::capture(),
            },
            call_stack,
            position,
        }
    }
}

#[derive(thiserror::Error, Debug)]
pub enum LookupExpressionMutErrorKind {
    #[error("{0}")]
    LookupExpressionError(LookupExpressionErrorKind),

    #[error("Cannot mutate other functions")]
    FunctionNotMutable { backtrace: Backtrace },
}

#[derive(thiserror::Error, Debug)]
#[error(
    "Error looking up a mutable expression: {kind}\nCallstack: {call_stack}\nposition: {position}"
)]
pub struct LookupExpressionMutError {
    #[source]
    kind: LookupExpressionMutErrorKind,
    call_stack: CallStack,
    position: usize,
}

impl LookupExpressionMutError {
    pub fn function_not_mutable(
        call_stack: CallStack,
        position: usize,
    ) -> LookupExpressionMutError {
        LookupExpressionMutError {
            kind: LookupExpressionMutErrorKind::FunctionNotMutable {
                backtrace: Backtrace::capture(),
            },
            call_stack,
            position,
        }
    }
}

impl From<LookupExpressionError> for LookupExpressionMutError {
    fn from(e: LookupExpressionError) -> LookupExpressionMutError {
        LookupExpressionMutError {
            kind: LookupExpressionMutErrorKind::LookupExpressionError(e.kind),
            call_stack: e.call_stack,
            position: e.position,
        }
    }
}

#[derive(thiserror::Error, Debug)]
pub enum LookupOutputTypeError {
    #[error("{0}")]
    LookupExpressionError(#[from] LookupExpressionError),

    #[error("{0}")]
    LookupError(#[from] LookupError),

    #[error("Empty block has no output type")]
    EmptyBlock,
}

#[derive(Debug, Clone)]
pub struct Libraries {
    native_libraries: IndexMap<String, NativeLibrary>,
    user_libraries: IndexMap<String, UserLibrary>,
    core_items: CoreItems,
}

impl Libraries {
    pub fn with_libraries(
        native_libraries: Vec<NativeLibrary>,
        user_libraries: Vec<UserLibrary>,
        core_items: CoreItems,
    ) -> Libraries {
        Libraries {
            native_libraries: native_libraries
                .into_iter()
                .map(|l| (l.name().to_string(), l))
                .collect(),
            user_libraries: user_libraries
                .into_iter()
                .map(|l| (l.name().to_string(), l))
                .collect(),
            core_items,
        }
    }

    pub fn native_libraries(&self) -> impl Iterator<Item = (&str, &NativeLibrary)> {
        self.native_libraries
            .iter()
            .map(|(key, library)| (key.as_str(), library))
    }

    pub fn user_libraries(&self) -> impl Iterator<Item = (&str, &UserLibrary)> {
        self.user_libraries
            .iter()
            .map(|(key, library)| (key.as_str(), library))
    }

    pub fn libraries(&self) -> impl Iterator<Item = (&str, LibraryRef)> {
        let native = self
            .native_libraries
            .iter()
            .map(|(key, library)| (key.as_str(), LibraryRef::Native(library)));

        let user = self
            .user_libraries
            .iter()
            .map(|(key, library)| (key.as_str(), LibraryRef::User(library)));

        native.chain(user)
    }

    pub fn insert_native_library(&mut self, library: NativeLibrary) {
        self.native_libraries
            .insert(library.name().to_string(), library);
    }

    pub fn insert_user_library(&mut self, library: UserLibrary) {
        self.user_libraries
            .insert(library.name().to_string(), library);
    }

    pub fn core_items(&self) -> &CoreItems {
        &self.core_items
    }

    pub fn lookup_type(&self, path: &ItemPath) -> Result<TypeRef, LookupError> {
        if let Some(native_library) = self.native_libraries.get(path.library()) {
            Ok(TypeRef::Native(native_library.lookup_type(path)?))
        } else if let Some(user_library) = self.user_libraries.get(path.library()) {
            Ok(TypeRef::User(user_library.lookup_type(path)?))
        } else {
            Err(LookupError::missing_library())
        }
    }

    pub fn lookup_fn(&self, path: &ItemPath) -> Result<FunctionRef, LookupError> {
        if let Some(native_library) = self.native_libraries.get(path.library()) {
            Ok(FunctionRef::Native(native_library.lookup_fn(path)?))
        } else if let Some(user_library) = self.user_libraries.get(path.library()) {
            Ok(FunctionRef::User(user_library.lookup_fn(path)?))
        } else {
            Err(LookupError::missing_library())
        }
    }

    pub fn lookup_module(&self, path: &ItemPath) -> Result<ModuleRef, LookupError> {
        if let Some(native_library) = self.native_libraries.get(path.library()) {
            Ok(ModuleRef::Native(native_library.lookup_module(path)?))
        } else if let Some(user_library) = self.user_libraries.get(path.library()) {
            Ok(ModuleRef::User(user_library.lookup_module(path)?))
        } else {
            Err(LookupError::missing_library())
        }
    }

    pub fn lookup_type_mut(&mut self, path: &ItemPath) -> Result<TypeMut, LookupError> {
        if let Some(native_library) = self.native_libraries.get_mut(path.library()) {
            Ok(TypeMut::Native(native_library.lookup_type_mut(path)?))
        } else if let Some(user_library) = self.user_libraries.get_mut(path.library()) {
            Ok(TypeMut::User(user_library.lookup_type_mut(path)?))
        } else {
            Err(LookupError::missing_library())
        }
    }

    pub fn lookup_fn_mut(&mut self, path: &ItemPath) -> Result<FunctionMut, LookupError> {
        if let Some(native_library) = self.native_libraries.get_mut(path.library()) {
            Ok(FunctionMut::Native(native_library.lookup_fn_mut(path)?))
        } else if let Some(user_library) = self.user_libraries.get_mut(path.library()) {
            Ok(FunctionMut::User(user_library.lookup_fn_mut(path)?))
        } else {
            Err(LookupError::missing_library())
        }
    }

    pub fn lookup_expression(
        &self,
        call_stack: CallStack,
    ) -> Result<&Expression, LookupExpressionError> {
        let root_fn = self
            .lookup_fn(call_stack.root())
            .map_err(|e| LookupExpressionError::missing_root_function(e, call_stack.clone()))?
            .user()
            .ok_or_else(|| {
                LookupExpressionError::native_root_function(
                    call_stack.root().clone(),
                    call_stack.clone(),
                )
            })?;

        let top_key = call_stack.key_at_position(1).ok_or_else(|| {
            LookupExpressionError::callstack_position_too_long(call_stack.clone(), 1)
        })?;

        let next_expression = match top_key {
            RelativeRuntimeKey::FunctionCallInput(_) | RelativeRuntimeKey::BlockExpression(_) => {
                Err(LookupExpressionError::invalid_key_at_root(
                    top_key.clone(),
                    call_stack.clone(),
                    1,
                ))?
            }
            RelativeRuntimeKey::FunctionCallOutput => &root_fn.body,
        };

        if call_stack.len() > 1 {
            next_expression.lookup_expression(call_stack, 2, self)
        } else {
            Ok(next_expression)
        }
    }

    pub fn lookup_expression_mut(
        &mut self,
        call_stack: CallStack,
    ) -> Result<&mut Expression, LookupExpressionMutError> {
        let root_fn = self
            .lookup_fn_mut(call_stack.root())
            .map_err(|e| LookupExpressionError::missing_root_function(e, call_stack.clone()))?;

        let user_fn = root_fn.user().ok_or_else(|| {
            LookupExpressionError::native_root_function(
                call_stack.root().clone(),
                call_stack.clone(),
            )
        })?;

        let top_key = call_stack.key_at_position(1).ok_or_else(|| {
            LookupExpressionError::callstack_position_too_long(call_stack.clone(), 1)
        })?;

        let next_expression = match top_key {
            RelativeRuntimeKey::FunctionCallInput(_) | RelativeRuntimeKey::BlockExpression(_) => {
                Err(LookupExpressionError::invalid_key_at_root(
                    top_key.clone(),
                    call_stack.clone(),
                    1,
                ))?
            }
            RelativeRuntimeKey::FunctionCallOutput => &mut user_fn.body,
        };

        if call_stack.len() > 1 {
            next_expression.lookup_expression_mut(call_stack, 2)
        } else {
            Ok(next_expression)
        }
    }

    pub fn lookup_fn_handle(
        &mut self,
        path: &ItemPath,
    ) -> Result<UserFunctionHandle, LookupHandleError> {
        match self.lookup_fn(path) {
            Ok(FunctionRef::User(_)) => {
                let handle = UserFunctionHandle {
                    function_path: path.clone(),
                    libraries: self,
                };
                Ok(handle)
            }
            Ok(FunctionRef::Native(_)) => Err(
                LookupHandleError::FoundNativeFunctionButExpectedUserFunction {
                    function_path: path.clone(),
                    backtrace: Backtrace::capture(),
                },
            ),
            Err(e) => Err(LookupHandleError::Lookup(e)),
        }
    }

    pub fn lookup_block_expression_handle(
        &mut self,
        call_stack: CallStack,
    ) -> Result<ExpressionBlockHandle, LookupExpressionBlockHandleError> {
        match self.lookup_expression_mut(call_stack.clone()) {
            Ok(Expression::Block(_)) => Ok(ExpressionBlockHandle {
                call_stack,
                libraries: self,
            }),
            Ok(_) => Err(LookupExpressionBlockHandleError::NotABlockExpression {
                call_stack: call_stack.clone(),
                backtrace: Backtrace::capture(),
            }),
            Err(e) => Err(LookupExpressionBlockHandleError::Lookup(e)),
        }
    }

    pub fn lookup_module_mut(&mut self, path: &ItemPath) -> Result<ModuleMut, LookupError> {
        if let Some(native_library) = self.native_libraries.get_mut(path.library()) {
            Ok(ModuleMut::Native(native_library.lookup_module_mut(path)?))
        } else if let Some(user_library) = self.user_libraries.get_mut(path.library()) {
            Ok(ModuleMut::User(user_library.lookup_module_mut(path)?))
        } else {
            Err(LookupError::missing_library())
        }
    }

    pub fn lookup_output_type(
        &self,
        call_stack: CallStack,
    ) -> Result<ItemPath, LookupOutputTypeError> {
        let expression = self.lookup_expression(call_stack.clone())?;

        match expression {
            Expression::Literal(type_path, _) => Ok(type_path.clone()),
            Expression::Value(key_stack) => {
                let relative_stack =
                    CallStack::new_with_stack(call_stack.root().clone(), key_stack.clone());
                self.lookup_output_type(relative_stack)
            }
            Expression::FunctionCall(function_call) => {
                let function = self.lookup_fn(&function_call.function)?;
                Ok(function.output_type().clone())
            }
            Expression::Block(block) => {
                if let Some((last_key, _)) = block.iter().last() {
                    let relative_stack = call_stack.with_push(last_key.into());
                    self.lookup_output_type(relative_stack)
                } else {
                    Err(LookupOutputTypeError::EmptyBlock)
                }
            }
        }
    }

    pub fn get(&self, name: &str) -> Option<LibraryRef> {
        if let Some(native_library) = self.native_libraries.get(name) {
            Some(LibraryRef::Native(native_library))
        } else if let Some(user_library) = self.user_libraries.get(name) {
            Some(LibraryRef::User(user_library))
        } else {
            None
        }
    }

    pub fn get_mut(&mut self, name: &str) -> Option<LibraryMut> {
        if let Some(native_library) = self.native_libraries.get_mut(name) {
            Some(LibraryMut::Native(native_library))
        } else if let Some(user_library) = self.user_libraries.get_mut(name) {
            Some(LibraryMut::User(user_library))
        } else {
            None
        }
    }

    pub fn list_type_paths(&self) -> impl Iterator<Item = ItemPath> + '_ {
        self.native_libraries
            .iter()
            .flat_map(|(_, library)| library.list_type_paths())
            .chain(
                self.user_libraries
                    .iter()
                    .flat_map(|(_, library)| library.list_type_paths()),
            )
    }

    pub fn list_fn_paths(&self) -> impl Iterator<Item = ItemPath> + '_ {
        self.native_libraries
            .iter()
            .flat_map(|(_, library)| library.list_fn_paths())
            .chain(
                self.user_libraries
                    .iter()
                    .flat_map(|(_, library)| library.list_fn_paths()),
            )
    }

    pub fn list_module_paths(&self) -> impl Iterator<Item = ItemPath> + '_ {
        self.native_libraries
            .iter()
            .flat_map(|(_, library)| library.list_module_paths())
            .chain(
                self.user_libraries
                    .iter()
                    .flat_map(|(_, library)| library.list_module_paths()),
            )
    }
}

#[derive(thiserror::Error, Debug)]
pub enum LookupHandleError {
    #[error("{0}")]
    Lookup(
        #[from]
        #[backtrace]
        LookupError,
    ),

    #[error("Found native function but expected user function {function_path}")]
    FoundNativeFunctionButExpectedUserFunction {
        function_path: ItemPath,
        backtrace: Backtrace,
    },
}

pub struct UserFunctionHandle<'libraries> {
    function_path: ItemPath,
    libraries: &'libraries mut Libraries,
}

impl<'libraries> UserFunctionHandle<'libraries> {
    pub fn path(&self) -> &ItemPath {
        &self.function_path
    }
    pub fn get(&self) -> &UserFunction {
        match self.libraries.lookup_fn(&self.function_path).unwrap() {
            FunctionRef::User(user_function) => user_function,
            FunctionRef::Native(_) => panic!(
                "Got a native function out of a user function handle that has already been checked"
            ),
        }
    }

    pub fn get_mut(&mut self) -> &mut UserFunction {
        match self.libraries.lookup_fn_mut(&self.function_path).unwrap() {
            FunctionMut::User(user_function) => user_function,
            FunctionMut::Native(_) => panic!(
                "Got a native function out of a user function handle that has already been checked"
            ),
        }
    }

    pub fn libraries(&self) -> &Libraries {
        self.libraries
    }

    pub fn body_handle(&mut self) -> ExpressionBlockHandle {
        ExpressionBlockHandle {
            call_stack: CallStack::new(self.function_path.clone())
                .with_push(RelativeRuntimeKey::FunctionCallOutput),
            libraries: self.libraries,
        }
    }
}

#[derive(thiserror::Error, Debug)]
pub enum LookupExpressionBlockHandleError {
    #[error("{0}")]
    Lookup(
        #[from]
        #[backtrace]
        LookupExpressionMutError,
    ),

    #[error("Did not find a block expression at {call_stack}")]
    NotABlockExpression {
        call_stack: CallStack,
        backtrace: Backtrace,
    },
}

pub struct ExpressionBlockHandle<'libraries> {
    call_stack: CallStack,
    libraries: &'libraries mut Libraries,
}

impl<'libraries> ExpressionBlockHandle<'libraries> {
    pub fn call_stack(&self) -> &CallStack {
        &self.call_stack
    }

    pub fn get(&self) -> &ExpressionBlock {
        match self.libraries.lookup_expression(self.call_stack.clone()) {
            Ok(Expression::Block(block)) => block,
            Ok(_) => {
                panic!("Didn't get a block out of a block handle that has already been checked")
            }
            Err(e) => {
                log::error!("{:#?}", e);
                panic!("Error getting block out of block handle that has already been checked: {e}")
            }
        }
    }

    pub fn get_mut(&mut self) -> &mut ExpressionBlock {
        match self
            .libraries
            .lookup_expression_mut(self.call_stack.clone())
        {
            Ok(Expression::Block(block)) => block,
            Ok(_) => {
                panic!("Didn't get a block out of a block handle that has already been checked")
            }
            Err(e) => {
                log::error!("{:#?}", e);
                panic!("Error getting block out of block handle that has already been checked: {e}")
            }
        }
    }

    pub fn libraries(&self) -> &Libraries {
        &self.libraries
    }
}

pub enum Library {
    Native(NativeLibrary),
    User(UserLibrary),
}

impl Library {
    pub fn as_ref(&self) -> LibraryRef {
        match self {
            Library::Native(l) => LibraryRef::Native(l),
            Library::User(l) => LibraryRef::User(l),
        }
    }

    pub fn as_mut<'a>(&'a mut self) -> LibraryMut<'a> {
        match self {
            Library::Native(l) => LibraryMut::Native(l),
            Library::User(l) => LibraryMut::User(l),
        }
    }

    pub fn name(&self) -> &str {
        self.as_ref().name()
    }

    pub fn lookup_type(&self, path: &ItemPath) -> Result<TypeRef, LookupError> {
        self.as_ref().lookup_type(path)
    }

    pub fn lookup_fn(&self, path: &ItemPath) -> Result<FunctionRef, LookupError> {
        self.as_ref().lookup_fn(path)
    }

    pub fn lookup_module(&self, path: &ItemPath) -> Result<ModuleRef, LookupError> {
        self.as_ref().lookup_module(path)
    }

    pub fn lookup_type_mut<'a>(&'a mut self, path: &ItemPath) -> Result<TypeMut<'a>, LookupError> {
        self.as_mut().lookup_type_mut(path)
    }

    pub fn lookup_fn_mut(&mut self, path: &ItemPath) -> Result<FunctionMut, LookupError> {
        self.as_mut().lookup_fn_mut(path)
    }

    pub fn lookup_module_mut(&mut self, path: &ItemPath) -> Result<ModuleMut, LookupError> {
        self.as_mut().lookup_module_mut(path)
    }

    pub fn list_type_paths(&self) -> Vec<ItemPath> {
        self.as_ref().list_type_paths()
    }

    pub fn list_fn_paths(&self) -> Vec<ItemPath> {
        self.as_ref().list_fn_paths()
    }

    pub fn list_module_paths(&self) -> Vec<ItemPath> {
        self.as_ref().list_module_paths()
    }
}

pub enum LibraryRef<'a> {
    Native(&'a NativeLibrary),
    User(&'a UserLibrary),
}

impl<'a> LibraryRef<'a> {
    pub fn name(&self) -> &'a str {
        match self {
            LibraryRef::Native(l) => l.name(),
            LibraryRef::User(l) => l.name(),
        }
    }

    pub fn lookup_type(&self, path: &ItemPath) -> Result<TypeRef<'a>, LookupError> {
        match self {
            LibraryRef::Native(l) => Ok(TypeRef::Native(l.lookup_type(path)?)),
            LibraryRef::User(l) => Ok(TypeRef::User(l.lookup_type(path)?)),
        }
    }

    pub fn lookup_fn(&self, path: &ItemPath) -> Result<FunctionRef<'a>, LookupError> {
        match self {
            LibraryRef::Native(l) => Ok(FunctionRef::Native(l.lookup_fn(path)?)),
            LibraryRef::User(l) => Ok(FunctionRef::User(l.lookup_fn(path)?)),
        }
    }

    pub fn lookup_module(&self, path: &ItemPath) -> Result<ModuleRef<'a>, LookupError> {
        match self {
            LibraryRef::Native(l) => Ok(ModuleRef::Native(l.lookup_module(path)?)),
            LibraryRef::User(l) => Ok(ModuleRef::User(l.lookup_module(path)?)),
        }
    }

    pub fn list_type_paths(&self) -> Vec<ItemPath> {
        match self {
            LibraryRef::Native(l) => l.list_type_paths().collect(),
            LibraryRef::User(l) => l.list_type_paths().collect(),
        }
    }

    pub fn list_fn_paths(&self) -> Vec<ItemPath> {
        match self {
            LibraryRef::Native(l) => l.list_fn_paths().collect(),
            LibraryRef::User(l) => l.list_fn_paths().collect(),
        }
    }

    pub fn list_module_paths(&self) -> Vec<ItemPath> {
        match self {
            LibraryRef::Native(l) => l.list_module_paths().collect(),
            LibraryRef::User(l) => l.list_module_paths().collect(),
        }
    }
}

pub enum LibraryMut<'a> {
    Native(&'a mut NativeLibrary),
    User(&'a mut UserLibrary),
}

impl<'a> LibraryMut<'a> {
    pub fn as_ref<'s: 'a>(&'s self) -> LibraryRef<'a> {
        match self {
            LibraryMut::Native(l) => LibraryRef::Native(l),
            LibraryMut::User(l) => LibraryRef::User(l),
        }
    }

    pub fn name<'s: 'a>(&'s self) -> &'a str {
        self.as_ref().name()
    }

    pub fn lookup_type<'s: 'a>(&'s self, path: &ItemPath) -> Result<TypeRef<'a>, LookupError> {
        self.as_ref().lookup_type(path)
    }

    pub fn lookup_fn<'s: 'a>(&'s self, path: &ItemPath) -> Result<FunctionRef<'a>, LookupError> {
        self.as_ref().lookup_fn(path)
    }

    pub fn lookup_module<'s: 'a>(&'s self, path: &ItemPath) -> Result<ModuleRef<'a>, LookupError> {
        self.as_ref().lookup_module(path)
    }

    pub fn lookup_type_mut(self, path: &ItemPath) -> Result<TypeMut<'a>, LookupError> {
        match self {
            LibraryMut::Native(l) => Ok(TypeMut::Native(l.lookup_type_mut(path)?)),
            LibraryMut::User(l) => Ok(TypeMut::User(l.lookup_type_mut(path)?)),
        }
    }

    pub fn lookup_fn_mut(self, path: &ItemPath) -> Result<FunctionMut<'a>, LookupError> {
        match self {
            LibraryMut::Native(l) => Ok(FunctionMut::Native(l.lookup_fn_mut(path)?)),
            LibraryMut::User(l) => Ok(FunctionMut::User(l.lookup_fn_mut(path)?)),
        }
    }

    pub fn lookup_module_mut(self, path: &ItemPath) -> Result<ModuleMut<'a>, LookupError> {
        match self {
            LibraryMut::Native(l) => Ok(ModuleMut::Native(l.lookup_module_mut(path)?)),
            LibraryMut::User(l) => Ok(ModuleMut::User(l.lookup_module_mut(path)?)),
        }
    }

    pub fn list_type_paths(&self) -> Vec<ItemPath> {
        self.as_ref().list_type_paths()
    }

    pub fn list_fn_paths(&self) -> Vec<ItemPath> {
        self.as_ref().list_fn_paths()
    }

    pub fn list_module_paths(&self) -> Vec<ItemPath> {
        self.as_ref().list_module_paths()
    }
}
