use std::fmt::{Debug, Display, Formatter};

use serde::{Deserialize, Serialize};

use crate::call_stack::CallStack;
use crate::expression::Expression;
use crate::inc_map::Key;
use crate::library::Libraries;
use crate::DeferredFormatter;

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum ValueKey {
    Runtime(AbsoluteRuntimeKey),
}

impl ValueKey {
    pub fn new_runtime_key(call_stack: CallStack) -> ValueKey {
        ValueKey::Runtime(AbsoluteRuntimeKey::new(call_stack))
    }

    pub fn new_input_key(call_stack: CallStack, input: FunctionCallInputKey) -> ValueKey {
        ValueKey::Runtime(AbsoluteRuntimeKey::with_input_key(call_stack, input))
    }

    pub fn new_output_key(call_stack: CallStack, output: BlockExpressionKey) -> ValueKey {
        ValueKey::Runtime(AbsoluteRuntimeKey::with_output_key(call_stack, output))
    }

    pub fn format<'s>(&'s self, libraries: &'s Libraries) -> impl Display + 's {
        DeferredFormatter::new(move |f| match self {
            ValueKey::Runtime(runtime_key) => write!(f, "{}", runtime_key.format(libraries)),
        })
    }
}

impl From<AbsoluteRuntimeKey> for ValueKey {
    fn from(value: AbsoluteRuntimeKey) -> Self {
        ValueKey::Runtime(value)
    }
}

impl Display for ValueKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ValueKey::Runtime(r) => Display::fmt(r, f),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum RelativeValueKey {
    Runtime(RelativeRuntimeKey),
}

impl RelativeValueKey {
    pub fn into_absolute(self, call_stack: CallStack) -> ValueKey {
        match self {
            RelativeValueKey::Runtime(relative_key) => {
                ValueKey::Runtime(relative_key.into_absolute(call_stack))
            }
        }
    }

    pub fn is_relative_output(&self, output_key: BlockExpressionKey) -> bool {
        matches!(self, RelativeValueKey::Runtime(RelativeRuntimeKey::BlockExpression(output_key2)) if *output_key2 == output_key)
    }
}

impl Display for RelativeValueKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            RelativeValueKey::Runtime(r) => Display::fmt(r, f),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct AbsoluteRuntimeKey {
    pub call_stack: CallStack,
}

impl Display for AbsoluteRuntimeKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "_abs_run_{}", self.call_stack)
    }
}

impl AbsoluteRuntimeKey {
    pub fn new(call_stack: CallStack) -> AbsoluteRuntimeKey {
        AbsoluteRuntimeKey { call_stack }
    }
    pub fn with_input_key(
        call_stack: CallStack,
        input: FunctionCallInputKey,
    ) -> AbsoluteRuntimeKey {
        AbsoluteRuntimeKey {
            call_stack: call_stack.with_push(RelativeRuntimeKey::FunctionCallInput(input)),
        }
    }

    pub fn with_output_key(
        call_stack: CallStack,
        output: BlockExpressionKey,
    ) -> AbsoluteRuntimeKey {
        AbsoluteRuntimeKey {
            call_stack: call_stack.with_push(RelativeRuntimeKey::BlockExpression(output)),
        }
    }

    pub fn call_stack(&self) -> &CallStack {
        &self.call_stack
    }

    pub fn format<'s>(&'s self, libraries: &'s Libraries) -> impl Display + 's {
        self.call_stack.format(None, libraries)
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub enum RelativeRuntimeKey {
    FunctionCallInput(FunctionCallInputKey),
    FunctionCallOutput,
    BlockExpression(BlockExpressionKey),
}

impl RelativeRuntimeKey {
    pub fn new_function_input(input_key: FunctionCallInputKey) -> RelativeRuntimeKey {
        RelativeRuntimeKey::FunctionCallInput(input_key)
    }

    pub fn new_function_output() -> RelativeRuntimeKey {
        RelativeRuntimeKey::FunctionCallOutput
    }

    pub fn new_block_expression(output_key: BlockExpressionKey) -> RelativeRuntimeKey {
        RelativeRuntimeKey::BlockExpression(output_key)
    }

    pub fn from_expression_key(expression_key: Key<Expression>) -> RelativeRuntimeKey {
        RelativeRuntimeKey::BlockExpression(BlockExpressionKey { expression_key })
    }

    pub fn to_value_key(self) -> RelativeValueKey {
        RelativeValueKey::Runtime(self)
    }

    pub fn output_key(&self) -> Option<&BlockExpressionKey> {
        if let RelativeRuntimeKey::BlockExpression(output_key) = self {
            Some(output_key)
        } else {
            None
        }
    }

    pub fn input_key(&self) -> Option<&FunctionCallInputKey> {
        if let RelativeRuntimeKey::FunctionCallInput(input_key) = self {
            Some(input_key)
        } else {
            None
        }
    }
}

impl Display for RelativeRuntimeKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            RelativeRuntimeKey::FunctionCallInput(input) => Display::fmt(input, f),
            RelativeRuntimeKey::FunctionCallOutput => write!(f, "_out"),
            RelativeRuntimeKey::BlockExpression(output) => Display::fmt(output, f),
        }
    }
}

impl From<BlockExpressionKey> for RelativeRuntimeKey {
    fn from(value: BlockExpressionKey) -> RelativeRuntimeKey {
        RelativeRuntimeKey::BlockExpression(value)
    }
}

impl From<FunctionCallInputKey> for RelativeRuntimeKey {
    fn from(value: FunctionCallInputKey) -> RelativeRuntimeKey {
        RelativeRuntimeKey::FunctionCallInput(value)
    }
}

impl RelativeRuntimeKey {
    pub fn into_absolute(self, call_stack: CallStack) -> AbsoluteRuntimeKey {
        AbsoluteRuntimeKey::new(call_stack.with_push(self.clone()))
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct FunctionCallInputKey {
    pub name: String,
}

impl FunctionCallInputKey {
    pub fn new(name: impl Into<String>) -> FunctionCallInputKey {
        FunctionCallInputKey { name: name.into() }
    }
}

impl Display for FunctionCallInputKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "_in_{}", self.name)
    }
}

impl From<String> for FunctionCallInputKey {
    fn from(value: String) -> Self {
        FunctionCallInputKey { name: value }
    }
}

impl From<&String> for FunctionCallInputKey {
    fn from(value: &String) -> Self {
        FunctionCallInputKey {
            name: value.clone(),
        }
    }
}

impl From<&str> for FunctionCallInputKey {
    fn from(value: &str) -> Self {
        FunctionCallInputKey {
            name: value.to_string(),
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct BlockExpressionKey {
    pub expression_key: Key<Expression>,
}

impl Display for BlockExpressionKey {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "_blk_{}", self.expression_key.index())
    }
}
