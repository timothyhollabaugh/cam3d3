use std::fmt::Formatter;
use std::hash::{Hash, Hasher};
use std::marker::PhantomData;

use serde::de::{Error, Visitor};
use serde::{Deserialize, Deserializer, Serialize, Serializer};

pub struct Key<K> {
    pub(super) index: u64,
    _k: PhantomData<K>,
}

impl<K> std::fmt::Debug for Key<K> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.index.fmt(f)
    }
}

impl<K> Key<K> {
    pub(super) fn new(index: u64) -> Key<K> {
        Key {
            index,
            _k: PhantomData::default(),
        }
    }

    pub fn index(&self) -> u64 {
        self.index
    }
}

impl<K> Clone for Key<K> {
    fn clone(&self) -> Self {
        Key {
            index: self.index,
            _k: PhantomData,
        }
    }
}

impl<K> Copy for Key<K> {}

impl<K> PartialEq for Key<K> {
    fn eq(&self, other: &Self) -> bool {
        PartialEq::eq(&self.index, &other.index)
    }
}

impl<K> Eq for Key<K> {}

impl<K> Hash for Key<K> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        Hash::hash::<H>(&self.index, state);
    }
}

impl<K> Serialize for Key<K> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        Serialize::serialize(&self.index, serializer)
    }
}

struct KeyVisitor<K> {
    _k: PhantomData<K>,
}

impl<'de, K> Visitor<'de> for KeyVisitor<K> {
    type Value = Key<K>;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        formatter.write_str("a key index")
    }

    fn visit_u64<E>(self, v: u64) -> Result<Self::Value, E>
    where
        E: Error,
    {
        Ok(Key::new(v as u64))
    }
}

impl<'de, K> Deserialize<'de> for Key<K> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_u64(KeyVisitor { _k: PhantomData })
    }
}
