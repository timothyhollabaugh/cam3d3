use std::collections::HashMap;
use std::fmt::Formatter;
use std::marker::PhantomData;
use std::ops::{Index, IndexMut};

use itertools::Itertools;
use serde::de::{MapAccess, Visitor};
use serde::{Deserialize, Deserializer, Serialize, Serializer};

use crate::inc_map::Key;

#[derive(Clone, Debug)]
pub struct IncMap<V, K = V> {
    map: HashMap<Key<K>, (usize, V)>,
    last_index: u64,
}

impl<K, V> IncMap<V, K> {
    pub fn new() -> IncMap<K, V> {
        IncMap {
            map: HashMap::new(),
            last_index: 0,
        }
    }

    pub fn contains_key(&self, key: Key<K>) -> bool {
        self.map.contains_key(&key)
    }

    pub fn first(&self) -> Option<&V> {
        self.get_first_key().and_then(|k| self.get(k))
    }

    pub fn last(&self) -> Option<&V> {
        self.get_last_key().and_then(|k| self.get(k))
    }

    pub fn insert_with(&mut self, f: impl FnOnce(Key<K>, usize) -> V) -> (Key<K>, &V) {
        let index = self.last_index + 1;
        let key = Key::new(index);
        let position = self.next_position();

        let v = f(key, position);

        self.map.insert(key, (position, v));
        self.last_index = index;

        (key, &self.map.get(&key).unwrap().1)
    }

    pub fn insert(&mut self, v: V) -> Key<K> {
        let index = self.last_index + 1;
        let key = Key::new(index);
        let position = self.next_position();
        self.map.insert(key, (position, v));
        self.last_index = index;
        key
    }

    pub fn insert_at(&mut self, p: usize, v: V) -> Key<K> {
        let new_position = p.min(self.next_position());

        for (_k, (position, _v)) in self.map.iter_mut() {
            if *position >= new_position {
                *position += 1;
            }
        }

        let index = self.last_index + 1;
        let key = Key::new(index);
        self.map.insert(key, (new_position, v));
        self.last_index = index;
        key
    }

    pub fn move_to(&mut self, p: usize, k: Key<K>) -> bool {
        let new_position = p.min(self.next_position());
        let old_position = if let Some((p, _v)) = self.map.get(&k) {
            *p
        } else {
            return false;
        };

        for (_k, (position, _v)) in self.map.iter_mut() {
            if *position > old_position {
                *position -= 1;
            }

            if *position >= new_position {
                *position += 1;
            }
        }

        if let Some((old_position, _v)) = self.map.get_mut(&k) {
            *old_position = new_position;
        } else {
            return false;
        }

        true
    }

    pub fn get(&self, key: Key<K>) -> Option<&V> {
        self.map.get(&key).map(|(_, v)| v)
    }

    pub fn get_mut(&mut self, key: Key<K>) -> Option<&mut V> {
        self.map.get_mut(&key).map(|(_, v)| v)
    }

    pub fn get_previous_key(&self, key: Key<K>) -> Option<Key<K>> {
        let position = self.position(key)?;
        let previous_position = if position > 0 {
            position - 1
        } else {
            return None;
        };

        self.map
            .iter()
            .find(|(_k, (p, _v))| *p == previous_position)
            .map(|(k, _)| *k)
    }

    pub fn get_first_key(&self) -> Option<Key<K>> {
        self.map
            .iter()
            .min_by_key(|(_k, (p, _v))| p)
            .map(|(k, _)| *k)
    }

    pub fn get_last_key(&self) -> Option<Key<K>> {
        self.map
            .iter()
            .max_by_key(|(_k, (p, _v))| p)
            .map(|(k, _)| *k)
    }

    pub fn remove(&mut self, key: Key<K>) -> Option<V> {
        if let Some((old_position, value)) = self.map.remove(&key) {
            for (_k, (position, _v)) in self.map.iter_mut() {
                if *position > old_position {
                    *position -= 1;
                }
            }

            Some(value)
        } else {
            None
        }
    }

    pub fn position(&self, key: Key<K>) -> Option<usize> {
        self.map.get(&key).map(|(p, _)| *p)
    }

    pub fn iter(&self) -> impl Iterator<Item = (Key<K>, &V)> {
        self.map
            .iter()
            .sorted_unstable_by_key(|(_, (i, _))| *i)
            .map(|(k, (_, v))| (*k, v))
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = (Key<K>, &mut V)> {
        self.map
            .iter_mut()
            .sorted_unstable_by_key(|(_, (i, _))| *i)
            .map(|(k, (_, v))| (*k, v))
    }

    pub fn map_into<V2, F: FnMut(V) -> V2>(self, mut f: F) -> IncMap<V2, K> {
        IncMap {
            map: self
                .map
                .into_iter()
                .map(|(k, (i, v))| (k, (i, (f)(v))))
                .collect(),
            last_index: self.last_index,
        }
    }

    pub fn len(&self) -> usize {
        self.map.len()
    }

    fn next_position(&self) -> usize {
        self.map.iter().map(|(_, (p, _))| *p + 1).max().unwrap_or(0)
    }
}

impl<K, V> Default for IncMap<V, K> {
    fn default() -> Self {
        IncMap::new()
    }
}

impl<K, V> IntoIterator for IncMap<V, K> {
    type Item = (Key<K>, V);
    type IntoIter = std::iter::Map<
        std::vec::IntoIter<(Key<K>, (usize, V))>,
        fn((Key<K>, (usize, V))) -> (Key<K>, V),
    >;

    fn into_iter(self) -> Self::IntoIter {
        self.map
            .into_iter()
            .sorted_unstable_by_key(|(_, (i, _))| *i)
            .map(|(k, (_, v))| (k, v))
    }
}

impl<K, V> FromIterator<(Key<K>, V)> for IncMap<V, K> {
    fn from_iter<T: IntoIterator<Item = (Key<K>, V)>>(iter: T) -> Self {
        let mut map = HashMap::new();
        let mut last_index = 0;
        for (p, (k, v)) in iter.into_iter().enumerate() {
            last_index = u64::max(last_index, k.index);
            map.insert(k, (p, v));
        }

        IncMap { map, last_index }
    }
}

impl<V: Clone> From<&[V]> for IncMap<V> {
    fn from(value: &[V]) -> Self {
        let mut map = IncMap::new();

        for v in value.iter() {
            map.insert(v.clone());
        }

        map
    }
}

impl<V, const N: usize> From<[V; N]> for IncMap<V> {
    fn from(value: [V; N]) -> Self {
        let mut map = IncMap::new();

        for v in value.into_iter() {
            map.insert(v);
        }

        map
    }
}

impl<K, V> Index<Key<K>> for IncMap<V, K> {
    type Output = V;

    fn index(&self, index: Key<K>) -> &Self::Output {
        self.get(index).unwrap()
    }
}

impl<K, V> IndexMut<Key<K>> for IncMap<V, K> {
    fn index_mut(&mut self, index: Key<K>) -> &mut Self::Output {
        self.get_mut(index).unwrap()
    }
}

impl<K, V> Serialize for IncMap<V, K>
where
    V: Serialize,
{
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        self.map.serialize(serializer)
    }
}

struct IncMapVisitor<K, V> {
    _k: PhantomData<K>,
    _v: PhantomData<V>,
}

impl<K, V> IncMapVisitor<K, V> {
    fn new() -> Self {
        IncMapVisitor {
            _k: PhantomData,
            _v: PhantomData,
        }
    }
}

impl<'de, K, V> Visitor<'de> for IncMapVisitor<K, V>
where
    V: Deserialize<'de>,
{
    type Value = IncMap<V, K>;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        formatter.write_str("a map")
    }

    fn visit_map<M>(self, mut access: M) -> Result<Self::Value, M::Error>
    where
        M: MapAccess<'de>,
    {
        let mut map = HashMap::with_capacity(access.size_hint().unwrap_or(0));
        let mut last_index = 0;

        while let Some((key, (position, value))) = access.next_entry::<Key<K>, (usize, V)>()? {
            last_index = u64::max(last_index, key.index);
            map.insert(key, (position, value));
        }

        Ok(IncMap {
            map: map,
            last_index,
        })
    }
}

impl<'de, K, V> Deserialize<'de> for IncMap<V, K>
where
    V: Deserialize<'de>,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_map(IncMapVisitor::new())
    }
}
