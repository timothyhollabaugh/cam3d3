pub mod native;
pub mod user;

use crate::function::{FunctionMut, FunctionRef};
use crate::item_path::ItemId;
use crate::type_::{TypeMut, TypeRef};
use native::NativeModule;
use user::UserModule;

#[derive(Debug, Clone)]
pub enum Module {
    Native(NativeModule),
    User(UserModule),
}

impl Module {
    pub fn as_ref(&self) -> ModuleRef {
        match self {
            Module::Native(m) => ModuleRef::Native(m),
            Module::User(m) => ModuleRef::User(m),
        }
    }

    pub fn as_mut(&mut self) -> ModuleMut {
        match self {
            Module::Native(m) => ModuleMut::Native(m),
            Module::User(m) => ModuleMut::User(m),
        }
    }

    pub fn lookup_type(&self, path: &[ItemId], item: &ItemId) -> Option<TypeRef> {
        let t = match self {
            Module::Native(n) => TypeRef::Native(n.lookup_type(path, item)?),
            Module::User(u) => TypeRef::User(u.lookup_type(path, item)?),
        };

        Some(t)
    }

    pub fn lookup_fn(&self, path: &[ItemId], item: &ItemId) -> Option<FunctionRef> {
        let f = match self {
            Module::Native(n) => FunctionRef::Native(n.lookup_fn(path, item)?),
            Module::User(u) => FunctionRef::User(u.lookup_fn(path, item)?),
        };

        Some(f)
    }

    pub fn lookup_module(&self, path: &[ItemId], item: &ItemId) -> Option<TypeRef> {
        let t = match self {
            Module::Native(n) => TypeRef::Native(n.lookup_type(path, item)?),
            Module::User(u) => TypeRef::User(u.lookup_type(path, item)?),
        };

        Some(t)
    }
}

#[derive(Debug, Clone)]
pub enum ModuleRef<'a> {
    Native(&'a NativeModule),
    User(&'a UserModule),
}

impl<'a> ModuleRef<'a> {
    pub fn lookup_type(&'a self, path: &[ItemId], item: &ItemId) -> Option<TypeRef<'a>> {
        let t = match self {
            ModuleRef::Native(n) => TypeRef::Native(n.lookup_type(path, item)?),
            ModuleRef::User(u) => TypeRef::User(u.lookup_type(path, item)?),
        };

        Some(t)
    }

    pub fn lookup_fn(&self, path: &[ItemId], item: &ItemId) -> Option<FunctionRef> {
        let f = match self {
            ModuleRef::Native(n) => FunctionRef::Native(n.lookup_fn(path, item)?),
            ModuleRef::User(u) => FunctionRef::User(u.lookup_fn(path, item)?),
        };

        Some(f)
    }

    pub fn lookup_module(&self, path: &[ItemId], item: &ItemId) -> Option<TypeRef> {
        let t = match self {
            ModuleRef::Native(n) => TypeRef::Native(n.lookup_type(path, item)?),
            ModuleRef::User(u) => TypeRef::User(u.lookup_type(path, item)?),
        };

        Some(t)
    }

    pub fn list_type_paths(&self) -> Vec<(Vec<ItemId>, ItemId)> {
        match self {
            ModuleRef::Native(m) => m.list_type_paths(),
            ModuleRef::User(m) => m.list_type_paths(),
        }
    }

    pub fn list_fn_paths(&self) -> Vec<(Vec<ItemId>, ItemId)> {
        match self {
            ModuleRef::Native(m) => m.list_fn_paths(),
            ModuleRef::User(m) => m.list_fn_paths(),
        }
    }

    pub fn list_module_paths(&self) -> Vec<(Vec<ItemId>, ItemId)> {
        match self {
            ModuleRef::Native(m) => m.list_module_paths(),
            ModuleRef::User(m) => m.list_module_paths(),
        }
    }
}

#[derive(Debug)]
pub enum ModuleMut<'a> {
    Native(&'a mut NativeModule),
    User(&'a mut UserModule),
}

impl<'a> ModuleMut<'a> {
    pub fn lookup_type_mut(&mut self, path: &[ItemId], item: &ItemId) -> Option<TypeMut> {
        let t = match self {
            ModuleMut::Native(n) => TypeMut::Native(n.lookup_type_mut(path, item)?),
            ModuleMut::User(u) => TypeMut::User(u.lookup_type_mut(path, item)?),
        };

        Some(t)
    }

    pub fn lookup_fn_mut(&mut self, path: &[ItemId], item: &ItemId) -> Option<FunctionMut> {
        let f = match self {
            ModuleMut::Native(n) => FunctionMut::Native(n.lookup_fn_mut(path, item)?),
            ModuleMut::User(u) => FunctionMut::User(u.lookup_fn_mut(path, item)?),
        };

        Some(f)
    }

    pub fn lookup_module_mut(&mut self, path: &[ItemId], item: &ItemId) -> Option<ModuleMut> {
        let t = match self {
            ModuleMut::Native(n) => ModuleMut::Native(n.lookup_module_mut(path, item)?),
            ModuleMut::User(u) => ModuleMut::User(u.lookup_module_mut(path, item)?),
        };

        Some(t)
    }
}
