use crate::call_stack::CallStack;
use crate::interpreter::RuntimeError;
use crate::item_path::ItemPath;
use crate::library::Libraries;
use crate::value_key::FunctionCallInputKey;
use crate::values::Values;
use indexmap::IndexMap;
use std::fmt::{Debug, Display, Formatter};
use std::sync::Arc;

#[derive(Clone)]
pub struct NativeFunction {
    pub name: String,
    pub input_types: IndexMap<FunctionCallInputKey, ItemPath>,
    pub output_type: ItemPath,
    pub f:
        Arc<dyn Fn(&CallStack, &mut Values, &Libraries) -> Result<(), RuntimeError> + Send + Sync>,
}

impl Debug for NativeFunction {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("NativeFunction")
            .field("name", &self.name)
            .field("input_types", &self.input_types)
            .field("output_type", &self.output_type)
            .finish()
    }
}

impl NativeFunction {
    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn input_types(&self) -> &IndexMap<FunctionCallInputKey, ItemPath> {
        &self.input_types
    }

    pub fn output_type(&self) -> &ItemPath {
        &self.output_type
    }

    pub fn call(
        &self,
        call_stack: &CallStack,
        values: &mut Values,
        libraries: &Libraries,
    ) -> Result<(), RuntimeError> {
        (self.f)(call_stack, values, libraries)
    }
}

impl Display for NativeFunction {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}(", self.name)?;
        for (input_key, input_type) in self.input_types() {
            write!(f, "{}: {},", input_key, input_type)?;
        }
        write!(f, ") -> {} <native-function>", self.output_type)
    }
}
