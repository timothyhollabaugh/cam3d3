use crate::call_stack::CallStack;
use crate::expression::Expression;
use crate::interpreter::{InterpreterEventSink, RuntimeError};
use crate::item_path::ItemPath;
use crate::library::Libraries;
use crate::value_key::{FunctionCallInputKey, RelativeRuntimeKey, ValueKey};
use crate::values::Values;
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserFunction {
    pub name: String,
    pub input_types: IndexMap<FunctionCallInputKey, ItemPath>,
    pub output_type: ItemPath,
    pub body: Expression,
}

impl UserFunction {
    /*
    pub fn available_values<'path>(
        &'path self,
        up_to_expression_key: Option<Key<Expression>>,
        libraries: &'path Libraries,
    ) -> Vec<(RelativeRuntimeKey, Result<&'path ItemPath, RuntimeError>)> {
        self.body
            .available_values(up_to_expression_key, &self.input_types, libraries)
    }
     */

    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn input_types(&self) -> &IndexMap<FunctionCallInputKey, ItemPath> {
        &self.input_types
    }

    pub fn output_type(&self) -> &ItemPath {
        &self.output_type
    }

    pub fn call(
        &self,
        call_stack: &CallStack,
        values: &mut Values,
        libraries: &Libraries,
        event_sink: impl InterpreterEventSink,
    ) -> Result<(), RuntimeError> {
        puffin::profile_function!();

        let relative_position = call_stack.len();

        let call_stack = call_stack
            .clone()
            .with_push(RelativeRuntimeKey::FunctionCallOutput);

        self.body.execute(
            &call_stack,
            relative_position,
            values,
            libraries,
            event_sink,
        )?;

        Ok(())
    }
}

impl Display for UserFunction {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}(", self.name)?;
        for (input_key, input_type) in self.input_types() {
            write!(f, "{}: {},", input_key, input_type)?;
        }
        write!(f, ") -> {} <user-function>", self.output_type)
    }
}
