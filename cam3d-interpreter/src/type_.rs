use crate::function::native::NativeFunction;
use crate::function::user::UserFunction;
use crate::item_path::{ItemId, ItemPath};
use crate::value::{Value, ValueDowncastError};
use crate::value_key::FunctionCallInputKey;
use indexmap::{IndexMap, IndexSet};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::fmt::{Debug, Formatter};
use std::sync::Arc;

#[derive(Debug, Clone)]
pub enum Type {
    Native(NativeType),
    User(UserType),
}

#[allow(dead_code)]
impl Type {
    pub fn as_ref(&self) -> TypeRef {
        match self {
            Type::Native(t) => TypeRef::Native(t),
            Type::User(t) => TypeRef::User(t),
        }
    }
    fn name(&self) -> &str {
        match self {
            Type::Native(t) => t.name(),
            Type::User(t) => &t.name,
        }
    }

    fn generics(&self) -> &IndexSet<String> {
        match self {
            Type::Native(t) => t.generics(),
            Type::User(t) => t.generics(),
        }
    }
}

#[derive(Debug, Clone)]
pub enum TypeRef<'a> {
    Native(&'a NativeType),
    User(&'a UserType),
}

#[allow(dead_code)]
impl<'a> TypeRef<'a> {
    fn name(&self) -> &str {
        match self {
            TypeRef::Native(t) => t.name(),
            TypeRef::User(t) => &t.name,
        }
    }

    fn generics(&self) -> &IndexSet<String> {
        match self {
            TypeRef::Native(t) => t.generics(),
            TypeRef::User(t) => t.generics(),
        }
    }
}

#[derive(Debug)]
pub enum TypeMut<'a> {
    Native(&'a mut NativeType),
    User(&'a mut UserType),
}

#[allow(dead_code)]
impl<'a> TypeMut<'a> {
    pub fn as_ref(&'a self) -> TypeRef<'a> {
        match self {
            TypeMut::Native(t) => TypeRef::Native(t),
            TypeMut::User(t) => TypeRef::User(t),
        }
    }
    fn name(&self) -> &str {
        match self {
            TypeMut::Native(t) => t.name(),
            TypeMut::User(t) => &t.name,
        }
    }

    fn generics(&self) -> &IndexSet<String> {
        match self {
            TypeMut::Native(t) => t.generics(),
            TypeMut::User(t) => t.generics(),
        }
    }
}

pub trait NativeMethodRange: Send + Sync {
    fn range(
        &self,
        value: &Value,
    ) -> Result<Box<dyn ExactSizeIterator<Item = Value>>, ValueDowncastError>;
}

impl<T> NativeMethodRange for T
where
    T: Fn(&Value) -> Result<Box<dyn ExactSizeIterator<Item = Value>>, ValueDowncastError>
        + Send
        + Sync,
{
    fn range(
        &self,
        value: &Value,
    ) -> Result<Box<dyn ExactSizeIterator<Item = Value>>, ValueDowncastError> {
        self(value)
    }
}

#[derive(Clone)]
pub struct NativeMethod {
    pub f: NativeFunction,
    pub ranges: HashMap<FunctionCallInputKey, Arc<dyn NativeMethodRange>>,
}

impl Debug for NativeMethod {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("NativeMethod")
            .field("f", &self.f)
            .field("ranges", &self.ranges.keys())
            .finish()
    }
}

impl NativeMethod {
    pub fn name(&self) -> &str {
        self.f.name()
    }

    pub fn function(&self) -> &NativeFunction {
        &self.f
    }

    pub fn function_mut(&mut self) -> &mut NativeFunction {
        &mut self.f
    }

    pub fn ranges(&self) -> &HashMap<FunctionCallInputKey, Arc<dyn NativeMethodRange>> {
        &self.ranges
    }
}

#[derive(Debug, Clone)]
pub struct NativeType {
    name: String,
    generics: IndexSet<String>,
    methods: IndexMap<ItemId, NativeMethod>,
}

impl NativeType {
    pub fn new(name: impl Into<String>) -> NativeType {
        NativeType {
            name: name.into(),
            generics: IndexSet::new(),
            methods: IndexMap::new(),
        }
    }

    pub fn new_with_generics<G: Into<String>>(
        name: impl Into<String>,
        generics: impl IntoIterator<Item = G>,
    ) -> NativeType {
        NativeType {
            name: name.into(),
            generics: generics.into_iter().map(|name| name.into()).collect(),
            methods: IndexMap::new(),
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    fn generics(&self) -> &IndexSet<String> {
        &self.generics
    }

    pub fn add_method(&mut self, method: NativeMethod) -> ItemId {
        let id = ItemId::new(method.name());
        self.methods.insert(id.clone(), method);
        id
    }

    pub fn methods(&self) -> impl Iterator<Item = &NativeMethod> {
        self.methods.values()
    }

    pub fn method(&self, item_id: &ItemId) -> Option<&NativeMethod> {
        self.methods.get(item_id)
    }

    pub fn method_mut(&mut self, item_id: &ItemId) -> Option<&mut NativeMethod> {
        self.methods.get_mut(item_id)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserType {
    pub name: String,
    pub kind: UserTypeKind,
    pub generics: IndexSet<String>,
    pub methods: IndexMap<ItemId, UserFunction>,
}

impl UserType {
    pub fn name(&self) -> &str {
        &self.name
    }
    pub fn generics(&self) -> &IndexSet<String> {
        &self.generics
    }
    pub fn add_method(&mut self, method: UserFunction) {
        self.methods.insert(ItemId::new(method.name()), method);
    }

    pub fn methods(&self) -> impl Iterator<Item = &UserFunction> {
        self.methods.values()
    }

    pub fn method(&self, item_id: &ItemId) -> Option<&UserFunction> {
        self.methods.get(item_id)
    }

    pub fn method_mut(&mut self, item_id: &ItemId) -> Option<&mut UserFunction> {
        self.methods.get_mut(item_id)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum UserTypeKind {
    Struct(UserStruct),
    Enum(UserEnum),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserStruct {
    fields: IndexMap<String, ItemPath>,
}

pub struct UserStructValue {
    #[allow(dead_code)]
    fields: IndexMap<String, Value>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserEnum {
    #[allow(dead_code)]
    variants: IndexMap<String, ItemPath>,
}

pub struct UserEnumValue {
    #[allow(dead_code)]
    variant: String,

    #[allow(dead_code)]
    value: Value,
}
