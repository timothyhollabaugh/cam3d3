use crate::value_key::ValueKey;
use dyn_clone::DynClone;
use serde::{Deserialize, Serialize};
use std::any::Any;
use std::backtrace::Backtrace;
use std::fmt::{Debug, Display, Formatter};

#[derive(thiserror::Error, Debug)]
pub enum ValueDowncastError {
    #[error("Could not downcast value of type {found} to type {expected}")]
    WrongType {
        expected: &'static str,
        found: &'static str,
        backtrace: Backtrace,
    },
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LiteralValue(Box<dyn LiteralValueBox>);

impl LiteralValue {
    pub fn from_value<V: LiteralValueBox + 'static>(value: V) -> LiteralValue {
        LiteralValue(Box::new(value))
    }

    pub fn into_value(self) -> Value {
        Value(self.0.into_value())
    }

    pub fn downcast<V: ValueUnboxed + 'static>(&self) -> Result<&V, ValueDowncastError> {
        self.0
            .as_any()
            .downcast_ref()
            .ok_or(ValueDowncastError::WrongType {
                expected: <V as ValueUnboxed>::name(),
                found: self.name(),
                backtrace: Backtrace::capture(),
            })
    }

    pub fn downcast_box<V: ValueUnboxed + 'static>(
        self: Box<Self>,
    ) -> Result<V, ValueDowncastError> {
        let found = self.name();
        let any = self.0.into_any();

        match any.downcast::<Box<V>>() {
            Ok(b) => Ok(**b),
            Err(_v) => Err(ValueDowncastError::WrongType {
                expected: <V as ValueUnboxed>::name(),
                found,
                backtrace: Backtrace::capture(),
            }),
        }
    }

    pub fn name(&self) -> &'static str {
        self.0.name()
    }
}

impl std::fmt::Display for LiteralValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.0, f)
    }
}

#[typetag::serde(tag = "type")]
pub trait LiteralValueBox: ValueBox {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox>;
}
dyn_clone::clone_trait_object!(LiteralValueBox);

#[derive(Debug, Clone)]
pub struct Value(Box<dyn ValueBox>);

impl Value {
    pub fn from_value<V: ValueBox + 'static>(value: V) -> Value {
        Value(Box::new(value))
    }

    pub fn downcast<V: ValueUnboxed + 'static>(&self) -> Result<&V, ValueDowncastError> {
        self.0
            .as_any()
            .downcast_ref()
            .ok_or(ValueDowncastError::WrongType {
                expected: <V as ValueUnboxed>::name(),
                found: self.name(),
                backtrace: Backtrace::capture(),
            })
    }

    pub fn downcast_mut<V: ValueUnboxed + 'static>(
        &mut self,
    ) -> Result<&mut V, ValueDowncastError> {
        let found = self.name();
        self.0
            .as_any_mut()
            .downcast_mut()
            .ok_or(ValueDowncastError::WrongType {
                expected: <V as ValueUnboxed>::name(),
                found,
                backtrace: Backtrace::capture(),
            })
    }

    pub fn downcast_box<V: ValueUnboxed + 'static>(
        self: Box<Self>,
    ) -> Result<V, ValueDowncastError> {
        let found = self.name();
        let any = self.0.into_any();

        match any.downcast::<Box<V>>() {
            Ok(b) => Ok(**b),
            Err(_v) => Err(ValueDowncastError::WrongType {
                expected: <V as ValueUnboxed>::name(),
                found,
                backtrace: Backtrace::capture(),
            }),
        }
    }

    pub fn name(&self) -> &'static str {
        self.0.name()
    }
}

impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.0, f)
    }
}

pub trait ValueBox: Debug + Send + DynClone + std::fmt::Display {
    fn as_any(&self) -> &dyn Any;
    fn as_any_mut(&mut self) -> &mut dyn Any;
    fn into_any(self: Box<Self>) -> Box<dyn Any>;
    fn name(&self) -> &'static str;
}

pub trait ValueUnboxed: ValueBox {
    fn name() -> &'static str;
}

dyn_clone::clone_trait_object!(ValueBox);

#[derive(Debug, Clone)]
pub enum RuntimeValue {
    Value(Value),
    Ref(ValueKey),
}

impl RuntimeValue {
    pub fn from_value<V: ValueBox + 'static>(value: V) -> RuntimeValue {
        RuntimeValue::Value(Value::from_value(value))
    }
}

impl Display for RuntimeValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            RuntimeValue::Value(v) => Display::fmt(v, f),
            RuntimeValue::Ref(k) => write!(f, "Ref {k}"),
        }
    }
}
