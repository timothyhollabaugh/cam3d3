use itertools::Itertools;
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};
use std::hash::Hash;
use std::str::FromStr;

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct ItemId {
    id: String,
    #[serde(default)]
    generics: Vec<(String, ItemPath)>,
}

impl ItemId {
    pub fn new(id: impl Into<String>) -> ItemId {
        ItemId::new_with_generics(id, &[])
    }

    pub fn new_with_generics(id: impl Into<String>, generics: &[(String, ItemPath)]) -> ItemId {
        ItemId {
            id: id.into(),
            generics: generics.to_vec(),
        }
    }

    pub fn generic_item(&self, name: &str) -> Option<&ItemPath> {
        self.generics
            .iter()
            .find(|item| item.0 == name)
            .map(|item| &item.1)
    }

    pub fn onto_path(self, path: ItemPath) -> ItemPath {
        path.with_item(self)
    }
}

impl Display for ItemId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.id.as_ref())?;
        if let [(name, path), remaining_generics @ ..] = &self.generics[..] {
            write!(f, "<{}={}", name, path)?;
            for (name, path) in remaining_generics {
                write!(f, ", {}={}", name, path)?;
            }
            write!(f, ">")?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub struct ItemPath {
    pub library: String,
    pub module_path: Vec<ItemId>,
    pub item_id: ItemId,
}

impl ItemPath {
    pub fn root_item(library: String, item: ItemId) -> ItemPath {
        ItemPath {
            library,
            module_path: Vec::new(),
            item_id: item,
        }
    }

    pub fn with_path(mut self, module_id: ItemId) -> ItemPath {
        self.module_path.push(module_id);
        ItemPath {
            library: self.library,
            module_path: self.module_path,
            item_id: self.item_id,
        }
    }

    pub fn without_first(mut self) -> (Option<ItemId>, ItemPath) {
        if let Some(first_module_key) = self.module_path.pop() {
            (Some(first_module_key), self)
        } else {
            (None, self)
        }
    }

    pub fn with_item(&self, item_id: ItemId) -> ItemPath {
        let mut module_path = self.module_path.clone();
        module_path.push(self.item_id.clone());
        ItemPath {
            library: self.library.clone(),
            module_path,
            item_id,
        }
    }

    pub fn library(&self) -> &str {
        &self.library
    }

    pub fn parents_string(&self) -> String {
        let mut f = String::new();

        f += self.library();
        f += "::";

        for module_id in self.module_path.iter() {
            f += &module_id.to_string();
            f += "::";
        }

        f
    }

    pub fn parents(&self) -> impl Iterator<Item = ItemPath> + '_ {
        let mut path: Option<ItemPath> = None;

        self.module_path.iter().map(move |module_id| {
            if let Some(path) = path.as_mut() {
                path.module_path.push(path.item_id.clone());
                path.item_id = module_id.clone();
                path.clone()
            } else {
                let new_path = ItemPath {
                    library: self.library.clone(),
                    module_path: Vec::new(),
                    item_id: module_id.clone(),
                };
                path = Some(new_path.clone());
                new_path
            }
        })
    }
}

impl FromStr for ItemPath {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let [library, module_path @ .., item_id] = &s.split("::").collect_vec()[..] {
            Ok(ItemPath {
                library: library.to_string(),
                module_path: module_path
                    .iter()
                    .map(|&m| ItemId::new(m.to_string()))
                    .collect_vec(),
                item_id: ItemId::new(item_id.to_string()),
            })
        } else {
            Err(())
        }
    }
}

impl Display for ItemPath {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.library())?;
        f.write_str("::")?;

        for module_id in self.module_path.iter() {
            write!(f, "{module_id}::")?;
        }

        write!(f, "{}", self.item_id)?;

        Ok(())
    }
}
