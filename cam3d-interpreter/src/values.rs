use crate::call_stack::CallStack;
use crate::interpreter::RuntimeError;
use crate::library::Libraries;
use crate::value::{RuntimeValue, Value, ValueUnboxed};
use crate::value_key::{AbsoluteRuntimeKey, FunctionCallInputKey, ValueKey};
use crate::DeferredFormatter;
use log::error;
use std::collections::HashMap;
use std::fmt::Display;

#[derive(Debug, Clone)]
pub struct Values {
    runtime: HashMap<AbsoluteRuntimeKey, RuntimeValue>,
}

impl Values {
    pub fn new() -> Values {
        Values {
            runtime: HashMap::new(),
        }
    }

    pub fn len(&self) -> usize {
        self.runtime.len()
    }

    pub fn get_input<T: ValueUnboxed + 'static, K: Into<FunctionCallInputKey>>(
        &self,
        call_stack: &CallStack,
        input_key: K,
    ) -> Result<&T, RuntimeError> {
        let input_key = input_key.into();

        let key = ValueKey::new_input_key(call_stack.clone(), input_key.clone());

        let value = self.get(&key).ok_or(RuntimeError::missing_input(
            call_stack.clone(),
            self.clone(),
            input_key,
        ))?;

        let t = value.downcast().map_err(|source| {
            RuntimeError::wrong_value_type(call_stack.clone(), self.clone(), key, source)
        })?;

        Ok(t)
    }

    pub fn get_input_mut<T: ValueUnboxed + 'static, K: Into<FunctionCallInputKey>>(
        &mut self,
        call_stack: &CallStack,
        input_key: K,
    ) -> Result<&mut T, RuntimeError> {
        let input_key = input_key.into();

        let key = ValueKey::new_input_key(call_stack.clone(), input_key.clone());

        let err_values = self.clone();

        let value = self.get_mut(&key).ok_or(RuntimeError::missing_input(
            call_stack.clone(),
            err_values.clone(),
            input_key,
        ))?;

        let t = value.downcast_mut().map_err(|source| {
            RuntimeError::wrong_value_type(call_stack.clone(), err_values, key, source)
        })?;

        Ok(t)
    }

    pub fn get(&self, key: &ValueKey) -> Option<&Value> {
        match key {
            ValueKey::Runtime(runtime_key) => match self.runtime.get(runtime_key) {
                Some(RuntimeValue::Value(v)) => Some(v),
                Some(RuntimeValue::Ref(key)) => self.get(key),
                None => None,
            },
        }
    }

    pub fn get_mut(&mut self, key: &ValueKey) -> Option<&mut Value> {
        match key {
            ValueKey::Runtime(runtime_key) => match self.runtime.get(&runtime_key) {
                Some(RuntimeValue::Value(_)) => {
                    let Some(RuntimeValue::Value(v)) = self.runtime.get_mut(&runtime_key) else {
                        error!("The runtime value disappeared!");
                        return None;
                    };
                    return Some(v);
                }
                Some(RuntimeValue::Ref(ref_key)) => self.get_mut(&(ref_key.clone())),
                None => return None,
            },
        }
    }

    pub fn contains(&self, key: &ValueKey) -> bool {
        match key {
            ValueKey::Runtime(runtime_key) => self.runtime.contains_key(runtime_key),
        }
    }

    pub fn remove_resolve_value(&mut self, value: RuntimeValue) -> Option<Value> {
        match value {
            RuntimeValue::Value(v) => Some(v),
            RuntimeValue::Ref(key) => match key {
                ValueKey::Runtime(key) => {
                    let removed_value = self.remove(&key)?;
                    self.remove_resolve_value(removed_value)
                }
            },
        }
    }

    pub fn remove(&mut self, key: &AbsoluteRuntimeKey) -> Option<RuntimeValue> {
        self.runtime.remove(key)
    }

    pub fn insert_value(&mut self, key: AbsoluteRuntimeKey, value: Value) {
        self.runtime.insert(key, RuntimeValue::Value(value));
    }

    pub fn insert_ref(&mut self, key: AbsoluteRuntimeKey, ref_key: ValueKey) {
        self.runtime.insert(key, RuntimeValue::Ref(ref_key));
    }

    pub fn insert(&mut self, key: AbsoluteRuntimeKey, value: RuntimeValue) {
        self.runtime.insert(key, value);
    }

    pub fn format<'s>(&'s self, libraries: &'s Libraries) -> impl Display + 's {
        DeferredFormatter::new(move |f| {
            writeln!(f, "Runtime:")?;

            for (i, (key, runtime_value)) in self.runtime.iter().enumerate() {
                writeln!(f, "Value {i}:")?;
                writeln!(f, "  Key:")?;
                writeln!(f, "{}", key.format(libraries))?;

                match runtime_value {
                    RuntimeValue::Value(value) => {
                        writeln!(f, "  Value: {:?} ({})", value.name(), value)?;
                    }
                    RuntimeValue::Ref(ref_key) => {
                        writeln!(f, "  Value Ref: ")?;
                        writeln!(f, "{}", ref_key.format(libraries))?;
                    }
                }
            }

            Ok(())
        })
    }

    pub fn iter(&self) -> impl Iterator<Item = (ValueKey, &Value)> {
        self.runtime.iter().filter_map(|(key, value)| {
            if let RuntimeValue::Value(value) = value {
                Some((ValueKey::Runtime(key.clone()), value))
            } else {
                None
            }
        })
    }
}
