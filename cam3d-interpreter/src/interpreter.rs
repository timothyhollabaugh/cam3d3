use crate::call_stack::CallStack;
use crate::item_path::ItemPath;
use crate::library::{Libraries, LookupError};
use crate::value::{Value, ValueDowncastError};
use crate::value_key::{AbsoluteRuntimeKey, FunctionCallInputKey, ValueKey};
use crate::values::Values;
use std::backtrace::Backtrace;

#[derive(thiserror::Error, Debug)]
pub enum RuntimeErrorType {
    #[error("Missing value {0:?}")]
    MissingValue(ValueKey),

    #[error("Wrong value type for {value:?}: {source}")]
    WrongValueType {
        value: ValueKey,

        #[backtrace]
        source: ValueDowncastError,
    },

    #[error("Missing output value")]
    MissingOutputValue,

    #[error("Missing input {0}")]
    MissingInput(FunctionCallInputKey),

    #[error("Lookup Error: {0}")]
    Lookup(#[from] LookupError),

    #[error("The position is longer than the callstack")]
    CallStackPositionTooLong {
        call_stack: CallStack,
        position: usize,
    },
}

#[derive(thiserror::Error, Debug)]
#[error("Runtime Error: {error_type} at {callstack:?}")]
pub struct RuntimeError {
    pub callstack: Option<CallStack>,
    pub values: Option<Values>,

    pub backtrace: Backtrace,

    #[source]
    pub error_type: RuntimeErrorType,
}

impl RuntimeError {
    pub fn from_error_type(
        callstack: impl Into<Option<CallStack>>,
        values: impl Into<Option<Values>>,
        error_type: RuntimeErrorType,
    ) -> RuntimeError {
        RuntimeError {
            callstack: callstack.into(),
            values: values.into(),
            backtrace: Backtrace::capture(),
            error_type,
        }
    }

    pub fn lookup(
        callstack: impl Into<Option<CallStack>>,
        values: impl Into<Option<Values>>,
        lookup_error: LookupError,
    ) -> RuntimeError {
        RuntimeError {
            callstack: callstack.into(),
            values: values.into(),
            backtrace: Backtrace::capture(),
            error_type: RuntimeErrorType::Lookup(lookup_error),
        }
    }

    pub fn wrong_value_type(
        callstack: impl Into<Option<CallStack>>,
        values: impl Into<Option<Values>>,
        value: ValueKey,
        source: ValueDowncastError,
    ) -> RuntimeError {
        RuntimeError {
            callstack: callstack.into(),
            values: values.into(),
            backtrace: Backtrace::capture(),
            error_type: RuntimeErrorType::WrongValueType { value, source },
        }
    }

    pub fn missing_output_value(
        callstack: impl Into<Option<CallStack>>,
        values: impl Into<Option<Values>>,
    ) -> RuntimeError {
        RuntimeError {
            callstack: callstack.into(),
            values: values.into(),
            backtrace: Backtrace::capture(),
            error_type: RuntimeErrorType::MissingOutputValue,
        }
    }

    pub fn missing_input(
        callstack: impl Into<Option<CallStack>>,
        values: impl Into<Option<Values>>,
        input_key: FunctionCallInputKey,
    ) -> RuntimeError {
        RuntimeError {
            callstack: callstack.into(),
            values: values.into(),
            backtrace: Backtrace::capture(),
            error_type: RuntimeErrorType::MissingInput(input_key),
        }
    }

    pub fn callstack_position_too_long(
        callstack: impl Into<Option<CallStack>>,
        values: impl Into<Option<Values>>,
        callstack2: CallStack,
        position: usize,
    ) -> RuntimeError {
        RuntimeError {
            callstack: callstack.into(),
            values: values.into(),
            backtrace: Backtrace::capture(),
            error_type: RuntimeErrorType::CallStackPositionTooLong {
                call_stack: callstack2,
                position,
            },
        }
    }

    pub fn with_callstack(mut self, callstack: CallStack) -> RuntimeError {
        if self.callstack.is_none() {
            self.callstack = Some(callstack);
        }
        self
    }

    pub fn with_values(mut self, values: Values) -> RuntimeError {
        if self.values.is_none() {
            self.values = Some(values);
        }
        self
    }
}

impl From<LookupError> for RuntimeError {
    fn from(error: LookupError) -> RuntimeError {
        RuntimeError::lookup(None, None, error)
    }
}

pub trait InterpreterEventSink: Clone {
    fn event(&mut self, event: InterpreterEvent);
}

impl<T> InterpreterEventSink for T
where
    T: Fn(InterpreterEvent) + Clone,
{
    fn event(&mut self, event: InterpreterEvent) {
        (self)(event);
    }
}

#[derive(Debug, Clone)]
pub struct InterpreterEvent {
    pub call_stack: CallStack,
    pub values: Values,
}

pub struct Interpreter<'libraries> {
    pub libraries: &'libraries Libraries,
    pub input_values: Vec<(FunctionCallInputKey, Value)>,
    pub main_function: ItemPath,
}

impl Interpreter<'_> {
    pub fn new(
        libraries: &Libraries,
        main_function: ItemPath,
        input_values: Vec<(FunctionCallInputKey, Value)>,
    ) -> Interpreter<'_> {
        Interpreter {
            libraries,
            main_function,
            input_values,
        }
    }

    pub fn run(self, event_sink: impl InterpreterEventSink) -> Result<Values, RuntimeError> {
        let callstack = CallStack::new(self.main_function.clone());

        let mut values = Values::new();

        for (input_key, input) in self.input_values.into_iter() {
            values.insert_value(
                AbsoluteRuntimeKey::with_input_key(callstack.clone(), input_key),
                input,
            );
        }

        let main_function = self
            .libraries
            .lookup_fn(&self.main_function)
            .map_err(|e| RuntimeError::lookup(callstack.clone(), values.clone(), e))?;

        main_function.call(&callstack, &mut values, &self.libraries, event_sink)?;

        Ok(values)
    }
}
