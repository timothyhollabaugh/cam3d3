use crate::function::user::UserFunction;
use crate::item_path::ItemId;
use crate::type_::UserType;
use indexmap::IndexMap;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct UserModule {
    pub name: String,
    pub types: IndexMap<ItemId, UserType>,
    pub functions: IndexMap<ItemId, UserFunction>,
    pub modules: IndexMap<ItemId, UserModule>,
}

impl UserModule {
    pub fn new(name: impl Into<String>) -> UserModule {
        UserModule {
            name: name.into(),
            types: IndexMap::new(),
            functions: IndexMap::new(),
            modules: IndexMap::new(),
        }
    }

    pub fn insert_type(&mut self, t: UserType) -> ItemId {
        let id = ItemId::new(t.name());
        self.types.insert(id.clone(), t);
        id
    }
    pub fn insert_fn(&mut self, f: UserFunction) -> ItemId {
        let id = ItemId::new(f.name());
        self.functions.insert(id.clone(), f);
        id
    }

    pub fn insert_module(&mut self, module: UserModule) -> ItemId {
        let id = ItemId::new(&module.name);
        self.modules.insert(id.clone(), module);
        id
    }

    pub fn lookup_type(&self, path: &[ItemId], item: &ItemId) -> Option<&UserType> {
        if let [module_id, remaining_path @ ..] = path {
            self.modules
                .get(module_id)?
                .lookup_type(remaining_path, item)
        } else {
            self.types.get(item)
        }
    }

    pub fn lookup_fn(&self, path: &[ItemId], item: &ItemId) -> Option<&UserFunction> {
        if let [module_id, remaining_path @ ..] = path {
            self.modules
                .get(module_id)?
                .lookup_fn(remaining_path, item)
                .or_else(|| self.types.get(module_id)?.method(item))
        } else {
            self.functions.get(item)
        }
    }

    pub fn lookup_module(&self, path: &[ItemId], item: &ItemId) -> Option<&UserModule> {
        if let [module_id, remaining_path @ ..] = path {
            self.modules
                .get(module_id)?
                .lookup_module(remaining_path, item)
        } else {
            self.modules.get(item)
        }
    }

    pub fn lookup_type_mut(&mut self, path: &[ItemId], item: &ItemId) -> Option<&mut UserType> {
        if let [module_id, remaining_path @ ..] = path {
            self.modules
                .get_mut(module_id)?
                .lookup_type_mut(remaining_path, item)
        } else {
            self.types.get_mut(item)
        }
    }

    pub fn lookup_fn_mut(&mut self, path: &[ItemId], item: &ItemId) -> Option<&mut UserFunction> {
        if let [module_id, remaining_path @ ..] = path {
            self.modules
                .get_mut(module_id)?
                .lookup_fn_mut(remaining_path, item)
        } else {
            self.functions.get_mut(item)
        }
    }

    pub fn lookup_module_mut(&mut self, path: &[ItemId], item: &ItemId) -> Option<&mut UserModule> {
        if let [module_id, remaining_path @ ..] = path {
            self.modules
                .get_mut(module_id)?
                .lookup_module_mut(remaining_path, item)
        } else {
            self.modules.get_mut(item)
        }
    }

    pub fn get_type(&self, item_id: &ItemId) -> Option<&UserType> {
        self.types.get(item_id)
    }

    pub fn get_fn(&self, item_id: &ItemId) -> Option<&UserFunction> {
        self.functions.get(item_id)
    }

    pub fn get_mod(&self, item_id: &ItemId) -> Option<&UserModule> {
        self.modules.get(item_id)
    }

    pub fn get_type_mut(&mut self, item_id: &ItemId) -> Option<&mut UserType> {
        self.types.get_mut(item_id)
    }

    pub fn get_fn_mut(&mut self, item_id: &ItemId) -> Option<&mut UserFunction> {
        self.functions.get_mut(item_id)
    }

    pub fn get_mod_mut(&mut self, item_id: &ItemId) -> Option<&mut UserModule> {
        self.modules.get_mut(item_id)
    }

    pub fn list_type_paths(&self) -> Vec<(Vec<ItemId>, ItemId)> {
        let mut paths = Vec::new();

        for (item_id, _) in self.types.iter() {
            paths.push((Vec::new(), item_id.clone()));
        }

        for (module_id, module) in self.modules.iter() {
            for (mut path, item_id) in module.list_type_paths() {
                path.insert(0, module_id.clone());
                paths.push((path, item_id.clone()));
            }
        }

        paths
    }

    pub fn list_fn_paths(&self) -> Vec<(Vec<ItemId>, ItemId)> {
        let mut paths = Vec::new();

        for (item_id, _) in self.functions.iter() {
            paths.push((Vec::new(), item_id.clone()));
        }

        for (module_id, module) in self.modules.iter() {
            for (mut path, item_id) in module.list_fn_paths() {
                path.insert(0, module_id.clone());
                paths.push((path, item_id.clone()));
            }
        }

        paths
    }

    pub fn list_module_paths(&self) -> Vec<(Vec<ItemId>, ItemId)> {
        let mut paths = Vec::new();

        for (item_id, _) in self.functions.iter() {
            paths.push((Vec::new(), item_id.clone()));
        }

        for (module_id, module) in self.modules.iter() {
            for (mut path, item_id) in module.list_module_paths() {
                path.insert(0, module_id.clone());
                paths.push((path, item_id.clone()));
            }
        }

        paths
    }
}
