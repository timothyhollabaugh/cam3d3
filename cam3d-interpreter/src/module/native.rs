use crate::function::native::NativeFunction;
use crate::item_path::ItemId;
use crate::type_::NativeType;
use indexmap::IndexMap;

#[derive(Debug, Clone, Default)]
pub struct NativeModule {
    pub name: String,
    pub types: IndexMap<ItemId, NativeType>,
    pub functions: IndexMap<ItemId, NativeFunction>,
    pub modules: IndexMap<ItemId, NativeModule>,
}

impl NativeModule {
    pub fn new(name: impl Into<String>) -> NativeModule {
        NativeModule {
            name: name.into(),
            types: IndexMap::new(),
            functions: IndexMap::new(),
            modules: IndexMap::new(),
        }
    }

    pub fn insert_type(&mut self, t: NativeType) -> ItemId {
        let id = ItemId::new(t.name());
        self.types.insert(id.clone(), t);
        id
    }
    pub fn insert_fn(&mut self, f: NativeFunction) -> ItemId {
        let id = ItemId::new(f.name());
        self.functions.insert(id.clone(), f);
        id
    }

    pub fn insert_module(&mut self, module: NativeModule) -> ItemId {
        let id = ItemId::new(module.name.clone());
        self.modules.insert(id.clone(), module);
        id
    }

    pub fn lookup_type(&self, path: &[ItemId], item: &ItemId) -> Option<&NativeType> {
        if let [module_id, remaining_path @ ..] = path {
            self.modules
                .get(module_id)?
                .lookup_type(remaining_path, item)
        } else {
            self.types.get(item)
        }
    }

    pub fn lookup_fn(&self, path: &[ItemId], item: &ItemId) -> Option<&NativeFunction> {
        if let [module_id, remaining_path @ ..] = path {
            self.modules
                .get(module_id)
                .and_then(|module| module.lookup_fn(remaining_path, item))
                .or_else(|| {
                    let type_ = self.types.get(module_id)?;
                    let method = type_.method(item)?;
                    Some(method.function())
                })
        } else {
            self.functions.get(item)
        }
    }

    pub fn lookup_module(&self, path: &[ItemId], item: &ItemId) -> Option<&NativeModule> {
        if let [module_id, remaining_path @ ..] = path {
            self.modules
                .get(module_id)?
                .lookup_module(remaining_path, item)
        } else {
            self.modules.get(item)
        }
    }

    pub fn lookup_type_mut(&mut self, path: &[ItemId], item: &ItemId) -> Option<&mut NativeType> {
        if let [module_id, remaining_path @ ..] = path {
            self.modules
                .get_mut(module_id)?
                .lookup_type_mut(remaining_path, item)
        } else {
            self.types.get_mut(item)
        }
    }

    pub fn lookup_fn_mut(&mut self, path: &[ItemId], item: &ItemId) -> Option<&mut NativeFunction> {
        if let [module_id, remaining_path @ ..] = path {
            self.modules
                .get_mut(module_id)?
                .lookup_fn_mut(remaining_path, item)
                .or_else(|| {
                    self.types
                        .get_mut(module_id)?
                        .method_mut(item)
                        .map(|native_method| native_method.function_mut())
                })
        } else {
            self.functions.get_mut(item)
        }
    }

    pub fn lookup_module_mut(
        &mut self,
        path: &[ItemId],
        item: &ItemId,
    ) -> Option<&mut NativeModule> {
        if let [module_id, remaining_path @ ..] = path {
            self.modules
                .get_mut(module_id)?
                .lookup_module_mut(remaining_path, item)
        } else {
            self.modules.get_mut(item)
        }
    }

    pub fn get_type(&self, item_id: &ItemId) -> Option<&NativeType> {
        self.types.get(item_id)
    }

    pub fn get_fn(&self, item_id: &ItemId) -> Option<&NativeFunction> {
        self.functions.get(item_id)
    }

    pub fn get_mod(&self, item_id: &ItemId) -> Option<&NativeModule> {
        self.modules.get(item_id)
    }

    pub fn get_type_mut(&mut self, item_id: &ItemId) -> Option<&mut NativeType> {
        self.types.get_mut(item_id)
    }

    pub fn get_fn_mut(&mut self, item_id: &ItemId) -> Option<&mut NativeFunction> {
        self.functions.get_mut(item_id)
    }

    pub fn get_mod_mut(&mut self, item_id: &ItemId) -> Option<&mut NativeModule> {
        self.modules.get_mut(item_id)
    }

    pub fn list_type_paths(&self) -> Vec<(Vec<ItemId>, ItemId)> {
        let mut paths = Vec::new();

        for (item_id, _) in self.types.iter() {
            paths.push((Vec::new(), item_id.clone()));
        }

        for (module_id, module) in self.modules.iter() {
            for (mut path, item_id) in module.list_type_paths() {
                path.insert(0, module_id.clone());
                paths.push((path, item_id.clone()));
            }
        }

        paths
    }

    pub fn list_fn_paths(&self) -> Vec<(Vec<ItemId>, ItemId)> {
        let mut paths = Vec::new();

        for (item_id, _) in self.functions.iter() {
            paths.push((Vec::new(), item_id.clone()));
        }

        for (module_id, module) in self.modules.iter() {
            for (mut path, item_id) in module.list_fn_paths() {
                path.insert(0, module_id.clone());
                paths.push((path, item_id.clone()));
            }
        }

        paths
    }

    pub fn list_module_paths(&self) -> Vec<(Vec<ItemId>, ItemId)> {
        let mut paths = Vec::new();

        for (item_id, _) in self.functions.iter() {
            paths.push((Vec::new(), item_id.clone()));
        }

        for (module_id, module) in self.modules.iter() {
            for (mut path, item_id) in module.list_module_paths() {
                path.insert(0, module_id.clone());
                paths.push((path, item_id.clone()));
            }
        }

        paths
    }
}
