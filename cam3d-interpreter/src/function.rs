pub mod native;
pub mod user;

use crate::call_stack::CallStack;
use crate::interpreter::{InterpreterEventSink, RuntimeError};
use crate::item_path::ItemPath;
use crate::library::Libraries;
use crate::value_key::FunctionCallInputKey;
use crate::values::Values;
use indexmap::IndexMap;
use native::NativeFunction;
use std::fmt::{Display, Formatter};
use user::UserFunction;

#[derive(Debug, Clone)]
pub enum Function {
    Native(NativeFunction),
    User(UserFunction),
}

impl Function {
    pub fn as_ref(&self) -> FunctionRef {
        match self {
            Function::Native(f) => FunctionRef::Native(f),
            Function::User(f) => FunctionRef::User(f),
        }
    }

    pub fn name(&self) -> &str {
        self.as_ref().name()
    }

    pub fn input_types(&self) -> &IndexMap<FunctionCallInputKey, ItemPath> {
        match self {
            Function::Native(f) => f.input_types(),
            Function::User(f) => f.input_types(),
        }
    }

    pub fn output_type(&self) -> &ItemPath {
        self.as_ref().output_type()
    }

    pub fn call(
        &self,
        call_stack: &CallStack,
        values: &mut Values,
        libraries: &Libraries,
        event_sink: impl InterpreterEventSink,
    ) -> Result<(), RuntimeError> {
        self.as_ref()
            .call(call_stack, values, libraries, event_sink)
    }
}

impl Display for Function {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Function::Native(native_fn) => Display::fmt(native_fn, f),
            Function::User(user_fn) => Display::fmt(user_fn, f),
        }
    }
}

impl From<NativeFunction> for Function {
    fn from(value: NativeFunction) -> Function {
        Function::Native(value)
    }
}

impl From<UserFunction> for Function {
    fn from(value: UserFunction) -> Function {
        Function::User(value)
    }
}

#[derive(Debug, Clone, Copy)]
pub enum FunctionRef<'a> {
    Native(&'a NativeFunction),
    User(&'a UserFunction),
}

impl<'a> FunctionRef<'a> {
    pub fn user(&self) -> Option<&'a UserFunction> {
        if let FunctionRef::User(f) = self {
            Some(f)
        } else {
            None
        }
    }

    pub fn name(&self) -> &'a str {
        match self {
            FunctionRef::Native(f) => f.name(),
            FunctionRef::User(f) => f.name(),
        }
    }

    pub fn input_types(&self) -> &IndexMap<FunctionCallInputKey, ItemPath> {
        match self {
            FunctionRef::Native(f) => f.input_types(),
            FunctionRef::User(f) => f.input_types(),
        }
    }

    pub fn output_type(&self) -> &'a ItemPath {
        match self {
            FunctionRef::Native(f) => f.output_type(),
            FunctionRef::User(f) => f.output_type(),
        }
    }

    pub fn call(
        &self,
        call_stack: &CallStack,
        values: &mut Values,
        libraries: &Libraries,
        event_sink: impl InterpreterEventSink,
    ) -> Result<(), RuntimeError> {
        match self {
            FunctionRef::Native(native_fn) => native_fn.call(call_stack, values, libraries),
            FunctionRef::User(user_fn) => user_fn.call(call_stack, values, libraries, event_sink),
        }
    }
}

impl<'a> Display for FunctionRef<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            FunctionRef::Native(native_fn) => Display::fmt(native_fn, f),
            FunctionRef::User(user_fn) => Display::fmt(user_fn, f),
        }
    }
}

impl<'a> From<&'a NativeFunction> for FunctionRef<'a> {
    fn from(value: &'a NativeFunction) -> FunctionRef<'a> {
        FunctionRef::Native(value)
    }
}

impl<'a> From<&'a UserFunction> for FunctionRef<'a> {
    fn from(value: &'a UserFunction) -> FunctionRef<'a> {
        FunctionRef::User(value)
    }
}

impl<'a> From<&'a mut NativeFunction> for FunctionRef<'a> {
    fn from(value: &'a mut NativeFunction) -> FunctionRef<'a> {
        FunctionRef::Native(value)
    }
}

impl<'a> From<&'a mut UserFunction> for FunctionRef<'a> {
    fn from(value: &'a mut UserFunction) -> FunctionRef<'a> {
        FunctionRef::User(value)
    }
}

#[derive(Debug)]
pub enum FunctionMut<'a> {
    Native(&'a mut NativeFunction),
    User(&'a mut UserFunction),
}

#[allow(dead_code)]
impl<'a> FunctionMut<'a> {
    pub fn as_ref(&self) -> FunctionRef {
        match self {
            FunctionMut::Native(f) => FunctionRef::Native(f),
            FunctionMut::User(f) => FunctionRef::User(f),
        }
    }

    pub fn user(self) -> Option<&'a mut UserFunction> {
        if let FunctionMut::User(f) = self {
            Some(f)
        } else {
            None
        }
    }

    fn name(&self) -> &str {
        self.as_ref().name()
    }

    fn input_types(&self) -> &IndexMap<FunctionCallInputKey, ItemPath> {
        match self {
            FunctionMut::Native(f) => f.input_types(),
            FunctionMut::User(f) => f.input_types(),
        }
    }

    fn output_type(&self) -> &ItemPath {
        self.as_ref().output_type()
    }

    fn call(
        &self,
        call_stack: &CallStack,
        values: &mut Values,
        libraries: &Libraries,
        event_sink: impl InterpreterEventSink,
    ) -> Result<(), RuntimeError> {
        self.as_ref()
            .call(call_stack, values, libraries, event_sink)
    }
}

impl<'a> Display for FunctionMut<'a> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            FunctionMut::Native(native_fn) => Display::fmt(native_fn, f),
            FunctionMut::User(user_fn) => Display::fmt(user_fn, f),
        }
    }
}

impl<'a> From<&'a mut NativeFunction> for FunctionMut<'a> {
    fn from(value: &'a mut NativeFunction) -> FunctionMut<'a> {
        FunctionMut::Native(value)
    }
}

impl<'a> From<&'a mut UserFunction> for FunctionMut<'a> {
    fn from(value: &'a mut UserFunction) -> FunctionMut<'a> {
        FunctionMut::User(value)
    }
}
