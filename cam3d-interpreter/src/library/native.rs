use crate::function::native::NativeFunction;
use crate::item_path::ItemPath;
use crate::library::LookupError;
use crate::module::native::NativeModule;
use crate::type_::{NativeMethod, NativeMethodRange, NativeType};
use crate::value::{Value, ValueUnboxed};
use crate::value_key::{AbsoluteRuntimeKey, FunctionCallInputKey};
use indexmap::IndexMap;
use std::collections::HashMap;
use std::sync::Arc;

#[derive(Debug, Clone)]
pub struct NativeLibrary {
    pub name: String,
    pub root: NativeModule,
}

impl NativeLibrary {
    pub fn new(name: impl Into<String>) -> NativeLibrary {
        let name = name.into();
        NativeLibrary {
            name: name.clone(),
            root: NativeModule::new(name),
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn insert_type(&mut self, item: NativeType) -> ItemPath {
        let key = self.root.insert_type(item);
        ItemPath::root_item(self.name.clone(), key)
    }

    pub fn insert_type_to(&mut self, module: &ItemPath, item: NativeType) -> ItemPath {
        let m = self.lookup_module_mut(module).unwrap();
        m.insert_type(item).onto_path(module.clone())
    }

    pub fn insert_fn(&mut self, item: NativeFunction) -> ItemPath {
        let key = self.root.insert_fn(item);
        ItemPath::root_item(self.name.clone(), key)
    }

    pub fn insert_fn_to(&mut self, module: &ItemPath, item: NativeFunction) -> ItemPath {
        let m = self.lookup_module_mut(module).unwrap();
        m.insert_fn(item).onto_path(module.clone())
    }

    pub fn insert_fn0_to<R>(
        &mut self,
        (module, name): (&ItemPath, impl Into<String>),
        r: &ItemPath,
        f: fn() -> R,
    ) -> ItemPath
    where
        R: ValueUnboxed + 'static,
    {
        let m = self.lookup_module_mut(module).unwrap();
        let native_fn = NativeFunction {
            name: name.into(),
            input_types: IndexMap::new(),
            output_type: r.clone(),
            f: Arc::new(move |call_stack, values, _libraries| {
                let output = f();
                values.insert_value(
                    AbsoluteRuntimeKey::new(call_stack.clone()),
                    Value::from_value(output),
                );
                Ok(())
            }),
        };
        m.insert_fn(native_fn).onto_path(module.clone())
    }

    pub fn insert_fn1_to<A, R>(
        &mut self,
        (module, name): (&ItemPath, impl Into<String>),
        a: (impl Into<String>, &ItemPath),
        r: &ItemPath,
        f: fn(&A) -> R,
    ) -> ItemPath
    where
        A: ValueUnboxed + 'static,
        R: ValueUnboxed + 'static,
    {
        let a_name = a.0.into();

        let m = self.lookup_module_mut(module).unwrap();
        let f = NativeFunction {
            name: name.into(),
            input_types: IndexMap::from([(FunctionCallInputKey::new(a_name.clone()), a.1.clone())]),
            output_type: r.clone(),
            f: Arc::new(move |call_stack, values, _libraries| {
                let input_a = values.get_input(call_stack, a_name.clone())?;
                let output = f(input_a);
                values.insert_value(
                    AbsoluteRuntimeKey::new(call_stack.clone()),
                    Value::from_value(output),
                );
                Ok(())
            }),
        };
        m.insert_fn(f).onto_path(module.clone())
    }

    pub fn insert_fn2_to<A, B, R>(
        &mut self,
        (module, name): (&ItemPath, impl Into<String>),
        a: (impl Into<String>, &ItemPath),
        b: (impl Into<String>, &ItemPath),
        r: &ItemPath,
        f: fn(&A, &B) -> R,
    ) -> ItemPath
    where
        A: ValueUnboxed + 'static,
        B: ValueUnboxed + 'static,
        R: ValueUnboxed + 'static,
    {
        let a_name = a.0.into();
        let b_name = b.0.into();

        let m = self.lookup_module_mut(module).unwrap();
        let f = NativeFunction {
            name: name.into(),
            input_types: IndexMap::from([
                (FunctionCallInputKey::new(a_name.clone()), a.1.clone()),
                (FunctionCallInputKey::new(b_name.clone()), b.1.clone()),
            ]),
            output_type: r.clone(),
            f: Arc::new(move |call_stack, values, _libraries| {
                let input_a = values.get_input(call_stack, a_name.clone())?;
                let input_b = values.get_input(call_stack, b_name.clone())?;
                let output = f(input_a, input_b);
                values.insert_value(
                    AbsoluteRuntimeKey::new(call_stack.clone()),
                    Value::from_value(output),
                );
                Ok(())
            }),
        };
        m.insert_fn(f).onto_path(module.clone())
    }

    pub fn insert_fn3_to<A, B, C, R>(
        &mut self,
        (module, name): (&ItemPath, impl Into<String>),
        a: (impl Into<String>, &ItemPath),
        b: (impl Into<String>, &ItemPath),
        c: (impl Into<String>, &ItemPath),
        r: &ItemPath,
        f: fn(&A, &B, &C) -> R,
    ) -> ItemPath
    where
        A: ValueUnboxed + 'static,
        B: ValueUnboxed + 'static,
        C: ValueUnboxed + 'static,
        R: ValueUnboxed + 'static,
    {
        let a_name = a.0.into();
        let b_name = b.0.into();
        let c_name = c.0.into();

        let m = self.lookup_module_mut(module).unwrap();
        let f = NativeFunction {
            name: name.into(),
            input_types: IndexMap::from([
                (FunctionCallInputKey::new(a_name.clone()), a.1.clone()),
                (FunctionCallInputKey::new(b_name.clone()), b.1.clone()),
                (FunctionCallInputKey::new(c_name.clone()), c.1.clone()),
            ]),
            output_type: r.clone(),
            f: Arc::new(move |call_stack, values, _libraries| {
                let input_a = values.get_input(call_stack, a_name.clone())?;
                let input_b = values.get_input(call_stack, b_name.clone())?;
                let input_c = values.get_input(call_stack, c_name.clone())?;
                let output = f(input_a, input_b, input_c);
                values.insert_value(
                    AbsoluteRuntimeKey::new(call_stack.clone()),
                    Value::from_value(output),
                );
                Ok(())
            }),
        };
        m.insert_fn(f).onto_path(module.clone())
    }

    pub fn insert_fn12_to<IN1, IN2, IN3, IN4, IN5, IN6, IN7, IN8, IN9, IN10, IN11, IN12, OUT>(
        &mut self,
        (module, name): (&ItemPath, impl Into<String>),
        in1: (impl Into<String>, &ItemPath),
        in2: (impl Into<String>, &ItemPath),
        in3: (impl Into<String>, &ItemPath),
        in4: (impl Into<String>, &ItemPath),
        in5: (impl Into<String>, &ItemPath),
        in6: (impl Into<String>, &ItemPath),
        in7: (impl Into<String>, &ItemPath),
        in8: (impl Into<String>, &ItemPath),
        in9: (impl Into<String>, &ItemPath),
        in10: (impl Into<String>, &ItemPath),
        in11: (impl Into<String>, &ItemPath),
        in12: (impl Into<String>, &ItemPath),
        out: &ItemPath,
        f: fn(&IN1, &IN2, &IN3, &IN4, &IN5, &IN6, &IN7, &IN8, &IN9, &IN10, &IN11, &IN12) -> OUT,
    ) -> ItemPath
    where
        IN1: ValueUnboxed + 'static,
        IN2: ValueUnboxed + 'static,
        IN3: ValueUnboxed + 'static,
        IN4: ValueUnboxed + 'static,
        IN5: ValueUnboxed + 'static,
        IN6: ValueUnboxed + 'static,
        IN7: ValueUnboxed + 'static,
        IN8: ValueUnboxed + 'static,
        IN9: ValueUnboxed + 'static,
        IN10: ValueUnboxed + 'static,
        IN11: ValueUnboxed + 'static,
        IN12: ValueUnboxed + 'static,
        OUT: ValueUnboxed + 'static,
    {
        let in1_name = in1.0.into();
        let in2_name = in2.0.into();
        let in3_name = in3.0.into();
        let in4_name = in4.0.into();
        let in5_name = in5.0.into();
        let in6_name = in6.0.into();
        let in7_name = in7.0.into();
        let in8_name = in8.0.into();
        let in9_name = in9.0.into();
        let in10_name = in10.0.into();
        let in11_name = in11.0.into();
        let in12_name = in12.0.into();

        let m = self.lookup_module_mut(module).unwrap();
        let f = NativeFunction {
            name: name.into(),
            input_types: IndexMap::from([
                (FunctionCallInputKey::new(in1_name.clone()), in1.1.clone()),
                (FunctionCallInputKey::new(in2_name.clone()), in2.1.clone()),
                (FunctionCallInputKey::new(in3_name.clone()), in3.1.clone()),
                (FunctionCallInputKey::new(in4_name.clone()), in4.1.clone()),
                (FunctionCallInputKey::new(in5_name.clone()), in5.1.clone()),
                (FunctionCallInputKey::new(in6_name.clone()), in6.1.clone()),
                (FunctionCallInputKey::new(in7_name.clone()), in7.1.clone()),
                (FunctionCallInputKey::new(in8_name.clone()), in8.1.clone()),
                (FunctionCallInputKey::new(in9_name.clone()), in9.1.clone()),
                (FunctionCallInputKey::new(in10_name.clone()), in10.1.clone()),
                (FunctionCallInputKey::new(in11_name.clone()), in11.1.clone()),
                (FunctionCallInputKey::new(in12_name.clone()), in12.1.clone()),
            ]),
            output_type: out.clone(),
            f: Arc::new(move |call_stack, values, _libraries| {
                let input_in1 = values.get_input(call_stack, in1_name.clone())?;
                let input_in2 = values.get_input(call_stack, in2_name.clone())?;
                let input_in3 = values.get_input(call_stack, in3_name.clone())?;
                let input_in4 = values.get_input(call_stack, in4_name.clone())?;
                let input_in5 = values.get_input(call_stack, in5_name.clone())?;
                let input_in6 = values.get_input(call_stack, in6_name.clone())?;
                let input_in7 = values.get_input(call_stack, in7_name.clone())?;
                let input_in8 = values.get_input(call_stack, in8_name.clone())?;
                let input_in9 = values.get_input(call_stack, in9_name.clone())?;
                let input_in10 = values.get_input(call_stack, in10_name.clone())?;
                let input_in11 = values.get_input(call_stack, in11_name.clone())?;
                let input_in12 = values.get_input(call_stack, in12_name.clone())?;
                let output = f(
                    input_in1, input_in2, input_in3, input_in4, input_in5, input_in6, input_in7,
                    input_in8, input_in9, input_in10, input_in11, input_in12,
                );
                values.insert_value(
                    AbsoluteRuntimeKey::new(call_stack.clone()),
                    Value::from_value(output),
                );
                Ok(())
            }),
        };
        m.insert_fn(f).onto_path(module.clone())
    }

    pub fn insert_method1_to<A, R>(
        &mut self,
        (type_, name): (&ItemPath, impl Into<String>),
        r: &ItemPath,
        f: fn(&A) -> R,
    ) -> ItemPath
    where
        A: ValueUnboxed + 'static,
        R: ValueUnboxed + 'static,
    {
        let t = self.lookup_type_mut(type_).unwrap();
        let f = NativeMethod {
            f: NativeFunction {
                name: name.into(),
                input_types: IndexMap::from([(FunctionCallInputKey::new("self"), type_.clone())]),
                output_type: r.clone(),
                f: Arc::new(move |call_stack, values, _libraries| {
                    let input_a = values.get_input(call_stack, "self")?;
                    let output = f(input_a);
                    values.insert_value(
                        AbsoluteRuntimeKey::new(call_stack.clone()),
                        Value::from_value(output),
                    );
                    Ok(())
                }),
            },
            ranges: HashMap::new(),
        };
        t.add_method(f).onto_path(type_.clone())
    }

    pub fn insert_method2_to<A, B, R>(
        &mut self,
        (type_, name): (&ItemPath, impl Into<String>),
        b: (impl Into<String>, &ItemPath),
        r: &ItemPath,
        f: fn(&A, &B) -> R,
    ) -> ItemPath
    where
        A: ValueUnboxed + 'static,
        B: ValueUnboxed + 'static,
        R: ValueUnboxed + 'static,
    {
        let b_name = b.0.into();

        let t = self.lookup_type_mut(type_).unwrap();
        let f = NativeMethod {
            f: NativeFunction {
                name: name.into(),
                input_types: IndexMap::from([
                    (FunctionCallInputKey::new("self"), type_.clone()),
                    (FunctionCallInputKey::new(b_name.clone()), b.1.clone()),
                ]),
                output_type: r.clone(),
                f: Arc::new(move |call_stack, values, _libraries| {
                    let input_a = values.get_input(call_stack, "self")?;
                    let input_b = values.get_input(call_stack, b_name.clone())?;
                    let output = f(input_a, input_b);
                    values.insert_value(
                        AbsoluteRuntimeKey::new(call_stack.clone()),
                        Value::from_value(output),
                    );
                    Ok(())
                }),
            },
            ranges: HashMap::new(),
        };
        t.add_method(f).onto_path(type_.clone())
    }

    pub fn insert_method2_ranged_to<A, B, BIter, R>(
        &mut self,
        (type_, name): (&ItemPath, impl Into<String>),
        b: (impl Into<String>, &ItemPath, Option<fn(&A) -> BIter>),
        r: &ItemPath,
        f: fn(&A, &B) -> R,
    ) -> ItemPath
    where
        A: ValueUnboxed + 'static,
        B: ValueUnboxed + 'static,
        BIter: ExactSizeIterator<Item = B> + 'static,
        R: ValueUnboxed + 'static,
    {
        let b_name = b.0.into();

        let mut ranges = HashMap::new();
        if let Some(b_iter) = b.2 {
            ranges.insert(
                FunctionCallInputKey::new(b_name.clone()),
                Arc::new(move |value: &Value| {
                    let v = value.downcast()?;
                    let iter = b_iter(v).map(Value::from_value);
                    Ok(Box::new(iter) as Box<dyn ExactSizeIterator<Item = Value>>)
                }) as Arc<dyn NativeMethodRange>,
            );
        }

        let t = self.lookup_type_mut(type_).unwrap();
        let f = NativeMethod {
            f: NativeFunction {
                name: name.into(),
                input_types: IndexMap::from([
                    (FunctionCallInputKey::new("self"), type_.clone()),
                    (FunctionCallInputKey::new(b_name.clone()), b.1.clone()),
                ]),
                output_type: r.clone(),
                f: Arc::new(move |call_stack, values, _libraries| {
                    let input_a = values.get_input(call_stack, "self")?;
                    let input_b = values.get_input(call_stack, b_name.clone())?;
                    let output = f(input_a, input_b);
                    values.insert_value(
                        AbsoluteRuntimeKey::new(call_stack.clone()),
                        Value::from_value(output),
                    );
                    Ok(())
                }),
            },
            ranges,
        };
        t.add_method(f).onto_path(type_.clone())
    }

    pub fn insert_method1_mut_to<A, R>(
        &mut self,
        (type_, name): (&ItemPath, impl Into<String>),
        r: &ItemPath,
        f: fn(&mut A) -> R,
    ) -> ItemPath
    where
        A: ValueUnboxed + 'static + Clone,
        R: ValueUnboxed + 'static,
    {
        let t = self.lookup_type_mut(type_).unwrap();
        let f = NativeMethod {
            f: NativeFunction {
                name: name.into(),
                input_types: IndexMap::from([(FunctionCallInputKey::new("self"), type_.clone())]),
                output_type: r.clone(),
                f: Arc::new(move |call_stack, values, _libraries| {
                    let input_a: &A = values.get_input(call_stack, "self")?;
                    let mut input_a = input_a.clone();
                    let output = f(&mut input_a);
                    *(values.get_input_mut(call_stack, "self")?) = input_a;
                    values.insert_value(
                        AbsoluteRuntimeKey::new(call_stack.clone()),
                        Value::from_value(output),
                    );
                    Ok(())
                }),
            },
            ranges: HashMap::new(),
        };
        t.add_method(f).onto_path(type_.clone())
    }

    pub fn insert_method2_mut_to<A, B, R>(
        &mut self,
        (type_, name): (&ItemPath, impl Into<String>),
        b: (impl Into<String>, &ItemPath),
        r: &ItemPath,
        f: fn(&mut A, &B) -> R,
    ) -> ItemPath
    where
        A: ValueUnboxed + 'static + Clone,
        B: ValueUnboxed + 'static,
        R: ValueUnboxed + 'static,
    {
        let b_name = b.0.into();

        let t = self.lookup_type_mut(type_).unwrap();
        let f = NativeMethod {
            f: NativeFunction {
                name: name.into(),
                input_types: IndexMap::from([
                    (FunctionCallInputKey::new("self"), type_.clone()),
                    (FunctionCallInputKey::new(b_name.clone()), b.1.clone()),
                ]),
                output_type: r.clone(),
                f: Arc::new(move |call_stack, values, _libraries| {
                    let input_a: &A = values.get_input(call_stack, "self")?;
                    let mut input_a = input_a.clone();
                    let input_b = values.get_input(call_stack, b_name.clone())?;
                    let output = f(&mut input_a, input_b);
                    *(values.get_input_mut(call_stack, "self")?) = input_a;
                    values.insert_value(
                        AbsoluteRuntimeKey::new(call_stack.clone()),
                        Value::from_value(output),
                    );
                    Ok(())
                }),
            },
            ranges: HashMap::new(),
        };
        t.add_method(f).onto_path(type_.clone())
    }

    pub fn insert_method3_to<A, B, C, R>(
        &mut self,
        (type_, name): (&ItemPath, impl Into<String>),
        b: (impl Into<String>, &ItemPath),
        c: (impl Into<String>, &ItemPath),
        r: &ItemPath,
        f: fn(&A, &B, &C) -> R,
    ) -> ItemPath
    where
        A: ValueUnboxed + 'static,
        B: ValueUnboxed + 'static,
        C: ValueUnboxed + 'static,
        R: ValueUnboxed + 'static,
    {
        let b_name = b.0.into();
        let b_name2 = b_name.clone();
        let c_name = c.0.into();
        let c_name2 = c_name.clone();

        let t = self.lookup_type_mut(type_).unwrap();
        let f = NativeMethod {
            f: NativeFunction {
                name: name.into(),
                input_types: IndexMap::from([
                    (FunctionCallInputKey::new("self"), type_.clone()),
                    (FunctionCallInputKey::new(b_name.clone()), b.1.clone()),
                    (FunctionCallInputKey::new(c_name.clone()), c.1.clone()),
                ]),
                output_type: r.clone(),
                f: Arc::new(move |call_stack, values, _libraries| {
                    let input_a = values.get_input(call_stack, "self")?;
                    let input_b = values.get_input(call_stack, b_name2.clone())?;
                    let input_c = values.get_input(call_stack, c_name2.clone())?;
                    let output = f(input_a, input_b, input_c);
                    values.insert_value(
                        AbsoluteRuntimeKey::new(call_stack.clone()),
                        Value::from_value(output),
                    );
                    Ok(())
                }),
            },
            ranges: HashMap::new(),
        };
        t.add_method(f).onto_path(type_.clone())
    }

    pub fn insert_method3_ranged_to<A, B, BIter, C, CIter, R>(
        &mut self,
        (type_, name): (&ItemPath, impl Into<String>),
        b: (impl Into<String>, &ItemPath, Option<fn(&A) -> BIter>),
        c: (impl Into<String>, &ItemPath, Option<fn(&A) -> CIter>),
        r: &ItemPath,
        f: fn(&A, &B, &C) -> R,
    ) -> ItemPath
    where
        A: ValueUnboxed + 'static,
        B: ValueUnboxed + 'static,
        BIter: ExactSizeIterator<Item = B> + 'static,
        C: ValueUnboxed + 'static,
        CIter: ExactSizeIterator<Item = C> + 'static,
        R: ValueUnboxed + 'static,
    {
        let b_name = b.0.into();
        let b_name2 = b_name.clone();
        let c_name = c.0.into();
        let c_name2 = c_name.clone();

        let mut ranges = HashMap::new();
        if let Some(b_iter) = b.2 {
            ranges.insert(
                FunctionCallInputKey::new(b_name.clone()),
                Arc::new(move |value: &Value| {
                    let v = value.downcast()?;
                    let iter = b_iter(v).map(Value::from_value);
                    Ok(Box::new(iter) as Box<dyn ExactSizeIterator<Item = Value>>)
                }) as Arc<dyn NativeMethodRange>,
            );
        }
        if let Some(c_iter) = c.2 {
            ranges.insert(
                FunctionCallInputKey::new(c_name.clone()),
                Arc::new(move |value: &Value| {
                    let v = value.downcast()?;
                    let iter = c_iter(v).map(Value::from_value);
                    Ok(Box::new(iter) as Box<dyn ExactSizeIterator<Item = Value>>)
                }) as Arc<dyn NativeMethodRange>,
            );
        }

        let t = self.lookup_type_mut(type_).unwrap();
        let f = NativeMethod {
            f: NativeFunction {
                name: name.into(),
                input_types: IndexMap::from([
                    (FunctionCallInputKey::new("self"), type_.clone()),
                    (FunctionCallInputKey::new(b_name.clone()), b.1.clone()),
                    (FunctionCallInputKey::new(c_name.clone()), c.1.clone()),
                ]),
                output_type: r.clone(),
                f: Arc::new(move |call_stack, values, _libraries| {
                    let input_a = values.get_input(call_stack, "self")?;
                    let input_b = values.get_input(call_stack, b_name2.clone())?;
                    let input_c = values.get_input(call_stack, c_name2.clone())?;
                    let output = f(input_a, input_b, input_c);
                    values.insert_value(
                        AbsoluteRuntimeKey::new(call_stack.clone()),
                        Value::from_value(output),
                    );
                    Ok(())
                }),
            },
            ranges,
        };
        t.add_method(f).onto_path(type_.clone())
    }

    pub fn insert_method5_to<SELF, IN1, IN2, IN3, IN4, OUT>(
        &mut self,
        (type_, name): (&ItemPath, impl Into<String>),
        in1: (impl Into<String>, &ItemPath),
        in2: (impl Into<String>, &ItemPath),
        in3: (impl Into<String>, &ItemPath),
        in4: (impl Into<String>, &ItemPath),
        out: &ItemPath,
        f: fn(&SELF, &IN1, &IN2, &IN3, &IN4) -> OUT,
    ) -> ItemPath
    where
        SELF: ValueUnboxed + 'static,
        IN1: ValueUnboxed + 'static,
        IN2: ValueUnboxed + 'static,
        IN3: ValueUnboxed + 'static,
        IN4: ValueUnboxed + 'static,
        OUT: ValueUnboxed + 'static,
    {
        let in1_name = in1.0.into();
        let in1_name2 = in1_name.clone();
        let in2_name = in2.0.into();
        let in2_name2 = in2_name.clone();
        let in3_name = in3.0.into();
        let in3_name2 = in3_name.clone();
        let in4_name = in4.0.into();
        let in4_name2 = in4_name.clone();

        let t = self.lookup_type_mut(type_).unwrap();
        let f = NativeMethod {
            f: NativeFunction {
                name: name.into(),
                input_types: IndexMap::from([
                    (FunctionCallInputKey::new("self"), type_.clone()),
                    (FunctionCallInputKey::new(in1_name.clone()), in1.1.clone()),
                    (FunctionCallInputKey::new(in2_name.clone()), in2.1.clone()),
                    (FunctionCallInputKey::new(in3_name.clone()), in3.1.clone()),
                    (FunctionCallInputKey::new(in4_name.clone()), in4.1.clone()),
                ]),
                output_type: out.clone(),
                f: Arc::new(move |call_stack, values, _libraries| {
                    let input_self = values.get_input(call_stack, "self")?;
                    let input_in1 = values.get_input(call_stack, in1_name2.clone())?;
                    let input_in2 = values.get_input(call_stack, in2_name2.clone())?;
                    let input_in3 = values.get_input(call_stack, in3_name2.clone())?;
                    let input_in4 = values.get_input(call_stack, in4_name2.clone())?;
                    let output = f(input_self, input_in1, input_in2, input_in3, input_in4);
                    values.insert_value(
                        AbsoluteRuntimeKey::new(call_stack.clone()),
                        Value::from_value(output),
                    );
                    Ok(())
                }),
            },
            ranges: HashMap::new(),
        };
        t.add_method(f).onto_path(type_.clone())
    }

    pub fn insert_module(&mut self, item: NativeModule) -> ItemPath {
        let key = self.root.insert_module(item);
        ItemPath::root_item(self.name.clone(), key)
    }

    pub fn lookup_type(&self, path: &ItemPath) -> Result<&NativeType, LookupError> {
        if path.library() != self.name {
            Err(LookupError::wrong_library(self.name.clone()))
        } else {
            self.root
                .lookup_type(&path.module_path, &path.item_id)
                .ok_or(LookupError::missing_type(path.clone()))
        }
    }

    pub fn lookup_fn(&self, path: &ItemPath) -> Result<&NativeFunction, LookupError> {
        if path.library() != self.name {
            Err(LookupError::wrong_library(self.name.clone()))
        } else {
            self.root
                .lookup_fn(&path.module_path, &path.item_id)
                .ok_or(LookupError::missing_function(path.clone()))
        }
    }

    pub fn lookup_module(&self, path: &ItemPath) -> Result<&NativeModule, LookupError> {
        if path.library() != self.name {
            Err(LookupError::wrong_library(self.name.clone()))
        } else {
            self.root
                .lookup_module(&path.module_path, &path.item_id)
                .ok_or(LookupError::missing_module(path.clone()))
        }
    }

    pub fn lookup_type_mut(&mut self, path: &ItemPath) -> Result<&mut NativeType, LookupError> {
        if path.library() != self.name {
            Err(LookupError::wrong_library(self.name.clone()))
        } else {
            self.root
                .lookup_type_mut(&path.module_path, &path.item_id)
                .ok_or(LookupError::missing_type(path.clone()))
        }
    }

    pub fn lookup_fn_mut(&mut self, path: &ItemPath) -> Result<&mut NativeFunction, LookupError> {
        if path.library() != self.name {
            Err(LookupError::wrong_library(self.name.clone()))
        } else {
            self.root
                .lookup_fn_mut(&path.module_path, &path.item_id)
                .ok_or(LookupError::missing_function(path.clone()))
        }
    }

    pub fn lookup_module_mut(&mut self, path: &ItemPath) -> Result<&mut NativeModule, LookupError> {
        if path.library() != self.name {
            Err(LookupError::wrong_library(self.name.clone()))
        } else {
            self.root
                .lookup_module_mut(&path.module_path, &path.item_id)
                .ok_or(LookupError::missing_module(path.clone()))
        }
    }

    pub fn list_type_paths(&self) -> impl Iterator<Item = ItemPath> + '_ {
        self.root
            .list_type_paths()
            .into_iter()
            .map(|(module_path, item_id)| ItemPath {
                library: self.name.clone(),
                module_path,
                item_id,
            })
    }

    pub fn list_fn_paths(&self) -> impl Iterator<Item = ItemPath> + '_ {
        self.root
            .list_fn_paths()
            .into_iter()
            .map(|(module_path, item_id)| ItemPath {
                library: self.name.clone(),
                module_path,
                item_id,
            })
    }

    pub fn list_module_paths(&self) -> impl Iterator<Item = ItemPath> + '_ {
        self.root
            .list_module_paths()
            .into_iter()
            .map(|(module_path, item_id)| ItemPath {
                library: self.name.clone(),
                module_path,
                item_id,
            })
    }
}
