use crate::function::user::UserFunction;
use crate::item_path::ItemPath;
use crate::library::LookupError;
use crate::module::user::UserModule;
use crate::type_::UserType;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct UserLibrary {
    name: String,
    pub root: UserModule,
}

impl UserLibrary {
    pub fn new(name: impl Into<String>) -> UserLibrary {
        let name = name.into();
        UserLibrary {
            name: name.clone(),
            root: UserModule::new(name),
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn insert_type(&mut self, item: UserType) -> ItemPath {
        let key = self.root.insert_type(item);
        ItemPath::root_item(self.name.clone(), key)
    }

    pub fn insert_fn(&mut self, item: UserFunction) -> ItemPath {
        let key = self.root.insert_fn(item);
        ItemPath::root_item(self.name.clone(), key)
    }

    pub fn insert_module(&mut self, item: UserModule) -> ItemPath {
        let key = self.root.insert_module(item);
        ItemPath::root_item(self.name.clone(), key)
    }

    pub fn lookup_type(&self, path: &ItemPath) -> Result<&UserType, LookupError> {
        if path.library() != self.name {
            Err(LookupError::wrong_library(self.name.clone()))
        } else {
            self.root
                .lookup_type(&path.module_path, &path.item_id)
                .ok_or(LookupError::missing_type(path.clone()))
        }
    }

    pub fn lookup_fn(&self, path: &ItemPath) -> Result<&UserFunction, LookupError> {
        if path.library() != self.name {
            Err(LookupError::wrong_library(self.name.clone()))
        } else {
            self.root
                .lookup_fn(&path.module_path, &path.item_id)
                .ok_or(LookupError::missing_function(path.clone()))
        }
    }

    pub fn lookup_module(&self, path: &ItemPath) -> Result<&UserModule, LookupError> {
        if path.library() != self.name {
            Err(LookupError::wrong_library(self.name.clone()))
        } else {
            self.root
                .lookup_module(&path.module_path, &path.item_id)
                .ok_or(LookupError::missing_module(path.clone()))
        }
    }

    pub fn lookup_type_mut(&mut self, path: &ItemPath) -> Result<&mut UserType, LookupError> {
        if path.library() != self.name {
            Err(LookupError::wrong_library(self.name.clone()))
        } else {
            self.root
                .lookup_type_mut(&path.module_path, &path.item_id)
                .ok_or(LookupError::missing_type(path.clone()))
        }
    }

    pub fn lookup_fn_mut(&mut self, path: &ItemPath) -> Result<&mut UserFunction, LookupError> {
        if path.library() != self.name {
            Err(LookupError::wrong_library(self.name.clone()))
        } else {
            self.root
                .lookup_fn_mut(&path.module_path, &path.item_id)
                .ok_or(LookupError::missing_function(path.clone()))
        }
    }

    pub fn lookup_module_mut(&mut self, path: &ItemPath) -> Result<&mut UserModule, LookupError> {
        if path.library() != self.name {
            Err(LookupError::wrong_library(self.name.clone()))
        } else {
            self.root
                .lookup_module_mut(&path.module_path, &path.item_id)
                .ok_or(LookupError::missing_module(path.clone()))
        }
    }

    pub fn list_type_paths(&self) -> impl Iterator<Item = ItemPath> + '_ {
        self.root
            .list_type_paths()
            .into_iter()
            .map(|(module_path, item_id)| ItemPath {
                library: self.name.clone(),
                module_path,
                item_id,
            })
    }

    pub fn list_fn_paths(&self) -> impl Iterator<Item = ItemPath> + '_ {
        self.root
            .list_fn_paths()
            .into_iter()
            .map(|(module_path, item_id)| ItemPath {
                library: self.name.clone(),
                module_path,
                item_id,
            })
    }

    pub fn list_module_paths(&self) -> impl Iterator<Item = ItemPath> + '_ {
        self.root
            .list_module_paths()
            .into_iter()
            .map(|(module_path, item_id)| ItemPath {
                library: self.name.clone(),
                module_path,
                item_id,
            })
    }
}
