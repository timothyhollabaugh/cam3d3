use crate::core_module::core_lib;
use crate::expression::{Expression, FunctionCall};
use crate::function::user::UserFunction;
use crate::function::Function;
use crate::inc_map::IncMap;
use crate::interpreter::Interpreter;
use crate::library::{Libraries, Library};
use crate::type_::{NativeTypeBox, Type};
use crate::value::Value;
use crate::value_key::{RelativeRuntimeKey, RelativeValueKey};

#[derive(Debug, Clone)]
pub struct TestType(String);

impl TestType {
    pub fn new_type(name: impl Into<String>) -> Type {
        Type::from_type(TestType(name.into()))
    }
}

impl NativeTypeBox for TestType {
    fn name(&self) -> &str {
        &self.0
    }
}

#[test]
fn test_core_add_literals() {
    let mut constant = IncMap::new();

    let (core_lib, core_items) = core_lib();

    let mut test_lib = Library::new("test");

    let mut function_body = IncMap::new();
    let output_value = function_body.insert(FunctionCall {
        function: "core::int::add".parse().unwrap(),
        input_values: vec![
            Expression::Literal(Value::from_value(3)),
            Expression::Literal(Value::from_value(5)),
        ],
    });

    let main_function = test_lib.insert(Function::from_function(UserFunction {
        name: "main".to_string(),
        input_types: Vec::new(),
        output_type: Some((
            "core::int::Integer".parse().unwrap(),
            RelativeRuntimeKey::BlockExpression(output_value),
        )),
        body: function_body,
    }));

    let libraries = Libraries::with_libraries(vec![core_lib, test_lib], core_items);
    let interpreter = Interpreter::new(libraries, main_function, constant);
    let value = interpreter.run().unwrap().unwrap();
    let integer_value = value.downcast::<i64>().unwrap();

    assert_eq!(*integer_value, 8);
}

#[test]
fn test_core_add_values() {
    let mut constant = IncMap::new();
    let three_key = constant.insert(Value::from_value(3));
    let five_key = constant.insert(Value::from_value(5));

    let (core_lib, core_items) = core_lib();

    let mut test_lib = Library::new("test");

    let mut function_body = IncMap::new();
    let output_value = function_body.insert(FunctionCall {
        function: "core::int::add".parse().unwrap(),
        input_values: vec![
            Expression::Value(RelativeValueKey::Constant(three_key)),
            Expression::Value(RelativeValueKey::Constant(five_key)),
        ],
    });

    let main_function = test_lib.insert(Function::from_function(UserFunction {
        name: "main".to_string(),
        input_types: Vec::new(),
        output_type: Some((
            "core::int::Integer".parse().unwrap(),
            RelativeRuntimeKey::BlockExpression(output_value),
        )),
        body: function_body,
    }));

    let libraries = Libraries::with_libraries(vec![core_lib, test_lib], core_items);
    let interpreter = Interpreter::new(libraries, main_function, constant);
    let value = interpreter.run().unwrap().unwrap();
    let integer_value = value.downcast::<i64>().unwrap();

    assert_eq!(*integer_value, 8);
}
