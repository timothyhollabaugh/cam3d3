use criterion::{black_box, criterion_group, criterion_main, BenchmarkId, Criterion, Throughput};

use cam3d3::cam::geometry2d::{LineString, MultiLineString, Vector2};
use cam3d3::cam::geometry3d::Plane;
use cam3d3::interpreter::{
    ReferenceValueKey, TopValueKey, TopValueKeyKind, Value, ValueKey, ValueType, Values,
};

fn init_values() -> Values {
    let mut values = Values::new();
    values.insert_reference_value(
        ReferenceValueKey::XY,
        Value::new_plane("XY", Plane::xy(0.0)),
    );
    values
}

pub fn criterion_benchmark(c: &mut Criterion) {
    let mut multipath_group = c.benchmark_group("multipath by number of paths");
    for number_paths in (1..=10).map(|n| n * 100) {
        let mut values = init_values();
        let path = LineString::circle(1.0, 1024);
        let multipath = MultiLineString {
            linestrings: (0..number_paths)
                .map(|n| path.clone() + Vector2::new(n as f32, 0.0))
                .collect(),
        };

        let plane_key = ValueKey::Top(TopValueKey {
            key: TopValueKeyKind::Reference(ReferenceValueKey::XY),
            t: ValueType::Plane,
        });

        let top_key = values.insert_reference_value(
            ReferenceValueKey::Origin,
            Value::new_multipath2d("Test", &plane_key, multipath),
        );

        let float_key = {
            let mut path_keys = values.sub_values(top_key).unwrap();
            let path_key = path_keys.nth(number_paths / 2).unwrap().0;
            let mut point_keys = values.sub_values(path_key).unwrap();
            let point_key = point_keys.nth(500).unwrap().0;
            let mut float_keys = values.sub_values(point_key).unwrap();
            float_keys.nth(1).unwrap().0
        };

        puffin::set_scopes_on(true);
        puffin::GlobalProfiler::lock().new_frame();
        multipath_group.bench_with_input(
            BenchmarkId::from_parameter(number_paths),
            &number_paths,
            move |b, n| {
                b.iter(|| {
                    puffin::profile_function!();
                    values.get_ref(&float_key);
                });
            },
        );
        puffin::GlobalProfiler::lock().new_frame();
    }
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
