#![feature(error_generic_member_access)]
#![feature(extract_if)]
#![feature(option_get_or_insert_default)]
#![feature(hash_extract_if)]
#![feature(associated_type_defaults)]
#![feature(array_windows)]
#![feature(iter_map_windows)]
#![feature(try_blocks)]
#![allow(dead_code)]
#![allow(unused)]

use crate::app::CamApp;
use cam3d_interpreter::DeferredFormatter;
use eframe::Renderer;
use egui::{Vec2, ViewportBuilder};
use egui_wgpu::WgpuConfiguration;
use flexi_logger::{DeferredNow, FileSpec, TS_DASHES_BLANK_COLONS_DOT_BLANK};
use log::Record;
use std::default::Default;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::sync::Arc;
use std::time::{Instant, SystemTime};
use wgpu::PowerPreference;

mod app;
mod cam;
mod cam_module;
mod task;
mod test;
mod ui;

fn log_format(
    write: &mut dyn Write,
    now: &mut DeferredNow,
    record: &Record<'_>,
) -> Result<(), std::io::Error> {
    write!(
        write,
        "{} {} {} {}:{} {}",
        now.format(TS_DASHES_BLANK_COLONS_DOT_BLANK),
        record.level(),
        record.module_path().unwrap_or("--"),
        record.file().unwrap_or(""),
        record
            .line()
            .map(|l| l.to_string())
            .unwrap_or("--".to_string()),
        record.args()
    )
}

pub fn main() {
    /*
    let mut log_builder = env_logger::builder();

    log_builder.format(|fmt, record| {
        writeln!(
            fmt,
            "{} {} {} {}:{} {}",
            fmt.timestamp(),
            record.level(),
            record.module_path().unwrap_or("--"),
            record.file().unwrap_or("--"),
            record
                .line()
                .map(|l| l.to_string())
                .unwrap_or("--".to_string()),
            record.args()
        )
    });

    let now = chrono::Local::now();
    let log_name = format!("log/cam3d_{}.log", now.to_rfc3339());
    let log_file = File::create_new(&log_name);

    match log_file {
        Ok(log_file) => {
            eprintln!("log: {}", &log_name);
            let writer = BufWriter::new(log_file);
            log_builder.target(env_logger::Target::Pipe(Box::new(writer)));
        }

        Err(e) => eprintln!(
            "Failed to configure log file {}: {e}. cwd {}",
            &log_name,
            std::env::current_dir().unwrap().to_string_lossy()
        ),
    }

    log_builder.init();
     */

    flexi_logger::Logger::try_with_env_or_str("info")
        .unwrap()
        .log_to_file(FileSpec::default().directory("log"))
        .duplicate_to_stderr(flexi_logger::Duplicate::All)
        .print_message()
        .format_for_files(log_format)
        .format_for_stderr(log_format)
        .start();

    let server_addr = format!("127.0.0.1:{}", puffin_http::DEFAULT_PORT);

    log::info!("Starting profiling server on {}", server_addr);

    match puffin_http::Server::new(&server_addr) {
        Ok(_puffin_server) => log::info!("Successfully started profiling server"),
        Err(e) => log::warn!("Failed to start profiling server: {}", e),
    }

    puffin::set_scopes_on(true);

    log::info!("Starting Cam3d3");

    let native_options = eframe::NativeOptions {
        viewport: ViewportBuilder::default()
            .with_title("Cam 3")
            .with_inner_size(Vec2::new(1280.0, 800.0)),
        renderer: Renderer::Wgpu,
        depth_buffer: 32,
        multisampling: 4,
        wgpu_options: WgpuConfiguration {
            present_mode: wgpu::PresentMode::Fifo,
            ..WgpuConfiguration::default()
        },
        ..eframe::NativeOptions::default()
    };

    log::info!("Options: {:#?}", native_options.wgpu_options);

    log::debug!("Starting eframe");

    eframe::run_native(
        "Cam3d3",
        native_options,
        Box::new(|cc| {
            let egui_context = cc.egui_ctx.clone();
            Ok(Box::new(CamApp::new(cc, move || {
                egui_context.request_repaint()
            })))
        }),
    );
}
