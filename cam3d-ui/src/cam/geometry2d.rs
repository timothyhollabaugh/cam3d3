#![allow(unused)]

mod bounding_box;
mod linestring;
mod medial_axis;
mod multilinestring;
mod multipolygon;
mod origin;
mod point;
mod polygon;
mod svg;
mod text;
mod transform;
mod vector;

pub use bounding_box::*;
pub use linestring::*;
pub use medial_axis::*;
pub use multilinestring::*;
pub use multipolygon::*;
pub use origin::*;
pub use point::*;
pub use polygon::*;
pub use svg::*;
pub use text::*;
pub use transform::*;
pub use vector::*;

use clipper_rs::Paths;
use clipper_rs::{ClipType, Clipper, FillType, PolyType};
use itertools::Itertools;
use serde::Deserialize;
use serde::Serialize;
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

pub const RESOLUTION: f32 = 10000.0;
pub const MITER_LIMIT: f32 = 2.0;
pub const ROUND_PRECISION: f32 = 0.005;

pub trait Geometry {
    fn clone_paths(&self) -> Paths;
    fn has_area(&self) -> bool;
    fn mirror(&mut self, p1: Point2, p2: Point2);
    fn transform_by_points(
        &mut self,
        p1_expected: Point2,
        p1_actual: Point2,
        p2_expected: Point2,
        p2_actual: Point2,
    );
}

pub trait FromClipper {
    type Execute: ClipperExecute;
    fn from_paths(paths: <<Self as FromClipper>::Execute as ClipperExecute>::Output) -> Self;
}

impl FromClipper for (MultiLineString, MultiPolygon) {
    type Execute = ClipperExecuteTree;

    fn from_paths((open_paths, closed_paths): (Paths, Paths)) -> Self {
        (
            MultiLineString::from_paths(open_paths),
            MultiPolygon::from_paths(closed_paths),
        )
    }
}

pub trait ClipperExecute {
    type Output;
    fn execute(clipper: Clipper, clip_type: ClipType) -> Self::Output;
}

pub struct ClipperExecutePaths;

impl ClipperExecute for ClipperExecutePaths {
    type Output = Paths;
    fn execute(clipper: Clipper, clip_type: ClipType) -> Paths {
        clipper
            .execute(clip_type, FillType::NonZero, FillType::NonZero)
            .unwrap_or(Paths::new())
    }
}

pub struct ClipperExecuteTree;

impl ClipperExecute for ClipperExecuteTree {
    type Output = (Paths, Paths);

    fn execute(clipper: Clipper, clip_type: ClipType) -> (Paths, Paths) {
        clipper
            .execute_tree(clip_type, FillType::NonZero, FillType::NonZero)
            .unwrap_or((Paths::new(), Paths::new()))
    }
}

pub fn union<'i, I, O>(paths: I) -> O
where
    I: IntoIterator<Item = &'i dyn Geometry>,
    O: FromClipper,
{
    let mut clipper = Clipper::new();

    for path in paths {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Subject, closed);
    }

    let out = O::Execute::execute(clipper, ClipType::Union);

    O::from_paths(out)
}

pub fn difference<'s, 'c, S, C, O>(subjects: S, clip: C) -> O
where
    S: IntoIterator<Item = &'s dyn Geometry>,
    C: IntoIterator<Item = &'c dyn Geometry>,
    O: FromClipper,
{
    let mut clipper = Clipper::new();

    for path in subjects {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Subject, closed);
    }

    for path in clip {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Clip, closed);
    }

    let out = O::Execute::execute(clipper, ClipType::Difference);

    O::from_paths(out)
}

pub fn intersection<'s, 'c, S, C, O>(subjects: S, clip: C) -> O
where
    S: IntoIterator<Item = &'s dyn Geometry>,
    C: IntoIterator<Item = &'c dyn Geometry>,
    O: FromClipper,
{
    let mut clipper = Clipper::new();

    for path in subjects {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Subject, closed);
    }

    for path in clip {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Clip, closed);
    }

    let out = O::Execute::execute(clipper, ClipType::Intersection);

    O::from_paths(out)
}

#[allow(dead_code)]
pub fn xor<'s, 'c, S, C, O>(subjects: S, clip: C) -> O
where
    S: IntoIterator<Item = &'s dyn Geometry>,
    C: IntoIterator<Item = &'c dyn Geometry>,
    O: FromClipper,
{
    let mut clipper = Clipper::new();

    for path in subjects {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Subject, closed);
    }

    for path in clip {
        let closed = path.has_area();
        clipper.add_paths(path.clone_paths(), PolyType::Clip, closed);
    }

    let out = O::Execute::execute(clipper, ClipType::Xor);

    O::from_paths(out)
}
