use super::{
    BoundingBox2, FromClipper, Geometry, LineString, MultiLineString, MultiPolygon, Point2,
    MITER_LIMIT, RESOLUTION, ROUND_PRECISION,
};
use crate::cam::geometry2d::vector::Vector2;
use crate::cam::geometry2d::{Matrix3, MedialAxis, Origin2};
use clipper_rs::{ClipperOffset, EndType, JoinType, Paths, PointPolygon};
use std::fmt::Formatter;
use std::ops::MulAssign;

#[derive(Clone, Debug, PartialEq)]
pub struct Polygon {
    pub exterior: LineString,
    pub interiors: Vec<LineString>,
}

impl Polygon {
    pub fn area(&self) -> f32 {
        self.clone().clone_paths().area() as f32 / (RESOLUTION * RESOLUTION)
    }

    pub fn into_boundary(self) -> MultiLineString {
        MultiLineString {
            linestrings: vec![self.exterior]
                .into_iter()
                .chain(self.interiors.into_iter())
                .collect(),
        }
    }

    pub fn point_inside(&self, point: Point2) -> PointPolygon {
        match self.exterior.point_inside(point) {
            PointPolygon::Outside => return PointPolygon::Outside,
            PointPolygon::On => return PointPolygon::On,
            PointPolygon::In => {
                for interior in &self.interiors {
                    match interior.point_inside(point) {
                        PointPolygon::Outside => {}
                        PointPolygon::On => return PointPolygon::On,
                        PointPolygon::In => return PointPolygon::Outside,
                    }
                }
            }
        }

        PointPolygon::In
    }

    pub fn offset(self, delta: f32) -> MultiPolygon {
        MultiPolygon::from_paths(
            ClipperOffset::new(
                (MITER_LIMIT * RESOLUTION) as f64,
                (ROUND_PRECISION * RESOLUTION) as f64,
            )
            .with_paths(self.clone_paths(), JoinType::Round, EndType::ClosedPolygon)
            .execute((delta * RESOLUTION) as f64),
        )
    }

    pub fn bounds(&self) -> BoundingBox2 {
        self.exterior.bounds()
    }

    pub fn clean(&mut self, distance: f32) {
        self.exterior.clean(distance);
        for interior in self.interiors.iter_mut() {
            interior.clean(distance);
        }

        self.interiors
            .retain(|linestring| linestring.points.len() > 0);
    }

    pub fn tessillate(
        &self,
    ) -> Result<three_d_asset::TriMesh, lyon::tessellation::TessellationError> {
        let mut fill_buffers: lyon::tessellation::VertexBuffers<three_d_asset::Vector3<f64>, u32> =
            lyon::tessellation::VertexBuffers::new();

        let mut fill_buffers_builder = lyon::tessellation::BuffersBuilder::new(
            &mut fill_buffers,
            |v: lyon::tessellation::FillVertex| {
                three_d_asset::Vector3::<f64>::new(
                    v.position().x as f64,
                    v.position().y as f64,
                    0.0,
                )
            },
        );

        let mut fill_tessellator = lyon::tessellation::FillTessellator::new();
        fill_tessellator.tessellate_polygon(
            lyon::path::Polygon {
                points: &self
                    .exterior
                    .points
                    .iter()
                    .map(|point| lyon::math::Point::new(point.x, point.y))
                    .collect::<Vec<_>>(),
                closed: true,
            },
            &lyon::tessellation::FillOptions::default(),
            &mut fill_buffers_builder,
        )?;

        Ok(three_d_asset::TriMesh {
            indices: three_d_asset::Indices::U32(fill_buffers.indices),
            normals: Some(vec![
                three_d_asset::Vec3::unit_z();
                fill_buffers.vertices.len()
            ]),
            tangents: None,
            uvs: None,
            positions: three_d_asset::Positions::F64(fill_buffers.vertices),
            colors: None,
        })
    }

    pub fn transform(&mut self, transform: &Matrix3) {
        self.exterior.transform(transform);

        for interior in self.interiors.iter_mut() {
            interior.transform(transform)
        }
    }
    pub fn transform_iso(&mut self, transform_iso: &Origin2) {
        self.exterior.transform_iso(transform_iso);

        for interior in self.interiors.iter_mut() {
            interior.transform_iso(transform_iso)
        }
    }

    pub fn medial_axis(&self) -> MedialAxis {
        let mut cleaned = self.clone();
        cleaned.clean(1e-3);
        MedialAxis::from_polygon(&cleaned)
    }
}

impl Geometry for Polygon {
    fn clone_paths(&self) -> Paths {
        let mut exterior = self.exterior.clone().into_path();
        if !exterior.orientation() {
            exterior.reverse();
        }
        exterior.orientation();

        let interiors = self.interiors.iter().map(|linestring| {
            let mut interior = linestring.clone().into_path();
            if interior.orientation() {
                interior.reverse();
            }
            interior.orientation();
            interior
        });

        Paths::from_paths(
            vec![exterior]
                .into_iter()
                .chain(interiors.into_iter())
                .collect(),
        )
    }

    fn has_area(&self) -> bool {
        true
    }

    fn mirror(&mut self, p1: Point2, p2: Point2) {
        self.exterior.mirror(p1, p2);

        for interior in self.interiors.iter_mut() {
            interior.mirror(p1, p2);
        }
    }

    fn transform_by_points(
        &mut self,
        p1_expected: Point2,
        p1_actual: Point2,
        p2_expected: Point2,
        p2_actual: Point2,
    ) {
        self.exterior
            .transform_by_points(p1_expected, p1_actual, p2_expected, p2_actual);

        for interior in self.interiors.iter_mut() {
            interior.transform_by_points(p1_expected, p1_actual, p2_expected, p2_actual);
        }
    }
}

impl std::fmt::Display for Polygon {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let bounds = self.bounds();
        write!(f, "{}, {} interiors", self.bounds(), self.interiors.len())
    }
}

impl std::ops::Add<Vector2> for Polygon {
    type Output = Polygon;

    fn add(self, rhs: Vector2) -> Self::Output {
        Polygon {
            exterior: self.exterior + rhs,
            interiors: self
                .interiors
                .into_iter()
                .map(|linestring| linestring + rhs)
                .collect(),
        }
    }
}

impl std::ops::AddAssign<Vector2> for Polygon {
    fn add_assign(&mut self, rhs: Vector2) {
        self.exterior += rhs;
        for interior in self.interiors.iter_mut() {
            *interior += rhs;
        }
    }
}

impl std::ops::Sub<Vector2> for Polygon {
    type Output = Polygon;

    fn sub(self, rhs: Vector2) -> Self::Output {
        Polygon {
            exterior: self.exterior - rhs,
            interiors: self
                .interiors
                .into_iter()
                .map(|linestring| linestring - rhs)
                .collect(),
        }
    }
}

impl std::ops::SubAssign<Vector2> for Polygon {
    fn sub_assign(&mut self, rhs: Vector2) {
        self.exterior -= rhs;
        for interior in self.interiors.iter_mut() {
            *interior -= rhs;
        }
    }
}

impl std::ops::Mul<f32> for Polygon {
    type Output = Polygon;

    fn mul(self, rhs: f32) -> Self::Output {
        Polygon {
            exterior: self.exterior * rhs,
            interiors: self
                .interiors
                .into_iter()
                .map(|linestring| linestring * rhs)
                .collect(),
        }
    }
}

impl MulAssign<f32> for Polygon {
    fn mul_assign(&mut self, rhs: f32) {
        self.exterior *= rhs;
        for interior in self.interiors.iter_mut() {
            *interior *= rhs;
        }
    }
}

impl From<LineString> for Polygon {
    fn from(value: LineString) -> Polygon {
        Polygon {
            exterior: value,
            interiors: Vec::new(),
        }
    }
}
