use super::super::geometry3d::Point3;
use super::{Geometry, RESOLUTION};
use crate::cam::geometry2d::vector::Vector2;
use crate::cam::geometry2d::{Matrix3, Origin2};
use crate::cam::geometry3d::Plane;
use clipper_rs::{Paths, Point as IntPoint};
use egui::Pos2;
use nalgebra as na;
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize, Default)]
pub struct Point2 {
    pub x: f32,
    pub y: f32,
}

impl Point2 {
    pub fn zero() -> Point2 {
        Point2 { x: 0.0, y: 0.0 }
    }

    pub fn new(x: f32, y: f32) -> Point2 {
        Point2 { x, y }
    }

    pub fn from_xy(xy: f32) -> Point2 {
        Point2 { x: xy, y: xy }
    }

    pub fn from_x(x: f32) -> Point2 {
        Point2 { x, y: 0.0 }
    }

    pub fn from_y(y: f32) -> Point2 {
        Point2 { x: 0.0, y }
    }

    pub fn from_int_point(point: IntPoint) -> Point2 {
        Point2 {
            x: point.x as f32 / RESOLUTION,
            y: point.y as f32 / RESOLUTION,
        }
    }

    pub fn into_int_point(self) -> IntPoint {
        IntPoint {
            x: (self.x * RESOLUTION) as i64,
            y: (self.y * RESOLUTION) as i64,
        }
    }

    pub fn into_3d(self, plane: &Plane) -> Point3 {
        // TODO handle non-xy planes
        Point3 {
            x: plane.point.x + self.x,
            y: plane.point.y + self.y,
            z: plane.point.z,
        }
    }

    pub fn distance(&self) -> f32 {
        (self.x * self.x + self.y * self.y).sqrt()
    }

    pub fn distance_squared(&self) -> f32 {
        (self.x * self.x + self.y * self.y)
    }

    pub fn distance_from_edge(&self, start: Point2, end: Point2) -> f32 {
        // https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
        let d = (start - end).magnitude();

        if d != 0.0 {
            ((end.x - start.x) * (start.y - self.y) - (start.x - self.x) * (end.y - start.y)).abs()
                / d
        } else {
            (*self - start).magnitude()
        }
    }

    pub fn project_onto_edge(&self, start: Point2, end: Point2) -> Point2 {
        let v1 = end - start;
        let v2 = *self - start;

        start + v2.project_onto(v1)
    }

    pub fn distance_along_edge(&self, start: Point2, end: Point2) -> f32 {
        let v1 = end - start;
        let v2 = *self - start;

        v2.project_onto(v1).magnitude()
    }

    pub fn close(&self, other: &Point2) -> bool {
        (self.x - other.x).abs() < 0.01 && (self.y - other.y).abs() < 0.01
    }

    pub fn as_vector_from_zero(&self) -> Vector2 {
        *self - Point2::zero()
    }

    pub fn transform(&mut self, transform: &Matrix3) {
        *self = transform.transform_point(*self)
    }

    pub fn transform_iso(&mut self, transform: &Origin2) {
        *self = transform.transform_point(*self)
    }
}

impl Geometry for Point2 {
    fn clone_paths(&self) -> Paths {
        Paths::from_points(vec![vec![self.into_int_point()]])
    }

    fn has_area(&self) -> bool {
        false
    }

    fn mirror(&mut self, p1: Point2, p2: Point2) {
        // https://stackoverflow.com/questions/8954326/how-to-calculate-the-mirror-point-along-a-line
        let a = p2.y - p1.y;
        let b = -(p2.x - p1.x);
        let c = -a * p1.x - b * p1.y;

        let m = (a * a + b * b).sqrt();

        let a2 = a / m;
        let b2 = b / m;
        let c2 = c / m;

        let d = a2 * self.x + b2 * self.y + c2;

        self.x -= 2.0 * a2 * d;
        self.y -= 2.0 * b2 * d;
    }

    fn transform_by_points(
        &mut self,
        p1_expected: Point2,
        p1_actual: Point2,
        p2_expected: Point2,
        p2_actual: Point2,
    ) {
        let lateral_offset = p1_actual - p1_expected;

        let actual_size = p2_actual - p1_actual;
        let actual_angle = f32::atan2(actual_size.y, actual_size.x);

        let expected_size = p2_expected - p1_expected;
        let expected_angle = f32::atan2(expected_size.y, expected_size.x);

        let angular_offset = expected_angle - actual_angle;

        *self += lateral_offset;
    }
}

impl std::ops::Neg for Point2 {
    type Output = Point2;

    fn neg(self) -> Self::Output {
        Point2 {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl std::ops::Sub for Point2 {
    type Output = Vector2;

    fn sub(self, rhs: Self) -> Self::Output {
        Vector2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl std::ops::Sub<Vector2> for Point2 {
    type Output = Point2;

    fn sub(self, rhs: Vector2) -> Self::Output {
        Point2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl std::ops::Add<Vector2> for Point2 {
    type Output = Point2;

    fn add(self, rhs: Vector2) -> Self::Output {
        Point2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl std::ops::AddAssign<Vector2> for Point2 {
    fn add_assign(&mut self, rhs: Vector2) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl std::ops::SubAssign<Vector2> for Point2 {
    fn sub_assign(&mut self, rhs: Vector2) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

impl std::ops::Mul<f32> for Point2 {
    type Output = Point2;

    fn mul(self, rhs: f32) -> Self::Output {
        Point2 {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

impl std::ops::Mul<f64> for Point2 {
    type Output = Point2;

    fn mul(self, rhs: f64) -> Self::Output {
        Point2 {
            x: self.x * rhs as f32,
            y: self.y * rhs as f32,
        }
    }
}

impl std::ops::MulAssign<f32> for Point2 {
    fn mul_assign(&mut self, rhs: f32) {
        *self = *self * rhs;
    }
}

impl std::ops::MulAssign<f64> for Point2 {
    fn mul_assign(&mut self, rhs: f64) {
        *self = *self * rhs as f32;
    }
}

impl std::ops::Div<f32> for Point2 {
    type Output = Point2;

    fn div(self, rhs: f32) -> Self::Output {
        Point2 {
            x: self.x / rhs,
            y: self.y / rhs,
        }
    }
}

impl std::ops::DivAssign<f32> for Point2 {
    fn div_assign(&mut self, rhs: f32) {
        *self = *self / rhs;
    }
}

impl std::ops::Div<f64> for Point2 {
    type Output = Point2;

    fn div(self, rhs: f64) -> Self::Output {
        Point2 {
            x: self.x / rhs as f32,
            y: self.y / rhs as f32,
        }
    }
}

impl std::ops::DivAssign<f64> for Point2 {
    fn div_assign(&mut self, rhs: f64) {
        *self = *self / rhs;
    }
}

impl From<Point2> for egui::Pos2 {
    fn from(value: Point2) -> Self {
        egui::Pos2::new(value.x, value.y)
    }
}

impl From<egui::Pos2> for Point2 {
    fn from(value: Pos2) -> Self {
        Point2::new(value.x, value.y)
    }
}

impl From<Point2> for na::Point2<f32> {
    fn from(value: Point2) -> Self {
        na::Point2::new(value.x, value.y)
    }
}

impl From<na::Point2<f32>> for Point2 {
    fn from(value: na::Point2<f32>) -> Self {
        Point2::new(value.x, value.y)
    }
}

impl From<usvg::tiny_skia_path::Point> for Point2 {
    fn from(value: usvg::tiny_skia_path::Point) -> Self {
        Point2::new(value.x, value.y)
    }
}

impl From<Point2> for usvg::tiny_skia_path::Point {
    fn from(value: Point2) -> Self {
        usvg::tiny_skia_path::Point::from_xy(value.x, value.y)
    }
}

impl From<lyon::geom::Point<f32>> for Point2 {
    fn from(value: lyon::geom::Point<f32>) -> Self {
        Point2::new(value.x, value.y)
    }
}

impl From<Point2> for lyon::geom::Point<f32> {
    fn from(value: Point2) -> Self {
        lyon::geom::Point::new(value.x, value.y)
    }
}

impl From<Point2> for boostvoronoi::Point<i64> {
    fn from(value: Point2) -> Self {
        boostvoronoi::Point {
            x: (value.x * RESOLUTION) as i64,
            y: (value.y * RESOLUTION) as i64,
        }
    }
}

impl From<&Point2> for boostvoronoi::Point<i64> {
    fn from(value: &Point2) -> Self {
        boostvoronoi::Point {
            x: (value.x * RESOLUTION) as i64,
            y: (value.y * RESOLUTION) as i64,
        }
    }
}

impl From<boostvoronoi::Point<i64>> for Point2 {
    fn from(value: boostvoronoi::Point<i64>) -> Self {
        Point2 {
            x: value.x as f32 / RESOLUTION,
            y: value.y as f32 / RESOLUTION,
        }
    }
}

impl From<&boostvoronoi::Point<i64>> for Point2 {
    fn from(value: &boostvoronoi::Point<i64>) -> Self {
        Point2 {
            x: value.x as f32 / RESOLUTION,
            y: value.y as f32 / RESOLUTION,
        }
    }
}

impl From<boostvoronoi::Vertex<f32>> for Point2 {
    fn from(value: boostvoronoi::Vertex<f32>) -> Self {
        Point2 {
            x: value.x() / RESOLUTION,
            y: value.y() / RESOLUTION,
        }
    }
}

impl Display for Point2 {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "({:.3}, {:.3})", self.x, self.y)
    }
}

#[cfg(test)]
mod point_tests {
    use crate::test::*;

    use crate::cam::geometry2d::point::Point2;
    use crate::cam::geometry2d::Geometry;

    #[test]
    fn transform_by_points_lateral_only() {
        let mut point_to_transform = Point2 { x: 1.0, y: 1.0 };

        let p1_expected = Point2 { x: 2.0, y: 3.0 };
        let p1_actual = Point2 { x: 2.4, y: 3.2 };

        let p2_expected = Point2 { x: 4.0, y: 5.0 };
        let p2_actual = Point2 { x: 4.4, y: 5.2 };

        point_to_transform.transform_by_points(p1_expected, p1_actual, p2_expected, p2_actual);

        assert_close(point_to_transform.x, 1.4);
        assert_close(point_to_transform.y, 1.2);
    }
}
