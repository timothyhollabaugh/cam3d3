use crate::cam::geometry2d::{LineString, MultiLineString, MultiPolygon, Point2, Polygon};
use eframe::emath::Vec2;
use nalgebra as na;
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

#[derive(Debug, Copy, Clone, PartialEq, Default, Serialize, Deserialize)]
pub struct Vector2 {
    pub x: f32,
    pub y: f32,
}

impl Vector2 {
    pub fn zero() -> Vector2 {
        Vector2 { x: 0.0, y: 0.0 }
    }
    pub fn new(x: f32, y: f32) -> Vector2 {
        Vector2 { x, y }
    }
    pub fn unit_x() -> Vector2 {
        Vector2 { x: 1.0, y: 0.0 }
    }
    pub fn unit_y() -> Vector2 {
        Vector2 { x: 0.0, y: 1.0 }
    }
    pub fn from_x(x: f32) -> Vector2 {
        Vector2 { x, y: 0.0 }
    }
    pub fn from_y(y: f32) -> Vector2 {
        Vector2 { x: 0.0, y }
    }
    pub fn unit_x_axis_angle(a: f32) -> Vector2 {
        Vector2 {
            x: f32::cos(a),
            y: f32::sin(a),
        }
    }
    pub fn from_x_axis_angle(a: f32, m: f32) -> Vector2 {
        Vector2 {
            x: m * f32::cos(a),
            y: m * f32::sin(a),
        }
    }
    pub fn magnitude_squared(&self) -> f32 {
        self.x * self.x + self.y * self.y
    }
    pub fn magnitude(&self) -> f32 {
        f32::sqrt(self.magnitude_squared())
    }
    pub fn normalized(&self) -> Vector2 {
        let magnitude = self.magnitude();
        if magnitude > 0.0 {
            Vector2 {
                x: self.x / magnitude,
                y: self.y / magnitude,
            }
        } else {
            Vector2::zero()
        }
    }
    pub fn dot(&self, other: Vector2) -> f32 {
        self.x * other.x + self.y * other.y
    }
    pub fn x_axis_angle(&self) -> f32 {
        f32::atan2(self.y, self.x)
    }

    pub fn as_point(&self) -> Point2 {
        *self + Point2::zero()
    }

    pub fn project_onto(&self, other: Vector2) -> Vector2 {
        other * self.dot(other) / other.magnitude_squared()
    }
}
impl Neg for Vector2 {
    type Output = Vector2;

    fn neg(self) -> Self::Output {
        Vector2 {
            x: -self.x,
            y: -self.y,
        }
    }
}

impl Add<Vector2> for Vector2 {
    type Output = Vector2;

    fn add(self, rhs: Vector2) -> Self::Output {
        Vector2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl AddAssign<Vector2> for Vector2 {
    fn add_assign(&mut self, rhs: Vector2) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl Sub<Vector2> for Vector2 {
    type Output = Vector2;

    fn sub(self, rhs: Vector2) -> Self::Output {
        Vector2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl SubAssign<Vector2> for Vector2 {
    fn sub_assign(&mut self, rhs: Vector2) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

impl Mul<f32> for Vector2 {
    type Output = Vector2;

    fn mul(self, rhs: f32) -> Self::Output {
        Vector2 {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

impl MulAssign<f32> for Vector2 {
    fn mul_assign(&mut self, rhs: f32) {
        self.x *= rhs;
        self.y *= rhs;
    }
}

impl Mul<Vector2> for f32 {
    type Output = Vector2;

    fn mul(self, rhs: Vector2) -> Self::Output {
        Vector2 {
            x: rhs.x * self,
            y: rhs.y * self,
        }
    }
}

impl Div<f32> for Vector2 {
    type Output = Vector2;

    fn div(self, rhs: f32) -> Self::Output {
        Vector2 {
            x: self.x / rhs,
            y: self.y / rhs,
        }
    }
}

impl DivAssign<f32> for Vector2 {
    fn div_assign(&mut self, rhs: f32) {
        self.x /= rhs;
        self.y /= rhs;
    }
}

impl Add<Point2> for Vector2 {
    type Output = Point2;

    fn add(self, rhs: Point2) -> Self::Output {
        Point2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl Add<LineString> for Vector2 {
    type Output = LineString;
    fn add(self, rhs: LineString) -> Self::Output {
        LineString {
            points: rhs.points.into_iter().map(|point| point + self).collect(),
        }
    }
}

impl Sub<LineString> for Vector2 {
    type Output = LineString;
    fn sub(self, rhs: LineString) -> Self::Output {
        LineString {
            points: rhs.points.into_iter().map(|point| point - self).collect(),
        }
    }
}

impl Add<MultiLineString> for Vector2 {
    type Output = MultiLineString;

    fn add(self, rhs: MultiLineString) -> Self::Output {
        MultiLineString {
            linestrings: rhs
                .linestrings
                .into_iter()
                .map(|linestring| linestring + self)
                .collect(),
        }
    }
}

impl Sub<MultiLineString> for Vector2 {
    type Output = MultiLineString;

    fn sub(self, rhs: MultiLineString) -> Self::Output {
        MultiLineString {
            linestrings: rhs
                .linestrings
                .into_iter()
                .map(|linestring| linestring - self)
                .collect(),
        }
    }
}

impl Add<Polygon> for Vector2 {
    type Output = Polygon;

    fn add(self, rhs: Polygon) -> Self::Output {
        Polygon {
            exterior: rhs.exterior + self,
            interiors: rhs
                .interiors
                .into_iter()
                .map(|linestring| linestring + self)
                .collect(),
        }
    }
}

impl Sub<Polygon> for Vector2 {
    type Output = Polygon;

    fn sub(self, rhs: Polygon) -> Self::Output {
        Polygon {
            exterior: rhs.exterior - self,
            interiors: rhs
                .interiors
                .into_iter()
                .map(|linestring| linestring - self)
                .collect(),
        }
    }
}

impl Add<MultiPolygon> for Vector2 {
    type Output = MultiPolygon;

    fn add(self, rhs: MultiPolygon) -> Self::Output {
        MultiPolygon {
            polygons: rhs
                .polygons
                .into_iter()
                .map(|polygon| polygon + self)
                .collect(),
        }
    }
}

impl Sub<MultiPolygon> for Vector2 {
    type Output = MultiPolygon;

    fn sub(self, rhs: MultiPolygon) -> Self::Output {
        MultiPolygon {
            polygons: rhs
                .polygons
                .into_iter()
                .map(|polygon| polygon - self)
                .collect(),
        }
    }
}

impl From<Vector2> for Vec2 {
    fn from(value: Vector2) -> Self {
        egui::Vec2::new(value.x, value.y)
    }
}

impl From<Vec2> for Vector2 {
    fn from(value: Vec2) -> Self {
        Vector2::new(value.x, value.y)
    }
}

impl From<Vector2> for na::Vector2<f32> {
    fn from(value: Vector2) -> Self {
        na::Vector2::new(value.x, value.y)
    }
}

impl From<na::Vector2<f32>> for Vector2 {
    fn from(value: na::Vector2<f32>) -> Self {
        Vector2::new(value.x, value.y)
    }
}

impl Display for Vector2 {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<{:.3}, {:.3}>", self.x, self.y)
    }
}
