use crate::cam::geometry2d::{LineString, Point2, Polygon, Vector2};
use boostvoronoi::SourceCategory;
use itertools::Itertools;
use log::{debug, trace};
use petgraph::prelude::StableGraph;
use petgraph::Undirected;
use std::collections::HashMap;
use std::f32::consts::PI;

#[derive(Copy, Clone, Debug)]
pub struct MedialAxisNode {
    pub center: Point2,
    pub radius: f32,
}

impl MedialAxisNode {
    pub fn non_overlapping_arc(
        &self,
        other: MedialAxisNode,
        segments_per_circle: f32,
        tool_diameter: f32,
    ) -> LineString {
        //trace!("non-overlapping");
        //https://www.desmos.com/calculator/byau36lh72

        let delta = other.center - self.center;
        let distance = delta.magnitude();

        let radius = self.radius - tool_diameter / 2.0;
        let other_radius = other.radius - tool_diameter / 2.0;

        assert!(radius > 0.0);
        assert!(other_radius > 0.0);

        let radical_line_distance = (radius * radius - other_radius * other_radius
            + distance * distance)
            / (2.0 * distance);

        let delta_angle = PI - f32::acos(f32::clamp(radical_line_distance / radius, -1.0, 1.0));
        let center_angle = (-delta).x_axis_angle();

        let angle_step = 2.0 * PI / segments_per_circle;
        let steps = (delta_angle / angle_step) as i32;

        trace!("generating arc. node {:0.3?} other {:0.3?} delta {:0.3?} distance {:0.3?} radius {:0.3?} other_radius {:0.3?} radical_line {:0.3?} delta_angle {:0.3?} center_angle {:0.3?}",
            self,
            other,
            delta,
            distance,
            radius,
            other_radius,
            radical_line_distance,
            delta_angle,
            center_angle
        );

        /*
        dbg!(
            self,
            other,
            radius,
            self.radius
            delta,
            distance,
            radical_line_distance,
            delta_angle,
            center_angle,
            angle_step,
            steps
        );
         */

        LineString {
            points: std::iter::once(center_angle - delta_angle)
                .chain((-steps..=steps).map(|step| step as f32 * angle_step + center_angle))
                .chain(std::iter::once(center_angle + delta_angle))
                .map(|angle| self.center + Vector2::from_x_axis_angle(angle, radius))
                .inspect(|point| {
                    assert!(!point.x.is_nan());
                    assert!(!point.y.is_nan());
                })
                .collect(),
        }
    }
}

#[derive(Copy, Clone, Debug)]
pub enum MedialAxisEdgeSources {
    SegmentSegment {
        segment1_start: Point2,
        segment1_end: Point2,
        segment2_start: Point2,
        segment2_end: Point2,
    },
    PointPoint {
        point1: Point2,
        point2: Point2,
    },
    PointSegment(MedialAxisEdgeSourcePointSegment),
    //Fallback {
    //point1: Point2,
    //radius1: f32,
    //point2: Point2,
    //radius2: f32
    //}
}

#[derive(Copy, Clone, Debug)]
pub struct MedialAxisEdgeSourcePointSegment {
    segment_start: Point2,
    segment_end: Point2,
    point: Point2,
}

// https://www.desmos.com/calculator/1apxgooxtx
impl MedialAxisEdgeSourcePointSegment {
    pub fn new(
        point: Point2,
        segment_start: Point2,
        segment_end: Point2,
        curve_start: Point2,
        curve_end: Point2,
    ) -> MedialAxisEdgeSourcePointSegment {
        let new_segment_start = curve_start.project_onto_edge(segment_start, segment_end);
        let new_segment_end = curve_end.project_onto_edge(segment_start, segment_end);

        trace!(
            "p {point} segment {segment_start} - {segment_end} new_segment {new_segment_start} - {new_segment_end} curve {curve_start} - {curve_end}"
        );

        //assert_ne!(segment_start, segment_end);
        //assert_ne!(new_segment_start, new_segment_end);
        //assert_ne!(point.distance_from_edge(segment_start, segment_end), 0.0);

        MedialAxisEdgeSourcePointSegment {
            segment_start: new_segment_start,
            segment_end: new_segment_end,
            point,
        }
    }

    pub fn reversed(&self) -> MedialAxisEdgeSourcePointSegment {
        MedialAxisEdgeSourcePointSegment {
            segment_start: self.segment_end,
            segment_end: self.segment_start,
            point: self.point,
        }
    }

    fn point_radius_at_t(&self, t: f32) -> Option<(Point2, f32)> {
        let segment_v = self.segment_end - self.segment_start;
        if segment_v != Vector2::zero() {
            let segment_q = self.segment_start + t * segment_v;

            let v = Vector2 {
                x: (self.segment_start.y - self.segment_end.y) / segment_v.magnitude(),
                y: (self.segment_end.x - self.segment_start.x) / segment_v.magnitude(),
            };

            let d = (self.point - segment_q).magnitude_squared()
                / (2.0 * (v.x * (self.point.x - segment_q.x) + v.y * (self.point.y - segment_q.y)));

            let q = segment_q + d * v;

            let r = f32::abs(d);

            Some((q, r))
        } else {
            None
        }
    }

    pub fn distance_at_t(&self, t: f32) -> Option<f32> {
        let segment_delta = self.segment_end - self.segment_start;
        if segment_delta != Vector2::zero() {
            let segment_length = segment_delta.magnitude();

            let point_delta = self.point - self.segment_start;
            let point_length = point_delta.magnitude();

            let point_along_edge = (segment_delta.x * point_delta.x
                + segment_delta.y * point_delta.y)
                / segment_length;
            let point_perp_edge = (segment_delta.x * point_delta.y
                - segment_delta.y * point_delta.x)
                / segment_length;

            let t_along_edge = segment_length * t;
            let t_from_point_along_edge = t_along_edge - point_along_edge;
            let t_point_length = f32::sqrt(
                t_from_point_along_edge * t_from_point_along_edge
                    + point_perp_edge * point_perp_edge,
            );

            let c = point_along_edge * point_length / (2.0 * f32::abs(point_perp_edge))
                - (f32::abs(point_perp_edge) / 2.0)
                    * f32::ln(f32::abs(
                        (point_length - point_along_edge) / point_perp_edge,
                    ));

            let distance = t_from_point_along_edge * t_point_length
                / (2.0 * f32::abs(point_perp_edge))
                + (f32::abs(point_perp_edge) / 2.0)
                    * f32::ln(f32::abs(
                        (t_from_point_along_edge + t_point_length) / point_perp_edge,
                    ))
                + c;

            assert!(distance.is_finite());

            Some(distance)
        } else {
            None
        }
    }

    pub fn total_distance(&self) -> f32 {
        let d = self.distance_at_t(1.0).unwrap_or(0.0);
        assert!(d.is_finite());
        d
    }

    pub fn t_at_distance(&self, distance: f32) -> Option<f32> {
        if distance == 0.0 {
            Some(0.0)
        } else {
            let mut t = 0.5;
            let mut t_step = 0.25;

            for i in (0..16) {
                let new_distance = self.distance_at_t(t)?;

                if new_distance == distance {
                    break;
                } else if new_distance > distance {
                    t -= t_step;
                } else {
                    t += t_step;
                }

                t_step /= 2.0;
            }

            Some(t)
        }
    }
}

#[cfg(test)]
mod edge_point_source_tests {
    use crate::cam::geometry2d::{MedialAxisEdgeSourcePointSegment, Point2};
    use crate::test::assert_close;

    #[test]
    fn equidistant_from_edge_and_point() {
        let source = MedialAxisEdgeSourcePointSegment {
            segment_start: Point2::new(1.0, 2.0),
            segment_end: Point2::new(2.0, 1.0),
            point: Point2::new(1.0, 1.0),
        };

        for i in 0..=100 {
            let t = i as f32 / 100.0;
            let (q, r) = source.point_radius_at_t(t).unwrap();
            let d1 = (source.point - q).magnitude();
            let d2 = q.distance_from_edge(source.segment_start, source.segment_end);

            assert_close(d1, d2);
            assert_close(r, d2);
            assert_close(r, d1);
        }
    }

    #[test]
    fn equidistant_from_edge_and_point2() {
        let source = MedialAxisEdgeSourcePointSegment {
            segment_start: Point2::new(1.0, 2.0),
            segment_end: Point2::new(2.0, 1.0),
            point: Point2::new(2.0, 2.0),
        };

        for i in 0..=100 {
            let t = i as f32 / 100.0;
            let (q, r) = source.point_radius_at_t(t).unwrap();
            let d1 = (source.point - q).magnitude();
            let d2 = q.distance_from_edge(source.segment_start, source.segment_end);

            assert_close(d1, d2);
            assert_close(r, d2);
            assert_close(r, d1);
        }
    }

    #[test]
    fn distance_at_t() {
        let source = MedialAxisEdgeSourcePointSegment {
            segment_start: Point2::new(1.0, 2.0),
            segment_end: Point2::new(2.0, 1.0),
            point: Point2::new(1.0, 1.0),
        };

        assert_close(source.distance_at_t(0.0).unwrap(), 0.0);
        assert_close(source.distance_at_t(0.1).unwrap(), 0.19035987);
        assert_close(source.distance_at_t(0.2).unwrap(), 0.36311631);
        assert_close(source.distance_at_t(0.3).unwrap(), 0.52139885);
        assert_close(source.distance_at_t(0.4).unwrap(), 0.66925403);
        assert_close(source.distance_at_t(0.5).unwrap(), 0.81161262);
        assert_close(source.distance_at_t(0.6).unwrap(), 0.95397121);
        assert_close(source.distance_at_t(0.7).unwrap(), 1.1018264);
        assert_close(source.distance_at_t(0.8).unwrap(), 1.2601089);
        assert_close(source.distance_at_t(0.9).unwrap(), 1.4328654);
        assert_close(source.distance_at_t(1.0).unwrap(), 1.6232252);
    }

    #[test]
    fn distance_at_t2() {
        let source = MedialAxisEdgeSourcePointSegment {
            segment_start: Point2::new(1.0, 2.0),
            segment_end: Point2::new(2.0, 1.0),
            point: Point2::new(2.0, 2.0),
        };

        assert_close(source.distance_at_t(0.0).unwrap(), 0.0);
        assert_close(source.distance_at_t(0.1).unwrap(), 0.19035987);
        assert_close(source.distance_at_t(0.2).unwrap(), 0.36311631);
        assert_close(source.distance_at_t(0.3).unwrap(), 0.52139885);
        assert_close(source.distance_at_t(0.4).unwrap(), 0.66925403);
        assert_close(source.distance_at_t(0.5).unwrap(), 0.81161262);
        assert_close(source.distance_at_t(0.6).unwrap(), 0.95397121);
        assert_close(source.distance_at_t(0.7).unwrap(), 1.1018264);
        assert_close(source.distance_at_t(0.8).unwrap(), 1.2601089);
        assert_close(source.distance_at_t(0.9).unwrap(), 1.4328654);
        assert_close(source.distance_at_t(1.0).unwrap(), 1.6232252);
    }

    #[test]
    fn t_at_distance() {
        let source = MedialAxisEdgeSourcePointSegment {
            segment_start: Point2::new(1.0, 2.0),
            segment_end: Point2::new(2.0, 1.0),
            point: Point2::new(1.0, 1.0),
        };

        assert_close(source.t_at_distance(0.0).unwrap(), 0.0);
        assert_close(source.t_at_distance(0.19035987).unwrap(), 0.1);
        assert_close(source.t_at_distance(0.36311631).unwrap(), 0.2);
        assert_close(source.t_at_distance(0.52139885).unwrap(), 0.3);
        assert_close(source.t_at_distance(0.66925403).unwrap(), 0.4);
        assert_close(source.t_at_distance(0.81161262).unwrap(), 0.5);
        assert_close(source.t_at_distance(0.95397121).unwrap(), 0.6);
        assert_close(source.t_at_distance(1.1018264).unwrap(), 0.7);
        assert_close(source.t_at_distance(1.2601089).unwrap(), 0.8);
        assert_close(source.t_at_distance(1.4328654).unwrap(), 0.9);
        assert_close(source.t_at_distance(1.6232252).unwrap(), 1.0);
    }

    #[test]
    fn t_at_distance2() {
        let source = MedialAxisEdgeSourcePointSegment {
            segment_start: Point2::new(1.0, 2.0),
            segment_end: Point2::new(2.0, 1.0),
            point: Point2::new(2.0, 2.0),
        };

        assert_close(source.t_at_distance(0.0).unwrap(), 0.0);
        assert_close(source.t_at_distance(0.19035987).unwrap(), 0.1);
        assert_close(source.t_at_distance(0.36311631).unwrap(), 0.2);
        assert_close(source.t_at_distance(0.52139885).unwrap(), 0.3);
        assert_close(source.t_at_distance(0.66925403).unwrap(), 0.4);
        assert_close(source.t_at_distance(0.81161262).unwrap(), 0.5);
        assert_close(source.t_at_distance(0.95397121).unwrap(), 0.6);
        assert_close(source.t_at_distance(1.1018264).unwrap(), 0.7);
        assert_close(source.t_at_distance(1.2601089).unwrap(), 0.8);
        assert_close(source.t_at_distance(1.4328654).unwrap(), 0.9);
        assert_close(source.t_at_distance(1.6232252).unwrap(), 1.0);
    }
}

#[derive(Copy, Clone, Debug)]
pub struct MedialAxisEdge {
    pub start: Point2,
    pub end: Point2,
    pub sources: MedialAxisEdgeSources,
}

impl MedialAxisEdge {
    pub fn point_radius_at_distance(&self, distance: f32, reverse: bool) -> (Point2, f32) {
        match self.sources {
            MedialAxisEdgeSources::SegmentSegment {
                segment1_start: edge1_start,
                segment1_end: edge1_end,
                segment2_start: edge2_start,
                segment2_end: edge2_end,
            } => {
                let q = if reverse {
                    let v = self.start - self.end;
                    self.end + distance * v.normalized()
                } else {
                    let v = self.end - self.start;
                    self.start + distance * v.normalized()
                };

                let d1 = q.distance_from_edge(edge1_start, edge1_end);
                let d2 = q.distance_from_edge(edge2_start, edge2_end);
                let d = f32::min(d1, d2);

                assert!(q.x.is_finite());
                assert!(q.y.is_finite());
                assert!(d.is_finite());

                (q, d)
            }
            MedialAxisEdgeSources::PointPoint { point1, point2 } => {
                let q = if reverse {
                    let v = self.start - self.end;
                    self.end + distance * v.normalized()
                } else {
                    let v = self.end - self.start;
                    self.start + distance * v.normalized()
                };

                let d1 = (q - point1).magnitude();
                let d2 = (q - point2).magnitude();
                let d = f32::min(d1, d2);

                assert!(q.x.is_finite());
                assert!(q.y.is_finite());
                assert!(d.is_finite());

                (q, d)
            }
            MedialAxisEdgeSources::PointSegment(source) => {
                let source = if reverse { source.reversed() } else { source };

                if let Some((q, d)) = source.t_at_distance(distance).and_then(|t| {
                    trace!("t {t}");
                    source.point_radius_at_t(t)
                }) {
                    assert!(q.x.is_finite());
                    assert!(q.y.is_finite());
                    assert!(d.is_finite());

                    trace!("q {q} d {d}");

                    (q, d)
                } else {
                    let q = if reverse {
                        let v = self.start - self.end;
                        self.end + distance * v.normalized()
                    } else {
                        let v = self.end - self.start;
                        self.start + distance * v.normalized()
                    };

                    let d1 = (q - source.point).magnitude();
                    let d2 = q.distance_from_edge(source.segment_start, source.segment_end);
                    let d = f32::min(d1, d2);

                    assert!(q.x.is_finite());
                    assert!(q.y.is_finite());
                    assert!(d.is_finite());

                    (q, d)
                }
            }
        }
    }

    pub fn total_distance(&self) -> f32 {
        match self.sources {
            MedialAxisEdgeSources::SegmentSegment { .. } => (self.start - self.end).magnitude(),
            MedialAxisEdgeSources::PointPoint { .. } => (self.start - self.end).magnitude(),
            MedialAxisEdgeSources::PointSegment(source) => source.total_distance(),
        }
    }

    pub fn linear_distance(&self) -> f32 {
        (self.start - self.end).magnitude()
    }

    pub fn with_new_start(&self, new_start: Point2) -> MedialAxisEdge {
        let sources = if let MedialAxisEdgeSources::PointSegment(source) = self.sources {
            MedialAxisEdgeSources::PointSegment(MedialAxisEdgeSourcePointSegment::new(
                source.point,
                source.segment_start,
                source.segment_end,
                new_start,
                self.end,
            ))
        } else {
            self.sources
        };

        MedialAxisEdge {
            start: new_start,
            end: self.end,
            sources,
        }
    }

    pub fn with_new_end(&self, new_end: Point2) -> MedialAxisEdge {
        let sources = if let MedialAxisEdgeSources::PointSegment(source) = self.sources {
            MedialAxisEdgeSources::PointSegment(MedialAxisEdgeSourcePointSegment::new(
                source.point,
                source.segment_start,
                source.segment_end,
                self.start,
                self.end,
            ))
        } else {
            self.sources
        };

        MedialAxisEdge {
            start: self.start,
            end: new_end,
            sources,
        }
    }
}

#[derive(Clone, Debug)]
pub struct MedialAxis {
    pub graph: StableGraph<MedialAxisNode, MedialAxisEdge, Undirected>,
}

impl MedialAxis {
    pub fn from_polygon(polygon: &Polygon) -> MedialAxis {
        let segments: Vec<(&Point2, &Point2)> = std::iter::once(&polygon.exterior)
            .chain(&polygon.interiors)
            .flat_map(|p| {
                p.points
                    .iter()
                    .circular_tuple_windows()
                    .map(|(p1, p2)| (p1, p2))
                //.filter(|(p1, p2)| p1.x != p2.x && p1.x != p2.x)
            })
            .collect();

        let diagram = boostvoronoi::Builder::default()
            .with_segments(
                segments
                    .clone()
                    .into_iter()
                    .map(|(p1, p2)| boostvoronoi::Line {
                        start: p1.into(),
                        end: p2.into(),
                    }),
            )
            .unwrap()
            .build()
            .unwrap();

        let mut graph = StableGraph::default();

        let mut inserted_nodes = HashMap::new();

        for vertex in diagram.vertices().into_iter() {
            let vertex = vertex.get();

            let point = vertex.into();

            if polygon.point_inside(point).in_or_on() {
                let edge_index = vertex.get_incident_edge().unwrap();
                let cell_index = diagram.edge_get_cell(edge_index).unwrap();
                let cell = diagram.get_cell(cell_index).unwrap().get();
                let (source_index, source_category) = cell.source_index_2();

                let radius = match source_category {
                    SourceCategory::SinglePoint => {
                        panic!("Cell with a point source but there are no points")
                    }
                    SourceCategory::SegmentStart => {
                        let source_edge = segments[source_index];
                        let source_point = *source_edge.0;
                        (point - source_point).magnitude()
                    }
                    SourceCategory::SegmentEnd => {
                        let source_edge = segments[source_index];
                        let source_point = *source_edge.1;
                        (point - source_point).magnitude()
                    }
                    SourceCategory::Segment => {
                        let source_edge = segments[source_index];
                        point.distance_from_edge(*source_edge.0, *source_edge.1)
                    }
                };

                let node = MedialAxisNode {
                    center: point,
                    radius,
                };

                debug!(
                    "vertex c {} r {:0.3} {:?}",
                    node.center, node.radius, vertex
                );

                let node_index = graph.add_node(node);
                inserted_nodes.insert(vertex.get_id(), node_index);
            }
        }

        for edge in diagram.edges().into_iter() {
            let edge = edge.get();
            let vertex_index0 = diagram.edge_get_vertex0(edge.id()).unwrap();
            let vertex_index1 = diagram.edge_get_vertex1(edge.id()).unwrap();

            let cell1_index = diagram.edge_get_cell(edge.id()).unwrap();
            let cell1 = diagram.get_cell(cell1_index).unwrap().get();
            let (cell1_source_index, cell1_source_category) = cell1.source_index_2();

            let twin_edge_index = edge.twin().unwrap();
            let cell2_index = diagram.edge_get_cell(twin_edge_index).unwrap();
            let cell2 = diagram.get_cell(cell2_index).unwrap().get();
            let (cell2_source_index, cell2_source_category) = cell2.source_index_2();

            debug!(
                "edge {:?} is_linear {} is_curved {} is_primary {} is_secondary {} source1 {:?} source2 {:?}",
                edge,
                edge.is_linear(),
                edge.is_curved(),
                edge.is_primary(),
                edge.is_secondary(),
                cell1_source_category,
                cell2_source_category
            );

            if let (Some(vertex_index0), Some(vertex_index1)) = (vertex_index0, vertex_index1) {
                let node_index0 = inserted_nodes.get(&vertex_index0);
                let node_index1 = inserted_nodes.get(&vertex_index1);
                if let (Some(node_index0), Some(node_index1)) = (node_index0, node_index1) {
                    if !graph.contains_edge(*node_index1, *node_index0)
                        && !graph.contains_edge(*node_index0, *node_index1)
                    {
                        let point0: Point2 =
                            diagram.vertex_get(vertex_index0).unwrap().get().into();
                        let point1: Point2 =
                            diagram.vertex_get(vertex_index1).unwrap().get().into();

                        if edge.is_primary() {
                            let sources = match (cell1_source_category, cell2_source_category) {
                                (SourceCategory::Segment, SourceCategory::Segment) => {
                                    let (&segment1_start, &segment1_end) =
                                        segments[cell1_source_index];
                                    let (&segment2_start, &segment2_end) =
                                        segments[cell2_source_index];
                                    MedialAxisEdgeSources::SegmentSegment {
                                        segment1_start,
                                        segment1_end,
                                        segment2_start,
                                        segment2_end,
                                    }
                                }
                                (SourceCategory::Segment, SourceCategory::SegmentStart) => {
                                    let (&segment_start, &segment_end) =
                                        segments[cell1_source_index];
                                    let (&point, _) = segments[cell2_source_index];
                                    assert_ne!(segment_start, segment_end);
                                    //assert_ne!(segment_start, point);
                                    //assert_ne!(point, segment_end);
                                    MedialAxisEdgeSources::PointSegment(
                                        MedialAxisEdgeSourcePointSegment::new(
                                            point,
                                            segment_start,
                                            segment_end,
                                            point0,
                                            point1,
                                        ),
                                    )
                                }
                                (SourceCategory::Segment, SourceCategory::SegmentEnd) => {
                                    let (&segment_start, &segment_end) =
                                        segments[cell1_source_index];
                                    let (_, &point) = segments[cell2_source_index];
                                    assert_ne!(segment_start, segment_end);
                                    //assert_ne!(segment_start, point);
                                    //assert_ne!(point, segment_end);
                                    MedialAxisEdgeSources::PointSegment(
                                        MedialAxisEdgeSourcePointSegment::new(
                                            point,
                                            segment_start,
                                            segment_end,
                                            point0,
                                            point1,
                                        ),
                                    )
                                }
                                (SourceCategory::SegmentStart, SourceCategory::Segment) => {
                                    let (&point, _) = segments[cell1_source_index];
                                    let (&segment_start, &segment_end) =
                                        segments[cell2_source_index];
                                    assert_ne!(segment_start, segment_end);
                                    //assert_ne!(segment_start, point);
                                    //assert_ne!(point, segment_end);
                                    MedialAxisEdgeSources::PointSegment(
                                        MedialAxisEdgeSourcePointSegment::new(
                                            point,
                                            segment_start,
                                            segment_end,
                                            point0,
                                            point1,
                                        ),
                                    )
                                }
                                (SourceCategory::SegmentEnd, SourceCategory::Segment) => {
                                    let (_, &point) = segments[cell1_source_index];
                                    let (&segment_start, &segment_end) =
                                        segments[cell2_source_index];
                                    assert_ne!(segment_start, segment_end);
                                    //assert_ne!(segment_start, point);
                                    //assert_ne!(point, segment_end);
                                    MedialAxisEdgeSources::PointSegment(
                                        MedialAxisEdgeSourcePointSegment::new(
                                            point,
                                            segment_start,
                                            segment_end,
                                            point0,
                                            point1,
                                        ),
                                    )
                                }
                                (SourceCategory::SegmentStart, SourceCategory::SegmentStart) => {
                                    let (&point1, _) = segments[cell1_source_index];
                                    let (&point2, _) = segments[cell2_source_index];
                                    MedialAxisEdgeSources::PointPoint { point1, point2 }
                                }
                                (SourceCategory::SegmentStart, SourceCategory::SegmentEnd) => {
                                    let (&point1, _) = segments[cell1_source_index];
                                    let (_, &point2) = segments[cell2_source_index];
                                    MedialAxisEdgeSources::PointPoint { point1, point2 }
                                }
                                (SourceCategory::SegmentEnd, SourceCategory::SegmentStart) => {
                                    let (_, &point1) = segments[cell1_source_index];
                                    let (&point2, _) = segments[cell2_source_index];
                                    MedialAxisEdgeSources::PointPoint { point1, point2 }
                                }
                                (SourceCategory::SegmentEnd, SourceCategory::SegmentEnd) => {
                                    let (_, &point1) = segments[cell1_source_index];
                                    let (_, &point2) = segments[cell2_source_index];
                                    MedialAxisEdgeSources::PointPoint { point1, point2 }
                                }
                                (SourceCategory::SinglePoint, _) => {
                                    panic!("Cell with a point source but there are no points")
                                }
                                (_, SourceCategory::SinglePoint) => {
                                    panic!("Cell with a point source but there are no points")
                                }
                            };

                            let medial_axis_edge = MedialAxisEdge {
                                start: point0,
                                end: point1,
                                sources,
                            };

                            graph.add_edge(*node_index0, *node_index1, medial_axis_edge);
                        }
                    }
                }
            }
        }

        MedialAxis { graph }
    }

    pub fn filter_min_radius(&mut self, min_radius: f32) {
        debug!("filtering nodes");

        // Find all the edges where
        let nodes_to_add = self
            .graph
            .edge_indices()
            .filter_map(|edge_index| {
                let axis_edge = self.graph[edge_index];
                let (node0_index, node1_index) = self.graph.edge_endpoints(edge_index).unwrap();
                let node0 = self.graph[node0_index];
                let node1 = self.graph[node1_index];

                if node0.radius < min_radius && node1.radius > min_radius {
                    let t = (min_radius - node0.radius) / (node1.radius - node0.radius);
                    let center = t * (node1.center - node0.center) + node0.center;

                    let new_axis_edge = axis_edge.with_new_start(center);

                    let new_node = MedialAxisNode {
                        center,
                        radius: min_radius,
                    };

                    Some((new_node, node1_index, new_axis_edge))
                } else if node1.radius < min_radius && node0.radius > min_radius {
                    let t = (min_radius - node1.radius) / (node0.radius - node1.radius);
                    let center = t * (node0.center - node1.center) + node1.center;

                    let new_axis_edge = axis_edge.with_new_end(center);

                    let new_node = MedialAxisNode {
                        center,
                        radius: min_radius,
                    };

                    Some((new_node, node0_index, new_axis_edge))
                } else {
                    None
                }
            })
            .collect_vec();

        for (new_node, from_node_index, axis_edge) in nodes_to_add {
            let new_node_index = self.graph.add_node(new_node);
            self.graph
                .add_edge(from_node_index, new_node_index, axis_edge);
        }

        self.graph.retain_nodes(|graph, node_index| {
            graph.neighbors(node_index).count() > 0 && graph[node_index].radius >= min_radius
        });

        loop {
            let mut filtered = false;

            trace!("filter pass!");
            self.graph.retain_nodes(|graph, node_index| {
                let node = &graph[node_index];

                let retain =
                    if let Ok(Some(neighbor_index)) = graph.neighbors(node_index).at_most_one() {
                        let neighbor: &MedialAxisNode = &graph[neighbor_index];
                        let d = (node.center - neighbor.center).magnitude();
                        d + node.radius - neighbor.radius > 0.01
                    } else {
                        true
                    };

                if !retain {
                    filtered = true
                }

                retain
            });

            if !filtered {
                break;
            }
        }

        debug!("filtered nodes");
    }
}
