use std::borrow::Borrow;
use std::borrow::Cow;
use std::cell::Ref;
use std::fmt::{Display, Formatter};
use std::fs::File;
use std::io::Read;
use std::num::ParseFloatError;
use std::path::PathBuf;
use std::sync::Arc;

use egui::Ui;
use log::{debug, trace};
use serde::{Deserialize, Serialize};
use svg::node::element::path::Command;
use usvg::tiny_skia_path::{Path, PathSegment};
use usvg::{Node, Options, Stroke};

use crate::cam::geometry2d::{
    union, Geometry, LineString, MultiLineString, MultiPolygon, Point2, Polygon,
};
use crate::task::Tasks;
use crate::ui::widgets::{
    checkbox_field, dropdown, dropdown_field, number_field, DropdownOptions, PickFileField,
};

#[derive(thiserror::Error, Debug)]
pub enum LoadSvgError {
    #[error("Cannot open svg file: {source}")]
    File {
        #[from]
        source: std::io::Error,
    },

    #[error("Cannot parse svg file: {source}")]
    SvgParse {
        #[from]
        source: usvg::Error,
    },

    #[error("Missing attribute \"{attribute}\" in tag \"{tag}\"")]
    MissingAttribute { attribute: String, tag: String },

    #[error("Couldn't parse number: {source}")]
    NumberParse {
        #[from]
        source: ParseFloatError,
    },

    #[error("A unicode attribute was empty")]
    EmptyUnicode,

    #[error("Invalid path data: {source}")]
    InvalidPath {
        #[from]
        source: svg::parser::Error,
    },

    #[error("Missing path parameter")]
    MissingPathParameter { glyph: String, index: usize },

    #[error("Unimplemented path command: {command:?}")]
    UnimplementedPathCommand { command: Command },
}

pub fn load_svg_multilinestring(
    svg_path: PathBuf,
    dpi: f32,
) -> Result<MultiLineString, LoadSvgError> {
    let mut svg_file = File::open(&svg_path)?;
    let mut svg_bytes = String::new();
    svg_file.read_to_string(&mut svg_bytes);

    let options = &Options {
        resources_dir: Some(svg_path.clone()),
        dpi,
        ..Options::default()
    };

    let tree = usvg::Tree::from_str(&svg_bytes, options)?;

    let mut svg = SvgState {
        geometry: MultiLineString::new(),
    };

    debug!("size: {:?}", tree.size());

    svg.svg_node(
        Node::Group(Box::new(tree.root().clone())),
        usvg::Transform::identity(),
        1,
    );

    Ok(svg.geometry)
}

trait SvgGeometry {
    fn push(&mut self, linestring: Option<LineString>, stroke: Option<&Stroke>);
}

impl SvgGeometry for MultiLineString {
    fn push(&mut self, linestring: Option<LineString>, stroke: Option<&Stroke>) {
        if let Some(mut linestring) = linestring {
            let linestring = LineString {
                points: linestring
                    .points
                    .into_iter()
                    .map(|p| Point2 { x: p.x, y: -p.y })
                    .collect(),
            };

            self.linestrings.push(linestring);
        }
    }
}

impl SvgGeometry for MultiPolygon {
    fn push(&mut self, linestring: Option<LineString>, stroke: Option<&Stroke>) {
        if let Some(mut linestring) = linestring {
            let linestring = LineString {
                points: linestring
                    .points
                    .into_iter()
                    .map(|p| Point2 { x: p.x, y: -p.y })
                    .collect(),
            };

            let polygon = if let Some(stroke) = stroke {
                Some(linestring.offset(stroke.width().get() / 2.0))
            } else if linestring.is_closed() {
                Some(Polygon {
                    exterior: linestring,
                    interiors: Vec::new(),
                })
            } else {
                None
            };

            if let Some(polygon) = polygon {
                self.polygons.push(polygon);
            }
        }
    }
}

struct SvgState<G> {
    geometry: G,
}

impl<G: SvgGeometry> SvgState<G> {
    fn svg_node(&mut self, node: Node, mut transform: usvg::Transform, depth: usize) {
        let data = node.borrow();
        trace!("{:>depth$} id={}", depth, data.id(), depth = depth);

        transform = transform.post_concat(data.abs_transform());

        trace!(
            "{:>depth$} transform={:?} ({:?})",
            depth,
            &transform,
            data.abs_transform(),
            depth = depth
        );

        match &*data {
            Node::Group(group) => {
                trace!("{:>depth$} group={:?}", depth, group, depth = depth);
                for child in group.children() {
                    self.svg_node(child.clone(), transform, depth + 1);
                }
            }
            Node::Path(path) => {
                trace!("{:>depth$} path={:?}", depth, path, depth = depth);

                let data: Path = (*(path.data())).clone();

                let mut path_data = data.transform(transform).unwrap();

                trace!("{:>depth$} data={:?}", depth, path_data, depth = depth);

                let mut current_linestring = None;
                for segment in path_data.segments() {
                    match segment {
                        PathSegment::MoveTo(p) => {
                            self.geometry.push(current_linestring.take(), path.stroke());

                            current_linestring
                                .get_or_insert_default()
                                .points
                                .push(p.into());
                        }
                        PathSegment::LineTo(p) => {
                            current_linestring
                                .get_or_insert_default()
                                .points
                                .push(p.into());
                        }
                        PathSegment::QuadTo(p1, p2) => {
                            if let Some(linestring) = current_linestring.as_mut() {
                                if let Some(p0) = linestring.points.last().cloned() {
                                    let curve =
                                        lyon::geom::quadratic_bezier::QuadraticBezierSegment {
                                            from: p0.into(),
                                            ctrl: Point2::from(p1).into(),
                                            to: Point2::from(p2).into(),
                                        };

                                    linestring
                                        .points
                                        .extend(curve.flattened(0.01).map(Point2::from))
                                }
                            }
                        }
                        PathSegment::CubicTo(p1, p2, p3) => {
                            if let Some(linestring) = current_linestring.as_mut() {
                                if let Some(p0) = linestring.points.last().cloned() {
                                    let curve = lyon::geom::cubic_bezier::CubicBezierSegment {
                                        from: p0.into(),
                                        ctrl1: Point2::from(p1).into(),
                                        ctrl2: Point2::from(p2).into(),
                                        to: Point2::from(p3).into(),
                                    };

                                    linestring
                                        .points
                                        .extend(curve.flattened(0.01).map(Point2::from))
                                }
                            }
                        }
                        PathSegment::Close => {
                            if let Some(linestring) = current_linestring.as_mut() {
                                linestring.close()
                            }
                        }
                    }
                }

                self.geometry.push(current_linestring, path.stroke());
            }
            Node::Image(image) => {
                trace!("{:>depth$} image={:?}", depth, image, depth = depth);
            }
            Node::Text(text) => {
                trace!("{:>depth$} text={:?}", depth, text, depth = depth);
            }
        }
    }
}
