use super::{
    BoundingBox2, ClipperExecutePaths, FromClipper, Geometry, LineString, MultiPolygon, Point2,
    MITER_LIMIT, RESOLUTION, ROUND_PRECISION,
};
use crate::cam::geometry2d::vector::Vector2;
use crate::cam::geometry2d::{Matrix3, Origin2};
use clipper_rs::{ClipperOffset, EndType, JoinType, Paths};
use std::fmt::Formatter;

#[derive(Clone, Debug, PartialEq)]
pub struct MultiLineString {
    pub linestrings: Vec<LineString>,
}

impl MultiLineString {
    pub fn new() -> MultiLineString {
        MultiLineString {
            linestrings: Vec::new(),
        }
    }

    /// Get a mutable reference to the last LineString, or create a new one and add it if there are
    /// no linestrings.
    pub fn back_or_new(&mut self) -> &mut LineString {
        if self.is_empty() {
            self.linestrings.push(LineString { points: Vec::new() });
        }

        self.linestrings.last_mut().unwrap()
    }

    pub fn is_empty(&self) -> bool {
        self.linestrings.len() == 0
    }

    pub fn offset(self, delta: f32) -> MultiPolygon {
        let offset = ClipperOffset::new(
            (MITER_LIMIT * RESOLUTION) as f64,
            (ROUND_PRECISION * RESOLUTION) as f64,
        )
        .with_paths(self.clone_paths(), JoinType::Round, EndType::OpenRound)
        .execute((delta * RESOLUTION) as f64);
        MultiPolygon::from_paths(offset)
    }

    pub fn append(&mut self, other: MultiLineString) {
        self.linestrings.extend(other.linestrings.into_iter());
    }

    pub fn bounds(&self) -> BoundingBox2 {
        if let [first_linestring, remaining_points @ ..] = self.linestrings.as_slice() {
            remaining_points.iter().fold(
                first_linestring.bounds(),
                |mut bounds: BoundingBox2, line_string| {
                    bounds.expand(line_string.bounds());
                    bounds
                },
            )
        } else {
            BoundingBox2::new(Point2 { x: 0.0, y: 0.0 }, Point2 { x: 0.0, y: 0.0 })
        }
    }

    pub fn clean(&mut self, distance: f32) {
        for linestring in self.linestrings.iter_mut() {
            linestring.clean(distance);
        }

        self.linestrings
            .retain(|linestring| linestring.points.len() > 0);
    }

    pub fn transform(&mut self, transform: &Matrix3) {
        for linestring in self.linestrings.iter_mut() {
            linestring.transform(transform)
        }
    }
    pub fn transform_iso(&mut self, transform_iso: &Origin2) {
        for linestring in self.linestrings.iter_mut() {
            linestring.transform_iso(transform_iso)
        }
    }
}

impl Geometry for MultiLineString {
    fn clone_paths(&self) -> Paths {
        Paths::from_paths(
            self.linestrings
                .clone()
                .into_iter()
                .map(LineString::into_path)
                .collect(),
        )
    }

    fn has_area(&self) -> bool {
        false
    }

    fn mirror(&mut self, p1: Point2, p2: Point2) {
        for linestring in self.linestrings.iter_mut() {
            linestring.mirror(p1, p2)
        }
    }

    fn transform_by_points(
        &mut self,
        p1_expected: Point2,
        p1_actual: Point2,
        p2_expected: Point2,
        p2_actual: Point2,
    ) {
        for linestring in self.linestrings.iter_mut() {
            linestring.transform_by_points(p1_expected, p1_actual, p2_expected, p2_actual);
        }
    }
}

impl FromClipper for MultiLineString {
    type Execute = ClipperExecutePaths;

    fn from_paths(paths: Paths) -> MultiLineString {
        MultiLineString {
            linestrings: paths
                .into_paths()
                .into_iter()
                .map(LineString::from_path)
                .collect(),
        }
    }
}

impl std::fmt::Display for MultiLineString {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let bounds = self.bounds();
        write!(
            f,
            "{}, {} linestrings",
            self.bounds(),
            self.linestrings.len()
        )
    }
}

impl std::ops::Add<Vector2> for MultiLineString {
    type Output = MultiLineString;

    fn add(self, rhs: Vector2) -> Self::Output {
        MultiLineString {
            linestrings: self
                .linestrings
                .into_iter()
                .map(|linestring| linestring + rhs)
                .collect(),
        }
    }
}

impl std::ops::AddAssign<Vector2> for MultiLineString {
    fn add_assign(&mut self, rhs: Vector2) {
        for linestring in self.linestrings.iter_mut() {
            *linestring += rhs;
        }
    }
}

impl std::ops::Sub<Vector2> for MultiLineString {
    type Output = MultiLineString;

    fn sub(self, rhs: Vector2) -> Self::Output {
        MultiLineString {
            linestrings: self
                .linestrings
                .into_iter()
                .map(|linestring| linestring - rhs)
                .collect(),
        }
    }
}

impl std::ops::SubAssign<Vector2> for MultiLineString {
    fn sub_assign(&mut self, rhs: Vector2) {
        for linestring in self.linestrings.iter_mut() {
            *linestring -= rhs;
        }
    }
}

impl std::ops::Mul<f32> for MultiLineString {
    type Output = MultiLineString;

    fn mul(self, rhs: f32) -> Self::Output {
        MultiLineString {
            linestrings: self
                .linestrings
                .into_iter()
                .map(|linestring| linestring * rhs)
                .collect(),
        }
    }
}

impl std::ops::MulAssign<f32> for MultiLineString {
    fn mul_assign(&mut self, rhs: f32) {
        for linestring in self.linestrings.iter_mut() {
            *linestring *= rhs;
        }
    }
}

impl std::ops::Div<f32> for MultiLineString {
    type Output = MultiLineString;

    fn div(self, rhs: f32) -> Self::Output {
        MultiLineString {
            linestrings: self
                .linestrings
                .into_iter()
                .map(|linestring| linestring / rhs)
                .collect(),
        }
    }
}

impl std::ops::DivAssign<f32> for MultiLineString {
    fn div_assign(&mut self, rhs: f32) {
        for linestring in self.linestrings.iter_mut() {
            *linestring /= rhs;
        }
    }
}

impl From<LineString> for MultiLineString {
    fn from(value: LineString) -> MultiLineString {
        MultiLineString {
            linestrings: vec![value],
        }
    }
}
