use crate::cam::geometry2d::{
    LineString, MultiLineString, Point2, Text, TextError, TextHorizontalAlign, TextLayout,
};
use crate::ui::widgets::{error_label, justified};
use egui::{Color32, Rounding, ScrollArea, Sense, Stroke, Ui};
use std::convert::Infallible;
use std::fs::File;
use std::io::Read;
use std::num::ParseFloatError;
use std::path::PathBuf;
use std::str::FromStr;
use svg::node::element::path::{Command, Data, Parameters, Position};
use svg::node::element::tag::Type;
use svg::node::Attributes;
use svg::parser::Event;

pub const FONT_FILE_FILTERS: &'static [(&'static str, &'static [&'static str])] =
    &[("SVG", &["svg"])];

#[derive(Debug, Clone)]
pub struct FontFace {
    pub font_family: String,
    pub units_per_em: f32,
    pub ascent: f32,
    pub descent: f32,
    pub cap_height: f32,
    pub x_height: f32,
}

#[derive(Debug, Clone)]
pub struct MissingGlyph {
    pub horizontal_advance_x: f32,
    pub path: MultiLineString,
}

#[derive(Debug, Clone)]
pub struct Glyph {
    pub unicode: char,
    pub name: String,
    pub horizontal_advance_x: f32,
    pub path: MultiLineString,
}

#[derive(Debug, Clone)]
pub struct StrokeFont {
    pub horizontal_advance_x: f32,
    pub id: String,
    pub font_face: Option<FontFace>,
    pub missing_glyph: Option<MissingGlyph>,
    pub glyphs: Vec<Glyph>,
}

#[derive(thiserror::Error, Debug)]
pub enum FontError {
    #[error("Cannot open font file: {source}")]
    FontFile {
        #[from]
        source: std::io::Error,
    },

    #[error("Missing attribute \"{attribute}\" in tag \"{tag}\"")]
    MissingAttribute { attribute: String, tag: String },

    #[error("Couldn't parse number: {source}")]
    NumberParse {
        #[from]
        source: ParseFloatError,
    },

    #[error("A unicode attribute was empty")]
    EmptyUnicode,

    #[error("Invalid path data: {source}")]
    InvalidPath {
        #[from]
        source: svg::parser::Error,
    },

    #[error("Missing path parameter")]
    MissingPathParameter { glyph: String, index: usize },

    #[error("Unimplemented path command: {command:?}")]
    UnimplementedPathCommand { command: Command },

    #[error("Infallible")]
    Infallible {
        #[from]
        source: Infallible,
    },
}

impl StrokeFont {
    pub fn load_svg(font_path: &PathBuf) -> Result<Vec<StrokeFont>, FontError> {
        let mut font_file = File::open(&font_path)?;
        let mut font_bytes = String::new();
        font_file.read_to_string(&mut font_bytes)?;

        let parser = svg::read(&font_bytes)?;

        let mut fonts = Vec::new();
        let mut current_font = None;

        for event in parser {
            //println!("{event:?}");
            match event {
                Event::Tag("font", Type::Start, attributes) => {
                    let font = StrokeFont {
                        horizontal_advance_x: get_attribute(&attributes, "font", "horiz-adv-x")?,
                        id: get_attribute(&attributes, "font", "id")?,
                        font_face: None,
                        missing_glyph: None,
                        glyphs: vec![],
                    };

                    current_font = Some(font);
                }

                Event::Tag("font-face", Type::Empty, attributes) => {
                    if let Some(font) = current_font.as_mut() {
                        let font_face = FontFace {
                            font_family: get_attribute(&attributes, "font-face", "font-family")?,
                            units_per_em: get_attribute(&attributes, "font-face", "units-per-em")?,
                            ascent: get_attribute(&attributes, "font-face", "ascent")?,
                            descent: get_attribute(&attributes, "font-face", "descent")?,
                            cap_height: get_attribute(&attributes, "font-face", "cap-height")?,
                            x_height: get_attribute(&attributes, "font-face", "x-height")?,
                        };

                        font.font_face = Some(font_face);
                    }
                }

                Event::Tag("missing-glyph", Type::Empty, attributes) => {
                    if let Some(font) = current_font.as_mut() {
                        let missing_glyph = MissingGlyph {
                            horizontal_advance_x: get_attribute(
                                &attributes,
                                "missing-glyph",
                                "horiz-adv-x",
                            )?,
                            path: MultiLineString {
                                linestrings: vec![],
                            },
                        };

                        font.missing_glyph = Some(missing_glyph);
                    }
                }

                Event::Tag("glyph", Type::Empty, attributes) => {
                    if let Some(font) = current_font.as_mut() {
                        let unicode_str: String = get_attribute(&attributes, "glyph", "unicode")?;
                        let unicode = htmlize::unescape(unicode_str)
                            .chars()
                            .nth(0)
                            .ok_or(FontError::EmptyUnicode)?;

                        let glyph_name: String = get_attribute(&attributes, "glyph", "glyph-name")?;

                        let mut path = MultiLineString::new();

                        if let Ok(path_data) = get_attribute::<String>(&attributes, "glyph", "d") {
                            let path_commands = Data::parse(&path_data)?;

                            let mut linestring = None;

                            for command in path_commands.into_iter() {
                                match command {
                                    Command::Move(Position::Absolute, parameters) => {
                                        if let Some(linestring) = linestring.take() {
                                            path.linestrings.push(linestring);
                                        }

                                        linestring = Some(LineString {
                                            points: vec![Point2::new(
                                                get_parameter(&parameters, &glyph_name, 0)?,
                                                get_parameter(&parameters, &glyph_name, 1)?,
                                            )],
                                        })
                                    }
                                    Command::Line(Position::Absolute, parameters) => {
                                        if let Some(linestring) = linestring.as_mut() {
                                            linestring.points.push(Point2::new(
                                                get_parameter(&parameters, &glyph_name, 0)?,
                                                get_parameter(&parameters, &glyph_name, 1)?,
                                            ));
                                        }
                                    }
                                    Command::CubicCurve(Position::Absolute, parameters) => {
                                        if let Some(linestring) = linestring.as_mut() {
                                            if let Some(p0) = linestring.points.last().cloned() {
                                                let p1 = Point2::new(
                                                    get_parameter(&parameters, &glyph_name, 0)?,
                                                    get_parameter(&parameters, &glyph_name, 1)?,
                                                );
                                                let p2 = Point2::new(
                                                    get_parameter(&parameters, &glyph_name, 2)?,
                                                    get_parameter(&parameters, &glyph_name, 3)?,
                                                );
                                                let p3 = Point2::new(
                                                    get_parameter(&parameters, &glyph_name, 4)?,
                                                    get_parameter(&parameters, &glyph_name, 5)?,
                                                );

                                                let curve =
                                                    lyon::geom::cubic_bezier::CubicBezierSegment {
                                                        from: p0.into(),
                                                        ctrl1: Point2::from(p1).into(),
                                                        ctrl2: Point2::from(p2).into(),
                                                        to: Point2::from(p3).into(),
                                                    };

                                                linestring
                                                    .points
                                                    .extend(curve.flattened(0.01).map(Point2::from))
                                            }
                                        }
                                    }
                                    Command::Close => {
                                        if let Some(linestring) = linestring.as_mut() {
                                            linestring.close();
                                        }
                                    }
                                    unimplemented_command => {
                                        return Err(FontError::UnimplementedPathCommand {
                                            command: unimplemented_command.clone(),
                                        });
                                    }
                                }
                            }

                            if let Some(linestring) = linestring {
                                path.linestrings.push(linestring);
                            }
                        }

                        let glyph = Glyph {
                            unicode,
                            name: glyph_name,
                            horizontal_advance_x: get_attribute(
                                &attributes,
                                "glyph",
                                "horiz-adv-x",
                            )?,
                            path,
                        };

                        font.glyphs.push(glyph);
                    }
                }

                Event::Tag("font", Type::End, attributes) => {
                    if let Some(font) = current_font.take() {
                        fonts.push(font);
                    }
                }
                _ => {}
            }
        }

        Ok(fonts)
    }
}

#[derive(Copy, Clone, Debug, PartialEq)]
pub enum SelectedGlyph {
    Missing,
    Char(char),
}

#[derive(Debug)]
pub struct StrokeFontUi {
    selected_glyph: Option<SelectedGlyph>,
    show_guides: bool,
    demo_text: Option<Result<Text, TextError>>,
}

impl StrokeFontUi {
    pub fn new() -> StrokeFontUi {
        StrokeFontUi {
            selected_glyph: None,
            show_guides: true,
            demo_text: None,
        }
    }

    pub fn ui(&mut self, ui: &mut Ui, font: &StrokeFont) {
        ui.vertical(|ui| {
            ui.vertical_centered(|ui| {
                ui.heading(&font.id);
            });

            if let Some(font_face) = &font.font_face {
                ui.separator();
                ui.heading("Font Face");
                justified(
                    ui,
                    |ui| ui.label("font-family"),
                    |ui| ui.label(&font_face.font_family),
                );
                justified(
                    ui,
                    |ui| ui.label("units-per-em"),
                    |ui| ui.label(&font_face.units_per_em.to_string()),
                );
                justified(
                    ui,
                    |ui| ui.label("ascent"),
                    |ui| ui.label(&font_face.ascent.to_string()),
                );
                justified(
                    ui,
                    |ui| ui.label("descent"),
                    |ui| ui.label(&font_face.descent.to_string()),
                );
                justified(
                    ui,
                    |ui| ui.label("cap-height"),
                    |ui| ui.label(&font_face.cap_height.to_string()),
                );
                justified(
                    ui,
                    |ui| ui.label("x-height"),
                    |ui| ui.label(&font_face.x_height.to_string()),
                );
            }

            ui.separator();
            ui.heading("Demo");

            let text_layout = TextLayout {
                height: 20.0,
                horizontal_align: TextHorizontalAlign::Left,
                char_spacing_offset: 0.0,
                line_spacing_offset: 0.0,
                wrap_width: None,
            };

            let demo_text = self.demo_text.get_or_insert_with(|| Text::generate("ABCDEFGHIJKLMNOPQRSTUVQXYZ\nabcdefghijklmnopqrstuvwxyz\n1234567890\n!@#$%^&*()-=_+;:'\",.<>?[]{}`~\\|", text_layout, font));

            match demo_text {
                Ok(demo_text) => {
                    let (min_bounds, max_bounds) = demo_text.bounds();
                    //dbg!(min_bounds, max_bounds);
                    let size = max_bounds - min_bounds;
                    //dbg!(size);

                    let (response, painter) = ui.allocate_painter(size.into(), Sense::focusable_noninteractive());
                    let rect = response.rect;
                    //dbg!(rect);

                    let min = Point2::from(rect.min) - min_bounds;

                    painter.rect_filled(rect, Rounding::ZERO, Color32::WHITE);

                    for linestring in demo_text.paths().linestrings.iter() {
                        for [p1, p2] in linestring.points.array_windows() {
                            let mut p1 = *p1 + min;
                            p1.y = -(p1.y - rect.min.y) + rect.max.y;

                            let mut p2 = *p2 + min;
                            p2.y = -(p2.y - rect.min.y) + rect.max.y;

                            painter.line_segment(
                                [p1.into(), p2.into()],
                                Stroke::new(2.0, Color32::BLACK),
                            );
                        }
                    }
                }

                Err(e) => { error_label(ui, &format!("Error generating demo text: {}", e)); }
            }

            ui.separator();
            ui.heading("Glyphs");

            ui.horizontal(|ui| {
                ui.set_height(400.0);
                ScrollArea::vertical().show(ui, |ui| {
                    ui.set_width(150.0);
                    ui.vertical(|ui| {
                        if font.missing_glyph.is_some() {
                            ui.selectable_value(
                                &mut self.selected_glyph,
                                Some(SelectedGlyph::Missing),
                                "Missing Glyph",
                            );
                        }

                        for glyph in &font.glyphs {
                            ui.selectable_value(
                                &mut self.selected_glyph,
                                Some(SelectedGlyph::Char(glyph.unicode)),
                                &format!("\"{}\": {}", glyph.unicode, &glyph.name),
                            );
                        }
                    });
                });

                ui.vertical(|ui| {
                    ui.set_width(300.0);
                    match &self.selected_glyph {
                        Some(SelectedGlyph::Missing) => {
                            if let Some(glyph) = &font.missing_glyph {
                                justified(
                                    ui,
                                    |ui| ui.label("horiz-adv-x"),
                                    |ui| ui.label(glyph.horizontal_advance_x.to_string()),
                                );
                            }
                        }
                        Some(SelectedGlyph::Char(c)) => {
                            if let Some(glyph) = font.glyphs.iter().find(|f| f.unicode == *c) {
                                justified(
                                    ui,
                                    |ui| ui.label("unicode"),
                                    |ui| ui.label(glyph.unicode.to_string()),
                                );
                                justified(
                                    ui,
                                    |ui| ui.label("glyph-name"),
                                    |ui| ui.label(&glyph.name),
                                );
                                justified(
                                    ui,
                                    |ui| ui.label("horiz-adv-x"),
                                    |ui| ui.label(glyph.horizontal_advance_x.to_string()),
                                );

                                let rect = ui.available_rect_before_wrap();
                                let (_response, painter) = ui.allocate_painter(
                                    rect.size(),
                                    Sense::focusable_noninteractive(),
                                );

                                //let painter = ui.painter();

                                painter.rect(rect, Rounding::ZERO, Color32::WHITE, Stroke::NONE);

                                if let Some(font_face) = &font.font_face {
                                    let rect = rect.shrink(50.0);

                                    let x_ratio = rect.width() / glyph.horizontal_advance_x;
                                    let y_ratio =
                                        rect.height() / (font_face.ascent - font_face.descent);

                                    let scale = f32::min(x_ratio, y_ratio);

                                    let min = Point2::from(rect.min)
                                        - Point2::from_y(font_face.descent) * scale; // - glyph_bounds.0 * scale;

                                    if self.show_guides {
                                        let mut guidelines = MultiLineString::new();

                                        guidelines.linestrings.push(LineString {
                                            points: vec![
                                                Point2::new(0.0, font_face.ascent),
                                                Point2::new(
                                                    glyph.horizontal_advance_x,
                                                    font_face.ascent,
                                                ),
                                            ],
                                        });

                                        guidelines.linestrings.push(LineString {
                                            points: vec![
                                                Point2::new(0.0, 0.0),
                                                Point2::new(glyph.horizontal_advance_x, 0.0),
                                            ],
                                        });

                                        guidelines.linestrings.push(LineString {
                                            points: vec![
                                                Point2::new(0.0, font_face.descent),
                                                Point2::new(
                                                    glyph.horizontal_advance_x,
                                                    font_face.descent,
                                                ),
                                            ],
                                        });

                                        guidelines.linestrings.push(LineString {
                                            points: vec![
                                                Point2::new(0.0, font_face.cap_height),
                                                Point2::new(
                                                    glyph.horizontal_advance_x,
                                                    font_face.cap_height,
                                                ),
                                            ],
                                        });

                                        guidelines.linestrings.push(LineString {
                                            points: vec![
                                                Point2::new(0.0, font_face.x_height),
                                                Point2::new(
                                                    glyph.horizontal_advance_x,
                                                    font_face.x_height,
                                                ),
                                            ],
                                        });

                                        guidelines.linestrings.push(LineString {
                                            points: vec![
                                                Point2::new(0.0, font_face.descent),
                                                Point2::new(0.0, font_face.ascent),
                                            ],
                                        });

                                        guidelines.linestrings.push(LineString {
                                            points: vec![
                                                Point2::new(
                                                    glyph.horizontal_advance_x,
                                                    font_face.descent,
                                                ),
                                                Point2::new(
                                                    glyph.horizontal_advance_x,
                                                    font_face.ascent,
                                                ),
                                            ],
                                        });

                                        for linestring in guidelines.linestrings {
                                            for [p1, p2] in linestring.points.array_windows() {
                                                let mut p1 = (*p1 * scale) + min;
                                                p1.y = -(p1.y - rect.min.y) + rect.max.y;

                                                let mut p2 = (*p2 * scale) + min;
                                                p2.y = -(p2.y - rect.min.y) + rect.max.y;

                                                painter.line_segment(
                                                    [p1.into(), p2.into()],
                                                    Stroke::new(2.0, Color32::GRAY),
                                                );
                                            }
                                        }
                                    }

                                    for linestring in glyph.path.linestrings.iter() {
                                        for [p1, p2] in linestring.points.array_windows() {
                                            let mut p1 = (*p1 * scale) + min;
                                            p1.y = -(p1.y - rect.min.y) + rect.max.y;

                                            let mut p2 = (*p2 * scale) + min;
                                            p2.y = -(p2.y - rect.min.y) + rect.max.y;

                                            painter.line_segment(
                                                [p1.into(), p2.into()],
                                                Stroke::new(2.0, Color32::BLACK),
                                            );
                                        }
                                    }
                                }
                                ui.checkbox(&mut self.show_guides, "Show Guides");
                            }
                        }
                        _ => {}
                    }
                });
            });
        });
    }
}

fn get_attribute<T>(attributes: &Attributes, tag: &str, attribute: &str) -> Result<T, FontError>
where
    T: FromStr,
    FontError: From<<T as FromStr>::Err>,
{
    Ok(attributes
        .get(attribute)
        .ok_or(FontError::MissingAttribute {
            attribute: attribute.into(),
            tag: tag.into(),
        })?
        .parse()?)
}

fn get_parameter(parameters: &Parameters, glyph: &str, index: usize) -> Result<f32, FontError> {
    Ok(*parameters
        .get(index)
        .ok_or(FontError::MissingPathParameter {
            glyph: glyph.to_string(),
            index,
        })?)
}
