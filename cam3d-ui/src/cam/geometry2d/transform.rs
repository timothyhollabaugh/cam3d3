use crate::cam::geometry2d::Point2;
use nalgebra as na;

pub struct Matrix3(pub(super) na::Matrix3<f32>);

impl Matrix3 {
    pub fn transform_point(&self, point: Point2) -> Point2 {
        self.0.transform_point(&point.into()).into()
    }
}
