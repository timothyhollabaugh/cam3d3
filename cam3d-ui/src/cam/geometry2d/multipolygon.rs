use super::{
    ClipperExecutePaths, FromClipper, Geometry, LineString, MultiLineString, Point2, Polygon,
    Vector2, MITER_LIMIT, RESOLUTION, ROUND_PRECISION,
};
use crate::cam::geometry2d::BoundingBox2;
use crate::cam::geometry2d::{union, Matrix3, Origin2};
use clipper_rs::{
    ClipType, Clipper, ClipperOffset, EndType, FillType, JoinType, Path, Paths, PointPolygon,
    PolyType,
};
use std::fmt::Formatter;

#[derive(Clone, Debug, PartialEq)]
pub struct MultiPolygon {
    pub polygons: Vec<Polygon>,
}

impl MultiPolygon {
    pub fn new() -> MultiPolygon {
        MultiPolygon {
            polygons: Vec::new(),
        }
    }

    pub fn is_empty(&self) -> bool {
        self.polygons.len() == 0
    }

    pub fn area(&self) -> f32 {
        self.clone().clone_paths().area() as f32 / (RESOLUTION * RESOLUTION)
    }

    pub fn into_boundary(self) -> MultiLineString {
        MultiLineString {
            linestrings: self
                .polygons
                .into_iter()
                .flat_map(|poly| poly.into_boundary().linestrings.into_iter())
                .collect(),
        }
    }

    pub fn point_inside(&self, point: Point2) -> PointPolygon {
        for polygon in &self.polygons {
            match polygon.exterior.point_inside(point) {
                PointPolygon::Outside => return PointPolygon::Outside,
                PointPolygon::On => return PointPolygon::On,
                PointPolygon::In => {
                    for interior in &polygon.interiors {
                        match interior.point_inside(point) {
                            PointPolygon::Outside => {}
                            PointPolygon::On => return PointPolygon::On,
                            PointPolygon::In => return PointPolygon::Outside,
                        }
                    }
                }
            }
        }

        PointPolygon::In
    }

    pub fn offset(self, delta: f32) -> MultiPolygon {
        MultiPolygon::from_paths(
            ClipperOffset::new(
                (MITER_LIMIT * RESOLUTION) as f64,
                (ROUND_PRECISION * RESOLUTION) as f64,
            )
            .with_paths(self.clone_paths(), JoinType::Round, EndType::ClosedPolygon)
            .execute((delta * RESOLUTION) as f64),
        )
    }

    pub fn bounds(&self) -> BoundingBox2 {
        if let [first_polygon, remaining_points @ ..] = self.polygons.as_slice() {
            remaining_points.iter().fold(
                first_polygon.bounds(),
                |mut bounds: BoundingBox2, line_string| {
                    bounds.expand(line_string.bounds());
                    bounds
                },
            )
        } else {
            BoundingBox2::new(Point2 { x: 0.0, y: 0.0 }, Point2 { x: 0.0, y: 0.0 })
        }
    }

    pub fn merge_connected(&mut self) {
        let merged = union(self.polygons.iter().map(|p| p as &dyn Geometry));
        *self = merged;
    }

    pub fn clean(&mut self, distance: f32) {
        for polygon in self.polygons.iter_mut() {
            polygon.clean(distance);
        }
    }

    pub fn transform(&mut self, transform: &Matrix3) {
        for polygon in self.polygons.iter_mut() {
            polygon.transform(transform)
        }
    }
    pub fn transform_iso(&mut self, transform_iso: &Origin2) {
        for polygon in self.polygons.iter_mut() {
            polygon.transform_iso(transform_iso)
        }
    }
}

impl Geometry for MultiPolygon {
    fn clone_paths(&self) -> Paths {
        Paths::from_paths(
            self.polygons
                .iter()
                .flat_map(|poly| poly.clone_paths().into_paths().into_iter())
                .collect(),
        )
    }

    fn has_area(&self) -> bool {
        true
    }

    fn mirror(&mut self, p1: Point2, p2: Point2) {
        for polygon in self.polygons.iter_mut() {
            polygon.mirror(p1, p2);
        }
    }

    fn transform_by_points(
        &mut self,
        p1_expected: Point2,
        p1_actual: Point2,
        p2_expected: Point2,
        p2_actual: Point2,
    ) {
        for polygon in self.polygons.iter_mut() {
            polygon.transform_by_points(p1_expected, p1_actual, p2_expected, p2_actual);
        }
    }
}

impl FromClipper for MultiPolygon {
    type Execute = ClipperExecutePaths;

    fn from_paths(paths: Paths) -> MultiPolygon {
        let (exteriors, interiors): (Vec<Path>, Vec<Path>) =
            paths.into_paths().into_iter().partition(Path::orientation);

        let multipolygon = MultiPolygon {
            polygons: exteriors
                .into_iter()
                .map(|exterior| {
                    let interior_paths = interiors
                        .iter()
                        .filter(|&interior| {
                            interior
                                .points()
                                .iter()
                                .any(|&point| exterior.point_in_polygon(point).in_or_on())
                        })
                        .cloned();
                    // .map(|path| {
                    //     let mut points = path.into_points();
                    //     if let (Some(first), Some(last)) =
                    //         (points.first().cloned(), points.last().cloned())
                    //     {
                    //         if first != last {
                    //             points.push(first);
                    //         }
                    //     }
                    //     Path::from_points(points)
                    // });

                    let mut clipper = Clipper::new();

                    for path in interior_paths {
                        clipper.add_path(path, PolyType::Subject, true);
                    }

                    let unioned_interior_paths = clipper
                        .execute(ClipType::Union, FillType::NonZero, FillType::NonZero)
                        .unwrap_or(Paths::new());

                    let interiors = MultiLineString::from_paths(unioned_interior_paths);

                    let mut exterior = LineString::from_path(exterior);
                    exterior.close();
                    Polygon {
                        exterior,
                        interiors: interiors
                            .linestrings
                            .into_iter()
                            .map(|mut linestring| {
                                linestring.close();
                                linestring
                            })
                            .collect(),
                    }
                })
                .collect(),
        };

        multipolygon
    }
}

impl std::fmt::Display for MultiPolygon {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let bounds = self.bounds();
        write!(f, "{}, {} polygons", self.bounds(), self.polygons.len())
    }
}

impl std::ops::Add<Vector2> for MultiPolygon {
    type Output = MultiPolygon;

    fn add(self, rhs: Vector2) -> Self::Output {
        MultiPolygon {
            polygons: self
                .polygons
                .into_iter()
                .map(|polygon| polygon + rhs)
                .collect(),
        }
    }
}

impl std::ops::AddAssign<Vector2> for MultiPolygon {
    fn add_assign(&mut self, rhs: Vector2) {
        for polygon in self.polygons.iter_mut() {
            *polygon += rhs;
        }
    }
}

impl std::ops::Sub<Vector2> for MultiPolygon {
    type Output = MultiPolygon;

    fn sub(self, rhs: Vector2) -> Self::Output {
        MultiPolygon {
            polygons: self
                .polygons
                .into_iter()
                .map(|polygon| polygon - rhs)
                .collect(),
        }
    }
}

impl std::ops::SubAssign<Vector2> for MultiPolygon {
    fn sub_assign(&mut self, rhs: Vector2) {
        for polygon in self.polygons.iter_mut() {
            *polygon -= rhs;
        }
    }
}

impl std::ops::Mul<f32> for MultiPolygon {
    type Output = MultiPolygon;

    fn mul(self, rhs: f32) -> Self::Output {
        MultiPolygon {
            polygons: self
                .polygons
                .into_iter()
                .map(|polygon| polygon * rhs)
                .collect(),
        }
    }
}

impl std::ops::MulAssign<f32> for MultiPolygon {
    fn mul_assign(&mut self, rhs: f32) {
        for polygon in self.polygons.iter_mut() {
            *polygon *= rhs;
        }
    }
}

impl From<Polygon> for MultiPolygon {
    fn from(value: Polygon) -> MultiPolygon {
        MultiPolygon {
            polygons: vec![value],
        }
    }
}

impl From<LineString> for MultiPolygon {
    fn from(value: LineString) -> MultiPolygon {
        MultiPolygon {
            polygons: vec![value.into()],
        }
    }
}
