mod font;

use crate::cam::geometry2d::{Matrix3, MultiLineString, Origin2, Point2, Vector2};
pub use font::*;
use serde::{Deserialize, Serialize};

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub enum TextHorizontalAlign {
    Left,
    Center,
    Right,
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct TextLayout {
    pub height: f32,
    pub horizontal_align: TextHorizontalAlign,
    pub char_spacing_offset: f32,
    pub line_spacing_offset: f32,
    pub wrap_width: Option<f32>,
}

#[derive(Clone, Debug)]
pub struct Text {
    text: String,
    layout: TextLayout,
    paths: MultiLineString,
    origin: Origin2,
    font_face: FontFace,
    min_bounds: Point2,
    max_bounds: Point2,
}

#[derive(thiserror::Error, Debug)]
pub enum TextError {
    #[error("Missing FontFace")]
    MissingFontFace,
}

impl Text {
    pub fn generate(
        text: impl Into<String>,
        layout: TextLayout,
        font: &StrokeFont,
    ) -> Result<Text, TextError> {
        let text = text.into();

        let font_face = font
            .font_face
            .as_ref()
            .ok_or(TextError::MissingFontFace)?
            .clone();

        let scale = layout.height / font_face.cap_height;

        let ascent = font_face.ascent * scale;
        let descent = font_face.descent * scale;
        let cap_height = font_face.cap_height * scale;

        let mut char_point = Vector2::zero();
        let mut paths = MultiLineString::new();

        let mut min_bounds = Point2::new(0.0, descent - layout.line_spacing_offset);
        let mut max_bounds = Point2::new(0.0, ascent + layout.line_spacing_offset);

        for c in text.chars() {
            match c {
                '\n' => {
                    char_point.x = 0.0;
                    char_point.y -= ascent + layout.line_spacing_offset;
                    min_bounds.y = char_point.y + descent - layout.line_spacing_offset;
                }
                c => {
                    let (path, x_adv) =
                        if let Some(glyph) = font.glyphs.iter().find(|f| f.unicode == c) {
                            (glyph.path.clone(), glyph.horizontal_advance_x * scale)
                        } else if let Some(missing_glyph) = font.missing_glyph.as_ref() {
                            (
                                missing_glyph.path.clone(),
                                missing_glyph.horizontal_advance_x * scale,
                            )
                        } else {
                            (MultiLineString::new(), font.horizontal_advance_x * scale)
                        };

                    let path = path * scale + char_point;
                    paths.linestrings.extend(path.linestrings);

                    char_point.x += x_adv;

                    if let Some(wrap_width) = layout.wrap_width {
                        if char_point.x > wrap_width {
                            char_point.x = 0.0;
                            char_point.y -= ascent + layout.line_spacing_offset;
                            min_bounds.y = char_point.y + descent - layout.line_spacing_offset;
                        } else {
                            max_bounds.x = f32::max(max_bounds.x, char_point.x);
                        }
                    } else {
                        max_bounds.x = f32::max(max_bounds.x, char_point.x);
                    }
                }
            }
        }

        Ok(Text {
            text,
            layout,
            paths,
            origin: Origin2::from_point_x_axis(Point2::zero()),
            font_face,
            min_bounds,
            max_bounds,
        })
    }

    pub fn paths(&self) -> &MultiLineString {
        &self.paths
    }

    pub fn bounds(&self) -> (Point2, Point2) {
        (self.min_bounds, self.max_bounds)
    }

    pub fn into_paths(self) -> MultiLineString {
        self.paths
    }

    pub fn text(&self) -> &str {
        &self.text
    }

    fn scale(&self) -> f32 {
        self.layout.height / self.font_face.cap_height
    }

    pub fn transform_iso(&mut self, transform: &Origin2) {
        self.paths.transform_iso(transform);
        self.origin.transform_iso(transform);
    }

    pub fn ascent_left_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                self.min_bounds.x,
                self.font_face.ascent * self.scale(),
            ))
    }

    pub fn ascent_right_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                self.max_bounds.x,
                self.font_face.ascent * self.scale(),
            ))
    }

    pub fn ascent_mid_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                (self.min_bounds.x + self.max_bounds.x) / 2.0,
                self.font_face.ascent * self.scale(),
            ))
    }

    pub fn cap_height_left_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                self.min_bounds.x,
                self.font_face.cap_height * self.scale(),
            ))
    }

    pub fn cap_height_right_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                self.max_bounds.x,
                self.font_face.cap_height * self.scale(),
            ))
    }

    pub fn cap_height_mid_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                (self.min_bounds.x + self.max_bounds.x) / 2.0,
                self.font_face.cap_height * self.scale(),
            ))
    }

    pub fn x_height_left_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                self.min_bounds.x,
                self.font_face.x_height * self.scale(),
            ))
    }

    pub fn x_height_right_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                self.max_bounds.x,
                self.font_face.x_height * self.scale(),
            ))
    }

    pub fn x_height_mid_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                (self.min_bounds.x + self.max_bounds.x) / 2.0,
                self.font_face.x_height * self.scale(),
            ))
    }

    pub fn base_left_origin(&self) -> Origin2 {
        self.origin * Origin2::from_point_x_axis(Point2::new(self.min_bounds.x, 0.0))
    }

    pub fn base_right_origin(&self) -> Origin2 {
        self.origin * Origin2::from_point_x_axis(Point2::new(self.max_bounds.x, 0.0))
    }

    pub fn base_mid_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                (self.min_bounds.x + self.max_bounds.x) / 2.0,
                0.0,
            ))
    }

    pub fn descent_left_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                self.min_bounds.x,
                self.font_face.descent * self.scale(),
            ))
    }

    pub fn descent_right_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                self.max_bounds.x,
                self.font_face.descent * self.scale(),
            ))
    }

    pub fn descent_mid_origin(&self) -> Origin2 {
        self.origin
            * Origin2::from_point_x_axis(Point2::new(
                (self.min_bounds.x + self.max_bounds.x) / 2.0,
                self.font_face.descent * self.scale(),
            ))
    }
}
