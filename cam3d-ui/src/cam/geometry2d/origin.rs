use crate::cam::geometry2d::vector::Vector2;
use crate::cam::geometry2d::{Matrix3, Point2};
use crate::cam::geometry3d::Matrix4;
use nalgebra as na;
use serde::{Deserialize, Serialize};

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize, Default)]
pub struct Origin2(pub(super) na::Isometry2<f32>);

impl Origin2 {
    pub fn from_point_angle(point: Point2, angle: f32) -> Origin2 {
        Origin2(na::Isometry2::new(
            point.as_vector_from_zero().into(),
            angle,
        ))
    }

    pub fn from_point_x_axis(point: Point2) -> Origin2 {
        Origin2(na::Isometry2::new(point.as_vector_from_zero().into(), 0.0))
    }

    pub fn point(&self) -> Point2 {
        Vector2::from(self.0.translation.vector).as_point()
    }

    pub fn angle(&self) -> f32 {
        self.0.rotation.angle()
    }

    pub fn direction(&self) -> Vector2 {
        Vector2::new(self.0.rotation.re, self.0.rotation.im)
    }

    pub fn transform_point(&self, point: Point2) -> Point2 {
        self.0.transform_point(&na::Point2::from(point)).into()
    }

    pub fn transform_iso(&mut self, transform: &Origin2) {
        self.0 *= transform.0;
    }

    pub fn transform_to(&self, to: Origin2) -> Origin2 {
        Origin2(to.0 * self.0.inverse())
    }
}

impl std::ops::Mul for Origin2 {
    type Output = Origin2;
    fn mul(self, rhs: Self) -> Self::Output {
        Origin2(self.0 * rhs.0)
    }
}

impl std::ops::MulAssign for Origin2 {
    fn mul_assign(&mut self, rhs: Self) {
        self.0 *= rhs.0;
    }
}

/*
impl std::ops::Sub for Origin2 {
    type Output = Vector2;

    fn sub(self, rhs: Self) -> Self::Output {
        Vector2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl std::ops::Sub<Vector2> for Origin2 {
    type Output = Origin2;

    fn sub(self, rhs: Vector2) -> Self::Output {
        Origin2 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            x_axis: self.x_axis,
        }
    }
}

impl std::ops::Add<Vector2> for Origin2 {
    type Output = Origin2;

    fn add(self, rhs: Vector2) -> Self::Output {
        Origin2 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            x_axis: self.x_axis,
        }
    }
}

impl std::ops::AddAssign<Vector2> for Origin2 {
    fn add_assign(&mut self, rhs: Vector2) {
        self.x += rhs.x;
        self.y += rhs.y;
    }
}

impl std::ops::SubAssign<Vector2> for Origin2 {
    fn sub_assign(&mut self, rhs: Vector2) {
        self.x -= rhs.x;
        self.y -= rhs.y;
    }
}

impl std::ops::Mul<f32> for Origin2 {
    type Output = Origin2;

    fn mul(self, rhs: f32) -> Self::Output {
        Origin2 {
            x: self.x * rhs,
            y: self.y * rhs,
            x_axis: self.x_axis,
        }
    }
}

impl std::ops::Mul<f64> for Origin2 {
    type Output = Origin2;

    fn mul(self, rhs: f64) -> Self::Output {
        Origin2 {
            x: self.x * rhs as f32,
            y: self.y * rhs as f32,
            x_axis: self.x_axis,
        }
    }
}

impl std::ops::MulAssign<f32> for Origin2 {
    fn mul_assign(&mut self, rhs: f32) {
        *self = *self * rhs;
    }
}

impl std::ops::MulAssign<f64> for Origin2 {
    fn mul_assign(&mut self, rhs: f64) {
        *self = *self * rhs as f32;
    }
}

impl std::ops::Div<f32> for Origin2 {
    type Output = Origin2;

    fn div(self, rhs: f32) -> Self::Output {
        Origin2 {
            x: self.x / rhs,
            y: self.y / rhs,
            x_axis: self.x_axis,
        }
    }
}

impl std::ops::DivAssign<f32> for Origin2 {
    fn div_assign(&mut self, rhs: f32) {
        *self = *self / rhs;
    }
}

impl std::ops::Div<f64> for Origin2 {
    type Output = Origin2;

    fn div(self, rhs: f64) -> Self::Output {
        Origin2 {
            x: self.x / rhs as f32,
            y: self.y / rhs as f32,
            x_axis: self.x_axis,
        }
    }
}
 */
