use super::{Geometry, Point2, Polygon, MITER_LIMIT, RESOLUTION, ROUND_PRECISION};
use crate::cam::geometry2d::vector::Vector2;
use crate::cam::geometry2d::BoundingBox2;
use crate::cam::geometry2d::{Matrix3, Origin2};
use clipper_rs::{ClipperOffset, EndType, JoinType, Path, Paths, PointPolygon};
use lyon::lyon_tessellation::{LineCap, LineJoin};
use ordered_float::OrderedFloat;
use std::f32::consts::PI;
use std::fmt::Formatter;

#[derive(Clone, Debug, PartialEq, Default)]
pub struct LineString {
    pub points: Vec<Point2>,
}

impl LineString {
    /// Create a new, empty linestring
    pub fn new() -> LineString {
        LineString { points: Vec::new() }
    }

    /// Is the linestring closed? Is the first point the same as the last point?
    pub fn is_closed(&self) -> bool {
        self.points.first() == self.points.last()
    }

    pub fn closed(mut self) -> LineString {
        self.close();
        self
    }

    /// Insert the first point at the end of the linesting to form a closed path
    pub fn close(&mut self) {
        if !self.is_closed() {
            if let Some(front) = self.points.first().cloned() {
                self.points.push(front);
            }
        }
    }

    /// If the linestring is closed, remove the last point to for an open path
    pub fn open(&mut self) {
        if self.is_closed() {
            self.points.pop();
        }
    }

    /// Whether there are any points in this linestring
    pub fn is_empty(&self) -> bool {
        self.points.is_empty()
    }

    pub fn from_path(path: Path) -> LineString {
        LineString {
            points: path
                .into_points()
                .into_iter()
                .map(Point2::from_int_point)
                .collect(),
        }
    }

    pub fn into_path(self) -> Path {
        Path::from_points(
            self.points
                .into_iter()
                .map(Point2::into_int_point)
                .collect(),
        )
    }

    pub fn offset(self, delta: f32) -> Polygon {
        let result = ClipperOffset::new(
            (MITER_LIMIT * RESOLUTION) as f64,
            (ROUND_PRECISION * RESOLUTION) as f64,
        )
        .with_path(self.into_path(), JoinType::Round, EndType::OpenRound)
        .execute((delta * RESOLUTION) as f64);

        let exterior = result
            .into_paths()
            .pop()
            .map(LineString::from_path)
            .unwrap_or(LineString::new());

        Polygon {
            exterior,
            interiors: Vec::new(),
        }
    }

    pub fn point_inside(&self, point: Point2) -> PointPolygon {
        let path = self.clone().into_path();
        path.point_in_polygon(point.into_int_point())
    }

    pub fn bounds(&self) -> BoundingBox2 {
        if let [first_point, remaining_points @ ..] = self.points.as_slice() {
            remaining_points.iter().fold(
                BoundingBox2::new(*first_point, *first_point),
                |mut bounds, point| {
                    bounds.expand_to_point(*point);
                    bounds
                },
            )
        } else {
            BoundingBox2::new(Point2 { x: 0.0, y: 0.0 }, Point2 { x: 0.0, y: 0.0 })
        }
    }

    pub fn closest_point(&self, point: &Point2) -> Option<(usize, f32)> {
        self.points
            .iter()
            .map(|&p| (p - *point).magnitude())
            .enumerate()
            .min_by_key(|(_, d)| OrderedFloat::from(*d))
    }

    pub fn clean(&mut self, distance: f32) {
        let closed = self.is_closed();
        let mut path = self.clone().into_path();
        path.clean((distance * RESOLUTION) as f64);
        *self = LineString::from_path(path);
        if closed {
            self.close();
        }
    }

    pub fn tessillate(
        &self,
        width: f32,
    ) -> Result<three_d_asset::TriMesh, lyon::tessellation::TessellationError> {
        for (i, point) in self.points.iter().enumerate() {
            assert!(point.x.is_finite());
            assert!(point.y.is_finite());
        }

        let mut stroke_buffers: lyon::tessellation::VertexBuffers<
            three_d_asset::Vector3<f64>,
            u32,
        > = lyon::tessellation::VertexBuffers::new();

        let mut stroke_buffers_builder = lyon::tessellation::BuffersBuilder::new(
            &mut stroke_buffers,
            |v: lyon::tessellation::StrokeVertex| {
                three_d_asset::Vector3::<f64>::new(
                    v.position().x as f64,
                    v.position().y as f64,
                    0.0,
                )
            },
        );

        let mut stroke_tessellator = lyon::tessellation::StrokeTessellator::new();
        stroke_tessellator.tessellate_polygon(
            lyon::path::Polygon {
                points: &self
                    .points
                    .iter()
                    .map(|point| lyon::math::Point::new(point.x, point.y))
                    .collect::<Vec<_>>(),
                closed: self.is_closed(),
            },
            &lyon::tessellation::StrokeOptions::default()
                .with_start_cap(LineCap::Round)
                .with_end_cap(LineCap::Round)
                .with_line_join(LineJoin::Round)
                .with_line_width(width),
            &mut stroke_buffers_builder,
        )?;

        Ok(three_d_asset::TriMesh {
            indices: three_d_asset::Indices::U32(stroke_buffers.indices),
            normals: Some(vec![
                three_d_asset::Vec3::unit_z();
                stroke_buffers.vertices.len()
            ]),
            tangents: None,
            uvs: None,
            positions: three_d_asset::Positions::F64(stroke_buffers.vertices),
            colors: None,
        })
    }

    pub fn rectangle(p1: Point2, p2: Point2) -> LineString {
        LineString {
            points: vec![
                Point2 { x: p1.x, y: p1.y },
                Point2 { x: p2.x, y: p1.y },
                Point2 { x: p2.x, y: p2.y },
                Point2 { x: p1.x, y: p2.y },
                Point2 { x: p1.x, y: p1.y },
            ],
        }
    }

    pub fn circle(radius: f32, number_segments: usize) -> LineString {
        LineString::circle_from_theta(radius, number_segments, 0.0)
    }

    pub fn circle_from_theta(radius: f32, number_segments: usize, start_theta: f32) -> LineString {
        LineString {
            points: (0..=number_segments)
                .map(|n| {
                    let a = PI * 2.0 * n as f32 / number_segments as f32 + start_theta;
                    Point2::new(radius * f32::cos(a), radius * f32::sin(a))
                })
                .collect(),
        }
    }

    pub fn transform(&mut self, transform: &Matrix3) {
        for p in self.points.iter_mut() {
            p.transform(&transform);
        }
    }

    pub fn transform_iso(&mut self, transform_iso: &Origin2) {
        for p in self.points.iter_mut() {
            p.transform_iso(&transform_iso);
        }
    }
}

impl Geometry for LineString {
    fn clone_paths(&self) -> Paths {
        Paths::from_paths(vec![self.clone().into_path()])
    }

    fn has_area(&self) -> bool {
        false
    }

    fn mirror(&mut self, p1: Point2, p2: Point2) {
        for p in self.points.iter_mut() {
            p.mirror(p1, p2);
        }

        // Reverse the points to preserve CW/CCW when mirroring so polygon filling works
        for i in 0..self.points.len() / 2 {
            let j = self.points.len() - 1 - i;
            self.points.swap(i, j);
        }
    }

    fn transform_by_points(
        &mut self,
        p1_expected: Point2,
        p1_actual: Point2,
        p2_expected: Point2,
        p2_actual: Point2,
    ) {
        for p in self.points.iter_mut() {
            p.transform_by_points(p1_expected, p1_actual, p2_expected, p2_actual);
        }
    }
}

impl std::fmt::Display for LineString {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let bounds = self.bounds();
        write!(f, "{}, {} points", self.bounds(), self.points.len())
    }
}

impl std::ops::Neg for LineString {
    type Output = LineString;

    fn neg(self) -> Self::Output {
        LineString {
            points: self.points.into_iter().map(|point| -point).collect(),
        }
    }
}

impl std::ops::Add<Vector2> for LineString {
    type Output = LineString;

    fn add(self, rhs: Vector2) -> Self::Output {
        LineString {
            points: self.points.into_iter().map(|point| point + rhs).collect(),
        }
    }
}

impl std::ops::AddAssign<Vector2> for LineString {
    fn add_assign(&mut self, rhs: Vector2) {
        for point in self.points.iter_mut() {
            *point += rhs;
        }
    }
}

impl std::ops::Sub<Vector2> for LineString {
    type Output = LineString;

    fn sub(self, rhs: Vector2) -> Self::Output {
        LineString {
            points: self.points.into_iter().map(|point| point - rhs).collect(),
        }
    }
}

impl std::ops::SubAssign<Vector2> for LineString {
    fn sub_assign(&mut self, rhs: Vector2) {
        for point in self.points.iter_mut() {
            *point -= rhs;
        }
    }
}

impl std::ops::Mul<f32> for LineString {
    type Output = LineString;

    fn mul(self, rhs: f32) -> Self::Output {
        LineString {
            points: self.points.into_iter().map(|point| point * rhs).collect(),
        }
    }
}

impl std::ops::MulAssign<f32> for LineString {
    fn mul_assign(&mut self, rhs: f32) {
        for point in self.points.iter_mut() {
            *point *= rhs;
        }
    }
}

impl std::ops::Mul<f64> for LineString {
    type Output = LineString;

    fn mul(self, rhs: f64) -> Self::Output {
        LineString {
            points: self.points.into_iter().map(|point| point * rhs).collect(),
        }
    }
}

impl std::ops::Div<f32> for LineString {
    type Output = LineString;

    fn div(self, rhs: f32) -> Self::Output {
        LineString {
            points: self.points.into_iter().map(|point| point / rhs).collect(),
        }
    }
}

impl std::ops::DivAssign<f32> for LineString {
    fn div_assign(&mut self, rhs: f32) {
        for point in self.points.iter_mut() {
            *point /= rhs;
        }
    }
}
