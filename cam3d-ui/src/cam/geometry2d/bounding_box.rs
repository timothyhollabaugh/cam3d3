use crate::cam::geometry2d::{Point2, Vector2};

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct BoundingBox2 {
    min: Point2,
    max: Point2,
}

impl BoundingBox2 {
    pub fn new(min: Point2, max: Point2) -> BoundingBox2 {
        BoundingBox2 { min, max }
    }

    pub fn range(&self) -> Vector2 {
        self.max - self.min
    }

    pub fn min_x(&self) -> f32 {
        self.min.x
    }

    pub fn max_x(&self) -> f32 {
        self.max.x
    }

    pub fn range_x(&self) -> f32 {
        self.max.x - self.min.x
    }

    pub fn min_y(&self) -> f32 {
        self.min.y
    }

    pub fn max_y(&self) -> f32 {
        self.max.y
    }

    pub fn range_y(&self) -> f32 {
        self.max.y - self.min.y
    }

    pub fn min_xy(&self) -> Point2 {
        self.min
    }

    pub fn max_xy(&self) -> Point2 {
        self.max
    }

    pub fn min_x_max_y(&self) -> Point2 {
        Point2 {
            x: self.min.x,
            y: self.max.y,
        }
    }

    pub fn max_x_min_y(&self) -> Point2 {
        Point2 {
            x: self.max.x,
            y: self.min.y,
        }
    }

    pub fn expand(&mut self, other: BoundingBox2) {
        self.min.x = self.min.x.min(other.min.x);
        self.min.y = self.min.y.min(other.min.y);
        self.max.x = self.max.x.max(other.max.x);
        self.max.y = self.max.y.max(other.max.y);
    }

    pub fn expand_to_point(&mut self, point: Point2) {
        self.min.x = self.min.x.min(point.x);
        self.min.y = self.min.y.min(point.y);
        self.max.x = self.max.x.max(point.x);
        self.max.y = self.max.y.max(point.y);
    }
}

impl std::fmt::Display for BoundingBox2 {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{} @ {}", self.range(), self.min)
    }
}
