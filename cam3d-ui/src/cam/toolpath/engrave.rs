use crate::cam::geometry2d::MultiLineString;
use crate::cam::geometry3d::Plane;
use crate::cam::toolpath::{Toolpath, ToolpathCommand};

pub fn engrave_multilinestring(
    multilinestring: &MultiLineString,
    tool: String,
    feedrate: f32,
    travel_z: f32,
    start_z: f32,
    end_z: f32,
    step_z: f32,
) -> Toolpath {
    let mut commands = Vec::new();

    commands.push(ToolpathCommand::RapidToZ(travel_z));

    for linestring in multilinestring.linestrings.iter() {
        let mut z = start_z - step_z;
        while z > end_z {
            if let [first_point, remaining_points @ ..] = linestring.points.as_slice() {
                commands.push(ToolpathCommand::RapidTo(
                    first_point.into_3d(&Plane::xy(travel_z)),
                ));

                commands.push(ToolpathCommand::MoveTo(
                    first_point.into_3d(&Plane::xy(z)),
                    feedrate,
                ));

                for point in remaining_points {
                    commands.push(ToolpathCommand::MoveTo(
                        point.into_3d(&Plane::xy(z)),
                        feedrate,
                    ));
                }
            }

            commands.push(ToolpathCommand::RapidToZ(travel_z));

            z -= step_z;
        }

        if let [first_point, remaining_points @ ..] = linestring.points.as_slice() {
            commands.push(ToolpathCommand::RapidTo(
                first_point.into_3d(&Plane::xy(travel_z)),
            ));

            commands.push(ToolpathCommand::MoveTo(
                first_point.into_3d(&Plane::xy(end_z)),
                feedrate,
            ));

            for point in remaining_points {
                commands.push(ToolpathCommand::MoveTo(
                    point.into_3d(&Plane::xy(end_z)),
                    feedrate,
                ));
            }
        }

        commands.push(ToolpathCommand::RapidToZ(travel_z));
    }

    Toolpath { tool, commands }
}
