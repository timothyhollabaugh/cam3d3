use crate::cam::geometry2d::{
    LineString, MedialAxis, MedialAxisNode, MultiLineString, MultiPolygon, Point2,
};
use crate::cam::geometry3d::Plane;
use crate::cam::toolpath::ToolpathCommand;
use itertools::Itertools;
use log::{debug, trace, warn};
use ordered_float::OrderedFloat;
use petgraph::algo::astar;
use petgraph::graph::{EdgeIndex, NodeIndex};
use petgraph::prelude::EdgeRef;
use std::collections::HashSet;
use std::f32::consts::PI;

#[derive(Clone, Copy, Debug)]
struct ClearEdgeSegment {
    edge_index: EdgeIndex,
    reverse_node_order: bool,
    clear_end: bool,
}

#[derive(Clone, Copy, Debug)]
enum AdaptiveClearSegment {
    ClearNode(NodeIndex),
    ClearEdge(ClearEdgeSegment),
    TravelCloseToCleared(NodeIndex),
    TravelFarToCleared(NodeIndex),
}

#[derive(Copy, Clone, Debug)]
pub struct AdaptiveClearConfig {
    pub tool_diameter: f32,
    pub min_radius: f32,
    pub helix_start_z: f32,
    pub helix_radius: f32,
    pub helix_step: f32,
    pub helix_feed: f32,
    pub cut_stepover: f32,
    pub cut_lead_in: f32,
    pub cut_lead_out: f32,
    pub cut_feed: f32,
    pub cut_z: f32,
    pub finish_lead_in: f32,
    pub finish_lead_out: f32,
    pub finish_xy: f32,
    pub finish_feed: f32,
    pub travel_feed_xy: f32,
    pub travel_feed_z: f32,
    pub travel_close_z: f32,
    pub travel_far_z: f32,
    pub points_per_turn: f32,
}

impl AdaptiveClearConfig {
    fn travel_close_cost(&self, distance: f32) -> f32 {
        self.travel_feed_z * (self.travel_close_z - self.cut_z) * 2.0
            + self.travel_feed_xy * distance
    }

    fn travel_far_cost(&self, distance: f32) -> f32 {
        self.travel_feed_z * (self.travel_far_z - self.cut_z) * 2.0 + self.travel_feed_xy * distance
    }
}

fn adaptive_clear_order_non_recursive(
    config: AdaptiveClearConfig,
    axis: &MedialAxis,
) -> Vec<Vec<AdaptiveClearSegment>> {
    let mut segment_groups = Vec::new();

    let mut cleared_nodes = HashSet::new();

    while let Some(start_node_index) = axis
        .graph
        .node_indices()
        .filter(|node_index| !cleared_nodes.contains(node_index))
        .max_by_key(|&node_index| OrderedFloat(axis.graph[node_index].radius))
    {
        cleared_nodes.insert(start_node_index);
        let mut segments = Vec::new();

        segments.push(AdaptiveClearSegment::ClearNode(start_node_index));

        let mut node_index = start_node_index;
        let mut cleared_edges = HashSet::new();

        debug!("graph {:#?}", axis.graph);

        loop {
            trace!(
                "node_index {:?}, next_edges {:?}, cleared_edges {:?}",
                node_index,
                axis.graph.edges(node_index).collect_vec(),
                cleared_edges
            );

            let mut next_edges = axis
                .graph
                .edges(node_index)
                .filter(|edge_ref| !cleared_edges.contains(&edge_ref.id()))
                .peekable();

            let next_edge = next_edges.next();

            if let Some(next_edge) = next_edge {
                let (next_node_index0, next_node_index1) =
                    axis.graph.edge_endpoints(next_edge.id()).unwrap();

                let (next_node_index, reverse_node_order) = if node_index == next_node_index0 {
                    (next_node_index1, false)
                } else if node_index == next_node_index1 {
                    (next_node_index0, true)
                } else {
                    panic!("The node and edge do not connect!")
                };

                //debug!("next_node_index {:?}", next_node_index);

                segments.push(AdaptiveClearSegment::ClearEdge(ClearEdgeSegment {
                    edge_index: next_edge.id(),
                    reverse_node_order,
                    clear_end: next_edges.next().is_some(),
                }));

                cleared_edges.insert(next_edge.id());
                node_index = next_node_index;
            } else {
                let next_node = axis
                    .graph
                    .node_indices()
                    .filter(|next_node_index| {
                        axis.graph
                            .edges(*next_node_index)
                            .any(|edge_ref| cleared_edges.contains(&edge_ref.id()))
                            && axis
                                .graph
                                .edges(*next_node_index)
                                .any(|edge_ref| !cleared_edges.contains(&edge_ref.id()))
                    })
                    .map(|next_node_index| {
                        let travel_close_path = astar(
                            &axis.graph,
                            node_index,
                            |goal_node_index| next_node_index == goal_node_index,
                            |edge_ref| {
                                config.travel_close_cost(edge_ref.weight().linear_distance())
                            },
                            |estimate_node_index| {
                                config.travel_close_cost(
                                    (axis.graph[estimate_node_index].center
                                        - axis.graph[node_index].center)
                                        .magnitude(),
                                )
                            },
                        );

                        let travel_far_cost = config.travel_far_cost(
                            (axis.graph[next_node_index].center - axis.graph[node_index].center)
                                .magnitude(),
                        );

                        if let Some((travel_close_cost, travel_close_path)) = travel_close_path {
                            if travel_close_cost <= travel_far_cost {
                                let travel_close_segments = travel_close_path
                                    .into_iter()
                                    .map(|path_node_index| {
                                        AdaptiveClearSegment::TravelCloseToCleared(path_node_index)
                                    })
                                    .collect();

                                (next_node_index, travel_close_cost, travel_close_segments)
                            } else {
                                (
                                    next_node_index,
                                    travel_far_cost,
                                    vec![AdaptiveClearSegment::TravelFarToCleared(next_node_index)],
                                )
                            }
                        } else {
                            (
                                next_node_index,
                                travel_far_cost,
                                vec![AdaptiveClearSegment::TravelFarToCleared(next_node_index)],
                            )
                        }
                    })
                    .min_by_key(|(next_node_index, cost, segments)| OrderedFloat(*cost));

                if let Some((next_node_index, cost, next_segments)) = next_node {
                    segments.extend(next_segments);
                    node_index = next_node_index;
                } else {
                    break;
                }
            }

            cleared_nodes.insert(node_index);
        }

        segment_groups.push(segments);
    }

    debug!("adaptive clear segments order done");

    segment_groups
}

fn generate_circular(
    config: AdaptiveClearConfig,
    axis: &MedialAxis,
    segments: &[AdaptiveClearSegment],
    tool_diameter: f32,
    xy_to_leave: f32,
    i: usize,
) -> (Vec<ToolpathCommand>, Vec<(String, MultiLineString)>) {
    let mut debug = Vec::new();

    let mut commands = Vec::new();

    let mut cut_commands_debug = MultiLineString::new();
    let mut travel_close_commands_debug = MultiLineString::new();
    let mut travel_far_commands_debug = MultiLineString::new();

    let mut cut_commands_linestring: Option<LineString> = None;
    let mut travel_close_commands_linestring: Option<LineString> = None;
    let mut travel_far_commands_linestring: Option<LineString> = None;

    let mut segments = segments.iter().peekable();

    while let Some(segment) = segments.next() {
        debug!("segment {:?}", segment);
        match segment {
            AdaptiveClearSegment::ClearNode(node_index) => {
                travel_close_commands_debug
                    .linestrings
                    .extend(travel_close_commands_linestring.take());
                travel_far_commands_debug
                    .linestrings
                    .extend(travel_far_commands_linestring.take());

                let node = axis.graph[*node_index];

                let helix_radius = f32::min(
                    config.helix_radius,
                    node.radius - config.tool_diameter / 2.0 - config.finish_xy,
                );

                let points = LineString::circle(helix_radius, config.points_per_turn as usize)
                    + node.center.as_vector_from_zero();

                if let Some(first) = points.points.first() {
                    commands.push(ToolpathCommand::RapidTo(
                        first.into_3d(&Plane::xy(config.travel_far_z)),
                    ));
                    commands.push(ToolpathCommand::MoveTo(
                        first.into_3d(&Plane::xy(config.helix_start_z)),
                        config.helix_feed,
                    ));
                }

                let mut z = config.helix_start_z;
                while z >= config.cut_z {
                    let points = (0..config.points_per_turn as usize).map(|n| {
                        let t = n as f32 / config.points_per_turn;
                        let a = PI * 2.0 * t;
                        let z = z - t * config.helix_step;
                        let z = z.max(config.cut_z);
                        let p = Point2::new(helix_radius * f32::cos(a), helix_radius * f32::sin(a))
                            + node.center.as_vector_from_zero();

                        p.into_3d(&Plane::xy(z))
                    });

                    commands.extend(
                        points
                            .clone()
                            .map(|p| ToolpathCommand::MoveTo(p, config.helix_feed)),
                    );

                    z -= config.helix_step;
                }

                commands.extend(points.points.iter().map(|p| {
                    ToolpathCommand::MoveTo(p.into_3d(&Plane::xy(config.cut_z)), config.helix_feed)
                }));

                let mut radius = helix_radius;

                while radius < node.radius - tool_diameter / 2.0 - config.finish_xy {
                    let points = (0..config.points_per_turn as usize).map(|n| {
                        let t = n as f32 / config.points_per_turn;
                        let a = PI * 2.0 * t;
                        let r = f32::min(
                            radius + t * config.cut_stepover,
                            node.radius - tool_diameter / 2.0 - config.finish_xy,
                        );
                        Point2::new(r * f32::cos(a), r * f32::sin(a))
                            + node.center.as_vector_from_zero()
                    });

                    commands.extend(points.clone().map(|p| {
                        ToolpathCommand::MoveTo(
                            p.into_3d(&Plane::xy(config.cut_z)),
                            config.cut_feed,
                        )
                    }));

                    cut_commands_linestring
                        .get_or_insert_default()
                        .points
                        .extend(points);

                    radius += config.cut_stepover;
                }

                let points = LineString::circle(
                    node.radius - tool_diameter / 2.0 - config.finish_xy,
                    config.points_per_turn as usize,
                ) + node.center.as_vector_from_zero();

                commands.extend(points.points.iter().map(|p| {
                    ToolpathCommand::MoveTo(p.into_3d(&Plane::xy(config.cut_z)), config.cut_feed)
                }));

                cut_commands_linestring
                    .get_or_insert_default()
                    .points
                    .extend(points.points);
            }
            AdaptiveClearSegment::ClearEdge(clear_edge_segment) => {
                travel_close_commands_debug
                    .linestrings
                    .extend(travel_close_commands_linestring.take());
                travel_far_commands_debug
                    .linestrings
                    .extend(travel_far_commands_linestring.take());

                let mut clear_path = Vec::new();
                clear_path.push(clear_edge_segment);

                while let Some(AdaptiveClearSegment::ClearEdge(clear_edge_segment)) =
                    segments.peek()
                {
                    trace!("stealing segment {:?}", segments.peek().unwrap());
                    clear_path.push(clear_edge_segment);
                    segments.next();
                }

                let mut clear_path = clear_path.into_iter().peekable();

                let mut remaining_center_distance = 0.0;

                let first_clear_edge_segment = clear_path.peek().unwrap();
                let mut last_generated_node = if first_clear_edge_segment.reverse_node_order {
                    let (_, node_index) = axis
                        .graph
                        .edge_endpoints(first_clear_edge_segment.edge_index)
                        .unwrap();
                    axis.graph[node_index]
                } else {
                    let (node_index, _) = axis
                        .graph
                        .edge_endpoints(first_clear_edge_segment.edge_index)
                        .unwrap();
                    axis.graph[node_index]
                };

                //trace!("Clear edge");

                // Go through the edges and generate arcs at the right spacing
                while let Some(clear_edge_segment) = clear_path.next() {
                    trace!("clear edge segment {:?}", clear_edge_segment);
                    let edge = axis.graph[clear_edge_segment.edge_index];
                    let (node0_index, node1_index) = if clear_edge_segment.reverse_node_order {
                        let (node1_index, node0_index) = axis
                            .graph
                            .edge_endpoints(clear_edge_segment.edge_index)
                            .unwrap();
                        (node0_index, node1_index)
                    } else {
                        axis.graph
                            .edge_endpoints(clear_edge_segment.edge_index)
                            .unwrap()
                    };

                    let node0 = axis.graph[node0_index];
                    let node1 = axis.graph[node1_index];

                    trace!("node0 {:?} node1 {:?} edge {:?}", node0, node1, edge);

                    let reverse_edge = (node0.center - edge.start).magnitude()
                        > (node0.center - edge.end).magnitude();

                    let direction = (node1.center - node0.center).normalized();

                    // TODO If the two nodes are very close together, the center delta can be small
                    // enough that it's within the noise of the radius calculated from the medial
                    // axis (~10^-6), which results in the ratio being larger than it should be
                    // and the center step too small. Luckily that doesn't hurt anything other than
                    // an extra arc generated
                    let center_delta = edge.total_distance();
                    let radius_delta = node1.radius - node0.radius;

                    let ratio = radius_delta / center_delta;

                    if ratio > -1.0 && center_delta > 0.0 {
                        let center_step = config.cut_stepover / (1.0 + radius_delta / center_delta);
                        let center_step = f32::min(
                            center_step,
                            f32::min(node0.radius, node1.radius)
                                - config.tool_diameter / 2.0
                                - xy_to_leave
                                + 1e-3,
                        );

                        trace!(
                            "node0 {:?} node1 {:?} center_delta {} radius_delta {} center_step {}",
                            node0,
                            node1,
                            center_delta,
                            radius_delta,
                            center_step
                        );

                        let mut center_distance = -remaining_center_distance;
                        let mut first = true;

                        // Loop through arcs generated on this edge
                        loop {
                            let next_center_distance = f32::max(center_distance + center_step, 0.0);

                            trace!(
                                    "center_distance {} center_step {} next_center_distance {} center_delta {} remaining_center_distance {} radius_delta {} center_delta {} ratio {}",
                                    center_distance,
                                    center_step,
                                    next_center_distance,
                                    center_delta,
                                    remaining_center_distance,
                                    radius_delta,
                                    center_delta,
                                    ratio
                                );

                            // We've reached the end of the edge
                            if next_center_distance > center_delta {
                                if clear_path.peek().is_some() {
                                    remaining_center_distance = center_delta - center_distance;
                                } else {
                                    remaining_center_distance = 0.0;
                                }

                                if clear_path.peek().is_none() || clear_edge_segment.clear_end {
                                    if (node1.center - last_generated_node.center).magnitude() > 0.0
                                    {
                                        // Generate the points for the cutting arc
                                        let points =
                                            if node1.radius > tool_diameter / 2.0 + xy_to_leave {
                                                let mut arc_points = node1.non_overlapping_arc(
                                                    last_generated_node,
                                                    config.points_per_turn,
                                                    tool_diameter + xy_to_leave * 2.0,
                                                );

                                                // Lead in
                                                if let Some(&first) = arc_points.points.first() {
                                                    let lead_in_distance = f32::min(
                                                        config.cut_lead_in,
                                                        node1.radius
                                                            - config.tool_diameter / 2.0
                                                            - xy_to_leave,
                                                    );

                                                    let lead_in_point = first
                                                        + lead_in_distance
                                                            * (node1.center - first).normalized();

                                                    arc_points.points.insert(0, lead_in_point);
                                                }

                                                // Lead out
                                                if let Some(&last) = arc_points.points.last() {
                                                    let lead_out_distance = f32::min(
                                                        config.cut_lead_out,
                                                        node1.radius
                                                            - config.tool_diameter / 2.0
                                                            - xy_to_leave,
                                                    );

                                                    let lead_out_point = last
                                                        + lead_out_distance
                                                            * (node1.center - last).normalized();

                                                    arc_points.points.push(lead_out_point);
                                                }

                                                arc_points
                                            } else {
                                                LineString {
                                                    points: vec![
                                                        last_generated_node.center,
                                                        node1.center,
                                                    ],
                                                }
                                            };

                                        if let [rapid_point, move_points @ ..] =
                                            points.points.as_slice()
                                        {
                                            commands.push(ToolpathCommand::RapidTo(
                                                rapid_point.into_3d(&Plane::xy(config.cut_z)),
                                            ));

                                            // Push the arc points to the toolpath
                                            commands.extend(move_points.iter().map(|p| {
                                                ToolpathCommand::MoveTo(
                                                    p.into_3d(&Plane::xy(config.cut_z)),
                                                    config.cut_feed,
                                                )
                                            }));
                                        }

                                        cut_commands_linestring
                                            .get_or_insert_default()
                                            .points
                                            .extend(points.points);
                                    }

                                    last_generated_node = node1;
                                }

                                break;
                            } else {
                                // Generate an arc somewhere in the middle of the edge

                                remaining_center_distance = 0.0;

                                let (next_center, next_radius) = edge
                                    .point_radius_at_distance(next_center_distance, reverse_edge);

                                if next_radius > node0.radius + 0.1
                                    && next_radius > node1.radius + 0.1
                                {
                                    warn!("Radius too big. next_center {next_center:?}, next_radius {next_radius:?}, node0 {node0:?}, node1 {node1:?}, edge {edge:?}",);
                                }

                                let next_generated_node = MedialAxisNode {
                                    center: next_center,
                                    radius: next_radius,
                                };

                                //debug!(
                                //"next_radius {:?}, next_center {:?}",
                                //next_radius, next_center
                                //);

                                if (next_center - last_generated_node.center).magnitude() > 0.0 {
                                    let points = if next_radius > tool_diameter / 2.0 + xy_to_leave
                                    {
                                        assert!(!last_generated_node.center.x.is_nan());
                                        assert!(!last_generated_node.center.y.is_nan());
                                        assert!(!last_generated_node.radius.is_nan());

                                        let mut arc_points = next_generated_node
                                            .non_overlapping_arc(
                                                last_generated_node,
                                                config.points_per_turn,
                                                tool_diameter + xy_to_leave * 2.0,
                                            );

                                        // Lead in
                                        if let Some(&first) = arc_points.points.first() {
                                            let lead_in_distance = f32::min(
                                                config.cut_lead_in,
                                                next_generated_node.radius
                                                    - config.tool_diameter / 2.0
                                                    - xy_to_leave,
                                            );

                                            let lead_in_point = first
                                                + lead_in_distance
                                                    * (next_generated_node.center - first)
                                                        .normalized();

                                            arc_points.points.insert(0, lead_in_point);
                                        }

                                        // Lead out
                                        if let Some(&last) = arc_points.points.last() {
                                            let lead_out_distance = f32::min(
                                                config.cut_lead_out,
                                                next_generated_node.radius
                                                    - config.tool_diameter / 2.0
                                                    - xy_to_leave,
                                            );

                                            let lead_out_point = last
                                                + lead_out_distance
                                                    * (next_generated_node.center - last)
                                                        .normalized();

                                            arc_points.points.push(lead_out_point);
                                        }

                                        arc_points
                                    } else {
                                        assert!(!next_center.x.is_nan());
                                        assert!(!next_center.y.is_nan());

                                        LineString {
                                            points: vec![last_generated_node.center, next_center],
                                        }
                                    };

                                    if let [rapid_point, move_points @ ..] =
                                        points.points.as_slice()
                                    {
                                        commands.push(ToolpathCommand::RapidTo(
                                            rapid_point.into_3d(&Plane::xy(config.cut_z)),
                                        ));

                                        // Push the arc points to the toolpath
                                        commands.extend(move_points.iter().map(|p| {
                                            ToolpathCommand::MoveTo(
                                                p.into_3d(&Plane::xy(config.cut_z)),
                                                config.cut_feed,
                                            )
                                        }));
                                    }

                                    cut_commands_linestring
                                        .get_or_insert_default()
                                        .points
                                        .extend(points.points);
                                }

                                center_distance = next_center_distance;
                                last_generated_node = next_generated_node;
                            }

                            first = false;
                        }
                    } else if center_delta < 1e-3 {
                        trace!("Zero length edge, skipping. ratio {ratio} center_delta {center_delta} ");
                    } else {
                        panic!("ratio {ratio} center_delta {center_delta}");
                    }
                }
            }
            AdaptiveClearSegment::TravelCloseToCleared(node_index) => {
                let node = axis.graph[*node_index];

                commands.push(ToolpathCommand::RapidToZ(config.travel_close_z));
                commands.push(ToolpathCommand::RapidTo(
                    node.center.into_3d(&Plane::xy(config.travel_close_z)),
                ));

                if let Some(last_point) = cut_commands_linestring
                    .as_ref()
                    .and_then(|l| l.points.last().cloned())
                {
                    travel_close_commands_linestring
                        .get_or_insert_default()
                        .points
                        .push(last_point);
                }

                travel_close_commands_linestring
                    .get_or_insert_default()
                    .points
                    .push(node.center);

                cut_commands_debug
                    .linestrings
                    .extend((cut_commands_linestring.take()));
                travel_far_commands_debug
                    .linestrings
                    .extend(travel_far_commands_linestring.take());
            }
            AdaptiveClearSegment::TravelFarToCleared(node_index) => {
                let node = axis.graph[*node_index];

                commands.push(ToolpathCommand::RapidToZ(config.travel_far_z));
                commands.push(ToolpathCommand::RapidTo(
                    node.center.into_3d(&Plane::xy(config.travel_far_z)),
                ));

                if let Some(last_point) = cut_commands_linestring
                    .as_ref()
                    .and_then(|l| l.points.last().cloned())
                {
                    travel_far_commands_linestring
                        .get_or_insert_default()
                        .points
                        .push(last_point);
                }

                travel_far_commands_linestring
                    .get_or_insert_default()
                    .points
                    .push(node.center);

                cut_commands_debug
                    .linestrings
                    .extend((cut_commands_linestring.take()));
                travel_close_commands_debug
                    .linestrings
                    .extend(travel_close_commands_linestring.take());
            }
        }
    }

    cut_commands_debug
        .linestrings
        .extend((cut_commands_linestring.take()));
    travel_close_commands_debug
        .linestrings
        .extend(travel_close_commands_linestring.take());
    travel_far_commands_debug
        .linestrings
        .extend(travel_far_commands_linestring.take());

    debug.push((format!("Cut Commands {}", i), cut_commands_debug));
    debug.push((
        format!("Travel Close Commands {}", i),
        travel_close_commands_debug,
    ));
    debug.push((
        format!("Travel Far Commands {}", i),
        travel_far_commands_debug,
    ));

    (commands, debug)
}

pub fn clear_polygons_adaptive(
    clear_polygons: MultiPolygon,
    config: AdaptiveClearConfig,
) -> (Vec<ToolpathCommand>, Vec<(String, MultiLineString)>) {
    let mut debug_linestrings = Vec::new();

    debug!("Calculating clear polygons");

    let min_radius = f32::max(config.min_radius, config.tool_diameter / 2.0);

    let mut commands = Vec::new();

    for (polygon_index, polygon) in clear_polygons.polygons.into_iter().enumerate() {
        debug!("Clearing polygon {}: {}", polygon_index, &polygon);
        commands.push(ToolpathCommand::Comment(format!(
            "Clear polygon {polygon_index} ({polygon})"
        )));
        commands.push(ToolpathCommand::Comment(format!(
            "Cut Z {}, Travel Close Z {}, Travel Far Z {}",
            config.cut_z, config.travel_close_z, config.travel_far_z
        )));

        debug!("Generating medial axis");
        let mut medial_axis = polygon.medial_axis();
        medial_axis.filter_min_radius(min_radius);

        //for node in medial_axis.graph.node_weights_mut() {
        //node.radius -= config.tool_diameter / 2.0;
        //}

        //medial_axis
        //.graph
        //.retain_nodes(|graph, node_index| graph[node_index].radius > 0.01);

        debug!("Nodes debug");
        let mut nodes_debug = MultiLineString {
            linestrings: medial_axis
                .graph
                .node_weights()
                .map(|node| LineString::circle(node.radius, 12) + node.center.as_vector_from_zero())
                .collect(),
        };
        debug_linestrings.push((format!("Nodes Debug {}", polygon_index), nodes_debug));

        debug!("Edges debug");
        let mut edges_debug = MultiLineString {
            linestrings: medial_axis
                .graph
                .edge_indices()
                .map(|edge_index| {
                    let edge = medial_axis.graph[edge_index];

                    trace!(
                        "debug index {:?} edge {:?} total_distance {}",
                        edge_index,
                        edge,
                        edge.total_distance()
                    );

                    LineString {
                        //points: (0..10).map(|i| i as f32 / 10.0).map(|t| t * edge.total_distance()).map(|d| edge.point_radius_at_distance(d).0).collect(),
                        points: (0..=10)
                            .map(|i| i as f32 / 10.0)
                            .map(|t| t * edge.total_distance())
                            .map(|d| edge.point_radius_at_distance(d, false).0)
                            .collect(),
                    }
                })
                .collect(),
        };

        debug_linestrings.push((format!("Edges Debug {}", polygon_index), edges_debug));

        debug!("Adaptive clear segment order");
        //let (commands, cost) = adaptive_clear_order(config, &medial_axis, HashSet::new());
        let segment_groups = adaptive_clear_order_non_recursive(config, &medial_axis);

        let mut last_point = commands.iter().rev().find_map(|command| match command {
            ToolpathCommand::Comment(_) => None,
            ToolpathCommand::MoveTo(p, _) => Some(p.into_2d()),
            ToolpathCommand::RapidTo(p) => Some(p.into_2d()),
            ToolpathCommand::RapidToZ(_) => None,
        });

        for segments in segment_groups {
            debug!("Clear segments debug");
            let mut clear_edge_segments_debug = MultiLineString::new();

            let mut edge_commands_linestring = None;

            for segment in segments.iter() {
                if let AdaptiveClearSegment::ClearEdge(clear_edge_segment) = segment {
                    let (node_index0, node_index1) = medial_axis
                        .graph
                        .edge_endpoints(clear_edge_segment.edge_index)
                        .unwrap();

                    let node0 = medial_axis.graph[node_index0];
                    let node1 = medial_axis.graph[node_index1];

                    edge_commands_linestring
                        .get_or_insert_with(|| LineString {
                            points: vec![node0.center],
                        })
                        .points
                        .push(node1.center);

                    clear_edge_segments_debug.linestrings.push(
                        LineString::circle(node0.radius, 12) + node0.center.as_vector_from_zero(),
                    );

                    clear_edge_segments_debug.linestrings.push(
                        LineString::circle(node1.radius, 12) + node1.center.as_vector_from_zero(),
                    );
                } else {
                    clear_edge_segments_debug
                        .linestrings
                        .extend(edge_commands_linestring.take());
                }
            }
            clear_edge_segments_debug
                .linestrings
                .extend(edge_commands_linestring.take());

            debug_linestrings.push((
                format!("Clear Edge Commands Debug {}", polygon_index),
                clear_edge_segments_debug,
            ));

            trace!("Generate circular");
            let (polygon_commands, debug) = generate_circular(
                config,
                &medial_axis,
                &segments,
                config.tool_diameter,
                config.finish_xy,
                polygon_index,
            );

            commands.extend(polygon_commands);
            debug_linestrings.extend(debug);

            last_point = commands.iter().rev().find_map(|command| match command {
                ToolpathCommand::Comment(_) => None,
                ToolpathCommand::MoveTo(p, _) => Some(p.into_2d()),
                ToolpathCommand::RapidTo(p) => Some(p.into_2d()),
                ToolpathCommand::RapidToZ(_) => None,
            });
        }

        debug!("Finish paths");
        if let Some(last_point) = last_point {
            debug!("Finishing polygon: {}", &polygon);
            let finish_polygons = polygon.offset(-min_radius);
            let finish_polygons = finish_polygons.offset(min_radius - config.tool_diameter / 2.0);
            debug!(
                "Offeset finish polygons: {}",
                finish_polygons.polygons.len()
            );

            for finish_polygon in finish_polygons.polygons {
                debug!("Finishing offset polygon: {}", &finish_polygon);
                let finish_paths = std::iter::once(finish_polygon.exterior).chain(
                    finish_polygon.interiors.into_iter().map(|mut interior| {
                        interior.points.reverse();
                        interior
                    }),
                );

                for mut finish_path in finish_paths {
                    debug!("Finish path: {}", &finish_path);

                    let start_node_and_point = medial_axis
                        .graph
                        .node_weights()
                        .filter_map(|node| {
                            finish_path
                                .points
                                .iter()
                                .enumerate()
                                .filter(|(_, point)| {
                                    (**point - node.center).magnitude()
                                        <= node.radius - config.finish_xy + 1e-3
                                })
                                .min_by_key(|(_, point)| {
                                    OrderedFloat((**point - node.center).magnitude())
                                })
                                .map(|(i, point)| (node, i, point))
                        })
                        .max_by_key(|(node, _, _)| OrderedFloat(node.radius));

                    if let Some((node, i, &start_point)) = start_node_and_point {
                        debug!("Closest node: {:?}, start_point: {}", node, &start_point);

                        finish_path.open();
                        finish_path.points.rotate_left(i);
                        finish_path.close();

                        assert_eq!(finish_path.points.first(), Some(start_point).as_ref());

                        let lead_in_distance = f32::min(config.finish_lead_in, node.radius);
                        let lead_in_point = start_point
                            + (node.center - start_point).normalized() * lead_in_distance;

                        commands.extend([
                            ToolpathCommand::RapidToZ(config.travel_far_z),
                            ToolpathCommand::RapidTo(
                                lead_in_point.into_3d(&Plane::xy(config.travel_far_z)),
                            ),
                            ToolpathCommand::MoveTo(
                                lead_in_point.into_3d(&Plane::xy(config.cut_z)),
                                config.finish_feed,
                            ),
                            ToolpathCommand::MoveTo(
                                start_point.into_3d(&Plane::xy(config.cut_z)),
                                config.finish_feed,
                            ),
                        ]);

                        commands.extend(finish_path.points.iter().map(|p| {
                            ToolpathCommand::MoveTo(
                                p.into_3d(&Plane::xy(config.cut_z)),
                                config.finish_feed,
                            )
                        }));

                        let lead_out_distance = f32::min(config.finish_lead_out, node.radius);
                        let lead_out_point = start_point
                            + (node.center - start_point).normalized() * lead_out_distance;

                        commands.extend([ToolpathCommand::MoveTo(
                            lead_out_point.into_3d(&Plane::xy(config.cut_z)),
                            config.finish_feed,
                        )]);
                    } else {
                        warn!("No start point or node")
                    }
                }
            }

            commands.push(ToolpathCommand::RapidToZ(config.travel_far_z));

            debug!("Done polygon {}", polygon_index);
        }

        commands.push(ToolpathCommand::Comment(format!(
            "Done polygon {polygon_index}"
        )));
    }

    debug!("Done");

    (commands, debug_linestrings)
}
