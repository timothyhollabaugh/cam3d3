use crate::cam::geometry2d::{
    difference, intersection, union, Geometry, LineString, MultiPolygon, Point2, Polygon, Vector2,
};
use crate::cam::geometry3d::{intersect_mesh_z, IntersectMeshZError, Plane, Point3, Vector3};
use crate::cam::toolpath::adaptive_2d::{clear_polygons_adaptive, AdaptiveClearConfig};
use crate::cam::toolpath::{Toolpath, ToolpathCommand};
use itertools::Itertools;
use log::trace;
use ordered_float::OrderedFloat;
use std::collections::{BTreeMap, BTreeSet, HashMap};
use tri_mesh::Mesh;

pub struct Clear3dAdaptiveConfig<'mesh> {
    pub mesh: &'mesh Mesh,
    pub start_z: f32,
    pub end_z: f32,
    pub travel_height: f32,
    pub step_depth: f32,
    pub exterior: Option<f32>,
    pub tool: String,
    pub tool_diameter: f32,
    pub min_radius: f32,
    pub helix_radius: f32,
    pub helix_step: f32,
    pub helix_feed: f32,
    pub cut_stepover: f32,
    pub cut_lead_in: f32,
    pub cut_lead_out: f32,
    pub cut_feed: f32,
    pub finish_lead_in: f32,
    pub finish_lead_out: f32,
    pub finish_xy: f32,
    pub finish_feed: f32,
    pub travel_feed_xy: f32,
    pub travel_feed_z: f32,
    pub tabs_length: f32,
    pub tabs_height: f32,
    pub points_per_turn: f32,
}

pub fn clear_3d_adaptive(config: Clear3dAdaptiveConfig) -> Result<Toolpath, IntersectMeshZError> {
    let mut commands = Vec::new();

    let bounding_box = config.mesh.axis_aligned_bounding_box();

    commands.extend(
        [
            format!(
                "Adaptive Clearing Solid <{}, {}, {}> @ ({}, {}, {})",
                bounding_box.size().x,
                bounding_box.size().y,
                bounding_box.size().z,
                bounding_box.min().x,
                bounding_box.min().y,
                bounding_box.min().z,
            ),
            format!("with tool {}", config.tool),
        ]
        .into_iter()
        .map(ToolpathCommand::Comment),
    );

    let z_range = config.start_z - config.end_z;
    let zs = (z_range / config.step_depth) as u32;

    log::debug!("z_range: {}, z_steps: {}", z_range, zs);

    let step_zs = (1..=zs)
        .map(|i| (config.start_z - i as f32 * config.step_depth, None))
        .chain(std::iter::once((config.end_z, None)))
        .chain(std::iter::once((config.end_z + config.tabs_height, None)));

    log::debug!("Step zs {:?}", step_zs.clone().collect_vec());

    let slope_zs = config
        .mesh
        .face_iter()
        .filter(|face_id| {
            let normal = Vector3::from(config.mesh.face_normal(*face_id)).normalized();
            normal.z > 0.999
        })
        .map(|face_id| {
            let p = config.mesh.face_positions(face_id);
            (p.0.z.max(p.1.z).max(p.2.z) as f32 + 1e-3, Some(p))
        })
        .filter(|(z, _)| *z >= config.end_z && *z <= config.start_z);

    //log::debug!("Slope zs {:?}", slope_zs.clone().collect_vec());

    let mut layer_zs = BTreeMap::new();

    for (z, new_points) in step_zs.chain(slope_zs) {
        let layer_points = layer_zs
            .entry(f32::round(z * 1000.0) as i64)
            .or_insert(Some(vec![]));

        let new_layer_points = match (layer_points.clone(), new_points) {
            (None, None) => None,
            (Some(layer_points), None) => None,
            (None, Some((p1, p2, p3))) => None,
            (Some(mut layer_points), Some((p1, p2, p3))) => {
                layer_points.extend([p1, p2, p3]);
                Some(layer_points)
            }
        };

        *layer_points = new_layer_points
    }

    commands.extend(
        layer_zs
            .iter()
            .rev()
            .map(|(z, _)| format!("z {}", (*z as f32) / 1000.0))
            .map(ToolpathCommand::Comment),
    );

    //let layer_zs: BTreeSet<_> = step_zs.chain(slope_zs).collect();

    log::debug!("Layer zs:");
    for (z, points) in layer_zs.iter() {
        if let Some(points) = points {
            log::debug!("{}: {} points", (*z as f32) / 1000.0, points.len());
        } else {
            log::debug!("{}", (*z as f32) / 1000.0);
        }
    }

    //log::debug!("layer_zs: {:?}", layer_zs);

    let layers: Result<Vec<_>, _> = layer_zs
        .into_iter()
        .rev()
        .map(|(z, points)| {
            let plane = Plane::xy((z as f32) / 1000.0);
            log::debug!("Plane {:?}", plane);
            let mut intersection = intersect_mesh_z(config.mesh, plane.point.z as f64)?;

            if let Some(exterior) = config.exterior {
                if plane.point.z < config.tabs_height {
                    let x_tabs = LineString::rectangle(
                        Point2::new(
                            bounding_box.min().x - exterior,
                            bounding_box.center().y - config.tabs_length / 2.0,
                        ),
                        Point2::new(
                            bounding_box.max().x + exterior,
                            bounding_box.center().y + config.tabs_height / 2.0,
                        ),
                    );

                    let y_tabs = LineString::rectangle(
                        Point2::new(
                            bounding_box.center().x - config.tabs_length / 2.0,
                            bounding_box.min().y - exterior,
                        ),
                        Point2::new(
                            bounding_box.center().x + config.tabs_height / 2.0,
                            bounding_box.max().y + exterior,
                        ),
                    );

                    intersection = difference(
                        std::iter::once(&intersection as &dyn Geometry),
                        [
                            &Polygon::from(x_tabs) as &dyn Geometry,
                            &Polygon::from(y_tabs) as &dyn Geometry,
                        ],
                    );
                }
            }

            Ok((plane, intersection, points))
        })
        .collect();

    let layers = layers?;

    for (plane, polygons, points) in layers.iter() {
        log::debug!(
            "Layer {}: {} polys {:?} points",
            plane.point.z,
            polygons.polygons.len(),
            points.as_ref().map(|p| p.len())
        );
    }

    let outlines = layers
        .iter()
        .map(|(_, p, _)| p.clone().offset(config.exterior.unwrap_or(0.0)))
        .collect_vec();

    let outline: MultiPolygon = union(outlines.iter().map(|p| p as &dyn Geometry));

    let mut layers_tree = LayerTree::new();

    for (layer_depth, (plane, layer_polygons, points)) in layers.into_iter().enumerate() {
        let clear_polygons: MultiPolygon = difference(
            [&outline as &dyn Geometry],
            [&layer_polygons as &dyn Geometry],
        );

        trace!(
            "Building layers tree, depth={} z={} polygons={} clear={}",
            layer_depth,
            plane.point.z,
            layer_polygons.polygons.len(),
            clear_polygons.polygons.len()
        );

        if layer_depth == 0 {
            for polygon in clear_polygons.polygons.into_iter() {
                trace!("Pushing top level polygon node");
                layers_tree.push_layer_node(LayerTreeNode::new(polygon, plane, false));
            }
        } else {
            let last_layer_depth = layer_depth - 1;
            let last_layer_nodes = layers_tree.nodes_at_layer_depth_mut(last_layer_depth);
            for last_layer_node in last_layer_nodes {
                trace!("Adding to last layer");
                let next_layer_polygons: MultiPolygon = intersection(
                    [&clear_polygons as &dyn Geometry],
                    [&last_layer_node.polygon as &dyn Geometry],
                );
                for polygon in next_layer_polygons.polygons.into_iter() {
                    trace!("Pushing to last layer");

                    let skip = if let Some(points) = &points {
                        !points
                            .iter()
                            .map(|point| Point2::new(point.x as f32, point.y as f32))
                            .any(|point| polygon.point_inside(point).in_or_on())
                    } else {
                        false
                    };

                    last_layer_node.push_layer_node(LayerTreeNode::new(polygon, plane, skip));
                }
            }
        }
    }

    //trace!("Layers Tree: {:?}", layers_tree);

    log::debug!("Clear");

    commands.push(ToolpathCommand::RapidToZ(
        config.start_z + config.travel_height,
    ));

    layers_tree.visit_depth_first(
        |depth, node| {
            if !node.skip {
                commands.push(ToolpathCommand::Comment(format!(
                    "Clear tree layer {} [{:.3}]: ({})",
                    depth, node.plane.point.z, node.polygon
                )));

                log::debug!("{} [{:.3}]: {}", depth, node.plane.point.z, node.polygon);

                let cut_z = node.plane.point.z;

                let adaptive_clear_config = AdaptiveClearConfig {
                    tool_diameter: config.tool_diameter,
                    min_radius: config.min_radius,
                    helix_start_z: cut_z + config.step_depth,
                    helix_radius: config.helix_radius,
                    helix_step: config.helix_step,
                    helix_feed: config.helix_feed,
                    cut_stepover: config.cut_stepover,
                    cut_lead_in: config.cut_lead_in,
                    cut_lead_out: config.cut_lead_out,
                    cut_feed: config.cut_feed,
                    cut_z,
                    finish_lead_in: config.finish_lead_in,
                    finish_lead_out: config.finish_lead_out,
                    finish_xy: config.finish_xy,
                    finish_feed: config.finish_feed,
                    travel_feed_xy: 24000.0,
                    travel_feed_z: 12000.0,
                    travel_close_z: cut_z + config.travel_height,
                    travel_far_z: config.start_z + config.travel_height,
                    points_per_turn: config.points_per_turn,
                };

                let (layer_node_commands, debug) = clear_polygons_adaptive(
                    MultiPolygon {
                        polygons: vec![node.polygon.clone()],
                    },
                    adaptive_clear_config,
                );

                //all_debug.extend(debug.into_iter().map(|(name, linestring)| (format!("Layer {}: {}", plane.point.z, name), linestring, node.plane_key.clone())));
                commands.extend(layer_node_commands);

                commands.push(ToolpathCommand::RapidToZ(
                    config.start_z + config.travel_height,
                ));

                commands.push(ToolpathCommand::Comment(format!(
                    "Done tree layer {}",
                    depth
                )));
            }
        },
        |layer_tree_node| {
            let depth = layer_tree_node.max_depth();
            let area = layer_tree_node.polygon.area();
            trace!("depth={} area={}", depth, area);
            (depth, OrderedFloat(area))
        },
    );

    commands.push(ToolpathCommand::RapidToZ(
        config.start_z + config.travel_height,
    ));

    let toolpath = Toolpath {
        commands,
        tool: config.tool.clone(),
    };

    Ok(toolpath)
}

#[derive(Clone, Debug)]
pub struct LayerTreeNode {
    layers: Vec<LayerTreeNode>,
    polygon: Polygon,
    plane: Plane,
    skip: bool,
}

impl LayerTreeNode {
    pub fn new(polygon: Polygon, plane: Plane, skip: bool) -> LayerTreeNode {
        LayerTreeNode {
            layers: Vec::new(),
            polygon,
            plane,
            skip,
        }
    }

    pub fn nodes_at_layer_depth(
        &self,
        layer_depth: usize,
    ) -> Box<dyn Iterator<Item = &LayerTreeNode> + '_> {
        if layer_depth == 0 {
            Box::new(self.layers.iter())
        } else {
            Box::new(
                self.layers
                    .iter()
                    .flat_map(move |sub_layer| sub_layer.nodes_at_layer_depth(layer_depth - 1)),
            )
        }
    }

    pub fn nodes_at_layer_depth_mut(
        &mut self,
        layer_depth: usize,
    ) -> Box<dyn Iterator<Item = &mut LayerTreeNode> + '_> {
        if layer_depth == 0 {
            Box::new(self.layers.iter_mut())
        } else {
            Box::new(
                self.layers
                    .iter_mut()
                    .flat_map(move |sub_layer| sub_layer.nodes_at_layer_depth_mut(layer_depth - 1)),
            )
        }
    }

    pub fn push_layer_node(&mut self, node: LayerTreeNode) {
        self.layers.push(node);
    }

    pub fn visit_depth_first<K: Ord>(
        &self,
        f: &mut dyn FnMut(usize, &LayerTreeNode),
        sort_key: &dyn Fn(&LayerTreeNode) -> K,
        depth: usize,
    ) {
        let mut sorted_layer_nodes = self.layers.iter().collect_vec();
        trace!("sorted_layer_nodes={}", sorted_layer_nodes.len());
        sorted_layer_nodes.sort_by_key(|&layer_tree_node| sort_key(layer_tree_node));
        for layer_node in sorted_layer_nodes {
            f(depth, layer_node);
            layer_node.visit_depth_first(f, sort_key, depth + 1);
        }
    }

    pub fn max_depth(&self) -> usize {
        let mut max_depth = 0;
        self.visit_depth_first(
            &mut |depth, _| max_depth = usize::max(max_depth, depth),
            &|_| (),
            0,
        );
        max_depth
    }
}

#[derive(Clone, Debug)]
pub struct LayerTree {
    layers: Vec<LayerTreeNode>,
}

impl LayerTree {
    pub fn new() -> LayerTree {
        LayerTree { layers: Vec::new() }
    }

    pub fn nodes_at_layer_depth(
        &self,
        layer_depth: usize,
    ) -> Box<dyn Iterator<Item = &LayerTreeNode> + '_> {
        if layer_depth == 0 {
            Box::new(self.layers.iter())
        } else {
            Box::new(
                self.layers
                    .iter()
                    .flat_map(move |sub_layer| sub_layer.nodes_at_layer_depth(layer_depth - 1)),
            )
        }
    }

    pub fn nodes_at_layer_depth_mut(
        &mut self,
        layer_depth: usize,
    ) -> Box<dyn Iterator<Item = &mut LayerTreeNode> + '_> {
        if layer_depth == 0 {
            Box::new(self.layers.iter_mut())
        } else {
            Box::new(
                self.layers
                    .iter_mut()
                    .flat_map(move |sub_layer| sub_layer.nodes_at_layer_depth_mut(layer_depth - 1)),
            )
        }
    }

    pub fn push_layer_node(&mut self, node: LayerTreeNode) {
        self.layers.push(node);
    }

    pub fn visit_depth_first<
        F: FnMut(usize, &LayerTreeNode),
        C: Fn(&LayerTreeNode) -> K,
        K: Ord,
    >(
        &self,
        mut f: F,
        sort_key: C,
    ) {
        let mut sorted_layer_nodes = self.layers.iter().collect_vec();
        trace!("sorted_layer_nodes={}", sorted_layer_nodes.len());
        sorted_layer_nodes.sort_by_key(|layer_tree_node| sort_key(*layer_tree_node));
        for layer_node in sorted_layer_nodes {
            f(0, layer_node);
            layer_node.visit_depth_first(&mut f, &sort_key, 1);
        }
    }
}
