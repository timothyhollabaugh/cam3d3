use crate::cam::geometry2d::MultiLineString;
use crate::cam::geometry3d::Plane;
use crate::cam::toolpath::{Toolpath, ToolpathCommand};

pub fn generate_drag_knife_gcode(
    path: &MultiLineString,
    tool: String,
    knife_radius: f32,
    start_z: f32,
    end_z: f32,
    step_z: f32,
    travel_z: f32,
    cut_feed: f32,
) -> Toolpath {
    let travel_plane = Plane::xy(travel_z);
    let start_plane = Plane::xy(start_z);

    let commands = path
        .linestrings
        .iter()
        .flat_map(|linestring| {
            if let Some(first_point) = linestring.points.first() {
                let zs = ((start_z - end_z) / step_z) as usize;
                Box::new(
                    (0..zs)
                        .map(|z_index| start_z - z_index as f32 * step_z)
                        .chain(std::iter::once(end_z))
                        .flat_map(|z| {
                            let z_plane = Plane::xy(z);
                            [
                                ToolpathCommand::RapidToZ(travel_z),
                                ToolpathCommand::RapidTo(first_point.into_3d(&travel_plane)),
                                ToolpathCommand::RapidTo(first_point.into_3d(&start_plane)),
                                ToolpathCommand::MoveTo(first_point.into_3d(&z_plane), cut_feed),
                            ]
                            .into_iter()
                            .chain(linestring.points.iter().map_windows(
                                move |[last_point, point]| {
                                    let delta = **point - **last_point;
                                    let offset_point = **point + delta.normalized() * knife_radius;
                                    ToolpathCommand::MoveTo(
                                        offset_point.into_3d(&z_plane),
                                        cut_feed,
                                    )
                                },
                            ))
                            .chain(std::iter::once(ToolpathCommand::RapidToZ(travel_z)))
                        }),
                ) as Box<dyn Iterator<Item = _>>
            } else {
                Box::new(std::iter::empty()) as Box<dyn Iterator<Item = _>>
            }
        })
        .chain(std::iter::once(ToolpathCommand::RapidToZ(travel_z)))
        .collect();

    Toolpath { commands, tool }
}
