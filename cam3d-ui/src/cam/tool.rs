pub enum MillingTool {
    Square {
        diameter: f32,
        length: f32,
        corner_radius: f32,
    },
    Conical {
        tip_angle: f32,
        tip_diameter: f32,
        corner_radius: f32,
    },
}
