use crate::cam::geometry2d::{
    difference, intersection, union, Geometry, LineString, MultiLineString, MultiPolygon,
};
use crate::cam::CutType;
use itertools::Itertools;
use ordered_float::OrderedFloat;
use serde::{Deserialize, Serialize};
use std::cmp::Ordering;
use std::collections::HashMap;
use std::hash::Hash;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Tool {
    pub name: String,
    pub diameter: f32,
    pub min_cut_area: f32,
    pub overcut: f32,
}

pub trait ToolKey: Hash + Clone + PartialEq + Eq {}

impl<T> ToolKey for T where T: Hash + Clone + PartialEq + Eq {}

fn cut_outline<T: ToolKey>(
    outline: MultiPolygon,
    _avoid: Vec<MultiPolygon>,
    tools: HashMap<T, Tool>,
    cut_type: CutType,
    mut on_progress: impl FnMut(f64, String),
) -> HashMap<T, MultiLineString> {
    on_progress(0.0, "Cutting".to_string());
    let mut sorted_tools = tools.clone().into_iter().collect::<Vec<_>>();

    sorted_tools.sort_by(|(_, tool1), (_, tool2)| {
        if tool1.diameter > tool2.diameter {
            Ordering::Less
        } else if tool1.diameter < tool2.diameter {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    });

    if let Some((tool_key, tool)) = sorted_tools.first() {
        let offset = match cut_type {
            CutType::Inside => outline.offset(-tool.diameter / 2.0),
            CutType::On => outline,
            CutType::Outside => outline.offset(tool.diameter / 2.0),
        };

        let mut out = HashMap::new();
        out.insert(tool_key.clone(), offset.into_boundary());
        on_progress(1.0, "Done!".to_string());
        out
    } else {
        on_progress(1.0, "Done!".to_string());
        HashMap::new()
    }
}

pub fn optimize_paths(
    mut paths: MultiLineString,
    tool: &Tool,
    mut on_progress: impl FnMut(f64, String),
) -> MultiLineString {
    if !paths.linestrings.is_empty() {
        let first_path = paths.linestrings.remove(0);
        let mut ordered_paths = vec![first_path];

        let num_paths = paths.linestrings.len();

        while paths.linestrings.len() > 0 {
            on_progress(
                1.0 - ((paths.linestrings.len() as f64 - 1.0) / num_paths as f64),
                format!("Optimizing {}", &tool.name),
            );

            let last_path_point = ordered_paths.last_mut().and_then(|path| {
                path.clone()
                    .points
                    .last()
                    .map(|point| (path, point.clone()))
            });

            if let Some((_last_path, last_point)) = last_path_point {
                let closest = paths
                    .linestrings
                    .iter()
                    .enumerate()
                    .flat_map(|(i, path)| path.closest_point(&last_point).map(|path| (i, path)))
                    .min_by_key(|(_, (_, d))| OrderedFloat::from(*d));

                if let Some((path_index, (point_index, _distance))) = closest {
                    let mut closest_path = paths.linestrings.remove(path_index);

                    if let (Some(&start_point), Some(&end_point)) =
                        (closest_path.points.first(), closest_path.points.last())
                    {
                        let almost_closed =
                            (start_point - end_point).magnitude() <= tool.diameter / 2.0;

                        let ordered_closest = if closest_path.points.len() > 2 && almost_closed {
                            let closed = closest_path.is_closed();

                            closest_path.open();

                            let mut closest_path_vec: Vec<_> =
                                closest_path.points.into_iter().collect();
                            closest_path_vec.as_mut_slice().rotate_left(point_index);
                            let mut ordered_closest = LineString {
                                points: closest_path_vec.into_iter().collect(),
                            };

                            if closed {
                                ordered_closest.close();
                            }

                            ordered_closest
                        } else {
                            let start_distance = (start_point - last_point).magnitude();
                            let end_distance = (end_point - last_point).magnitude();

                            if end_distance < start_distance {
                                let mut closest_path_vec: Vec<_> =
                                    closest_path.points.into_iter().collect();
                                closest_path_vec.reverse();
                                let ordered_closest = LineString {
                                    points: closest_path_vec.into_iter().collect(),
                                };

                                ordered_closest
                            } else {
                                closest_path
                            }
                        };

                        ordered_paths.push(ordered_closest);

                        /*
                        if let Some(&first_point) = ordered_closest.points.first() {
                            let rapid = LineString {
                                points: vec![last_point, first_point],
                            };

                            let rapid_length = (first_point - last_point).distance();

                            let cut = rapid.offset(tool.diameter / 2.0);

                            let avoid_cut: MultiLineString = intersection(
                                vec![&cut as &dyn Geometry],
                                vec![avoid as &dyn Geometry],
                            );

                            if !avoid_cut.is_empty() || rapid_length > tool.diameter * 3.0 {
                                ordered_paths.push(ordered_closest);
                            } else {
                                last_path.points.extend(ordered_closest.points);
                            }
                        } else {
                            ordered_paths.push_back(ordered_closest);
                        }
                        */
                    }
                } else {
                    paths.linestrings.clear();
                }
            }
        }

        MultiLineString {
            linestrings: ordered_paths,
        }
    } else {
        paths
    }
}

fn clear_polygons(
    polygons: &MultiPolygon,
    avoid: &MultiPolygon,
    within: Option<&MultiPolygon>,
    tool: &Tool,
    i: usize,
    on_progress: &mut dyn FnMut(f64, String),
) -> MultiLineString {
    let mut paths = MultiLineString {
        linestrings: vec![],
    };

    let tool_radius = tool.diameter / 2.0;

    // TODO: Is this 0.90 causing issues?
    let expanded_avoid = avoid.clone().offset(tool_radius * 0.90);
    let shrunk_within = within.map(|within| within.clone().offset(-tool_radius * 0.90));

    for (poly_index, outline) in polygons.polygons.iter().enumerate() {
        if outline.area() > tool.min_cut_area {
            let progress = poly_index as f64 / polygons.polygons.len() as f64;

            on_progress(progress, format!("Clearing with {}", &tool.name));

            let mut child_on_progress = |p: f64, msg: String| {
                let child_p = p / polygons.polygons.len() as f64;
                on_progress(progress + child_p, msg);
            };

            let path = outline.clone().offset(-tool_radius);

            if !path.is_empty() {
                let mut path = if i > 0 {
                    outline.clone().offset(-tool_radius + tool.overcut)
                } else {
                    path
                };

                path.clean(0.01);

                let path = path.into_boundary();

                let cut = path.clone().offset(tool_radius);

                let remaining =
                    difference(vec![outline as &dyn Geometry], vec![&cut as &dyn Geometry]);

                let next_paths = clear_polygons(
                    &remaining,
                    avoid,
                    within,
                    tool,
                    i + 1,
                    &mut child_on_progress,
                );

                paths.append(path);
                paths.append(next_paths);
            } else {
                let local_path = outline.clone().into_boundary();

                let cut = local_path.clone().offset(tool_radius);

                let avoid_boundary = avoid.clone().into_boundary();

                let (cut_avoid, _): (MultiLineString, MultiPolygon) = intersection(
                    vec![&avoid_boundary as &dyn Geometry],
                    vec![&cut as &dyn Geometry],
                );

                let avoids_avoid = cut_avoid.is_empty();

                let stays_within = if let Some(within) = within {
                    let (cut_outside1, cut_outside2): (MultiLineString, MultiPolygon) =
                        difference(vec![&cut as &dyn Geometry], vec![within as &dyn Geometry]);
                    cut_outside1.is_empty() && cut_outside2.is_empty()
                } else {
                    true
                };

                if avoids_avoid && stays_within {
                    let useful_cut: MultiPolygon =
                        intersection(vec![&cut as &dyn Geometry], vec![outline as &dyn Geometry]);

                    if useful_cut.area() > tool.min_cut_area {
                        paths.append(local_path);
                    }
                } else {
                    // Generate a bunch of paths that will cut this polygon
                    let local_path = cut_avoid.offset(tool_radius).into_boundary();

                    // Remove parts of the path that would cut avoid areas
                    let (local_path, _): (MultiLineString, MultiPolygon) = difference(
                        vec![&local_path as &dyn Geometry],
                        vec![&expanded_avoid as &dyn Geometry],
                    );

                    // Remove parts of the path that would cut outside the within
                    let local_path = if let Some(ref shrunk_within) = shrunk_within {
                        let (local_path, _): (MultiLineString, MultiPolygon) = intersection(
                            vec![&local_path as &dyn Geometry],
                            vec![shrunk_within as &dyn Geometry],
                        );
                        local_path
                    } else {
                        local_path
                    };

                    // Keep only the parts of the path that actually cut what we want
                    let expanded_outline = outline.clone().offset(tool_radius);
                    let (local_path, _): (MultiLineString, MultiPolygon) = intersection(
                        vec![&local_path as &dyn Geometry],
                        vec![&expanded_outline as &dyn Geometry],
                    );

                    let cut = local_path.clone().offset(tool_radius);

                    let useful_cut: MultiPolygon =
                        intersection(vec![&cut as &dyn Geometry], vec![outline as &dyn Geometry]);

                    if useful_cut.area() > tool.min_cut_area {
                        paths.append(local_path);
                    }
                }
            }
        }
    }

    paths
}

fn cut_path<T: ToolKey>(
    paths: HashMap<T, MultiLineString>,
    tools: HashMap<T, Tool>,
) -> MultiPolygon {
    let cuts: Vec<_> = paths
        .into_iter()
        .map(|(tool_key, paths)| {
            let tool = tools.get(&tool_key).unwrap();
            paths.offset(tool.diameter / 2.0)
        })
        .collect();

    union(cuts.iter().map(|cut| cut as &dyn Geometry))
}

pub fn clear<T: ToolKey>(
    outline: MultiPolygon,
    traces: MultiPolygon,
    allow_outside_cuts: bool, // Whether to allow cutting outside the outline
    tools: HashMap<T, Tool>,
    mut on_progress: impl FnMut(f64, String),
) -> HashMap<T, MultiLineString> {
    let cut_progress_modifier = 0.8;
    let optimize_progress_modifier = 1.0 - cut_progress_modifier;

    let mut sorted_tools = tools.clone().into_iter().collect::<Vec<_>>();

    sorted_tools.sort_by(|(_, tool1), (_, tool2)| {
        if tool1.diameter > tool2.diameter {
            Ordering::Less
        } else if tool1.diameter < tool2.diameter {
            Ordering::Greater
        } else {
            Ordering::Equal
        }
    });

    let within = if !allow_outside_cuts {
        Some(&outline)
    } else {
        None
    };

    let mut out = HashMap::new();

    if let [(first_key, first_tool), rest @ ..] = sorted_tools.as_slice() {
        let mut on_first_tool_progress = |p: f64, msg: String| {
            let tool_p = p * 1.0 / sorted_tools.len() as f64;
            on_progress(tool_p * cut_progress_modifier, msg)
        };

        let to_cut = difference(
            vec![&outline as &dyn Geometry],
            vec![&traces as &dyn Geometry],
        );

        let paths = clear_polygons(
            &to_cut,
            &traces,
            within,
            first_tool,
            0,
            &mut on_first_tool_progress,
        );
        out.insert(first_key.clone(), paths);

        for (tool_index, (tool_key, tool)) in rest.iter().enumerate() {
            let progress = (tool_index as f64 + 1.0) / sorted_tools.len() as f64;

            let mut on_tool_progress = |p: f64, msg: String| {
                let tool_p = p * 1.0 / sorted_tools.len() as f64;
                on_progress((progress + tool_p) * cut_progress_modifier, msg)
            };

            let to_cut: MultiPolygon = difference(
                vec![&outline as &dyn Geometry],
                vec![&traces as &dyn Geometry],
            );

            let cut_so_far = cut_path(out.clone(), tools.clone());
            let remaining = difference(
                vec![&to_cut as &dyn Geometry],
                vec![&cut_so_far as &dyn Geometry],
            );

            let paths = clear_polygons(&remaining, &traces, within, tool, 0, &mut on_tool_progress);
            out.insert(tool_key.clone(), paths);
        }
    }

    for (index, (tool_key, paths)) in out.iter_mut().enumerate() {
        if let Some(tool) = tools.get(tool_key) {
            let progress = index as f64 / sorted_tools.len() as f64;

            let on_optimize_progress = |p: f64, msg: String| {
                let optimize_p = p / sorted_tools.len() as f64;
                on_progress(
                    cut_progress_modifier + (progress + optimize_p) * optimize_progress_modifier,
                    msg,
                )
            };

            *paths = optimize_paths(paths.clone(), tool, on_optimize_progress);
        }
    }

    on_progress(1.0, "Done!".to_owned());

    out
}
