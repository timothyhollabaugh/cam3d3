use crate::cam::geometry3d::{Point3, RectangularPrism};
use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Default, Serialize, Deserialize)]
pub struct BoundingBox3 {
    pub min: Point3,
    pub max: Point3,
}

impl BoundingBox3 {
    pub fn valid(&self) -> bool {
        self.min.x <= self.max.x && self.min.y <= self.max.y && self.min.z <= self.max.z
    }

    pub fn prism(&self) -> RectangularPrism {
        RectangularPrism {
            size: self.max - self.min,
        }
    }
}
