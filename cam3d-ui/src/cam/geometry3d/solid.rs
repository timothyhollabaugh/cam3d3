use crate::cam::geometry2d::{FromClipper, Geometry, LineString, MultiLineString, MultiPolygon};
use crate::cam::geometry3d::Point3;
use log::{debug, trace};
use std::fs::File;
use std::path::Path;
use tri_mesh::{HalfEdgeID, InnerSpace, Mesh, Vec3};

#[derive(thiserror::Error, Debug)]
pub enum LoadStlError {
    #[error("Cannot open stl file: {source}")]
    File {
        #[from]
        source: std::io::Error,
    },

    #[error("Cannot parse stl file: {source}")]
    StlParse {
        #[from]
        source: three_d_asset::Error,
    },
}
pub fn load_stl(path: &Path) -> Result<Mesh, LoadStlError> {
    let mut stl_file = File::open(path)?;

    let stl = stl_io::read_stl(&mut stl_file)?;

    let render_mesh = three_d_asset::TriMesh {
        positions: three_d_asset::Positions::F32(
            stl.vertices
                .iter()
                .map(|v| three_d_asset::Vec3::from((v[0], v[1], v[2])))
                .collect(),
        ),
        indices: three_d_asset::Indices::U32(
            stl.faces
                .iter()
                .flat_map(|f| f.vertices)
                .map(|i| i as u32)
                .collect(),
        ),
        normals: None,
        tangents: None,
        uvs: None,
        colors: None,
    };

    let geometry_mesh = tri_mesh::Mesh::new(&render_mesh);
    Ok(geometry_mesh)
}

fn intersect_edge_z(mesh: &Mesh, edge_id: HalfEdgeID, z: f64) -> Option<Vec3> {
    let (v1, v2) = mesh.edge_positions(edge_id);

    if (v1.z > z && v2.z <= z) || (v2.z > z && v1.z <= z) {
        let v = v2 - v1;
        let t = (z - v1.z) / v.z;
        let point = v1 + v * t;
        Some(point)
    } else {
        None
    }
}

#[derive(thiserror::Error, Clone, Debug)]
pub enum IntersectMeshZError {
    #[error("Internal Error: Triangle with no intersections")]
    TriangleWithNoIntersections,

    #[error("Internal Error: Triangle with more than 2 intersections")]
    TriangleWithTooManyIntersections,
}

pub fn intersect_mesh_z(mesh: &Mesh, z: f64) -> Result<MultiPolygon, IntersectMeshZError> {
    debug!("Intersecting Mesh");
    let intersecting_mesh = mesh.clone_subset(&|mesh, face_id| {
        let (v1, v2, v3) = mesh.face_vertices(face_id);
        let v1 = mesh.vertex_position(v1);
        let v2 = mesh.vertex_position(v2);
        let v3 = mesh.vertex_position(v3);

        let edge_intersects = (v1.z > z && (v2.z <= z || v3.z <= z))
            || (v2.z > z && (v1.z <= z || v3.z <= z))
            || (v3.z > z && (v1.z <= z || v2.z <= z));

        //trace!("{} {} {} {} {}", z, v1.z, v2.z, v3.z, edge_intersects);

        edge_intersects
    });

    debug!("Splitting faces");

    let split_faces = intersecting_mesh.connected_components();

    let mut linestrings = Vec::new();

    debug!("Re-connecting faces");
    for connected_faces in split_faces.iter() {
        debug!("Connected faces: {}", connected_faces.len());
        let mut points = Vec::new();
        if let Some(start) = connected_faces.iter().next().cloned() {
            let intersections: Vec<(HalfEdgeID, Vec3)> = mesh
                .face_halfedge_iter(start)
                .filter_map(|edge_id| intersect_edge_z(&mesh, edge_id, z).map(|p| (edge_id, p)))
                .collect();

            let (start_edge_id, start_point) = match intersections.as_slice() {
                &[] => return Err(IntersectMeshZError::TriangleWithNoIntersections),
                &[(edge_id, point)] => (edge_id, point),
                &[(edge_id1, point1), (edge_id2, point2)] => {
                    let face_direction = mesh.face_direction(start);
                    let winding_direction = Vec3::unit_z().cross(face_direction);

                    let intersect_direction = point1 - point2;
                    let intersect_dot_winding = intersect_direction.dot(winding_direction);

                    if intersect_dot_winding > 0.0 {
                        (edge_id1, point1)
                    } else {
                        (edge_id2, point2)
                    }
                }
                too_many_points => {
                    return Err(IntersectMeshZError::TriangleWithTooManyIntersections)
                }
            };

            points.push(Point3::from(start_point).into_2d());
            let mut walker = mesh
                .walker_from_halfedge(start_edge_id)
                .into_twin()
                .into_next();

            loop {
                if let (Some(edge_id), Some(face_id)) = (walker.halfedge_id(), walker.face_id()) {
                    if let Some(point) = intersect_edge_z(&mesh, edge_id, z) {
                        points.push(Point3::from(point).into_2d());

                        walker = walker.into_twin().into_next();
                    } else {
                        walker = walker.into_next();
                    }

                    if face_id == start {
                        debug!("start");
                        break;
                    }
                } else {
                    walker = walker.into_next();
                }
            }
        }
        linestrings.push(LineString { points });
    }

    debug!("Clone paths");
    let multilinestring = MultiLineString { linestrings };
    let paths = multilinestring.clone_paths();
    let multipolygon = MultiPolygon::from_paths(paths);

    Ok(multipolygon)
}
