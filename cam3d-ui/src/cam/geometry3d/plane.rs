use crate::cam::geometry3d::{Point3, Vector3};
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Plane {
    pub point: Point3,
    pub normal: Vector3,
}

impl Plane {
    pub fn from_point_normal(point: Point3, normal: Vector3) -> Plane {
        Plane { point, normal }
    }
    pub fn xy(z: f32) -> Plane {
        Plane {
            point: Point3::from_z(z),
            normal: Vector3::from_z(1.0),
        }
    }

    pub fn offset(self, distance: f32) -> Plane {
        Plane {
            point: self.point + self.normal * distance,
            normal: self.normal,
        }
    }
}

impl Display for Plane {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} ↥ {}", self.point, self.normal)
    }
}
