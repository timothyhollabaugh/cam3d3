use super::Vector3;
use crate::cam::geometry3d::{BoundingBox3, Point3};
use serde::{Deserialize, Serialize};

#[derive(Copy, Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
pub struct RectangularPrism {
    pub size: Vector3,
}

impl RectangularPrism {
    pub fn bounds(&self) -> BoundingBox3 {
        BoundingBox3 {
            min: Point3::zero(),
            max: Point3::zero() + self.size,
        }
    }
}
