use crate::cam::geometry3d::{Point3, Vector3};
use nalgebra as na;
use std::ops::Mul;
use three_d_asset::TriMesh;

#[derive(Copy, Clone, Debug)]
pub struct Matrix4(na::Transform3<f32>);

impl Matrix4 {
    pub fn identity() -> Matrix4 {
        Matrix4(na::Transform3::identity())
    }

    pub fn translate(v: Vector3) -> Matrix4 {
        Matrix4(na::convert(na::Translation3::new(v.x, v.y, v.z)))
    }

    pub fn scale(v: Vector3) -> Matrix4 {
        Matrix4(na::convert(na::Scale3::new(v.x, v.y, v.z)))
    }

    pub fn scale_uniform(s: f32) -> Matrix4 {
        Matrix4(na::convert(na::Scale3::new(s, s, s)))
    }

    pub fn rotate(v: Vector3) -> Matrix4 {
        Matrix4(na::convert(na::Rotation3::face_towards(
            &v.into(),
            &Vector3::unit_y().into(),
        )))
    }
}

impl From<Matrix4> for [[f32; 4]; 4] {
    fn from(value: Matrix4) -> Self {
        value.0.to_homogeneous().into()
    }
}

impl From<Matrix4> for three_d_asset::Matrix4<f32> {
    fn from(value: Matrix4) -> Self {
        let array: [[f32; 4]; 4] = value.into();
        three_d_asset::Matrix4::from(array)
    }
}

impl From<Matrix4> for three_d_asset::Matrix4<f64> {
    fn from(value: Matrix4) -> Self {
        three_d_asset::Matrix4::<f32>::from(value).cast().unwrap()
    }
}

impl From<na::Isometry3<f32>> for Matrix4 {
    fn from(value: na::Isometry3<f32>) -> Self {
        Matrix4(na::convert(value))
    }
}

impl Mul<TriMesh> for Matrix4 {
    type Output = TriMesh;

    fn mul(self, mut rhs: TriMesh) -> Self::Output {
        rhs.transform(&self.into());
        rhs
    }
}

impl Mul<Matrix4> for TriMesh {
    type Output = TriMesh;

    fn mul(mut self, rhs: Matrix4) -> Self::Output {
        self.transform(&rhs.into());
        self
    }
}
