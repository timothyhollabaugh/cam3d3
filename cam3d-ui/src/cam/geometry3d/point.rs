use crate::cam::geometry2d::Point2;
use crate::cam::geometry3d::vector::Vector3;
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};
use std::ops::{Add, AddAssign, Sub, SubAssign};

#[derive(Debug, Copy, Clone, PartialEq, Default, Serialize, Deserialize)]
pub struct Point3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Point3 {
    pub fn zero() -> Point3 {
        Point3 {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        }
    }

    pub fn into_2d(self) -> Point2 {
        Point2 {
            x: self.x,
            y: self.y,
        }
    }

    pub fn new(x: f32, y: f32, z: f32) -> Point3 {
        Point3 { x, y, z }
    }

    pub fn from_xyz(xyz: f32) -> Point3 {
        Point3 {
            x: xyz,
            y: xyz,
            z: xyz,
        }
    }

    pub fn from_x(x: f32) -> Point3 {
        Point3 { x, y: 0.0, z: 0.0 }
    }

    pub fn from_y(y: f32) -> Point3 {
        Point3 { x: 0.0, y, z: 0.0 }
    }

    pub fn from_z(z: f32) -> Point3 {
        Point3 { x: 0.0, y: 0.0, z }
    }

    pub fn as_vector(&self) -> Vector3 {
        Vector3::new(self.x, self.y, self.z)
    }

    pub fn distance(&self) -> f32 {
        f32::sqrt(self.distance_squared())
    }

    pub fn distance_squared(&self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }
}

impl From<Point3> for three_d_asset::Point3<f32> {
    fn from(value: Point3) -> Self {
        three_d_asset::Point3::new(value.x, value.y, value.z)
    }
}

impl From<three_d_asset::Point3<f32>> for Point3 {
    fn from(value: three_d_asset::Point3<f32>) -> Self {
        Point3::new(value.x, value.y, value.z)
    }
}

impl From<Point3> for three_d_asset::Point3<f64> {
    fn from(value: Point3) -> Self {
        three_d_asset::Point3::new(value.x as f64, value.y as f64, value.z as f64)
    }
}

impl From<three_d_asset::Point3<f64>> for Point3 {
    fn from(value: three_d_asset::Point3<f64>) -> Self {
        Point3::new(value.x as f32, value.y as f32, value.z as f32)
    }
}

impl From<Point3> for three_d_asset::Vector3<f32> {
    fn from(value: Point3) -> Self {
        three_d_asset::Vector3::new(value.x, value.y, value.z)
    }
}

impl From<three_d_asset::Vector3<f32>> for Point3 {
    fn from(value: three_d_asset::Vector3<f32>) -> Self {
        Point3::new(value.x, value.y, value.z)
    }
}

impl From<Point3> for three_d_asset::Vector3<f64> {
    fn from(value: Point3) -> Self {
        three_d_asset::Vector3::new(value.x as f64, value.y as f64, value.z as f64)
    }
}

impl From<three_d_asset::Vector3<f64>> for Point3 {
    fn from(value: three_d_asset::Vector3<f64>) -> Self {
        Point3::new(value.x as f32, value.y as f32, value.z as f32)
    }
}

impl Add<Vector3> for Point3 {
    type Output = Point3;

    fn add(self, rhs: Vector3) -> Self::Output {
        Point3 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl AddAssign<Vector3> for Point3 {
    fn add_assign(&mut self, rhs: Vector3) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl Sub<Vector3> for Point3 {
    type Output = Point3;

    fn sub(self, rhs: Vector3) -> Self::Output {
        Point3 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl Sub<Point3> for Point3 {
    type Output = Vector3;

    fn sub(self, rhs: Point3) -> Self::Output {
        Vector3 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl SubAssign<Vector3> for Point3 {
    fn sub_assign(&mut self, rhs: Vector3) {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self.z -= rhs.z;
    }
}

impl std::ops::Mul<f32> for Point3 {
    type Output = Point3;

    fn mul(self, rhs: f32) -> Self::Output {
        Point3 {
            x: self.x * rhs,
            y: self.y * rhs,
            z: self.z * rhs,
        }
    }
}

impl std::ops::Mul<f64> for Point3 {
    type Output = Point3;

    fn mul(self, rhs: f64) -> Self::Output {
        Point3 {
            x: self.x * rhs as f32,
            y: self.y * rhs as f32,
            z: self.z * rhs as f32,
        }
    }
}

impl std::ops::MulAssign<f32> for Point3 {
    fn mul_assign(&mut self, rhs: f32) {
        *self = *self * rhs;
    }
}

impl std::ops::MulAssign<f64> for Point3 {
    fn mul_assign(&mut self, rhs: f64) {
        *self = *self * rhs as f32;
    }
}

impl std::ops::Div<f32> for Point3 {
    type Output = Point3;

    fn div(self, rhs: f32) -> Self::Output {
        Point3 {
            x: self.x / rhs,
            y: self.y / rhs,
            z: self.z / rhs,
        }
    }
}

impl std::ops::DivAssign<f32> for Point3 {
    fn div_assign(&mut self, rhs: f32) {
        *self = *self / rhs;
    }
}

impl std::ops::Div<f64> for Point3 {
    type Output = Point3;

    fn div(self, rhs: f64) -> Self::Output {
        Point3 {
            x: self.x / rhs as f32,
            y: self.y / rhs as f32,
            z: self.z / rhs as f32,
        }
    }
}

impl std::ops::DivAssign<f64> for Point3 {
    fn div_assign(&mut self, rhs: f64) {
        *self = *self / rhs;
    }
}

impl Display for Point3 {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "({:.3}, {:.3}, {:.3})", self.x, self.y, self.z)
    }
}
