use crate::cam::geometry3d::Point3;
use nalgebra as na;
use serde::{Deserialize, Serialize};
use std::fmt::{Display, Formatter};
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

#[derive(Debug, Copy, Clone, PartialEq, Default, Serialize, Deserialize)]
pub struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Vector3 {
    pub fn zero() -> Vector3 {
        Vector3 {
            x: 0.0,
            y: 0.0,
            z: 0.0,
        }
    }

    pub fn new(x: f32, y: f32, z: f32) -> Vector3 {
        Vector3 { x, y, z }
    }

    pub fn from_xyz(xyz: f32) -> Vector3 {
        Vector3 {
            x: xyz,
            y: xyz,
            z: xyz,
        }
    }

    pub fn from_x(x: f32) -> Vector3 {
        Vector3 { x, y: 0.0, z: 0.0 }
    }
    pub fn from_y(y: f32) -> Vector3 {
        Vector3 { x: 0.0, y, z: 0.0 }
    }
    pub fn from_z(z: f32) -> Vector3 {
        Vector3 { x: 0.0, y: 0.0, z }
    }

    pub fn unit_x() -> Vector3 {
        Vector3 {
            x: 1.0,
            y: 0.0,
            z: 0.0,
        }
    }
    pub fn unit_y() -> Vector3 {
        Vector3 {
            x: 0.0,
            y: 1.0,
            z: 0.0,
        }
    }
    pub fn unit_z() -> Vector3 {
        Vector3 {
            x: 0.0,
            y: 0.0,
            z: 1.0,
        }
    }

    pub fn magnitude_squared(&self) -> f32 {
        self.x * self.x + self.y * self.y + self.z * self.z
    }

    pub fn magnitude(&self) -> f32 {
        f32::sqrt(self.magnitude_squared())
    }

    pub fn normalized(&self) -> Vector3 {
        let magnitude = self.magnitude();

        Vector3 {
            x: self.x / magnitude,
            y: self.y / magnitude,
            z: self.z / magnitude,
        }
    }

    pub fn dot(&self, other: Vector3) -> f32 {
        self.x * other.x + self.y * other.y + self.z * other.z
    }

    pub fn cross(&self, other: Vector3) -> Vector3 {
        Vector3 {
            x: self.y * other.z - self.z * other.y,
            y: self.z * other.x - self.x * other.y,
            z: self.x * other.y - self.y * other.x,
        }
    }

    pub fn as_point(&self) -> Point3 {
        Point3::new(self.x, self.y, self.z)
    }
}

impl Neg for Vector3 {
    type Output = Vector3;

    fn neg(self) -> Self::Output {
        Vector3 {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl Add<Vector3> for Vector3 {
    type Output = Vector3;

    fn add(self, rhs: Vector3) -> Self::Output {
        Vector3 {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
            z: self.z + rhs.z,
        }
    }
}

impl AddAssign<Vector3> for Vector3 {
    fn add_assign(&mut self, rhs: Vector3) {
        self.x += rhs.x;
        self.y += rhs.y;
        self.z += rhs.z;
    }
}

impl Sub<Vector3> for Vector3 {
    type Output = Vector3;

    fn sub(self, rhs: Vector3) -> Self::Output {
        Vector3 {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
            z: self.z - rhs.z,
        }
    }
}

impl SubAssign<Vector3> for Vector3 {
    fn sub_assign(&mut self, rhs: Vector3) {
        self.x -= rhs.x;
        self.y -= rhs.y;
        self.z -= rhs.z;
    }
}

impl Mul<f32> for Vector3 {
    type Output = Vector3;

    fn mul(self, rhs: f32) -> Self::Output {
        Vector3 {
            x: self.x * rhs,
            y: self.y * rhs,
            z: self.z * rhs,
        }
    }
}

impl MulAssign<f32> for Vector3 {
    fn mul_assign(&mut self, rhs: f32) {
        self.x *= rhs;
        self.y *= rhs;
        self.z *= rhs;
    }
}

impl Div<f32> for Vector3 {
    type Output = Vector3;

    fn div(self, rhs: f32) -> Self::Output {
        Vector3 {
            x: self.x / rhs,
            y: self.y / rhs,
            z: self.z / rhs,
        }
    }
}

impl DivAssign<f32> for Vector3 {
    fn div_assign(&mut self, rhs: f32) {
        self.x /= rhs;
        self.y /= rhs;
        self.z /= rhs;
    }
}

impl From<Vector3> for na::Vector3<f32> {
    fn from(value: Vector3) -> Self {
        na::Vector3::new(value.x, value.y, value.z)
    }
}
impl From<na::Vector3<f32>> for Vector3 {
    fn from(value: na::Vector3<f32>) -> Self {
        Vector3::new(value.x, value.y, value.z)
    }
}

impl From<Vector3> for three_d_asset::Vector3<f32> {
    fn from(value: Vector3) -> Self {
        three_d_asset::Vector3::new(value.x, value.y, value.z)
    }
}

impl From<three_d_asset::Vector3<f32>> for Vector3 {
    fn from(value: three_d_asset::Vector3<f32>) -> Self {
        Vector3::new(value.x, value.y, value.z)
    }
}

impl From<Vector3> for three_d_asset::Vector3<f64> {
    fn from(value: Vector3) -> Self {
        three_d_asset::Vector3::new(value.x as f64, value.y as f64, value.z as f64)
    }
}

impl From<three_d_asset::Vector3<f64>> for Vector3 {
    fn from(value: three_d_asset::Vector3<f64>) -> Self {
        Vector3::new(value.x as f32, value.y as f32, value.z as f32)
    }
}

impl Display for Vector3 {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "<{:.3}, {:.3}, {:.3}>", self.x, self.y, self.z)
    }
}
