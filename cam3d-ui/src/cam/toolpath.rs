use crate::cam::geometry3d::Point3;
use std::fmt::{Display, Formatter};
use std::sync::Arc;

pub mod adaptive_2d;
pub mod adaptive_3d;
pub mod drag_knife;
pub mod engrave;

#[derive(Debug, Clone)]
pub struct Toolpath {
    pub commands: Vec<ToolpathCommand>,
    pub tool: String,
}

impl Display for Toolpath {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} cmds, tool {}", self.commands.len(), &self.tool)
    }
}

#[derive(thiserror::Error, Clone, Debug)]
pub enum GenerateGcodeError {
    #[error("Error templating gcode: {0}")]
    TemplateError(#[from] Arc<tera::Error>),
}

impl Toolpath {
    pub fn generate_gcode(
        &self,
        rapid_feed: f32,
        start_gcode: &str,
        end_gcode: &str,
        toolchange_gcode: &str,
    ) -> Result<String, GenerateGcodeError> {
        let mut gcode = String::new();
        gcode.push_str(start_gcode);
        gcode.push('\n');

        let mut context = tera::Context::new();
        context.insert("tool", &self.tool);

        let toolchange_gcode =
            tera::Tera::one_off(toolchange_gcode, &context, false).map_err(Arc::new)?;
        gcode.push_str(&toolchange_gcode);
        gcode.push('\n');

        for command in self.commands.iter() {
            let gcode_command = match command {
                ToolpathCommand::Comment(s) => format!("; {s}\n"),
                ToolpathCommand::MoveTo(point, feed) => format!(
                    "G1 X{:0.3} Y{:0.3} Z{:0.3} F{:0.3}\n",
                    point.x, point.y, point.z, feed
                ),
                ToolpathCommand::RapidTo(point) => format!(
                    "G0 X{:0.3} Y{:0.3} Z{:0.3} F{:0.3}\n",
                    point.x, point.y, point.z, rapid_feed
                ),
                ToolpathCommand::RapidToZ(z) => {
                    format!("G0 Z{:0.3} F{:0.3}\n", z, rapid_feed)
                }
            };

            gcode.push_str(&gcode_command);
        }

        gcode.push_str(end_gcode);
        gcode.push('\n');

        Ok(gcode)
    }
}

#[derive(Debug, Clone)]
pub enum ToolpathCommand {
    Comment(String),
    MoveTo(Point3, f32),
    RapidTo(Point3),
    RapidToZ(f32),
}

impl Display for ToolpathCommand {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ToolpathCommand::Comment(s) => write!(f, "Comment \"{s}\""),
            ToolpathCommand::MoveTo(p, feed) => write!(f, "MoveTo {p} F={feed}"),
            ToolpathCommand::RapidTo(p) => write!(f, "RapidTo {p}"),
            ToolpathCommand::RapidToZ(z) => write!(f, "RapidToZ Z={z}"),
        }
    }
}
