use egui::epaint::Shadow;
use egui::style::{Selection, WidgetVisuals};
use egui::{FontId, Margin, Rounding, Stroke, TextStyle, Ui, Vec2, WidgetText};

use crate::ui::widgets::justified;

pub fn styles_ui(ui: &mut Ui) {
    let styles = (*ui.ctx().style()).clone();

    labeled(ui, "Override Text Style", |ui| {
        option_ui(ui, styles.override_text_style.as_ref(), text_style_ui)
    });

    labeled(ui, "Override Font Id", |ui| {
        option_ui(ui, styles.override_font_id.as_ref(), font_id_ui)
    });

    ui.collapsing("Text Styles", |ui| {
        for (text_style, font_id) in styles.text_styles.iter() {
            justified(
                ui,
                |ui| text_style_ui(ui, text_style),
                |ui| font_id_ui(ui, font_id),
            );
        }
    });

    labeled(ui, "Drag Value Text Style", |ui| {
        text_style_ui(ui, &styles.drag_value_text_style)
    });

    ui.collapsing("Spacing", |ui| {
        let spacing = ui.spacing().clone();

        labeled(ui, "Item Spacing", |ui| vec2_ui(ui, &spacing.item_spacing));
        labeled(ui, "Window Margin", |ui| {
            margin_ui(ui, &spacing.window_margin)
        });
        labeled(ui, "Button Padding", |ui| {
            vec2_ui(ui, &spacing.button_padding)
        });
        labeled(ui, "Menu Margin", |ui| margin_ui(ui, &spacing.menu_margin));
        labeled(ui, "Indent", |ui| ui.label(spacing.indent.to_string()));
        labeled(ui, "Interact Size", |ui| {
            vec2_ui(ui, &spacing.interact_size)
        });
        labeled(ui, "Slider Width", |ui| {
            ui.label(spacing.slider_width.to_string())
        });
        labeled(ui, "Slider Rail Height", |ui| {
            ui.label(spacing.slider_rail_height.to_string())
        });
        labeled(ui, "Combo Width", |ui| {
            ui.label(spacing.combo_width.to_string())
        });
        labeled(ui, "Text Edit Width", |ui| {
            ui.label(spacing.text_edit_width.to_string())
        });
        labeled(ui, "Icon Width", |ui| {
            ui.label(spacing.icon_width.to_string())
        });
        labeled(ui, "Icon Width Inner", |ui| {
            ui.label(spacing.icon_width_inner.to_string())
        });
        labeled(ui, "Icon Spacing", |ui| {
            ui.label(spacing.icon_spacing.to_string())
        });
        labeled(ui, "Tooltip Width", |ui| {
            ui.label(spacing.tooltip_width.to_string())
        });
        labeled(ui, "Indent ends with horizontal line", |ui| {
            ui.label(spacing.indent_ends_with_horizontal_line.to_string())
        });
        labeled(ui, "Combo Height", |ui| {
            ui.label(spacing.combo_height.to_string())
        });
    });

    ui.collapsing("Interaction", |ui| {
        let interaction = styles.interaction.clone();

        labeled(ui, "Resize grab radius side", |ui| {
            ui.label(interaction.resize_grab_radius_side.to_string())
        });
        labeled(ui, "Resize grab radius corner", |ui| {
            ui.label(interaction.resize_grab_radius_corner.to_string())
        });
        labeled(ui, "Show tooltips only when still", |ui| {
            ui.label(interaction.show_tooltips_only_when_still.to_string())
        });
    });

    ui.collapsing("Visuals", |ui| {
        let mut visuals = styles.visuals.clone();

        labeled(ui, "Dark Mode", |ui| {
            ui.label(visuals.dark_mode.to_string())
        });
        labeled(ui, "Override Text Color", |ui| {
            option_ui(ui, visuals.override_text_color.as_mut(), |ui, t| {
                ui.color_edit_button_srgba(t)
            })
        });

        ui.collapsing("Widgets", |ui| {
            let mut widgets = visuals.widgets.clone();

            ui.collapsing("Noninteractive", |ui| {
                widget_visuals_ui(ui, &mut widgets.noninteractive)
            });
            ui.collapsing("Inactive", |ui| {
                widget_visuals_ui(ui, &mut widgets.inactive)
            });
            ui.collapsing("Hovered", |ui| widget_visuals_ui(ui, &mut widgets.hovered));
            ui.collapsing("Active", |ui| widget_visuals_ui(ui, &mut widgets.active));
            ui.collapsing("Open", |ui| widget_visuals_ui(ui, &mut widgets.open));
        });

        labeled(ui, "Selection", |ui| {
            selection_ui(ui, &mut visuals.selection)
        });

        labeled(ui, "Hyperlink Color", |ui| {
            ui.color_edit_button_srgba(&mut visuals.hyperlink_color)
        });
        labeled(ui, "Faint BG Color", |ui| {
            ui.color_edit_button_srgba(&mut visuals.faint_bg_color)
        });
        labeled(ui, "Extreme BG Color", |ui| {
            ui.color_edit_button_srgba(&mut visuals.extreme_bg_color)
        });
        labeled(ui, "Code BG Color", |ui| {
            ui.color_edit_button_srgba(&mut visuals.code_bg_color)
        });
        labeled(ui, "Warn FG Color", |ui| {
            ui.color_edit_button_srgba(&mut visuals.warn_fg_color)
        });
        labeled(ui, "Error FG Color", |ui| {
            ui.color_edit_button_srgba(&mut visuals.error_fg_color)
        });
        labeled(ui, "Window Rounding", |ui| {
            rounding_ui(ui, &visuals.window_rounding)
        });
        labeled(ui, "Window Shadow", |ui| {
            shadow_ui(ui, &mut visuals.window_shadow)
        });
        labeled(ui, "Window Fill", |ui| {
            ui.color_edit_button_srgba(&mut visuals.window_fill)
        });
        labeled(ui, "Window Stroke", |ui| {
            stroke_ui(ui, &mut visuals.window_stroke)
        });
        labeled(ui, "Menu Rounding", |ui| {
            rounding_ui(ui, &visuals.menu_rounding)
        });
        labeled(ui, "Panel Fill", |ui| {
            ui.color_edit_button_srgba(&mut visuals.panel_fill)
        });
        labeled(ui, "Popup Shadow", |ui| {
            shadow_ui(ui, &mut visuals.popup_shadow)
        });
        labeled(ui, "Resize corner size", |ui| {
            ui.label(visuals.resize_corner_size.to_string())
        });
        labeled(ui, "Clip rect margin", |ui| {
            ui.label(visuals.clip_rect_margin.to_string())
        });
        labeled(ui, "Button frame", |ui| {
            ui.label(visuals.button_frame.to_string())
        });
        labeled(ui, "Collapsing header frame", |ui| {
            ui.label(visuals.collapsing_header_frame.to_string())
        });
        labeled(ui, "Indent has left vline", |ui| {
            ui.label(visuals.indent_has_left_vline.to_string())
        });
        labeled(ui, "Striped", |ui| ui.label(visuals.striped.to_string()));
        labeled(ui, "Slider trailing fill", |ui| {
            ui.label(visuals.slider_trailing_fill.to_string())
        });
    });

    labeled(ui, "Animation time", |ui| {
        ui.label(styles.animation_time.to_string())
    });

    #[cfg(debug_assertions)]
    ui.collapsing("Debug options", |ui| {
        let debug_options = styles.debug.clone();
        labeled(ui, "Debug on hover", |ui| {
            ui.label(debug_options.debug_on_hover.to_string())
        });
        labeled(ui, "Show expand width", |ui| {
            ui.label(debug_options.show_expand_width.to_string())
        });
        labeled(ui, "Show expand height", |ui| {
            ui.label(debug_options.show_expand_height.to_string())
        });
        labeled(ui, "Show resize", |ui| {
            ui.label(debug_options.show_resize.to_string())
        });
        labeled(ui, "Show interactive widgets", |ui| {
            ui.label(debug_options.show_interactive_widgets.to_string())
        });
        //labeled(ui, "Show blocking widget", |ui| {
        //ui.label(debug_options.show_blocking_widget.to_string())
        //});
    });

    labeled(ui, "Explanation tooltips", |ui| {
        ui.label(styles.explanation_tooltips.to_string())
    });
}

fn selection_ui(ui: &mut Ui, selection: &mut Selection) {
    ui.horizontal(|ui| {
        ui.label("bg ");
        ui.color_edit_button_srgba(&mut selection.bg_fill);
        ui.label(", stroke ");
        stroke_ui(ui, &mut selection.stroke);
    });
}

fn widget_visuals_ui(ui: &mut Ui, widget_visuals: &mut WidgetVisuals) {
    labeled(ui, "BG Fill", |ui| {
        ui.color_edit_button_srgba(&mut widget_visuals.bg_fill)
    });
    labeled(ui, "Weak BG Fill", |ui| {
        ui.color_edit_button_srgba(&mut widget_visuals.weak_bg_fill)
    });
    labeled(ui, "BG Stroke", |ui| {
        stroke_ui(ui, &mut widget_visuals.bg_stroke)
    });
    labeled(ui, "Rounding", |ui| {
        rounding_ui(ui, &mut widget_visuals.rounding)
    });
    labeled(ui, "FG Stroke", |ui| {
        stroke_ui(ui, &mut widget_visuals.fg_stroke)
    });
    labeled(ui, "Expansion", |ui| {
        ui.label(widget_visuals.expansion.to_string())
    });
}

fn labeled<R>(ui: &mut Ui, label: impl Into<WidgetText>, child_ui: impl FnOnce(&mut Ui) -> R) {
    justified(ui, |ui| ui.label(label), child_ui);
}

fn rounding_ui(ui: &mut Ui, rounding: &Rounding) {
    ui.label(format!(
        "(nw {}, ne {}, sw {}, se {})",
        rounding.nw, rounding.ne, rounding.sw, rounding.se
    ));
}

fn margin_ui(ui: &mut Ui, margin: &Margin) {
    ui.label(format!(
        "(l {}, r {}, t {}, b {})",
        margin.left, margin.right, margin.top, margin.bottom
    ));
}

fn vec2_ui(ui: &mut Ui, vec2: &Vec2) {
    ui.label(format!("({}, {})", vec2.x, vec2.y));
}

fn option_ui<T, R>(ui: &mut Ui, option: Option<T>, child_ui: impl Fn(&mut Ui, T) -> R) {
    match option {
        None => {
            ui.weak("None");
        }
        Some(t) => {
            child_ui(ui, t);
        }
    }
}

fn shadow_ui(ui: &mut Ui, shadow: &mut Shadow) {
    ui.horizontal(|ui| {
        ui.label(format!(
            "offset {}x{} blur {} spread {}",
            shadow.offset.x, shadow.offset.y, shadow.blur, shadow.spread
        ));
        ui.color_edit_button_srgba(&mut shadow.color);
    });
}

fn stroke_ui(ui: &mut Ui, stroke: &mut Stroke) {
    ui.horizontal(|ui| {
        ui.label(format!("{}pt ", stroke.width));
        ui.color_edit_button_srgba(&mut stroke.color);
    });
}

fn font_id_ui(ui: &mut Ui, font_id: &FontId) {
    ui.label(format!("{}pt {}", font_id.size, font_id.family));
}

fn text_style_ui(ui: &mut Ui, text_style: &TextStyle) {
    ui.label(text_style.to_string());
}
