struct TransformUniform {
    view_proj: mat4x4<f32>
}

struct Position {
    @location(0) position: vec3<f32>,
    @location(1) color: vec3<f32>,
}

struct LineVertexIndex {
    @location(0) start_position: vec3<f32>,
    @location(1) end_position: vec3<f32>,
    @location(2) color: vec4<f32>,
    @location(3) vertex_index: u32,
}

struct VertexOutput {
    @builtin(position) position: vec4<f32>,
    @location(0) color: vec4<f32>,
}

const line_width = 2.0;

@group(0) @binding(0)
var<uniform> camera: TransformUniform;

@group(0) @binding(1)
var<uniform> size: vec2<f32>;

@vertex
fn vs_main(line_index: LineVertexIndex) -> VertexOutput {
    var out: VertexOutput;

    let start_vertex = (camera.view_proj * vec4<f32>(line_index.start_position, 1.0));
    let end_vertex = (camera.view_proj * vec4<f32>(line_index.end_position, 1.0));

    let tangent = normalize(end_vertex.xy - start_vertex.xy);
    let normal = vec2<f32>(-tangent.y, tangent.x);

    var delta = vec2<f32>(0.0, 0.0);
    var position = vec4<f32>(0.0, 0.0, 0.0, 0.0);

    switch (line_index.vertex_index) {
        case 0u: {
            delta = -line_width * tangent + line_width * normal;
            position = start_vertex;
        }
        case 1u: {
            delta = -line_width * tangent - line_width * normal;
            position = start_vertex;
        }
        case 2u: {
            delta = line_width * tangent + line_width * normal;
            position = end_vertex;
        }
        case 3u: {
            delta = -line_width * tangent - line_width * normal;
            position = start_vertex;
        }
        case 4u: {
            delta = line_width * tangent + line_width * normal;
            position = end_vertex;
        }
        case 5u: {
            delta = line_width * tangent - line_width * normal;
            position = end_vertex;
        }
        default: { }
    };

    delta.x /= size.x;
    delta.y /= size.y;

    out.position = position + vec4<f32>(delta, 0.0, 0.0);
    out.position.z -= 0.0001;
    out.color = line_index.color;

    return out;
}

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {
    return vec4<f32>(in.color);
}
