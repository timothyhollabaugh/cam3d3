use crate::cam::geometry3d::{Point3, Vector3};
use std::f32::consts::FRAC_PI_2;
use std::time::Instant;

#[derive(Clone, Debug)]
enum OrbitControlsAnimation {
    Pitch {
        start: f32,
        end: f32,
        timestamp: Instant,
    },
    Yaw {
        start: f32,
        end: f32,
        timestamp: Instant,
    },
}

#[derive(Clone, Debug)]
pub struct OrbitControls {
    target: Point3,
    yaw: f32,
    pitch: f32,
    distance: f32,
    zoom: f32,
    animations: Vec<OrbitControlsAnimation>,
}

impl OrbitControls {
    pub fn new(target: Point3, yaw: f32, pitch: f32, zoom: f32) -> OrbitControls {
        let horizontal_distance = zoom * f32::cos(pitch);

        OrbitControls {
            target,
            yaw,
            pitch,
            distance: 2000.0,
            zoom,
            animations: Vec::new(),
        }
    }

    pub fn update(&mut self, scroll_delta: egui::Vec2, response: &egui::Response) {
        if self.animations.is_empty() {
            if response.dragged_by(egui::PointerButton::Primary) {
                let delta = response.drag_delta();
                self.yaw += -delta.x / 100.0;
                self.pitch += delta.y / 100.0;
                self.pitch = self.pitch.clamp(-FRAC_PI_2, FRAC_PI_2);
            }

            if response.dragged_by(egui::PointerButton::Secondary) {
                let mut delta = response.drag_delta();
                delta.x /= self.zoom;
                delta.y /= self.zoom;

                self.target.x += -delta.x * f32::cos(self.yaw + FRAC_PI_2)
                    + -delta.y * f32::sin(self.yaw + FRAC_PI_2) * -f32::cos(self.pitch + FRAC_PI_2);
                self.target.y += -delta.x * f32::sin(self.yaw + FRAC_PI_2)
                    + delta.y * f32::cos(self.yaw + FRAC_PI_2) * -f32::cos(self.pitch + FRAC_PI_2);
                self.target.z += delta.y * f32::sin(self.pitch + FRAC_PI_2);
            }

            let mouse_in_area = response
                .hover_pos()
                .map(|p| response.rect.contains(p))
                .unwrap_or(false);

            if mouse_in_area && scroll_delta.y != 0.0 {
                self.zoom += scroll_delta.y * self.zoom * 0.001;
            }
        } else {
            let now = Instant::now();

            self.animations.extract_if(|animation| match animation {
                OrbitControlsAnimation::Pitch {
                    start,
                    end,
                    timestamp,
                } => {
                    let t = (now - *timestamp).as_secs_f32().clamp(0.0, 1.0);
                    self.pitch = (*end - *start) * t + *start;
                    t >= 1.0
                }
                OrbitControlsAnimation::Yaw {
                    start,
                    end,
                    timestamp,
                } => {
                    let t = (now - *timestamp).as_secs_f32().clamp(0.0, 1.0);
                    self.yaw = (*end - *start) * t + *start;
                    t >= 1.0
                }
            });
        }
    }

    pub fn transform(&self, rect: egui::Rect) -> three_d_asset::Matrix4<f32> {
        let view_matrix = three_d_asset::Matrix4::<f32>::from_scale(self.zoom)
            * three_d_asset::Matrix4::<f32>::look_at_rh(
                self.camera_position().into(),
                self.target.into(),
                self.camera_up().into(),
            );

        let projection_matrix = three_d_asset::ortho(
            -rect.width() / 2.0,
            rect.width() / 2.0,
            -rect.height() / 2.0,
            rect.height() / 2.0,
            -2.0 * self.distance * self.zoom,
            2.0 * self.distance * self.zoom,
        );

        projection_matrix * view_matrix
    }

    pub fn camera_position(&self) -> Point3 {
        let distance = self.distance;
        let horizontal_distance = distance * f32::cos(self.pitch);

        let position = Point3 {
            x: self.target.x + horizontal_distance * f32::cos(self.yaw),
            y: self.target.y + horizontal_distance * f32::sin(self.yaw),
            z: self.target.z + distance * f32::sin(self.pitch),
        };

        position
    }

    pub fn camera_up(&self) -> Vector3 {
        let up_horizontal_distance = f32::cos(self.pitch + FRAC_PI_2);

        let up = Vector3 {
            x: up_horizontal_distance * f32::cos(self.yaw),
            y: up_horizontal_distance * f32::sin(self.yaw),
            z: f32::sin(self.pitch + FRAC_PI_2),
        };

        up
    }

    pub fn animate_yaw(&mut self, yaw: f32) {
        self.animations.push(OrbitControlsAnimation::Yaw {
            start: self.yaw,
            end: yaw,
            timestamp: Instant::now(),
        });
    }

    pub fn animate_pitch(&mut self, pitch: f32) {
        self.animations.push(OrbitControlsAnimation::Pitch {
            start: self.pitch,
            end: pitch,
            timestamp: Instant::now(),
        })
    }

    pub fn set_yaw(&mut self, yaw: f32) {
        self.yaw = yaw;
    }

    pub fn set_pitch(&mut self, pitch: f32) {
        self.pitch = pitch;
    }

    pub fn yaw(&self) -> f32 {
        self.yaw
    }

    pub fn pitch(&self) -> f32 {
        self.pitch
    }

    pub fn target(&self) -> Point3 {
        self.target
    }
}
