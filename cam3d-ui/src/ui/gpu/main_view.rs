use std::mem;
use std::sync::Arc;

use bytemuck::{Pod, Zeroable};
use eframe::epaint::PaintCallbackInfo;
use egui_wgpu::{CallbackResources, RenderState, ScreenDescriptor};
use itertools::Itertools;
use three_d_asset::{Indices, Srgba, TriMesh};
use wgpu::naga::VectorSize::Tri;
use wgpu::util::DeviceExt;
use wgpu::PipelineCompilationOptions;
use wgpu::PolygonMode::Line;

use crate::cam::geometry3d::Matrix4;
use crate::cam::geometry3d::{Point3, Vector3};
use crate::ui::gpu::orbit_controls::OrbitControls;
use crate::ui::selection::Selection;
use crate::ui::GpuInfo;

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
struct GpuVertex {
    position: [f32; 3],
    color: [f32; 3],
}

impl GpuVertex {
    fn desc() -> wgpu::VertexBufferLayout<'static> {
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<GpuVertex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttribute {
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float32x3,
                },
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format: wgpu::VertexFormat::Float32x3,
                },
            ],
        }
    }
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
struct GpuLineVertex {
    start_position: [f32; 3],
    end_position: [f32; 3],
    color: [f32; 4],
    vertex_index: u32,
}

impl GpuLineVertex {
    fn desc() -> wgpu::VertexBufferLayout<'static> {
        use std::mem;
        wgpu::VertexBufferLayout {
            array_stride: mem::size_of::<GpuLineVertex>() as wgpu::BufferAddress,
            step_mode: wgpu::VertexStepMode::Vertex,
            attributes: &[
                wgpu::VertexAttribute {
                    offset: 0,
                    shader_location: 0,
                    format: wgpu::VertexFormat::Float32x3,
                },
                wgpu::VertexAttribute {
                    offset: mem::size_of::<[f32; 3]>() as wgpu::BufferAddress,
                    shader_location: 1,
                    format: wgpu::VertexFormat::Float32x3,
                },
                wgpu::VertexAttribute {
                    offset: (mem::size_of::<[f32; 3]>() + mem::size_of::<[f32; 3]>())
                        as wgpu::BufferAddress,
                    shader_location: 2,
                    format: wgpu::VertexFormat::Float32x4,
                },
                wgpu::VertexAttribute {
                    offset: (mem::size_of::<[f32; 3]>()
                        + mem::size_of::<[f32; 3]>()
                        + mem::size_of::<[f32; 4]>())
                        as wgpu::BufferAddress,
                    shader_location: 3,
                    format: wgpu::VertexFormat::Uint32,
                },
            ],
        }
    }
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Pod, Zeroable)]
struct GpuTransform {
    matrix: [[f32; 4]; 4],
}

impl From<Matrix4> for GpuTransform {
    fn from(value: Matrix4) -> Self {
        GpuTransform {
            matrix: value.into(),
        }
    }
}

struct LinePipelineCallback {
    transform: GpuTransform,
    size: [f32; 2],
    lines: Vec<GpuLineVertex>,
}

impl egui_wgpu::CallbackTrait for LinePipelineCallback {
    fn prepare(
        &self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        screen_descriptor: &ScreenDescriptor,
        _egui_encoder: &mut wgpu::CommandEncoder,
        callback_resources: &mut CallbackResources,
    ) -> Vec<wgpu::CommandBuffer> {
        let pipeline: &mut LinePipeline = callback_resources.get_mut().unwrap();
        pipeline.prepare(device, queue, self.transform, self.size, &self.lines)
    }

    fn paint<'a>(
        &'a self,
        info: PaintCallbackInfo,
        render_pass: &mut wgpu::RenderPass<'a>,
        callback_resources: &'a CallbackResources,
    ) {
        let pipeline: &LinePipeline = callback_resources.get().unwrap();
        pipeline.paint(render_pass)
    }
}

struct LinePipeline {
    pipeline: wgpu::RenderPipeline,
    bind_group: wgpu::BindGroup,
    transform_buffer: wgpu::Buffer,
    size_buffer: wgpu::Buffer,
    vertex_buffer: wgpu::Buffer,
    vertex_buffer_len: u32,
}

impl LinePipeline {
    fn new(device: &wgpu::Device, target: wgpu::ColorTargetState) -> LinePipeline {
        let shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: Some("Line Shader"),
            source: wgpu::ShaderSource::Wgsl(include_str!("./lines.wgsl").into()),
        });

        let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: Some("Line Bind Group Layout"),
            entries: &[
                // Transform uniform
                wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                },
                // Size uniform
                wgpu::BindGroupLayoutEntry {
                    binding: 1,
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                },
            ],
        });

        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("Lines Pipeline Layout"),
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
        });

        let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("Lines Pipeline"),
            layout: Some(&pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[GpuLineVertex::desc()],
                compilation_options: PipelineCompilationOptions::default(),
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(target)],
                compilation_options: PipelineCompilationOptions::default(),
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: None,
                unclipped_depth: false,
                polygon_mode: wgpu::PolygonMode::Fill,
                conservative: false,
            },
            depth_stencil: Some(wgpu::DepthStencilState {
                format: wgpu::TextureFormat::Depth32Float,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::LessEqual,
                stencil: wgpu::StencilState::default(),
                bias: wgpu::DepthBiasState::default(),
            }),
            multisample: wgpu::MultisampleState {
                count: 4,
                ..wgpu::MultisampleState::default()
            },
            multiview: None,
        });

        let initial_transform: GpuTransform = Matrix4::identity().into();

        let transform_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Line Transform Buffer"),
            contents: bytemuck::cast_slice(&[initial_transform]),
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::UNIFORM,
        });

        let size_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Line Size Buffer"),
            contents: bytemuck::cast_slice(&[[100f32, 100f32]]),
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::UNIFORM,
        });

        let vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Line Vertex Buffer"),
            contents: &[],
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::VERTEX,
        });

        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("Line Bind Group"),
            layout: &bind_group_layout,
            entries: &[
                wgpu::BindGroupEntry {
                    binding: 0,
                    resource: transform_buffer.as_entire_binding(),
                },
                wgpu::BindGroupEntry {
                    binding: 1,
                    resource: size_buffer.as_entire_binding(),
                },
            ],
        });

        LinePipeline {
            pipeline,
            bind_group,
            transform_buffer,
            size_buffer,
            vertex_buffer,
            vertex_buffer_len: 0,
        }
    }

    fn prepare(
        &mut self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        transform: GpuTransform,
        size: [f32; 2],
        vertices: &[GpuLineVertex],
    ) -> Vec<wgpu::CommandBuffer> {
        puffin::profile_function!();

        queue.write_buffer(
            &self.transform_buffer,
            0,
            bytemuck::cast_slice(&[transform]),
        );

        queue.write_buffer(&self.size_buffer, 0, bytemuck::cast_slice(&[size]));

        self.vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Line Vertex Buffer"),
            contents: bytemuck::cast_slice(vertices),
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::VERTEX,
        });

        self.vertex_buffer_len = vertices.len() as u32;

        vec![]
    }

    fn paint<'rpass>(&'rpass self, render_pass: &mut wgpu::RenderPass<'rpass>) {
        puffin::profile_function!();
        render_pass.set_pipeline(&self.pipeline);
        render_pass.set_bind_group(0, &self.bind_group, &[]);
        render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
        render_pass.draw(0..self.vertex_buffer_len, 0..1);
    }
}

struct TrianglePipelineCallback {
    transform: GpuTransform,
    vertices: Vec<GpuVertex>,
    indices: Vec<u32>,
}

impl egui_wgpu::CallbackTrait for TrianglePipelineCallback {
    fn prepare(
        &self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        screen_descriptor: &ScreenDescriptor,
        egui_encoder: &mut wgpu::CommandEncoder,
        callback_resources: &mut CallbackResources,
    ) -> Vec<wgpu::CommandBuffer> {
        let pipeline: &mut TrianglePipeline = callback_resources.get_mut().unwrap();
        pipeline.prepare(device, queue, self.transform, &self.vertices, &self.indices)
    }

    fn paint<'a>(
        &'a self,
        info: PaintCallbackInfo,
        render_pass: &mut wgpu::RenderPass<'a>,
        callback_resources: &'a CallbackResources,
    ) {
        let pipeline: &TrianglePipeline = callback_resources.get().unwrap();
        pipeline.paint(render_pass);
    }
}

struct TrianglePipeline {
    pipeline: wgpu::RenderPipeline,
    bind_group: wgpu::BindGroup,
    transform_buffer: wgpu::Buffer,
    vertex_buffer: wgpu::Buffer,
    index_buffer: wgpu::Buffer,
    index_buffer_len: u32,
}

impl TrianglePipeline {
    fn new(device: &wgpu::Device, target: wgpu::ColorTargetState) -> TrianglePipeline {
        let shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: Some("Triangle Shader"),
            source: wgpu::ShaderSource::Wgsl(include_str!("./shader.wgsl").into()),
        });

        let bind_group_layout = device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
            label: Some("Triangle Bind Group Layout"),
            entries: &[wgpu::BindGroupLayoutEntry {
                binding: 0,
                visibility: wgpu::ShaderStages::VERTEX,
                ty: wgpu::BindingType::Buffer {
                    ty: wgpu::BufferBindingType::Uniform,
                    has_dynamic_offset: false,
                    min_binding_size: None,
                },
                count: None,
            }],
        });

        let pipeline_layout = device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
            label: Some("Triangle Pipeline Layout"),
            bind_group_layouts: &[&bind_group_layout],
            push_constant_ranges: &[],
        });

        let pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("Triangle Pipeline"),
            layout: Some(&pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[GpuVertex::desc()],
                compilation_options: PipelineCompilationOptions::default(),
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(target)],
                compilation_options: PipelineCompilationOptions::default(),
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleList,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: None,
                polygon_mode: wgpu::PolygonMode::Fill,
                unclipped_depth: false,
                conservative: false,
            },
            depth_stencil: Some(wgpu::DepthStencilState {
                format: wgpu::TextureFormat::Depth32Float,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::Less,
                stencil: wgpu::StencilState::default(),
                bias: wgpu::DepthBiasState::default(),
            }),
            multisample: wgpu::MultisampleState {
                count: 4,
                ..wgpu::MultisampleState::default()
            },
            multiview: None,
        });

        let initial_transform: GpuTransform = Matrix4::identity().into();

        let transform_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Triangle Transform Buffer"),
            contents: bytemuck::cast_slice(&[initial_transform]),
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::UNIFORM,
        });

        let vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Triangle Vertex Buffer"),
            contents: &[],
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::VERTEX,
        });

        let index_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Triangle Index Buffer"),
            contents: &[],
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::INDEX,
        });

        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            label: Some("Triangle Bind Group"),
            layout: &bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: transform_buffer.as_entire_binding(),
            }],
        });

        TrianglePipeline {
            pipeline,
            bind_group,
            transform_buffer,
            vertex_buffer,
            index_buffer,
            index_buffer_len: 0,
        }
    }

    fn prepare(
        &mut self,
        device: &wgpu::Device,
        queue: &wgpu::Queue,
        transform: GpuTransform,
        vertices: &[GpuVertex],
        indices: &[u32],
    ) -> Vec<wgpu::CommandBuffer> {
        puffin::profile_function!();

        queue.write_buffer(
            &self.transform_buffer,
            0,
            bytemuck::cast_slice(&[transform]),
        );

        self.vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Triangle Vertex Buffer"),
            contents: bytemuck::cast_slice(vertices),
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::VERTEX,
        });

        self.index_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("Triangle Index Buffer"),
            contents: bytemuck::cast_slice(indices),
            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::INDEX,
        });

        self.index_buffer_len = indices.len() as u32;

        vec![]
    }

    fn paint<'rpass>(&'rpass self, render_pass: &mut wgpu::RenderPass<'rpass>) {
        puffin::profile_function!();
        render_pass.set_pipeline(&self.pipeline);
        render_pass.set_bind_group(0, &self.bind_group, &[]);
        render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));
        render_pass.set_index_buffer(self.index_buffer.slice(..), wgpu::IndexFormat::Uint32);
        render_pass.draw_indexed(0..self.index_buffer_len, 0, 0..1);
    }
}

#[derive(Copy, Clone, Default)]
pub struct MainViewStats {
    pub number_meshes: usize,
    pub number_triangles: usize,
}

#[derive(Debug)]
pub struct MainView {
    controls: OrbitControls,
}

impl MainView {
    pub fn new(render_state: Option<&RenderState>) -> MainView {
        puffin::profile_function!();

        let render_state = render_state.expect("No WGPU Renderer");

        render_state
            .renderer
            .write()
            .callback_resources
            .insert(LinePipeline::new(
                &render_state.device,
                render_state.target_format.into(),
            ));

        render_state
            .renderer
            .write()
            .callback_resources
            .insert(TrianglePipeline::new(
                &render_state.device,
                render_state.target_format.into(),
            ));

        Self {
            controls: OrbitControls::new(Point3::zero(), 0.0, std::f32::consts::FRAC_PI_2, 1.0),
        }
    }

    pub fn debug_ui(&mut self, ui: &mut egui::Ui) {
        ui.label(format!("{:#?}", self.controls));
    }

    pub fn update<'mesh>(
        &mut self,
        ui: &mut egui::Ui,
        triangles: impl Iterator<Item = (&'mesh TriMesh, Srgba)>,
        lines: impl Iterator<Item = (Point3, Point3, Srgba)>,
    ) -> MainViewStats {
        puffin::profile_function!();

        let mut stats = MainViewStats::default();

        let helper_meshes = [
            //(
            //TriMesh::cube() * Matrix4::translate(self.controls.target() - Point3::zero()),
            //three_d_asset::Srgba::new(255, 0, 255, 255),
            //)
        ];

        let meshes = triangles.map(|(m, c)| (m.clone(), c)).chain(helper_meshes);

        let (vertices, indices) = {
            puffin::profile_scope!("meshes");

            meshes.fold(
                (vec![], vec![]),
                |(mut vertices, mut indices), (mesh, color)| {
                    puffin::profile_scope!("mesh");

                    stats.number_meshes += 1;

                    let index_offset = vertices.len() as u32;

                    vertices.extend(mesh.positions.to_f32().iter().map(|&v| GpuVertex {
                        position: v.into(),
                        color: color.into(),
                    }));

                    if let Some(mesh_indices) = mesh.indices.to_u32() {
                        indices.extend(
                            mesh.indices
                                .to_u32()
                                .unwrap_or_default()
                                .iter()
                                .map(|i| i + index_offset),
                        );
                    } else {
                        indices.extend(index_offset..index_offset + mesh.positions.len() as u32);
                    }

                    (vertices, indices)
                },
            )
        };

        stats.number_triangles = indices.len() / 3;

        let helper_lines = [
            (
                Point3::new(0.0, 0.0, 0.0),
                Point3::new(10.0, 0.0, 0.0),
                Srgba::RED,
            ),
            (
                Point3::new(0.0, 0.0, 0.0),
                Point3::new(0.0, 10.0, 0.0),
                Srgba::GREEN,
            ),
            (
                Point3::new(0.0, 0.0, 0.0),
                Point3::new(0.0, 0.0, 10.0),
                Srgba::BLUE,
            ),
        ];

        let line_vertices = lines
            .chain(helper_lines)
            .flat_map(|(p1, p2, c)| {
                (0..6).into_iter().map(move |vertex_index| GpuLineVertex {
                    start_position: [p1.x, p1.y, p1.z],
                    end_position: [p2.x, p2.y, p2.z],
                    color: c.into(),
                    vertex_index,
                })
            })
            .collect();

        let rect = ui.available_rect_before_wrap();
        let response = ui.allocate_rect(rect, egui::Sense::click_and_drag());
        let scroll_delta = ui.input(|input| input.raw_scroll_delta);

        self.controls.update(scroll_delta, &response);

        let transform = GpuTransform {
            matrix: self.controls.transform(rect).into(),
        };

        ui.painter().add(egui_wgpu::Callback::new_paint_callback(
            rect,
            LinePipelineCallback {
                transform,
                size: rect.size().into(),
                lines: line_vertices,
            },
        ));

        ui.painter().add(egui_wgpu::Callback::new_paint_callback(
            rect,
            TrianglePipelineCallback {
                transform,
                vertices,
                indices,
            },
        ));

        stats
    }
}
