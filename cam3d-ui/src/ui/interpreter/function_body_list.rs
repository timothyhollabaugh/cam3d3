use crate::app::UserFunctionRunStatus;
use crate::ui::interpreter::function_call_edit::FunctionCallEdit;
use crate::ui::interpreter::value_detail::ValueDetailUi;
use crate::ui::project::operation::OperationUi;
use crate::ui::selection::SelectTarget;
use crate::ui::widgets::{error_label, justified};
use crate::ui::{ActiveEdit, CamUiState, UiTabs};
use cam3d_interpreter::call_stack::CallStack;
use cam3d_interpreter::expression::{Expression, ExpressionBlock, FunctionCall};
use cam3d_interpreter::function::user::UserFunction;
use cam3d_interpreter::inc_map::Key;
use cam3d_interpreter::item_path::ItemPath;
use cam3d_interpreter::library::{Libraries, UserFunctionHandle};
use cam3d_interpreter::value_key::{
    RelativeOutputKey, RelativeRuntimeKey, RelativeValueKey, ValueKey,
};
use cam3d_interpreter::values::Values;
use egui::emath::TSTransform;
use egui::epaint::PathStroke;
use egui::{
    Button, Color32, CursorIcon, LayerId, Order, Pos2, Rect, RichText, ScrollArea, SelectableLabel,
    Sense, Ui, Vec2, Widget,
};
use std::cmp::Ordering;
use std::collections::HashMap;
use std::fmt::Debug;
use std::time::Instant;

fn drop_separator(ui: &mut Ui) -> bool {
    if let Some(pointer_pos) = ui.ctx().pointer_interact_pos() {
        let interact_height = ui.style().spacing.interact_size.y;

        let rect = ui.available_rect_before_wrap();

        let center = rect.top() - ui.style().spacing.item_spacing.y / 2.0;
        let left = rect.left();
        let right = rect.right();

        let pointer_in_range = pointer_pos.y > center - interact_height / 2.0
            && pointer_pos.y < center + interact_height / 2.0;

        if pointer_in_range {
            ui.painter().line_segment(
                [Pos2::new(left, center), Pos2::new(right, center)],
                ui.visuals().widgets.active.fg_stroke,
            );

            true
        } else {
            false
        }
    } else {
        false
    }
}

pub enum FunctionBodyListResponse {
    None,
    Render,
    Operate,
}

pub fn function_body_list(
    ui: &mut Ui,
    ui_state: &mut CamUiState,
    function: &mut UserFunctionHandle,
    value_history: &HashMap<CallStack, Values>,
    running_call_stack: Option<&CallStack>,
) -> FunctionBodyListResponse {
    let mut response = FunctionBodyListResponse::None;

    let ui_id = ui.id();

    let mut dragged =
        function
            .get()
            .body
            .expressions
            .iter()
            .enumerate()
            .find_map(|(position, (key, _item))| {
                let drag_id = ui_id.with(key);

                if ui.ctx().is_being_dragged(drag_id)
                    || ui.ctx().drag_stopped_id().is_some_and(|id| id == drag_id)
                {
                    Some((key, drag_id, position, None))
                } else {
                    None
                }
            });

    let running_function_call = running_call_stack
        .and_then(|call_stack| call_stack.stack().first())
        .and_then(|runtime_key| runtime_key.output_key());

    let running_position = running_function_call.and_then(|running_function_call| {
        function
            .get()
            .body
            .expressions
            .position(running_function_call.expression_key)
    });

    ScrollArea::vertical().drag_to_scroll(false).show(ui, |ui| {
        ui.add_space(ui.style().spacing.item_spacing.y);

        if let Some((_drag_key, _drag_id, _drag_position, drop_position)) = dragged.as_mut() {
            let dropped = drop_separator(ui);

            if dropped {
                *drop_position = Some(0);
            }
        }

        let mut delete = None;
        for (position, (expression_key, expression)) in
            function.get().body.expressions.iter().enumerate()
        {
            if let Expression::FunctionCall(function_call) = expression {
                let running_order =
                    running_position.map(|running_position| position.cmp(&running_position));

                if dragged.is_some_and(|(drag_key, _, _, _)| drag_key == expression_key) {
                    ui.add_enabled_ui(false, |ui| {
                        function_call_row_ui(
                            ui,
                            ui_state,
                            function.path(),
                            expression_key,
                            function_call,
                            function.get(),
                            function.libraries(),
                            value_history,
                            running_order,
                        )
                    });
                } else {
                    let item_response = function_call_row_ui(
                        ui,
                        ui_state,
                        function.path(),
                        expression_key,
                        function_call,
                        function.get(),
                        function.libraries(),
                        value_history,
                        running_order,
                    );

                    match item_response {
                        FunctionCallRowUiResponse::None => {}
                        FunctionCallRowUiResponse::Drag => {
                            log::trace!("Dragging");
                            ui.ctx().set_dragged_id(ui_id.with(expression_key));
                        }
                        FunctionCallRowUiResponse::Delete => {
                            delete = Some(expression_key);
                        }
                        FunctionCallRowUiResponse::Render => {
                            dbg!(&ui_state.active_edit);
                            response = FunctionBodyListResponse::Render
                        }
                    }
                }

                if let Some((_drag_key, _drag_id, drag_position, drop_position)) = dragged.as_mut()
                {
                    let in_range = drop_separator(ui);

                    if in_range && ui.ctx().drag_stopped_id().is_some() {
                        if *drag_position >= position {
                            *drop_position = Some(position + 1);
                        } else {
                            *drop_position = Some(position);
                        }
                    }
                }
            }
        }

        if let Some(delete_key) = delete {
            function.get_mut().body.expressions.remove(delete_key);
        }

        if let Some((drag_key, drag_id, _drag_position, drop_position)) = dragged {
            if let Some(Expression::FunctionCall(item)) =
                function.get_mut().body.expressions.get_mut(drag_key)
            {
                ui.ctx().set_cursor_icon(CursorIcon::Grabbing);

                let layer_id = LayerId::new(Order::Tooltip, drag_id);
                let response = ui.with_layer_id(layer_id, |ui| ui.label(item.function.to_string()));

                if let Some(pointer_pos) = ui.ctx().pointer_interact_pos() {
                    let delta = pointer_pos.y - response.response.rect.center().y;
                    ui.ctx().transform_layer_shapes(
                        layer_id,
                        TSTransform::from_translation(Vec2::new(0.0, delta)),
                    );
                }
            }

            if let Some(position) = drop_position {
                function
                    .get_mut()
                    .body
                    .expressions
                    .move_to(position, drag_key);
            }
        }
    });

    response
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum FunctionCallRowUiResponse {
    None,
    Drag,
    Delete,
    Render,
}

fn function_call_row_ui(
    ui: &mut Ui,
    ui_state: &mut CamUiState,
    function_path: &ItemPath,
    function_key: Key<Expression>,
    function_call: &FunctionCall,
    caller_function: &UserFunction,
    libraries: &Libraries,
    value_history: &HashMap<CallStack, Values>,
    running_order: Option<Ordering>,
) -> FunctionCallRowUiResponse {
    let output_key = RelativeRuntimeKey::Output(RelativeOutputKey {
        expression_key: function_key,
    });
    let caller_call_stack = CallStack::new(function_path.clone());
    let callee_call_stack = caller_call_stack.clone().with_push(output_key);
    let absolute_key = output_key
        .clone()
        .to_value_key()
        .into_absolute(caller_call_stack);

    match libraries.lookup_fn(&function_call.function) {
        Ok(function) => {
            let responses = ui.horizontal(|ui| {
                let mut status_rect = ui.available_rect_before_wrap();
                status_rect.set_width(9.0);
                ui.allocate_rect(status_rect, Sense::hover());

                let responses = ui.vertical(|ui| {
                    let (edit_response, delete_response) = justified(
                        ui,
                        |ui| {
                            ui.heading(function.name())
                                .on_hover_text(function_call.function.to_string())
                        },
                        |ui| {
                            ui.add_enabled_ui(running_order.is_none(), |ui| {
                                let drag_button = Button::new("↕").sense(Sense::drag());
                                //let drag_response = ui.add(drag_button);
                                let delete_response = ui.button("x");
                                let edit_response = ui.button("E");

                                (edit_response, delete_response)
                            })
                            .inner
                        },
                    )
                    .1;

                    let output_type = function.output_type();
                    ui.horizontal(|ui| {
                        ui.label("➡");

                        let values = value_history.get(&callee_call_stack);
                        let value = values.and_then(|values| values.get(&absolute_key));

                        let targeted = ui_state
                            .selection
                            .target()
                            .and_then(|target| target.relative_value_select_target())
                            .is_some_and(|(call_stack, target_type)| {
                                let is_type_targeted = function.output_type() == target_type;

                                let is_output_targeted = if let Some(target_function_call_key) =
                                    call_stack.stack().first()
                                {
                                    caller_function
                                        .available_values(
                                            Some(*target_function_call_key),
                                            libraries,
                                        )
                                        .iter()
                                        .find(|(key, _)| *key == output_key)
                                        .is_some()
                                } else {
                                    true
                                };

                                is_type_targeted && is_output_targeted
                            });

                        let enabled = targeted || ui_state.selection.target().is_none();

                        let output_selected =
                            ui_state.selection.selection_iter().any(|selection| {
                                selection.relative_value().is_some_and(|selected_key| {
                                    *selected_key == output_key.clone().to_value_key()
                                })
                            });

                        let (value_select_response, value_detail_response) = ui
                            .add_enabled_ui(enabled, |ui| {
                                let mut text = RichText::new(output_key.to_string());

                                if targeted {
                                    text = text.strong();
                                }

                                let value_select_response = ui
                                    .selectable_label(output_selected, text)
                                    .on_hover_text(output_type.to_string());

                                let value_detail_response = if let Some(value) = value {
                                    ui.monospace("=");

                                    let first_line = value
                                        .to_string()
                                        .lines()
                                        .nth(0)
                                        .unwrap_or_default()
                                        .to_string();

                                    let first_line_truncated = if first_line.chars().count() > 32 {
                                        let truncated =
                                            first_line.chars().take(32).collect::<String>();
                                        format!("{truncated}...")
                                    } else {
                                        first_line.to_string()
                                    };

                                    ui.button(RichText::new(first_line_truncated).monospace())
                                } else {
                                    ui.button("...")
                                };

                                (value_select_response, value_detail_response)
                            })
                            .inner;

                        if value_select_response.clicked() && enabled {
                            ui_state
                                .selection
                                .add_selection(RelativeValueKey::Runtime(output_key));
                        }

                        if value_detail_response.clicked() {
                            ui_state.new_window(
                                UiTabs::ValueDetail(ValueDetailUi::new(
                                    output_type.clone(),
                                    function_path.clone(),
                                    callee_call_stack.clone(),
                                    absolute_key.clone(),
                                )),
                                true,
                            );
                        }

                        //ui.small(output_type.to_string())
                    });

                    (edit_response, delete_response)
                });

                status_rect.set_height(responses.response.rect.height());

                let mut painter = ui.painter_at(status_rect);

                let drag_sense = if running_order.is_none() {
                    Sense::drag()
                } else {
                    Sense::hover()
                };

                let mut drag_response = ui.allocate_rect(status_rect, drag_sense);

                let visuals = ui.style().interact(&drag_response);

                match running_order {
                    None => {
                        drag_response = drag_response.on_hover_cursor(CursorIcon::Grab);
                        painter.line_segment(
                            [status_rect.left_top(), status_rect.left_bottom()],
                            visuals.fg_stroke,
                        );

                        painter.line_segment(
                            [status_rect.center_top(), status_rect.center_bottom()],
                            visuals.fg_stroke,
                        );

                        painter.line_segment(
                            [status_rect.right_top(), status_rect.right_bottom()],
                            visuals.fg_stroke,
                        );
                    }
                    Some(Ordering::Less) => {
                        painter.rect(
                            status_rect,
                            0.0,
                            ui.visuals().widgets.inactive.fg_stroke.color,
                            ui.visuals().widgets.inactive.fg_stroke,
                        );
                    }
                    Some(Ordering::Equal) => {
                        let t = ((ui.input(|input| input.time) % 1.0) / 1.0) as f32;

                        let height = status_rect.height() / 4.0;

                        let y = if t < 0.5 {
                            status_rect.min.y + t * 2.0 * (status_rect.height() - height)
                        } else {
                            status_rect.max.y
                                - (t - 0.5) * 2.0 * (status_rect.height() - height)
                                - height
                        };

                        let rect = Rect::from_min_size(
                            Pos2::new(status_rect.min.x, y),
                            Vec2::new(status_rect.width(), height),
                        );

                        painter.rect(
                            rect,
                            0.0,
                            ui.visuals().widgets.active.fg_stroke.color,
                            ui.visuals().widgets.active.fg_stroke,
                        );
                    }
                    Some(Ordering::Greater) => {
                        painter.rect(
                            status_rect,
                            0.0,
                            ui.visuals().widgets.noninteractive.bg_fill,
                            ui.visuals().widgets.noninteractive.fg_stroke,
                        );
                    }
                }
                let (edit_response, delete_response) = responses.inner;

                (edit_response, drag_response, delete_response)
            });

            let (edit_response, drag_response, delete_response) = responses.inner;

            if edit_response.clicked() {
                ui_state.active_edit = ActiveEdit::FunctionCall(FunctionCallEdit::from_existing(
                    function_path.clone(),
                    function_key,
                ));
                ui_state.selection.clear_selection();
                ui_state.selection.clear_target();

                FunctionCallRowUiResponse::Render
            } else if drag_response.dragged() && drag_response.drag_delta().length() > 2.0 {
                FunctionCallRowUiResponse::Drag
            } else if delete_response.clicked() {
                FunctionCallRowUiResponse::Delete
            } else {
                FunctionCallRowUiResponse::None
            }
        }

        Err(e) => {
            error_label(ui, &e.to_string());
            FunctionCallRowUiResponse::None
        }
    }
}
