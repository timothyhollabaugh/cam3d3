use crate::app::InternalError;
use crate::ui::interpreter::literals::{LiteralUi, LiteralUiError};
use crate::ui::selection::SelectedItem;
use crate::ui::widgets::{error_label, justified};
use crate::ui::UiContext;
use cam3d_interpreter::call_stack::CallStack;
use cam3d_interpreter::inc_map::IncMap;
use cam3d_interpreter::interpreter::Interpreter;
use cam3d_interpreter::item_path::{ItemId, ItemPath};
use cam3d_interpreter::type_::TypeRef;
use cam3d_interpreter::value::Value;
use cam3d_interpreter::value_key::{AbsoluteRuntimeKey, FunctionCallInputKey, ValueKey};
use egui::collapsing_header::CollapsingState;
use egui::{CollapsingHeader, Id, Ui, WidgetText};
use itertools::Itertools;
use log::{debug, trace};
use std::collections::HashMap;

#[derive(Debug)]
pub struct ValueDetailUi {
    type_path: ItemPath,
    call_stack: CallStack,
    value_key: ValueKey,
    input_expression_uis: HashMap<(ItemId, FunctionCallInputKey), LiteralUi>,
}

impl PartialEq for ValueDetailUi {
    fn eq(&self, other: &Self) -> bool {
        self.type_path == other.type_path
            && self.call_stack == other.call_stack
            && self.value_key == other.value_key
    }
}

impl ValueDetailUi {
    pub fn new(type_path: ItemPath, call_stack: CallStack, value_key: ValueKey) -> ValueDetailUi {
        debug!("Opening value detail UI");
        debug!("call stack {call_stack}");
        debug!("value key {value_key}");
        ValueDetailUi {
            type_path,
            call_stack,
            value_key,
            input_expression_uis: HashMap::new(),
        }
    }

    pub fn title(&self) -> WidgetText {
        self.type_path.to_string().into()
    }

    pub fn show(&mut self, ui: &mut Ui, ui_context: &mut UiContext) -> Result<(), InternalError> {
        let libraries = &mut ui_context.cam_context.libraries;
        let type_ = libraries.lookup_type(&self.type_path)?;
        let run_status = ui_context
            .cam_context
            .function_run_status
            .get(self.call_stack.root());

        let value = run_status.and_then(|status| status.value(&self.call_stack, &self.value_key));

        match type_ {
            TypeRef::Native(native_type) => {
                if let Some(value) = value {
                    for method in native_type.methods() {
                        let method_id = ItemId::new(method.name());
                        let method_path = self.type_path.with_item(method_id.clone());

                        let mut ranges = method.ranges().clone();

                        let input_types = method.function().input_types();

                        let number_inputs = input_types.len();

                        let inputs: Option<Vec<_>> = input_types
                            .iter()
                            .map(|(input_key, input_type_path)| {
                                if input_key.name == "self" {
                                    Some((
                                        input_key,
                                        Box::new(std::iter::once(value.clone()))
                                            as Box<dyn ExactSizeIterator<Item = Value>>,
                                    ))
                                } else {
                                    let range = ranges.remove(&input_key);

                                    if let Some(range) = range {
                                        let range = range.range(value).ok()?;
                                        Some((input_key, range))
                                    } else {
                                        let literal_ui = self
                                            .input_expression_uis
                                            .entry((method_id.clone(), input_key.clone()))
                                            .or_insert_with(|| {
                                                LiteralUi::new(input_type_path.clone(), None)
                                            });

                                        literal_ui.value().map(|value| {
                                            (
                                                input_key,
                                                Box::new(std::iter::once(value.into_value()))
                                                    as Box<dyn ExactSizeIterator<Item = Value>>,
                                            )
                                        })
                                    }
                                }
                            })
                            .collect();

                        let mut outputs = if let Some(inputs) = inputs {
                            let outputs = inputs
                                .into_iter()
                                .map(|(key, range)| range.map(|v| (key.clone(), v)).collect_vec())
                                .multi_cartesian_product()
                                .map(|inputs| {
                                    let interpreter = Interpreter::new(
                                        libraries,
                                        method_path.clone(),
                                        inputs.clone(),
                                    );

                                    let output_result = interpreter.run(|_| ());

                                    (inputs, output_result)
                                })
                                .peekable();

                            Some(outputs)
                        } else {
                            None
                        };

                        if number_inputs == 1 {
                            justified(
                                ui,
                                |ui| {
                                    ui.strong(method.name());
                                },
                                |ui| {
                                    ui_context.state.selection.select_button_output_filter(
                                        ui,
                                        Some(method.f.output_type()),
                                        SelectedItem::NewFunctionCall(method_path.clone()),
                                    );

                                    if let Some((inputs, output_result)) =
                                        outputs.as_mut().and_then(|outputs| outputs.peek())
                                    {
                                        match output_result {
                                            Ok(output) => {
                                                if let Some(output) = output.get(
                                                    &ValueKey::Runtime(AbsoluteRuntimeKey::new(
                                                        CallStack::new(method_path.clone()),
                                                    )),
                                                ) {
                                                    ui.monospace(output.to_string());
                                                } else {
                                                    ui.weak("N/A");
                                                }
                                            }
                                            Err(e) => {
                                                log::error!("{e}");
                                                error_label(ui, &format!("{e}"));
                                            }
                                        }
                                    } else {
                                        ui.weak("N/A");
                                    }
                                },
                            );
                        } else {
                            CollapsingState::load_with_default_open(
                                ui.ctx(),
                                Id::new(method.name()),
                                false,
                            )
                            .show_header(ui, |ui| {
                                justified(
                                    ui,
                                    |ui| {
                                        ui.strong(method.name())
                                            .on_hover_text(format!("{}", method.f));
                                    },
                                    |ui| {
                                        ui_context.state.selection.select_button_output_filter(
                                            ui,
                                            Some(method.f.output_type()),
                                            SelectedItem::NewFunctionCall(method_path.clone()),
                                        )
                                    },
                                )
                            })
                            .body(|ui| {
                                for ((ui_method_id, input_index), literal_ui) in
                                    self.input_expression_uis.iter_mut()
                                {
                                    if *ui_method_id == method_id {
                                        justified(
                                            ui,
                                            |ui| {
                                                ui.label(format!("{}", input_index));
                                            },
                                            |ui| {
                                                literal_ui
                                                    .show(ui, &mut ui_context.cam_context.tasks)
                                            },
                                        );
                                    }
                                }

                                if let Some(outputs) = outputs {
                                    let text_style = egui::TextStyle::Body;
                                    let row_height = ui.text_style_height(&text_style);

                                    egui::ScrollArea::vertical().show_rows(
                                        ui,
                                        row_height,
                                        outputs.size_hint().0,
                                        |ui, range| {
                                            let visible_outputs =
                                                outputs.skip(range.start).take(range.len());
                                            for (inputs, output_result) in visible_outputs {
                                                justified(
                                                    ui,
                                                    |ui| {
                                                        for (key, input) in inputs.iter().rev() {
                                                            ui.monospace(input.to_string());
                                                        }
                                                    },
                                                    |ui| match output_result {
                                                        Ok(output) => {
                                                            if let Some(output) =
                                                                output.get(&ValueKey::Runtime(
                                                                    AbsoluteRuntimeKey::new(
                                                                        CallStack::new(
                                                                            method_path.clone(),
                                                                        ),
                                                                    ),
                                                                ))
                                                            {
                                                                ui.monospace(output.to_string());
                                                            } else {
                                                                ui.weak("N/A");
                                                            }
                                                        }
                                                        Err(e) => {
                                                            log::error!("{e}");
                                                            error_label(ui, &format!("{e}"));
                                                        }
                                                    },
                                                );
                                            }
                                        },
                                    );
                                }
                            });
                        }
                    }
                } else {
                    for method in native_type.methods() {
                        let method_id = ItemId::new(method.name());
                        let method_path = self.type_path.with_item(method_id.clone());

                        justified(
                            ui,
                            |ui| {
                                ui.strong(method.name())
                                    .on_hover_text(format!("{}", method.f));
                            },
                            |ui| {
                                ui_context.state.selection.select_button_output_filter(
                                    ui,
                                    Some(method.f.output_type()),
                                    SelectedItem::NewFunctionCall(method_path),
                                )
                            },
                        );
                    }
                }
            }

            TypeRef::User(user_type) => {
                for method in user_type.methods() {
                    ui.strong(method.name());
                }
            }
        }

        Ok(())
    }
}
