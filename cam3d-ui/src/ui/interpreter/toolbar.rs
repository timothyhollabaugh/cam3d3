use crate::app::InternalError;
use crate::ui::UiContext;
use cam3d_interpreter::item_path::ItemPath;
use egui::{Button, Image, ImageSource, Ui, WidgetText};

#[derive(Debug)]
pub enum ToolbarItemSize {
    Large,
    Small,
}

#[derive(Debug)]
pub struct ToolbarItem {
    pub name: String,
    pub icon: Option<egui::ImageSource<'static>>,
    pub size: ToolbarItemSize,
    pub function_path: ItemPath,
}

#[derive(Debug)]
pub struct ToolbarSection {
    pub name: String,
    pub items: Vec<ToolbarItem>,
}

#[derive(Debug)]
pub struct ToolbarUi {
    pub name: String,
    pub sections: Vec<ToolbarSection>,
}

impl PartialEq for ToolbarUi {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}

impl ToolbarUi {
    pub fn new(
        name: impl Into<String>,
        sections: Vec<(
            impl Into<String>,
            Vec<(
                impl Into<String>,
                Option<ImageSource<'static>>,
                ToolbarItemSize,
                ItemPath,
            )>,
        )>,
    ) -> ToolbarUi {
        let sections = sections
            .into_iter()
            .map(|(name, items)| {
                let items = items
                    .into_iter()
                    .map(|(name, icon, size, function_path)| ToolbarItem {
                        name: name.into(),
                        icon,
                        size,
                        function_path,
                    })
                    .collect();

                ToolbarSection {
                    name: name.into(),
                    items,
                }
            })
            .collect();

        ToolbarUi {
            name: name.into(),
            sections,
        }
    }

    pub fn show(&mut self, ui: &mut Ui, ui_context: &mut UiContext) -> Result<(), InternalError> {
        ui.horizontal(|ui| {
            for section in self.sections.iter() {
                ui.vertical(|ui| {
                    ui.vertical(|ui| {
                        for item in section.items.iter() {
                            let r = if let Some(image_source) = &item.icon {
                                ui.add(Button::image_and_text(
                                    Image::new(image_source.clone()),
                                    &item.name,
                                ))
                            } else {
                                ui.button(&item.name)
                            };

                            let r = r.on_hover_text(item.function_path.to_string());

                            /*
                            if r.clicked() {
                                if let Some(active_function_edit) =
                                    ui_context.state.active_function_edit.as_ref()
                                {
                                    log::debug!("Add item {}", item.function_path.clone());
                                    ui_context.state.active_edit =
                                        ActiveEdit::FunctionCall(FunctionCallEdit::new(
                                            active_function_edit.clone(),
                                            item.function_path.clone(),
                                        ));
                                    ui_context.state.selection.clear_selection();
                                    ui_context.state.selection.clear_target();
                                }
                            }
                             */
                        }
                    });
                    ui.label(&section.name);
                });
            }
        });

        Ok(())
    }

    pub fn title(&self) -> WidgetText {
        self.name.to_string().into()
    }
}
