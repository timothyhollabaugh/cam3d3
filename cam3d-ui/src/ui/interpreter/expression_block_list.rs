use crate::app::InternalError;
use crate::task::Tasks;
use crate::ui::interpreter::literals::LiteralUi;
use crate::ui::interpreter::value_detail::ValueDetailUi;
use crate::ui::selection::{SelectTargetType, SelectedItem};
use crate::ui::widgets::justified;
use crate::ui::{CamUiState, UiTabs};
use cam3d_interpreter::call_stack::{CallStack, KeyStack};
use cam3d_interpreter::expression::{Expression, ExpressionBlock, FunctionCall};
use cam3d_interpreter::inc_map::Key;
use cam3d_interpreter::item_path::ItemPath;
use cam3d_interpreter::library::{ExpressionBlockHandle, Libraries};
use cam3d_interpreter::value_key::{
    AbsoluteRuntimeKey, BlockExpressionKey, FunctionCallInputKey, RelativeRuntimeKey,
    RelativeValueKey, ValueKey,
};
use cam3d_interpreter::values::Values;
use egui::{Button, Frame, ScrollArea, SelectableLabel, Ui, WidgetText};
use indexmap::IndexMap;
use itertools::Itertools;
use std::collections::HashMap;

#[derive(Debug)]
enum FunctionCallInputUiKind {
    Empty,
    Literal(LiteralUi),
    Value(KeyStack),
    FunctionCall(FunctionCallUi),
    Block(ExpressionBlock, ExpressionBlockList),
}

#[derive(Debug)]
struct FunctionCallInputUi {
    kind: FunctionCallInputUiKind,
    type_path: ItemPath,
}

impl FunctionCallInputUi {
    fn new(type_path: ItemPath) -> FunctionCallInputUi {
        let literal_ui = LiteralUi::new(type_path.clone(), None);
        let kind = if literal_ui.is_ok() {
            FunctionCallInputUiKind::Literal(literal_ui)
        } else {
            FunctionCallInputUiKind::Empty
        };

        FunctionCallInputUi { kind, type_path }
    }

    fn from_expression(
        type_path: ItemPath,
        expression: &Expression,
        libraries: &Libraries,
    ) -> Result<FunctionCallInputUi, InternalError> {
        let kind = match expression {
            Expression::Literal(type_path, value) => FunctionCallInputUiKind::Literal(
                LiteralUi::new(type_path.clone(), Some(value.clone())),
            ),
            Expression::Value(value_stack) => FunctionCallInputUiKind::Value(value_stack.clone()),
            Expression::FunctionCall(function_call) => FunctionCallInputUiKind::FunctionCall(
                FunctionCallUi::from_existing(function_call, libraries)?,
            ),
            Expression::Block(block) => {
                FunctionCallInputUiKind::Block(block.clone(), ExpressionBlockList::new())
            }
        };

        Ok(FunctionCallInputUi { kind, type_path })
    }

    fn show(
        &mut self,
        ui: &mut Ui,
        ui_state: &mut CamUiState,
        tasks: &mut Tasks,
        label: &str,
        call_stack: &CallStack,
        libraries: &Libraries,
    ) {
        ui.horizontal(|ui| {
            match &mut self.kind {
                FunctionCallInputUiKind::Empty => {
                    ui.label(label);
                }
                FunctionCallInputUiKind::Literal(literal_ui) => {
                    ui.label(label);
                    ui.vertical(|ui| {
                        //ui.small(literal_ui.type_path().to_string());
                        literal_ui.show(ui, tasks);
                    });
                }
                FunctionCallInputUiKind::Value(key_stack) => {
                    ui.label(label);
                    ui.label(key_stack.iter().map(ToString::to_string).join(" -> "));
                }
                FunctionCallInputUiKind::FunctionCall(function_call_ui) => {
                    ui.vertical(|ui| {
                        ui.label(label);
                        ui.horizontal(|ui| {
                            ui.add_space(ui.style().spacing.icon_spacing);
                            Frame::group(ui.style()).show(ui, |ui| {
                                ui.vertical(|ui| {
                                    ui.small(function_call_ui.function_path.to_string());
                                    function_call_ui.show(
                                        ui,
                                        ui_state,
                                        tasks,
                                        call_stack.clone(),
                                        libraries,
                                    );
                                })
                            })
                        });
                    });
                }
                FunctionCallInputUiKind::Block(block, block_ui) => {
                    ui.label(label);
                    ui.label("TODO: block ui");
                }
            }

            let selected_expression = ui_state.selection.target_label(
                ui,
                "↗",
                SelectTargetType::Expression(call_stack.clone(), Some(self.type_path.clone())),
            );

            match selected_expression {
                Some(SelectedItem::NewLiteral(type_path)) => {
                    log::info!("Editing new literal {label}: {type_path}");
                    let literal_ui = LiteralUi::new(type_path.clone(), None);
                    self.kind = FunctionCallInputUiKind::Literal(literal_ui);
                }
                Some(SelectedItem::RelativeValue(selected_call_stack)) => {
                    if selected_call_stack.root() == call_stack.root() {
                        let key_stack = selected_call_stack.stack().clone();
                        log::info!("New value {label}: {key_stack}");
                        self.kind = FunctionCallInputUiKind::Value(key_stack);
                    } else {
                        log::info!("Selected value from a different function");
                    }
                }
                Some(SelectedItem::NewFunctionCall(function_path)) => {
                    log::info!("New function call {label}: {function_path}");
                    let r: Result<(), InternalError> = try {
                        self.kind = FunctionCallInputUiKind::FunctionCall(FunctionCallUi::new(
                            function_path,
                            libraries,
                        )?);
                    };

                    if let Err(e) = r {
                        log::error!("Failed to edit function call {label}: {e:#?}");
                    }
                }
                Some(SelectedItem::NewBlock) => {
                    log::info!("New block {label}");
                    self.kind = FunctionCallInputUiKind::Block(
                        ExpressionBlock::new(),
                        ExpressionBlockList::new(),
                    );
                }
                _ => {}
            }
        });
    }

    fn expression(&self) -> Option<Expression> {
        match &self.kind {
            FunctionCallInputUiKind::Empty => None,
            FunctionCallInputUiKind::Literal(literal_ui) => literal_ui
                .value()
                .map(|v| Expression::Literal(literal_ui.type_path().clone(), v)),
            FunctionCallInputUiKind::Value(key) => Some(Expression::Value(key.clone())),
            FunctionCallInputUiKind::FunctionCall(function_call_ui) => function_call_ui
                .function_call()
                .map(Expression::FunctionCall),
            FunctionCallInputUiKind::Block(block, _) => Some(Expression::Block(block.clone())),
        }
    }
}

#[derive(Debug)]
struct FunctionCallUi {
    function_path: ItemPath,
    inputs: IndexMap<FunctionCallInputKey, FunctionCallInputUi>,
}

impl FunctionCallUi {
    pub fn new(
        function_path: ItemPath,
        libraries: &Libraries,
    ) -> Result<FunctionCallUi, InternalError> {
        let function = libraries.lookup_fn(&function_path)?;
        let inputs = function
            .input_types()
            .iter()
            .map(|(input_key, input_type)| {
                let input_ui = FunctionCallInputUi::new(input_type.clone());
                (input_key.clone(), input_ui)
            })
            .collect();

        Ok(FunctionCallUi {
            function_path,
            inputs,
        })
    }

    pub fn from_existing(
        function_call: &FunctionCall,
        libraries: &Libraries,
    ) -> Result<FunctionCallUi, InternalError> {
        let function = libraries.lookup_fn(&function_call.function)?;
        let inputs = function
            .input_types()
            .iter()
            .map(|(input_key, input_type)| try {
                let input_ui =
                    if let Some(input_expression) = function_call.input_values.get(input_key) {
                        FunctionCallInputUi::from_expression(
                            input_type.clone(),
                            input_expression,
                            libraries,
                        )?
                    } else {
                        FunctionCallInputUi::new(input_type.clone())
                    };

                (input_key.clone(), input_ui)
            })
            .collect::<Result<IndexMap<FunctionCallInputKey, FunctionCallInputUi>, InternalError>>(
            )?;

        Ok(FunctionCallUi {
            function_path: function_call.function.clone(),
            inputs,
        })
    }

    pub fn show(
        &mut self,
        ui: &mut Ui,
        ui_state: &mut CamUiState,
        tasks: &mut Tasks,
        call_stack: CallStack,
        libraries: &Libraries,
    ) {
        ui.vertical(|ui| {
            for (input_key, input_ui) in self.inputs.iter_mut() {
                let input_call_stack = call_stack
                    .clone()
                    .with_push(RelativeRuntimeKey::FunctionCallInput(input_key.clone()));

                ui.push_id(input_key, |ui| {
                    input_ui.show(
                        ui,
                        ui_state,
                        tasks,
                        &input_key.to_string(),
                        &input_call_stack,
                        libraries,
                    );
                });
            }
        });
    }

    pub fn function_call(&self) -> Option<FunctionCall> {
        let mut input_values = HashMap::new();
        for (input_key, input_ui) in self.inputs.iter() {
            if let Some(input_expression) = input_ui.expression() {
                input_values.insert(input_key.clone(), input_expression);
            } else {
                return None;
            }
        }

        Some(FunctionCall {
            function: self.function_path.clone(),
            input_values,
        })
    }
}

#[derive(Debug)]
enum InlineExpressionEdit {
    Literal(LiteralUi),
    FunctionCall(FunctionCallUi),
}

impl InlineExpressionEdit {
    fn show(
        &mut self,
        ui: &mut Ui,
        ui_state: &mut CamUiState,
        tasks: &mut Tasks,
        call_stack: CallStack,
        libraries: &Libraries,
    ) -> (bool, Option<Expression>) {
        let mut close_edit_field = false;
        let mut confirm_expression = None;

        ui.horizontal(|ui| match self {
            InlineExpressionEdit::Literal(literal_ui) => {
                literal_ui.show(ui, tasks);
                let new_value = literal_ui.value();

                let confirm_button = Button::new("✔");
                let confirm_response = ui
                    .add_enabled(new_value.is_some(), confirm_button)
                    .on_hover_text("Confirm");

                if confirm_response.clicked() {
                    if let Some(new_value) = new_value {
                        confirm_expression = Some(Expression::Literal(
                            literal_ui.type_path().clone(),
                            new_value,
                        ));
                        log::info!("Confirm new literal");
                        close_edit_field = true;
                    }
                }

                let cancel_response = ui.button("🗙").on_hover_text("Cancel");

                if cancel_response.clicked() {
                    log::info!("Cancel new literal");
                    close_edit_field = true;
                }
            }
            InlineExpressionEdit::FunctionCall(function_call_ui) => {
                ui.vertical(|ui| {
                    ui.horizontal(|ui| {
                        ui.label(function_call_ui.function_path.item_id.to_string());
                        let new_function_call = function_call_ui.function_call();

                        let confirm_button = Button::new("✔");
                        let confirm_response = ui
                            .add_enabled(new_function_call.is_some(), confirm_button)
                            .on_hover_text("Confirm");

                        if confirm_response.clicked() {
                            if let Some(new_function_call) = new_function_call {
                                confirm_expression =
                                    Some(Expression::FunctionCall(new_function_call));
                                log::info!("Confirm new function call");
                                close_edit_field = true;
                            }
                        }

                        let cancel_response = ui.button("🗙").on_hover_text("Cancel");

                        if cancel_response.clicked() {
                            log::info!("Cancel new literal");
                            close_edit_field = true;
                        }
                    });

                    function_call_ui.show(ui, ui_state, tasks, call_stack, libraries);
                });
            }
        });

        (close_edit_field, confirm_expression)
    }
}

fn expression_row_ui(
    ui: &mut Ui,
    ui_state: &mut CamUiState,
    tasks: &mut Tasks,
    expression: &Expression,
    expression_edit_field: Option<&mut InlineExpressionEdit>,
    expression_key: &BlockExpressionKey,
    call_stack: CallStack,
    libraries: &Libraries,
) -> (
    Option<Expression>,
    Option<Option<InlineExpressionEdit>>,
    bool,
) {
    let mut new_expression = None;
    let mut new_inline_field = None;
    let mut delete = false;

    ui.horizontal(|ui| {
        if let Some(expression_edit) = expression_edit_field {
            let (close, new_inline_expression) =
                expression_edit.show(ui, ui_state, tasks, call_stack, libraries);
            if close {
                new_inline_field = Some(None);
            }
            new_expression = new_inline_expression;
        } else {
            match expression {
                Expression::Literal(type_path, value) => {
                    ui.label(format!("{}", value.clone().into_value()));

                    let edit_response = ui.button("✏").on_hover_text("Edit");
                    if edit_response.clicked() {
                        log::info!("Editing literal");
                        let literal_ui = LiteralUi::new(type_path.clone(), Some(value.clone()));
                        let inline_edit = InlineExpressionEdit::Literal(literal_ui);
                        new_inline_field = Some(Some(inline_edit));
                    }
                }
                Expression::Value(value_key) => {
                    ui.label(value_key.to_string());
                }
                Expression::FunctionCall(function_call) => {
                    ui.label(function_call.function.item_id.to_string());

                    let edit_response = ui.button("✏").on_hover_text("Edit");
                    if edit_response.clicked() {
                        log::info!("Editing function_call");
                        let r: Result<(), InternalError> = try {
                            let function_call_ui =
                                FunctionCallUi::from_existing(function_call, libraries)?;
                            let inline_edit = InlineExpressionEdit::FunctionCall(function_call_ui);
                            new_inline_field = Some(Some(inline_edit));
                        };

                        if let Err(e) = r {
                            log::error!("Failed to edit function call ui: {e:#?}");
                        }
                    }
                }
                Expression::Block(_) => {
                    ui.label("block ui");
                }
            };

            let selected_expression = ui_state.selection.target_label(
                ui,
                "↗",
                SelectTargetType::Expression(call_stack.clone(), None),
            );

            match selected_expression {
                Some(SelectedItem::NewLiteral(type_path)) => {
                    log::info!("Editing new literal {expression_key}: {type_path}");
                    let literal_ui = LiteralUi::new(type_path.clone(), None);
                    let inline_edit = InlineExpressionEdit::Literal(literal_ui);
                    new_inline_field = Some(Some(inline_edit));
                }
                Some(SelectedItem::RelativeValue(selected_call_stack)) => {
                    if selected_call_stack.root() == call_stack.root() {
                        let key_stack = selected_call_stack.stack().clone();
                        log::info!("New value {expression_key}: {key_stack}");
                        new_expression = Some(Expression::Value(key_stack));
                    } else {
                        log::info!("Selected value from different function");
                    }
                }
                Some(SelectedItem::NewFunctionCall(function_path)) => {
                    log::info!("New function call {expression_key}: {function_path}");
                    let r: Result<(), InternalError> = try {
                        let function_call_ui = FunctionCallUi::new(function_path, libraries)?;
                        let inline_edit = InlineExpressionEdit::FunctionCall(function_call_ui);
                        new_inline_field = Some(Some(inline_edit));
                    };

                    if let Err(e) = r {
                        log::error!("Failed to add new function call ui: {e:#?}");
                    }
                }
                Some(SelectedItem::NewBlock) => {
                    log::info!("New block {expression_key}");
                    new_expression = Some(Expression::Block(ExpressionBlock::new()));
                }
                _ => {}
            }

            if ui.button("x").clicked() {
                delete = true;
            }
        };
    });

    (new_expression, new_inline_field, delete)
}

#[derive(Debug)]
enum InlineExpressionField {
    None,
    New(InlineExpressionEdit),
    Existing(BlockExpressionKey, InlineExpressionEdit),
}

impl InlineExpressionField {
    pub fn for_key(
        &mut self,
        expression_key: &BlockExpressionKey,
    ) -> Option<&mut InlineExpressionEdit> {
        match self {
            InlineExpressionField::None => None,
            InlineExpressionField::New(_) => None,
            InlineExpressionField::Existing(key, inline_edit) if key == expression_key => {
                Some(inline_edit)
            }
            InlineExpressionField::Existing(_, _) => None,
        }
    }
}

#[derive(Debug)]
pub struct ExpressionBlockList {
    expression_edit_field: InlineExpressionField,
}

impl ExpressionBlockList {
    pub fn new() -> ExpressionBlockList {
        ExpressionBlockList {
            expression_edit_field: InlineExpressionField::None,
        }
    }

    pub fn show(
        &mut self,
        ui: &mut Ui,
        ui_state: &mut CamUiState,
        tasks: &mut Tasks,
        mut block_handle: ExpressionBlockHandle,
        value_history: &IndexMap<CallStack, Values>,
        running_call_stack: Option<&CallStack>,
    ) {
        let call_stack = block_handle.call_stack().clone();

        ScrollArea::vertical().drag_to_scroll(false).show(ui, |ui| {
            let mut move_to = None;
            let mut delete = None;
            let mut new_expression = None;
            let block = block_handle.get();
            for (index, (block_key, expression)) in block.iter().enumerate() {
                let expression_call_stack = call_stack.clone().with_push(block_key.into());
                let absolute_key =
                    ValueKey::Runtime(AbsoluteRuntimeKey::new(expression_call_stack.clone()));
                let output_type = match block_handle
                    .libraries()
                    .lookup_output_type(expression_call_stack.clone())
                {
                    Ok(output_type) => Some(output_type),
                    Err(e) => {
                        log::error!("{e:#?}");
                        None
                    }
                };

                //log::trace!("{expression_call_stack}");
                //log::trace!("{:#?}", &value_history);
                let values = value_history.get(&expression_call_stack);
                //log::trace!("{}", values.is_some());
                let value = values.and_then(|values| values.get(&absolute_key));
                //log::trace!("{}", value.is_some());

                egui::Frame::group(ui.style()).show(ui, |ui| {
                    let ui_rect = ui.available_rect_before_wrap();
                    ui.expand_to_include_x(ui_rect.max.x);

                    ui.horizontal(|ui| {
                        ui.vertical(|ui| {
                            if ui.button("⬆").clicked() {
                                move_to = Some((block_key, index - 1));
                            }

                            if ui.button("⬇").clicked() {
                                move_to = Some((block_key, index + 2));
                            }
                        });

                        ui.vertical(|ui| {
                            match expression {
                                Expression::Literal(type_path, _) => {
                                    ui.small(format!("{}", type_path));
                                }
                                Expression::Value(_) => {}
                                Expression::FunctionCall(function_call) => {
                                    ui.small(function_call.function.parents_string());
                                }
                                Expression::Block(_) => {}
                            };

                            let r = expression_row_ui(
                                ui,
                                ui_state,
                                tasks,
                                expression,
                                self.expression_edit_field.for_key(&block_key),
                                &block_key,
                                expression_call_stack.clone(),
                                block_handle.libraries(),
                            );

                            let (new_row_expression, new_edit_field, delete_row) = r;

                            if let Some(expression) = new_row_expression {
                                new_expression = Some((block_key, expression));
                            }

                            match new_edit_field {
                                Some(Some(inline_edit)) => {
                                    self.expression_edit_field =
                                        InlineExpressionField::Existing(block_key, inline_edit)
                                }
                                Some(None) => {
                                    self.expression_edit_field = InlineExpressionField::None
                                }
                                None => {}
                            }

                            if delete_row {
                                delete = Some(block_key);
                            }

                            let ((open_value_detail_ui), _) = justified(
                                ui,
                                |ui| {
                                    ui.label(format!("{block_key}"));

                                    let value_detail_response = if let Some(value) = value {
                                        ui.monospace("=");

                                        let first_line = value
                                            .to_string()
                                            .lines()
                                            .nth(0)
                                            .unwrap_or_default()
                                            .to_string();

                                        ui.button(first_line)
                                    } else {
                                        ui.button("...")
                                    };

                                    value_detail_response.clicked()
                                },
                                |ui| {
                                    ui_state.selection.select_button_output_filter(
                                        ui,
                                        output_type.as_ref(),
                                        SelectedItem::RelativeValue(expression_call_stack.clone()),
                                    );
                                },
                            );

                            if open_value_detail_ui {
                                match block_handle
                                    .libraries()
                                    .lookup_output_type(expression_call_stack.clone())
                                {
                                    Ok(output_type) => {
                                        let value_detail_ui = ValueDetailUi::new(
                                            output_type,
                                            expression_call_stack.clone(),
                                            absolute_key,
                                        );
                                        ui_state
                                            .new_window(UiTabs::ValueDetail(value_detail_ui), true);
                                    }
                                    Err(e) => {
                                        log::error!(
                                            "Error looking up output type for value detail: {e:#?}"
                                        );
                                    }
                                }
                            }
                        });
                    });
                });
            }

            if let Some((block_key, new_expression)) = new_expression {
                if let Some(old_expression) = block_handle
                    .get_mut()
                    .expressions
                    .get_mut(block_key.expression_key)
                {
                    *old_expression = new_expression;
                }
            }

            if let Some((key, position)) = move_to {
                block_handle
                    .get_mut()
                    .expressions
                    .move_to(position, key.expression_key);
            }

            if let Some(delete_key) = delete {
                block_handle
                    .get_mut()
                    .expressions
                    .remove(delete_key.expression_key);
            }

            let mut close_new_edit_field = false;
            if let InlineExpressionField::New(inline_edit) = &mut self.expression_edit_field {
                let (close, new_expression) =
                    inline_edit.show(ui, ui_state, tasks, call_stack, block_handle.libraries());
                if close {
                    log::info!("Closing edit field");
                    self.expression_edit_field = InlineExpressionField::None;
                }
                if let Some(new_expression) = new_expression {
                    block_handle.get_mut().expressions.insert(new_expression);
                    log::info!("Confirm new literal");
                }
            } else {
                let selected_expression = ui_state.selection.target_label(
                    ui,
                    "New Expression",
                    SelectTargetType::Expression(call_stack.clone(), None),
                );

                match selected_expression {
                    Some(SelectedItem::NewLiteral(type_path)) => {
                        let literal_ui = LiteralUi::new(type_path.clone(), None);
                        let inline_edit = InlineExpressionEdit::Literal(literal_ui);
                        self.expression_edit_field = InlineExpressionField::New(inline_edit);
                    }
                    Some(SelectedItem::RelativeValue(selected_call_stack)) => {
                        if selected_call_stack.root() == call_stack.root() {
                            block_handle
                                .get_mut()
                                .expressions
                                .insert(Expression::Value(selected_call_stack.stack().clone()));
                        } else {
                            log::info!("Selected value from different function")
                        }
                    }
                    Some(SelectedItem::NewFunctionCall(function_path)) => {
                        log::info!("New function call: {function_path}");
                        let r: Result<(), InternalError> = try {
                            let function_call_ui =
                                FunctionCallUi::new(function_path, block_handle.libraries())?;
                            let inline_edit = InlineExpressionEdit::FunctionCall(function_call_ui);
                            self.expression_edit_field = InlineExpressionField::New(inline_edit);
                        };

                        log::trace!("{:?}", self.expression_edit_field);

                        if let Err(e) = r {
                            log::error!("Failed creating new function call edit: {e:#?}");
                        }
                    }
                    Some(SelectedItem::NewBlock) => {
                        block_handle
                            .get_mut()
                            .expressions
                            .insert(Expression::Block(ExpressionBlock::new()));
                    }
                    _ => {}
                }
            }
        });
    }
}
