use crate::app::InternalError;
use crate::task::render_interpreter_task::{RenderInterpreterTask, RenderMesh};
use crate::task::{RunningTaskHandle, RunningTaskHandleDone};
use crate::ui::gpu::main_view::MainView;
use crate::ui::UiContext;
use cam3d_interpreter::function::FunctionRef;
use cam3d_interpreter::inc_map::IncMap;
use cam3d_interpreter::item_path::ItemPath;
use cam3d_interpreter::library::LookupHandleError;
use cam3d_interpreter::value_key::ValueKey;
use cam3d_interpreter::values::Values;
use egui::{Color32, RichText, Ui, WidgetText};
use indexmap::IndexMap;
use std::backtrace::Backtrace;
use std::collections::{HashMap, HashSet};
use std::time::Instant;

#[derive(Debug)]
enum RenderState {
    NotRendered,
    Rendering(Instant, RunningTaskHandle<RenderInterpreterTask>),
    Done(Instant, IndexMap<ValueKey, RenderMesh>),
}

#[derive(Debug)]
pub struct Function3dVeiw {
    function_path: ItemPath,
    main_view: MainView,
    render_state: RenderState,
}

impl PartialEq for Function3dVeiw {
    fn eq(&self, other: &Self) -> bool {
        self.function_path == other.function_path
    }
}

impl Function3dVeiw {
    pub fn new(
        function_path: ItemPath,
        render_state: Option<&egui_wgpu::RenderState>,
    ) -> Function3dVeiw {
        Function3dVeiw {
            function_path,
            main_view: MainView::new(render_state),
            render_state: RenderState::NotRendered,
        }
    }

    pub fn title(&self, ui_context: &UiContext) -> WidgetText {
        let libraries = &ui_context.cam_context.libraries;
        let function = libraries.lookup_fn(&self.function_path);

        match function {
            Ok(FunctionRef::User(function)) => format!("{} 3D", &function.name).into(),
            Ok(FunctionRef::Native(function)) => RichText::new(format!("{} 3D", function.name()))
                .italics()
                .into(),
            Err(e) => RichText::new(format!("Error: {}", e))
                .color(Color32::RED)
                .into(),
        }
    }

    pub fn show(&mut self, ui: &mut Ui, ui_context: &mut UiContext) -> Result<(), InternalError> {
        let libraries = &ui_context.cam_context.libraries;
        let FunctionRef::User(function) = libraries.lookup_fn(&self.function_path)? else {
            return Err(InternalError::LookupHandle(
                LookupHandleError::FoundNativeFunctionButExpectedUserFunction {
                    function_path: self.function_path.clone(),
                    backtrace: Backtrace::capture(),
                },
            ));
        };

        let render_state = std::mem::replace(&mut self.render_state, RenderState::NotRendered);

        let render_state = match render_state {
            RenderState::NotRendered => {
                let timestamp_value_history = ui_context
                    .cam_context
                    .function_run_status
                    .get(&self.function_path)
                    .map(|status| (status.last_update(), status.value_history()));

                if let Some((timestamp, value_history)) = timestamp_value_history {
                    let values =
                        value_history
                            .iter()
                            .fold(Values::new(), |mut all_values, (_, values)| {
                                for (key, value) in values.iter() {
                                    if let ValueKey::Runtime(runtime_key) = key {
                                        all_values.insert_value(runtime_key, value.clone());
                                    }
                                }
                                all_values
                            });

                    if values.len() > 0 {
                        let task = RenderInterpreterTask::new(values.clone());
                        let handle = ui_context.cam_context.tasks.spawn(task);
                        log::debug!("Rendering {} values", values.len());
                        RenderState::Rendering(timestamp, handle)
                    } else {
                        RenderState::NotRendered
                    }
                } else {
                    RenderState::NotRendered
                }
            }
            RenderState::Rendering(last_update, handle) => match handle.take_done() {
                RunningTaskHandleDone::Done(result) => {
                    let result_len = result.meshes.len();

                    let meshes: IndexMap<_, _> = result
                        .meshes
                        .into_iter()
                        .flat_map(|(key, mesh)| mesh.ok().map(|mesh| (key, mesh)))
                        .collect();

                    log::debug!("Done rendering. {}/{} values", meshes.len(), result_len);

                    RenderState::Done(last_update, meshes)
                }
                RunningTaskHandleDone::NotDone(handle) => {
                    RenderState::Rendering(last_update, handle)
                }
                RunningTaskHandleDone::Panicked => RenderState::NotRendered,
            },
            RenderState::Done(last_update, meshes) => {
                let timestamp_value_history = ui_context
                    .cam_context
                    .function_run_status
                    .get(&self.function_path)
                    .map(|status| (status.last_update(), status.value_history()));

                let done_render_state = RenderState::Done(last_update, meshes);

                if let Some((timestamp, value_history)) = timestamp_value_history {
                    if timestamp > last_update {
                        let values = value_history.iter().fold(
                            Values::new(),
                            |mut all_values, (_, values)| {
                                for (key, value) in values.iter() {
                                    if let ValueKey::Runtime(runtime_key) = key {
                                        all_values.insert_value(runtime_key, value.clone());
                                    }
                                }
                                all_values
                            },
                        );

                        if values.len() > 0 {
                            let task = RenderInterpreterTask::new(values.clone());
                            let handle = ui_context.cam_context.tasks.spawn(task);
                            log::debug!("Rendering {} values", values.len());
                            RenderState::Rendering(timestamp, handle)
                        } else {
                            RenderState::NotRendered
                        }
                    } else {
                        done_render_state
                    }
                } else {
                    done_render_state
                }
            }
        };

        std::mem::replace(&mut self.render_state, render_state);

        let meshes = if let RenderState::Done(_, meshes) = &self.render_state {
            meshes
        } else {
            &IndexMap::new()
        };

        let selection_color = three_d_asset::Srgba::from(
            ui.style()
                .visuals
                .selection
                .bg_fill
                .to_normalized_gamma_f32(),
        );

        let selection: HashSet<_> = ui_context.state.selection.selection_iter().collect();

        let triangles = meshes.iter().flat_map(move |(k, m)| {
            //let selected = selection.contains(&SelectedItem::RelativeValue(k.clone()));
            m.triangles.iter().map(|(m, c)| (m, *c))
            //.map(move |(m, c)| (m, if selected { selection_color } else { *c }))
        });

        let lines = meshes.iter().flat_map(move |(k, m)| {
            //let selected = selection.contains(&SelectedItem::InterpreterValue(k.clone()));
            m.lines.iter().map(|(p1, p2, c)| (*p1, *p2, *c))
            //.map(move |(p1, p2, c)| (*p1, *p2, if selected { selection_color } else { *c }))
        });

        self.main_view.update(ui, triangles, lines);

        Ok(())
    }
}
