use crate::ui::interpreter::UserFunctionUi;
use crate::ui::selection::SelectedItem;
use crate::ui::widgets::justified;
use crate::ui::{CamUiState, UiContext, UiTabs};
use cam3d_interpreter::function::FunctionRef;
use cam3d_interpreter::item_path::{ItemId, ItemPath};
use cam3d_interpreter::module::native::NativeModule;
use cam3d_interpreter::module::user::UserModule;
use cam3d_interpreter::type_::NativeType;
use egui::SelectableLabel;

fn insert_fn_button(
    ui: &mut egui::Ui,
    ui_state: &mut CamUiState,
    function: FunctionRef,
    path: ItemPath,
) {
    let selected_item = SelectedItem::NewFunctionCall(path.clone());

    let targeted = ui_state
        .selection
        .target()
        .and_then(|target| target.expression_select_target())
        .is_some_and(|(_, type_path)| {
            type_path.is_none()
                || type_path
                    .as_ref()
                    .is_some_and(|type_path| type_path == function.output_type())
        });

    let selected = ui_state
        .selection
        .selection_iter()
        .find(|selection| **selection == selected_item)
        .is_some();

    let fn_button = SelectableLabel::new(selected, function.name());
    let fn_button_response = ui.add_enabled(targeted, fn_button);

    if fn_button_response.clicked() {
        if selected {
            ui_state.selection.unselect(selected_item);
        } else {
            ui_state.selection.add_selection(selected_item);
        }
    }
}

pub fn native_type_tree(
    ui: &mut egui::Ui,
    ui_state: &mut CamUiState,
    type_: &NativeType,
    library: &str,
    module_path: Vec<ItemId>,
) {
    ui.vertical(|ui| {
        for method in type_.methods() {
            let p = ItemPath {
                library: library.to_string(),
                module_path: module_path.clone(),
                item_id: ItemId::new(method.name().to_string()),
            };
            insert_fn_button(ui, ui_state, method.function().into(), p);
        }
    });
}

pub fn native_module_tree(
    ui: &mut egui::Ui,
    ui_state: &mut CamUiState,
    module: &NativeModule,
    library: &str,
    module_path: Vec<ItemId>,
) {
    ui.vertical(|ui| {
        if !module.types.is_empty() {
            ui.collapsing(format!("Types ({})", module.types.len()), |ui| {
                for (id, type_) in module.types.iter() {
                    if type_.methods().next().is_some() {
                        ui.collapsing(type_.name(), |ui| {
                            let mut type_path = module_path.clone();
                            type_path.push(id.clone());
                            native_type_tree(ui, ui_state, type_, library, type_path);
                        });
                    } else {
                        ui.label(type_.name());
                    }
                }
            });
        }

        if !module.functions.is_empty() {
            ui.collapsing(format!("Functions ({})", module.functions.len()), |ui| {
                for (id, function) in module.functions.iter() {
                    let p = ItemPath {
                        library: library.to_string(),
                        module_path: module_path.clone(),
                        item_id: id.clone(),
                    };

                    insert_fn_button(ui, ui_state, function.into(), p);
                }
            });
        }

        if !module.modules.is_empty() {
            ui.collapsing(format!("Modules ({})", module.modules.len()), |ui| {
                for (id, sub_module) in module.modules.iter() {
                    ui.collapsing(&sub_module.name, |ui| {
                        let mut sub_module_path = module_path.clone();
                        sub_module_path.push(id.clone());
                        native_module_tree(ui, ui_state, sub_module, library, sub_module_path);
                    });
                }
            });
        }
    });
}

pub fn user_module_tree(
    ui: &mut egui::Ui,
    ui_state: &mut CamUiState,
    module: &UserModule,
    library_name: &str,
    path: Vec<ItemId>,
) {
    ui.vertical(|ui| {
        if !module.types.is_empty() {
            ui.collapsing(format!("Types ({})", module.types.len()), |ui| {
                for (id, type_) in module.types.iter() {
                    ui.label(&type_.name);
                }
            });
        }

        if !module.functions.is_empty() {
            ui.collapsing(format!("Functions ({})", module.functions.len()), |ui| {
                for (id, function) in module.functions.iter() {
                    let function_path = ItemPath {
                        library: library_name.to_string(),
                        module_path: path.clone(),
                        item_id: ItemId::new(&function.name),
                    };

                    justified(
                        ui,
                        |ui| {
                            ui.label(&function.name);
                        },
                        |ui| {
                            if ui.button("E").clicked() {
                                let new_tab =
                                    UiTabs::UserFunction(UserFunctionUi::new(function_path));
                                ui_state.new_tab(new_tab, true);
                            }
                        },
                    );
                }
            });
        }

        if !module.modules.is_empty() {
            ui.collapsing(format!("Modules ({})", module.modules.len()), |ui| {
                for (id, sub_module) in module.modules.iter() {
                    let mut new_path = path.clone();
                    new_path.push(ItemId::new(&sub_module.name));

                    ui.collapsing(&sub_module.name, |ui| {
                        user_module_tree(ui, ui_state, sub_module, library_name, new_path);
                    });
                }
            });
        }
    });
}

pub fn library_tree(ui: &mut egui::Ui, ui_context: &mut UiContext) {
    let libraries = &ui_context.cam_context.libraries;
    ui.vertical(|ui| {
        for (name, library) in libraries.native_libraries() {
            ui.collapsing(name, |ui| {
                native_module_tree(ui, ui_context.state, &library.root, name, vec![])
            });
        }
        for (name, library) in libraries.user_libraries() {
            ui.collapsing(name, |ui| {
                user_module_tree(
                    ui,
                    ui_context.state,
                    &library.root,
                    library.name(),
                    Vec::new(),
                )
            });
        }
    });
}
