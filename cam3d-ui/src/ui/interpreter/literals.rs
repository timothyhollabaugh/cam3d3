use crate::cam::geometry2d::{Point2, Vector2};
use crate::cam::geometry3d::{Point3, Vector3};
use crate::task::Tasks;
use crate::ui::selection::SelectedItem;
use crate::ui::widgets::{error_label, PickFileField};
use crate::ui::UiContext;
use cam3d_interpreter::call_stack::KeyStack;
use cam3d_interpreter::core_module::{FloatOptionValue, PathValue};
use cam3d_interpreter::item_path::ItemPath;
use cam3d_interpreter::value::{LiteralValue, LiteralValueBox, ValueDowncastError, ValueUnboxed};
use egui::{Checkbox, DragValue, SelectableLabel, Ui};
use indexmap::IndexMap;
use std::path::PathBuf;
use std::sync::LazyLock;

type LiteralUiConstructor = Box<
    dyn Fn(Option<LiteralValue>) -> Result<Box<dyn LiteralUiType>, LiteralUiError> + Send + Sync,
>;

fn literal_ui<T, UI>(f: fn(Option<T>) -> UI) -> LiteralUiConstructor
where
    T: LiteralValueBox + ValueUnboxed + Clone + 'static,
    UI: LiteralUiType + 'static,
{
    Box::new(move |value| {
        let downcast_value = if let Some(v) = value {
            Some(v.downcast::<T>()?.clone())
        } else {
            None
        };
        let ui = f(downcast_value);
        Ok(Box::new(ui))
    })
}

static LITERAL_TYPE_MAP: LazyLock<IndexMap<ItemPath, LiteralUiConstructor>> = LazyLock::new(|| {
    [
        ("core::int::Integer", literal_ui(IntegerLiteralUi::new)),
        ("core::float::Float", literal_ui(FloatLiteralUi::new)),
        (
            "core::float::Option<Float>",
            literal_ui(FloatOptionLiteralUi::new),
        ),
        ("core::bool::Bool", literal_ui(BoolLiteralUi::new)),
        ("core::fs::PathOpen", literal_ui(PathLiteralUi::new_open)),
        ("core::fs::PathSave", literal_ui(PathLiteralUi::new_save)),
        ("core::String", literal_ui(StringLiteralUi::new_singleline)),
        (
            "core::MultiLineString",
            literal_ui(StringLiteralUi::new_multiline),
        ),
        ("cam::geometry::Point2d", literal_ui(Point2LiteralUi::new)),
        ("cam::geometry::Vector2d", literal_ui(Vector2LiteralUi::new)),
        ("cam::geometry::Point3d", literal_ui(Point3LiteralUi::new)),
        ("cam::geometry::Vector3d", literal_ui(Vector3LiteralUi::new)),
    ]
    .into_iter()
    .map(|(path_string, f)| (path_string.parse().unwrap(), f))
    .collect()
});

#[derive(thiserror::Error, Debug)]
pub enum LiteralUiError {
    #[error("No UI exists for literals of type {0:?}")]
    NoUiForType(ItemPath),
    #[error("Value literal has wrong type for field")]
    ExistingValueHasWrongType(
        #[from]
        #[backtrace]
        ValueDowncastError,
    ),
}

trait LiteralUiType: std::fmt::Debug {
    fn show(&mut self, ui: &mut Ui, tasks: &mut Tasks);
    fn value(&self) -> Option<LiteralValue>;
}

#[derive(Debug)]
pub struct LiteralUi {
    ui_type: Result<Box<dyn LiteralUiType>, LiteralUiError>,
    type_path: ItemPath,
}

impl LiteralUi {
    pub fn new(type_path: ItemPath, value: Option<LiteralValue>) -> LiteralUi {
        let ui_type = LITERAL_TYPE_MAP
            .get(&type_path)
            .ok_or_else(|| LiteralUiError::NoUiForType(type_path.clone()))
            .and_then(|ui_constructor| ui_constructor(value));

        LiteralUi { ui_type, type_path }
    }

    pub fn has_ui(type_path: &ItemPath) -> bool {
        LITERAL_TYPE_MAP.contains_key(type_path)
    }

    pub fn is_ok(&self) -> bool {
        self.ui_type.is_ok()
    }

    pub fn show(&mut self, ui: &mut Ui, tasks: &mut Tasks) {
        match &mut self.ui_type {
            Ok(ui_type) => ui_type.show(ui, tasks),
            Err(e) => {
                error_label(ui, &format!("{}", e));
            }
        }
    }

    pub fn value(&self) -> Option<LiteralValue> {
        self.ui_type
            .as_ref()
            .ok()
            .and_then(|ui_type| ui_type.value())
    }

    pub fn type_path(&self) -> &ItemPath {
        &self.type_path
    }
}

#[derive(Debug)]
struct IntegerLiteralUi {
    value: i32,
}

impl IntegerLiteralUi {
    pub fn new(value: Option<i32>) -> IntegerLiteralUi {
        IntegerLiteralUi {
            value: value.unwrap_or(1),
        }
    }
}

impl LiteralUiType for IntegerLiteralUi {
    fn show(&mut self, ui: &mut Ui, tasks: &mut Tasks) {
        ui.add(DragValue::new(&mut self.value));
    }

    fn value(&self) -> Option<LiteralValue> {
        Some(LiteralValue::from_value(self.value))
    }
}

#[derive(Debug)]
pub struct FloatLiteralUi {
    value: f32,
}

impl FloatLiteralUi {
    pub fn new(value: Option<f32>) -> FloatLiteralUi {
        FloatLiteralUi {
            value: value.unwrap_or(1.0),
        }
    }
}

impl LiteralUiType for FloatLiteralUi {
    fn show(&mut self, ui: &mut Ui, tasks: &mut Tasks) {
        ui.add(DragValue::new(&mut self.value));
    }

    fn value(&self) -> Option<LiteralValue> {
        Some(LiteralValue::from_value(self.value))
    }
}

#[derive(Debug)]
pub struct FloatOptionLiteralUi {
    has_value: bool,
    value: f32,
}

impl FloatOptionLiteralUi {
    pub fn new(value: Option<FloatOptionValue>) -> FloatOptionLiteralUi {
        if let Some(float) = value {
            FloatOptionLiteralUi {
                value: float.0.unwrap_or(0.0),
                has_value: float.0.is_some(),
            }
        } else {
            FloatOptionLiteralUi {
                value: 0.0,
                has_value: false,
            }
        }
    }
}

impl LiteralUiType for FloatOptionLiteralUi {
    fn show(&mut self, ui: &mut Ui, tasks: &mut Tasks) {
        ui.horizontal(|ui| {
            ui.checkbox(&mut self.has_value, "");
            ui.add_enabled(self.has_value, DragValue::new(&mut self.value));
        });
    }

    fn value(&self) -> Option<LiteralValue> {
        Some(LiteralValue::from_value(FloatOptionValue(
            self.has_value.then_some(self.value),
        )))
    }
}

#[derive(Debug)]
pub struct BoolLiteralUi {
    value: bool,
}

impl BoolLiteralUi {
    pub fn new(value: Option<bool>) -> BoolLiteralUi {
        BoolLiteralUi {
            value: value.unwrap_or(false),
        }
    }
}

impl LiteralUiType for BoolLiteralUi {
    fn show(&mut self, ui: &mut Ui, tasks: &mut Tasks) {
        ui.add(Checkbox::new(&mut self.value, ""));
    }

    fn value(&self) -> Option<LiteralValue> {
        Some(LiteralValue::from_value(self.value))
    }
}

#[derive(Debug)]
pub struct StringLiteralUi {
    value: String,
    multiline: bool,
}

impl StringLiteralUi {
    pub fn new(value: Option<String>, multiline: bool) -> StringLiteralUi {
        StringLiteralUi {
            value: value.unwrap_or_default(),
            multiline,
        }
    }

    pub fn new_singleline(value: Option<String>) -> StringLiteralUi {
        StringLiteralUi::new(value, false)
    }

    pub fn new_multiline(value: Option<String>) -> StringLiteralUi {
        StringLiteralUi::new(value, true)
    }
}

impl LiteralUiType for StringLiteralUi {
    fn show(&mut self, ui: &mut Ui, tasks: &mut Tasks) {
        if self.multiline {
            ui.text_edit_multiline(&mut self.value);
        } else {
            ui.text_edit_singleline(&mut self.value);
        }
    }

    fn value(&self) -> Option<LiteralValue> {
        Some(LiteralValue::from_value(self.value.clone()))
    }
}

#[derive(Debug)]
pub struct PathLiteralUi {
    path_picker: PickFileField,
    write: bool,
}

impl PathLiteralUi {
    pub fn new(value: Option<PathBuf>, write: bool) -> PathLiteralUi {
        PathLiteralUi {
            path_picker: if let Some(value) = value {
                PickFileField::with_picked(value)
            } else {
                PickFileField::new(false)
            },
            write,
        }
    }

    pub fn new_open(value: Option<PathValue>) -> PathLiteralUi {
        PathLiteralUi::new(value.map(|p| p.0), false)
    }

    pub fn new_save(value: Option<PathValue>) -> PathLiteralUi {
        PathLiteralUi::new(value.map(|p| p.0), true)
    }
}

impl LiteralUiType for PathLiteralUi {
    fn show(&mut self, ui: &mut Ui, tasks: &mut Tasks) {
        self.path_picker.ui(ui, tasks, "Pick File", &[], self.write);
    }

    fn value(&self) -> Option<LiteralValue> {
        self.path_picker
            .picked()
            .map(|path| LiteralValue::from_value(PathValue(path.clone())))
    }
}

#[derive(Debug)]
pub struct Point2LiteralUi {
    vector2: Point2,
}

impl Point2LiteralUi {
    pub fn new(vector2: Option<Point2>) -> Point2LiteralUi {
        let vector2 = vector2.unwrap_or(Point2::new(0.0, 0.0));
        Point2LiteralUi { vector2 }
    }
}

impl LiteralUiType for Point2LiteralUi {
    fn show(&mut self, ui: &mut Ui, tasks: &mut Tasks) {
        ui.vertical(|ui| {
            ui.horizontal(|ui| {
                ui.label("point x: ");
                ui.add(DragValue::new(&mut self.vector2.x));
            });
            ui.horizontal(|ui| {
                ui.label("point y: ");
                ui.add(DragValue::new(&mut self.vector2.y));
            });
        });
    }

    fn value(&self) -> Option<LiteralValue> {
        Some(LiteralValue::from_value(self.vector2))
    }
}

#[derive(Debug)]
pub struct Vector2LiteralUi {
    vector2: Vector2,
}

impl Vector2LiteralUi {
    pub fn new(vector2: Option<Vector2>) -> Vector2LiteralUi {
        let vector2 = vector2.unwrap_or(Vector2::new(0.0, 0.0));
        Vector2LiteralUi { vector2 }
    }
}

impl LiteralUiType for Vector2LiteralUi {
    fn show(&mut self, ui: &mut Ui, tasks: &mut Tasks) {
        ui.vertical(|ui| {
            ui.horizontal(|ui| {
                ui.label("vector x: ");
                ui.add(DragValue::new(&mut self.vector2.x));
            });
            ui.horizontal(|ui| {
                ui.label("vector y: ");
                ui.add(DragValue::new(&mut self.vector2.y));
            });
        });
    }

    fn value(&self) -> Option<LiteralValue> {
        Some(LiteralValue::from_value(self.vector2))
    }
}

#[derive(Debug)]
pub struct Point3LiteralUi {
    vector3: Point3,
}

impl Point3LiteralUi {
    pub fn new(vector3: Option<Point3>) -> Point3LiteralUi {
        let vector3 = vector3.unwrap_or(Point3::new(0.0, 0.0, 0.0));
        Point3LiteralUi { vector3 }
    }
}

impl LiteralUiType for Point3LiteralUi {
    fn show(&mut self, ui: &mut Ui, tasks: &mut Tasks) {
        ui.vertical(|ui| {
            ui.horizontal(|ui| {
                ui.label("point x: ");
                ui.add(DragValue::new(&mut self.vector3.x));
            });
            ui.horizontal(|ui| {
                ui.label("point y: ");
                ui.add(DragValue::new(&mut self.vector3.y));
            });
            ui.horizontal(|ui| {
                ui.label("point z: ");
                ui.add(DragValue::new(&mut self.vector3.z));
            });
        });
    }

    fn value(&self) -> Option<LiteralValue> {
        Some(LiteralValue::from_value(self.vector3))
    }
}

#[derive(Debug)]
pub struct Vector3LiteralUi {
    vector3: Vector3,
}

impl Vector3LiteralUi {
    pub fn new(vector3: Option<Vector3>) -> Vector3LiteralUi {
        let vector3 = vector3.unwrap_or(Vector3::new(0.0, 0.0, 0.0));
        Vector3LiteralUi { vector3 }
    }
}

impl LiteralUiType for Vector3LiteralUi {
    fn show(&mut self, ui: &mut Ui, tasks: &mut Tasks) {
        ui.vertical(|ui| {
            ui.horizontal(|ui| {
                ui.label("vector x: ");
                ui.add(DragValue::new(&mut self.vector3.x));
            });
            ui.horizontal(|ui| {
                ui.label("vector y: ");
                ui.add(DragValue::new(&mut self.vector3.y));
            });
            ui.horizontal(|ui| {
                ui.label("vector z: ");
                ui.add(DragValue::new(&mut self.vector3.z));
            });
        });
    }

    fn value(&self) -> Option<LiteralValue> {
        Some(LiteralValue::from_value(self.vector3))
    }
}

#[derive(Debug)]
pub struct ValueKeyUi {
    key_stack: KeyStack,
}

impl ValueKeyUi {
    pub fn new(key_stack: KeyStack) -> ValueKeyUi {
        ValueKeyUi { key_stack }
    }

    pub fn show(&mut self, ui: &mut Ui, targeted: bool) -> bool {
        ui.selectable_label(targeted, self.key_stack.to_string())
            .clicked()
    }
}

pub fn literals_ui(ui: &mut Ui, ui_context: &mut UiContext) {
    for (type_path, _literal_ui_constructor) in LITERAL_TYPE_MAP.iter() {
        let selected_item = SelectedItem::NewLiteral(type_path.clone());

        let targeted = ui_context
            .state
            .selection
            .target()
            .and_then(|target| target.expression_select_target())
            .is_some_and(|(_, p)| p.is_none() || p.as_ref().is_some_and(|p| p == type_path));

        let selected = ui_context
            .state
            .selection
            .selection_iter()
            .find(|selection| **selection == selected_item)
            .is_some();

        let fn_button = SelectableLabel::new(selected, format!("{type_path}"));
        let fn_button_response = ui.add_enabled(targeted, fn_button);

        if fn_button_response.clicked() {
            if selected {
                ui_context.state.selection.unselect(selected_item);
            } else {
                ui_context.state.selection.add_selection(selected_item);
            }
        }
    }
}
