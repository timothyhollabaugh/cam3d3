/*
pub struct RelativeValueSelectField {
    type_path: ItemPath,
    call_stack: CallStack,
    selected: Option<RelativeValueKey>,
}

impl RelativeValueSelectField {
    pub fn new(
        type_path: ItemPath,
        call_stack: CallStack,
        selected: Option<RelativeValueKey>,
    ) -> RelativeValueSelectField {
        RelativeValueSelectField {
            type_path,
            call_stack,
            selected,
        }
    }

    pub fn new_unselected(type_path: ItemPath, call_stack: CallStack) -> RelativeValueSelectField {
        RelativeValueSelectField::new(type_path, call_stack, None)
    }

    pub fn new_selected(
        type_path: ItemPath,
        call_stack: CallStack,
        selected: RelativeValueKey,
    ) -> RelativeValueSelectField {
        RelativeValueSelectField::new(type_path, call_stack, Some(selected))
    }

    pub fn selected(&self) -> Option<RelativeValueKey> {
        self.selected.clone()
    }

    pub fn show(
        &mut self,
        ui: &mut Ui,
        selection: &mut Selection,
        function: &UserFunction,
        label: &str,
    ) {
        let field_selected = selection.is_target_for(label);

        let mut rect = ui
            .available_rect_before_wrap()
            .shrink2(ui.style().spacing.item_spacing);

        rect.set_height(
            ui.style().spacing.interact_size.y + ui.style().spacing.button_padding.y * 2.0,
        );

        let response = ui.allocate_rect(rect, Sense::click_and_drag());
        let painter = ui.painter();
        let mut field_visuals = ui.style().interact_selectable(&response, field_selected);

        painter.rect(
            rect,
            field_visuals.rounding,
            field_visuals.bg_fill,
            field_visuals.bg_stroke,
        );

        let ui_rect = rect.shrink2(ui.spacing().button_padding);
        let mut select_ui = ui.child_ui(ui_rect, Layout::top_down(Align::Min), None);

        let mut clear_selection = false;

        if let Some(selected) = &self.selected {
            let mut text = RichText::new(selected.to_string());
            if field_selected {
                text = text.strong();
            }

            justified(
                ui,
                |ui| ui.label(text),
                |ui| clear_selection = ui.button("x").clicked(),
            );
        } else {
            let mut text = RichText::new(format!("Select {}", label));
            if field_selected {
                text = text.strong();
            }
            select_ui.label(text);
        }

        if clear_selection {
            self.selected = None;
            selection.clear_selection();
        }

        if response.clicked() {
            if field_selected {
                selection.clear_selection();
                selection.clear_target();
            } else {
                if let Some(selected) = self.selected.as_ref() {
                    selection.select_only(selected);
                } else {
                    selection.clear_selection();
                }

                selection.set_target(
                    label,
                    false,
                    SelectTargetType::RelativeValue(
                        self.call_stack.clone(),
                        self.type_path.clone(),
                    ),
                )
            }
        } else {
            if field_selected {
                self.selected = selection
                    .selection_iter()
                    .find_map(SelectedItem::relative_value)
                    .cloned()
            }
        }
    }
}
*/
