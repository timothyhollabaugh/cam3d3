use crate::app::{InternalError, UserFunctionRun, UserFunctionRunStatus};
use crate::task::interpreter_task::{InterpreterDone, InterpreterProgress, InterpreterTask};
use crate::task::{
    RunningTaskHandle, RunningTaskHandleDone, RunningTaskHandleDoneProgress, TaskProgress,
};
use crate::ui::interpreter::expression_block_list::ExpressionBlockList;
use crate::ui::interpreter::function_3d::Function3dVeiw;
use crate::ui::interpreter::library_tree::user_module_tree;
use crate::ui::widgets::{error_label, justified, string_field};
use crate::ui::{ActiveEdit, UiContext, UiTabs};
use cam3d_interpreter::call_stack::CallStack;
use cam3d_interpreter::expression::FunctionCall;
use cam3d_interpreter::function::user::UserFunction;
use cam3d_interpreter::function::{FunctionMut, FunctionRef};
use cam3d_interpreter::inc_map::{IncMap, Key};
use cam3d_interpreter::interpreter::RuntimeError;
use cam3d_interpreter::item_path::{ItemId, ItemPath};
use cam3d_interpreter::library::user::UserLibrary;
use cam3d_interpreter::library::{Libraries, LibraryRef};
use cam3d_interpreter::values::Values;
use egui::{Button, Color32, PopupCloseBehavior, RichText, TextEdit, Ui, WidgetText};
use egui_dock::Split;
use indexmap::IndexMap;
use log::error;
use std::collections::HashMap;
use std::time::Instant;

mod expression_block_list;
pub mod function_3d;
mod literals;
//pub mod function_body_list;
pub mod library_tree;
pub mod relative_value_select_field;
pub mod toolbar;
pub mod value_detail;
pub mod value_list;

pub use literals::literals_ui;

#[derive(Default, Debug, PartialEq)]
pub struct InterpreterUi {}

impl InterpreterUi {
    pub fn show(&mut self, ui: &mut Ui, ui_context: &mut UiContext) {
        let mut save = false;
        let mut save_as = false;
        if let Some(library_project) = ui_context.cam_context.library_project {
            ui.vertical(|ui| {
                justified(
                    ui,
                    |ui| {
                        ui.heading(&library_project.name);
                    },
                    |ui| {
                        if ui.button("Save As").clicked() {
                            save_as = true;
                        }
                        let save_response = ui
                            .add_enabled(library_project.file_path.is_some(), Button::new("Save"));
                        if save_response.clicked() {
                            save = true;
                        }
                    },
                );

                if let Some(file_path) = library_project.file_path.as_ref() {
                    ui.label(file_path.to_string_lossy());
                } else {
                    ui.label("Not saved yet");
                }

                let library = ui_context.cam_context.libraries.get(&library_project.name);

                match library {
                    Some(LibraryRef::User(lib)) => {
                        user_module_tree(ui, ui_context.state, &lib.root, lib.name(), Vec::new());
                    }
                    Some(_) => {
                        error_label(ui, "Error: Project library is a native library");
                    }
                    None => {
                        error_label(ui, "Error: Project library not found");
                    }
                }
            });
        } else {
            ui.vertical(|ui| {
                if ui.button("New").clicked() {
                    let main_fn = ui_context.cam_context.new_library_project();
                    ui_context.state.new_split(
                        UiTabs::UserFunction(UserFunctionUi::new(main_fn)),
                        Split::Below,
                        0.2,
                        true,
                    );
                }

                if ui.button("Open").clicked() {
                    ui_context.cam_context.load_interpreter_project();
                }
            });
        }

        if save {
            ui_context.cam_context.save_interpreter_project();
        }

        if save_as {
            ui_context.cam_context.save_as_interpreter_project();
        }
    }
}

#[derive(Debug)]
pub struct UserFunctionUi {
    function_path: ItemPath,
    block_list: ExpressionBlockList,
}

impl PartialEq for UserFunctionUi {
    fn eq(&self, other: &Self) -> bool {
        self.function_path == other.function_path
    }
}

impl UserFunctionUi {
    pub fn new(function_path: ItemPath) -> UserFunctionUi {
        UserFunctionUi {
            function_path,
            block_list: ExpressionBlockList::new(),
        }
    }

    pub fn title(&self, ui_context: &UiContext) -> WidgetText {
        let libraries = &ui_context.cam_context.libraries;
        let function = libraries.lookup_fn(&self.function_path);

        match function {
            Ok(FunctionRef::User(function)) => function.name.clone().into(),
            Ok(FunctionRef::Native(function)) => RichText::new(function.name()).italics().into(),
            Err(e) => RichText::new(format!("Error: {}", e))
                .color(Color32::RED)
                .into(),
        }
    }

    pub fn function_path(&self) -> &ItemPath {
        &self.function_path
    }

    pub fn show(&mut self, ui: &mut Ui, ui_context: &mut UiContext) -> Result<(), InternalError> {
        puffin::profile_function!();

        let libraries = &mut ui_context.cam_context.libraries;
        let mut function = libraries.lookup_fn_handle(&self.function_path)?;

        let status = ui_context
            .cam_context
            .function_run_status
            .get(&self.function_path);

        let r = ui.vertical(|ui| {
            ui.heading(function.get().name());

            let enabled = status.is_none_or(|status| !status.running());
            let button = Button::new("Run");
            let run_interpreter = ui.add_enabled(enabled, button).clicked();
            if ui.button("3d").clicked() {
                ui_context.state.new_center(
                    UiTabs::UserFunction3d(Function3dVeiw::new(
                        self.function_path.clone(),
                        ui_context.render_context.frame.wgpu_render_state(),
                    )),
                    true,
                );
            }

            let empty_map = IndexMap::new();
            let value_history = status
                .map(|status| status.value_history())
                .unwrap_or(&empty_map);

            let running_call_stack = status
                .map(|status| status.running_call_stack())
                .unwrap_or_default();

            string_field(ui, "Name", &mut function.get_mut().name);

            self.block_list.show(
                ui,
                &mut ui_context.state,
                &mut ui_context.cam_context.tasks,
                function.body_handle(),
                value_history,
                running_call_stack,
            );

            /*
            function_body_list(
                ui,
                ui_context.state,
                &mut function,
                value_history,
                running_call_stack,
            );
             */

            /*
            let new_function = self.new_function_call_ui.show(ui, function.libraries());

            if let Some(new_function) = new_function {
                let new_function_call = FunctionCall {
                    function: new_function,
                    input_values: Vec::new(),
                };

                function.get_mut().body.insert(new_function_call);
            }

            self.user_function_values_ui
                .show(ui, function.get(), function.libraries());
             */

            run_interpreter
        });

        let run_interpreter = r.inner;

        if run_interpreter {
            let task = InterpreterTask {
                libraries: function.libraries().clone(),
                main_fn: self.function_path.clone(),
            };

            let task_handle = ui_context.cam_context.tasks.spawn(task);
            ui_context.cam_context.function_run_status.insert(
                self.function_path.clone(),
                UserFunctionRun {
                    last_update: Instant::now(),
                    value_history: IndexMap::new(),
                    status: UserFunctionRunStatus::Running {
                        running_call_stack: None,
                        task_handle,
                    },
                },
            );
        }

        Ok(())
    }
}

/*
#[derive(Default, Debug, Clone)]
pub struct UserFunctionValuesUi {}

impl UserFunctionValuesUi {
    fn show(&mut self, ui: &mut Ui, function: &UserFunction, libraries: &Libraries) {
        ui.vertical(|ui| {
            for (relative_key, type_path) in function.available_values(None, libraries) {
                match type_path {
                    Ok(type_path) => {
                        let value_row = Button::new(format!("{relative_key}: {type_path}"));
                        ui.add(value_row);
                    }
                    Err(e) => {
                        error_label(ui, &format!("{e}"));
                    }
                }
            }
        });
    }
}

#[derive(Default, Debug, Clone)]
pub struct NewFunctionCallUi {
    search: String,
}

impl NewFunctionCallUi {
    fn show(&mut self, ui: &mut Ui, libraries: &Libraries) -> Option<ItemPath> {
        let mut confirm = false;
        let mut selected = None;

        ui.horizontal(|ui| {
            let available_width = ui.available_width();
            let submit_button_width = ui.style().spacing.button_padding.x * 12.0;
            let search_bar_width =
                available_width - submit_button_width - ui.style().spacing.item_spacing.x;

            let search_bar = TextEdit::singleline(&mut self.search).desired_width(search_bar_width);

            let response = ui.add(search_bar);
            let popup_id = ui.make_persistent_id("function_results_popup");

            if response.has_focus() {
                ui.memory_mut(|mem| mem.open_popup(popup_id));
            }

            egui::containers::popup::popup_below_widget(
                ui,
                popup_id,
                &response,
                PopupCloseBehavior::CloseOnClickOutside,
                |ui| {
                    let search = self.search.clone();
                    let filtered_functions = libraries
                        .list_fn_paths()
                        .map(|item_path| item_path.to_string())
                        .filter(|path| path.contains(&search));

                    for function_path in filtered_functions {
                        let button = egui::Button::new(function_path.clone()).frame(false);
                        let response = ui.add(button);
                        if response.clicked() {
                            self.search = function_path;
                        }
                    }
                },
            );

            let item_path = self.search.parse::<ItemPath>();
            if let Ok(item_path) = item_path {
                let found = libraries.lookup_fn(&item_path).is_ok();
                if found {
                    selected = Some(item_path);
                } else {
                    selected = None;
                }
            }

            if ui.input(|i| i.key_pressed(egui::Key::Enter)) {
                confirm = true;
            }

            ui.with_layout(egui::Layout::right_to_left(egui::Align::Center), |ui| {
                let confirm_button = egui::Button::new("✔").fill(Color32::DARK_GREEN);
                let response = ui.add_enabled(selected.is_some(), confirm_button);
                if response.clicked() {
                    confirm = true;
                }
            });
        });

        if confirm {
            self.search.clear();
        }

        selected.filter(|_| confirm)
    }
}
*/
