use std::fmt::Display;
use std::hash::Hash;
use std::path::PathBuf;

use egui::{Response, RichText, ScrollArea, TextStyle};

use crate::cam::geometry2d::{LineString, Point2};
use crate::cam::geometry3d::{Point3, Vector3};
use crate::task::pick_file::PickFileTask;
use crate::task::{OptionalRunningTaskHandle, RunningTaskHandle, Tasks};

pub fn error_label(ui: &mut egui::Ui, text: &str) -> Response {
    ui.label(
        RichText::new(text)
            .color(ui.visuals().error_fg_color)
            .italics(),
    )
}

pub fn justified<R, L>(
    ui: &mut egui::Ui,
    left: impl FnOnce(&mut egui::Ui) -> L,
    right: impl FnOnce(&mut egui::Ui) -> R,
) -> (L, R) {
    ui.horizontal(|ui| {
        let left_response = ui.horizontal(left).inner;
        let right_response = ui
            .with_layout(egui::Layout::right_to_left(egui::Align::Center), right)
            .inner;
        (left_response, right_response)
    })
    .inner
}

pub fn number_field(
    ui: &mut egui::Ui,
    label: impl Into<egui::WidgetText>,
    value: &mut f32,
) -> egui::Response {
    let (_, response) = justified(
        ui,
        |ui| ui.label(label),
        |ui| ui.add(egui::DragValue::new(value)),
    );

    response
}

pub fn checkbox_field(
    ui: &mut egui::Ui,
    label: impl Into<egui::WidgetText>,
    value: &mut bool,
) -> egui::Response {
    let (_, response) = justified(ui, |ui| ui.label(label), |ui| ui.checkbox(value, ""));

    response
}

pub fn string_field(
    ui: &mut egui::Ui,
    label: impl Into<egui::WidgetText>,
    value: &mut String,
) -> egui::Response {
    let (_, response) = justified(
        ui,
        |ui| ui.label(label),
        |ui| ui.text_edit_singleline(value),
    );

    response
}

pub fn point_field(
    ui: &mut egui::Ui,
    label: impl Into<egui::WidgetText>,
    value: &mut Point2,
) -> egui::Response {
    let (_, response) = justified(
        ui,
        |ui| ui.label(label),
        |ui| {
            ui.horizontal(|ui| {
                ui.add(egui::DragValue::new(&mut value.y));
                ui.add(egui::DragValue::new(&mut value.x));
            })
            .response
        },
    );
    response
}

pub fn point3_field(
    ui: &mut egui::Ui,
    label: impl Into<egui::WidgetText>,
    value: &mut Point3,
) -> egui::Response {
    let (_, response) = justified(
        ui,
        |ui| ui.label(label),
        |ui| {
            ui.horizontal(|ui| {
                ui.add(egui::DragValue::new(&mut value.y));
                ui.add(egui::DragValue::new(&mut value.x));
                ui.add(egui::DragValue::new(&mut value.z));
            })
            .response
        },
    );
    response
}

pub fn vector3_field(
    ui: &mut egui::Ui,
    label: impl Into<egui::WidgetText>,
    value: &mut Vector3,
) -> egui::Response {
    let (_, response) = justified(
        ui,
        |ui| ui.label(label),
        |ui| {
            ui.horizontal(|ui| {
                ui.add(egui::DragValue::new(&mut value.y));
                ui.add(egui::DragValue::new(&mut value.x));
                ui.add(egui::DragValue::new(&mut value.z));
            })
            .response
        },
    );
    response
}

pub fn linestring_points(ui: &mut egui::Ui, linestring: &LineString) {
    ScrollArea::vertical().show_rows(
        ui,
        ui.text_style_height(&TextStyle::Body),
        linestring.points.len(),
        |ui, row_range| {
            for row in row_range {
                if let Some(point) = linestring.points.get(row) {
                    justified(
                        ui,
                        |ui| ui.label(row.to_string()),
                        |ui| ui.label(format!("({}, {})", point.x, point.y)),
                    );
                }
            }
        },
    );
}

pub trait DropdownOptions: Display + PartialEq {
    type Options: IntoIterator<Item = Self>;
    fn options() -> Self::Options;
}

pub fn dropdown<T: DropdownOptions>(
    ui: &mut egui::Ui,
    id_source: impl Hash,
    selected: &mut T,
) -> egui::Response {
    egui::ComboBox::from_id_source(id_source)
        .selected_text(selected.to_string())
        .show_ui(ui, |ui| {
            for option in T::options().into_iter() {
                let label = option.to_string();
                ui.selectable_value(selected, option, label);
            }
        })
        .response
}

pub fn dropdown_field<T: DropdownOptions>(
    ui: &mut egui::Ui,
    label: impl Into<egui::WidgetText> + Hash + Copy,
    selected: &mut T,
) {
    justified(
        ui,
        |ui| ui.label(label.into().clone()),
        |ui| dropdown(ui, label, selected),
    );
}

#[derive(Debug)]
pub struct PickFileField {
    task: Option<RunningTaskHandle<PickFileTask>>,
    path: Option<PathBuf>,
    start_open: bool,
}

impl PickFileField {
    pub fn new(open: bool) -> PickFileField {
        PickFileField {
            task: None,
            path: None,
            start_open: open,
        }
    }

    pub fn with_picked(path: PathBuf) -> PickFileField {
        PickFileField {
            task: None,
            path: Some(path),
            start_open: false,
        }
    }

    pub fn ui(
        &mut self,
        ui: &mut egui::Ui,
        tasks: &mut Tasks,
        name: &str,
        filters: &'static [(&'static str, &'static [&'static str])],
        allow_new: bool,
    ) {
        if self.start_open {
            let handle = tasks.spawn(PickFileTask {
                name: name.to_string(),
                file_types: filters,
                allow_new,
            });

            self.task = Some(handle);
            self.start_open = false;
        }

        if let Some(Ok(done)) = self.task.take_done() {
            self.path = Some(done.file_path);
        }

        if self.task.is_some() {
            ui.spinner();
        } else if let Some(path) = self.path.clone() {
            if ui.button("x").clicked() {
                self.path = None;
            }

            let label = if let Some(filename) = path.file_name() {
                filename.to_string_lossy()
            } else {
                path.to_string_lossy()
            };

            ui.label(label);
        } else {
            if ui.button("Select").clicked() {
                let handle = tasks.spawn(PickFileTask {
                    name: name.to_string(),
                    file_types: filters,
                    allow_new,
                });

                self.task = Some(handle);
            }
        }
    }

    pub fn picked(&self) -> Option<&PathBuf> {
        self.path.as_ref()
    }
}
