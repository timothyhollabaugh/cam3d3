use crate::app::CamContext;
use crate::app::RenderContext;
use crate::ui::{CamUi, CamUiState};
use egui::{Color32, Frame, RichText, Stroke, Ui};
use log::{error, info, warn};
use std::time::Instant;

pub struct Notification {
    title: String,
    description: String,
    severity: NotificationSeverity,
    timestamp: Instant,
}

impl Notification {
    pub fn error(title: String, description: String) -> Notification {
        error!("{}: {}", title, description);
        Notification {
            title,
            description,
            severity: NotificationSeverity::Error,
            timestamp: Instant::now(),
        }
    }

    pub fn warning(title: String, description: String) -> Notification {
        warn!("{}: {}", title, description);
        Notification {
            title,
            description,
            severity: NotificationSeverity::Warning,
            timestamp: Instant::now(),
        }
    }

    pub fn info(title: String, description: String) -> Notification {
        info!("{}: {}", title, description);
        Notification {
            title,
            description,
            severity: NotificationSeverity::Info,
            timestamp: Instant::now(),
        }
    }
}

pub enum NotificationSeverity {
    Error,
    Warning,
    Info,
}

impl CamUiState {
    pub fn notifications_ui(&mut self, ui: &mut Ui, _cam_context: &mut CamContext) {
        self.notifications.retain(|notification| {
            let mut close = false;

            let color = match notification.severity {
                NotificationSeverity::Error => Color32::RED,
                NotificationSeverity::Warning => Color32::YELLOW,
                NotificationSeverity::Info => Color32::GREEN,
            };

            let severity = match notification.severity {
                NotificationSeverity::Error => "Error",
                NotificationSeverity::Warning => "Warning",
                NotificationSeverity::Info => "Info",
            };

            let mut frame = Frame::group(ui.style());
            frame.fill = color;

            frame.show(ui, |ui| {
                ui.heading(format!("{}: {}", severity, &notification.title));
                ui.label(&notification.description);
                close = ui.button("Dismiss").clicked();
            });

            !close
        });
    }
}
