use bytemuck::ByteEq;
use cam3d_interpreter::call_stack::{CallStack, KeyStack};
use cam3d_interpreter::expression::{Expression, ExpressionBlock, FunctionCall};
use cam3d_interpreter::inc_map::Key;
use cam3d_interpreter::item_path::ItemPath;
use cam3d_interpreter::type_::Type;
use cam3d_interpreter::value_key::RelativeValueKey;
use egui::WidgetText;
use itertools::Itertools;
use std::cmp::PartialEq;
use std::collections::HashSet;
use std::fmt::{Debug, Display, Formatter};
use std::hash::{DefaultHasher, Hash, Hasher};
use std::sync::Arc;

#[derive(Clone, Debug)]
pub struct Selection {
    selection: Vec<SelectedItem>,
    target: Option<SelectTarget>,
}

impl Selection {
    pub fn new() -> Selection {
        Selection {
            selection: Vec::new(),
            target: None,
        }
    }

    pub fn selection_iter(&self) -> impl Iterator<Item = &SelectedItem> {
        self.selection.iter()
    }

    /// Remove all items from the selection
    pub fn clear_selection(&mut self) {
        self.selection.clear()
    }

    /// Add another item to the selection, keeping existing items selected. If there is a select
    /// target that doesn't allow multiple selection, this behaves the same as `select_only`
    pub fn add_selection(&mut self, selection: impl Into<SelectedItem>) {
        log::debug!("add selection");
        let selection = selection.into();
        if self.target.as_ref().is_some_and(|t| !t.allow_multiple) {
            self.selection.clear();
            self.selection.push(selection);
        } else if !self.selection.contains(&selection) {
            self.selection.push(selection);
        }
    }

    /// Clear the selection and select only a single item
    pub fn select_only(&mut self, selection: impl Into<SelectedItem>) {
        log::debug!("select only");
        self.selection.clear();
        self.selection.push(selection.into());
    }

    /// Clear the selection and select many items
    pub fn set_selection<S: Into<SelectedItem>, I: Iterator<Item = S>>(&mut self, selection: I) {
        log::debug!("set selection");
        self.selection.clear();
        self.selection.extend(selection.map(|s| s.into()));
    }

    /// Remove the item from the selection. No-op if the item was not selected.
    pub fn unselect(&mut self, selection: impl Into<SelectedItem>) {
        log::debug!("unselect");
        let selection = selection.into();
        self.selection.retain(|s| s != &selection);
    }

    /// Check if the item is selected
    pub fn is_selected(&self, selection: impl Into<SelectedItem>) -> bool {
        self.selection.contains(&selection.into())
    }

    /// Get the selection target, if there is one
    pub fn target(&self) -> Option<&SelectTarget> {
        self.target.as_ref()
    }

    /// Cleat the selection target
    pub fn clear_target(&mut self) {
        log::debug!("clear target");
        self.target = None;
    }

    /// Check if the selection target has the given id
    pub fn is_target_for(&self, id: egui::Id) -> bool {
        self.target.as_ref().is_some_and(|target| target.id == id)
    }

    /// Set the selection target for the given id and target.
    ///
    /// The id is used to determined where the selected items are intended to
    /// go. It should be unique to the select field.
    ///
    /// The target is what type of selected items will be able to be selected.
    pub fn set_target(&mut self, id: egui::Id, allow_multiple: bool, target: SelectTargetType) {
        log::debug!("set target for {id:?}: {target:?}");
        self.target = Some(SelectTarget {
            id,
            allow_multiple,
            t: target,
        });
    }

    pub fn target_label(
        &mut self,
        ui: &mut egui::Ui,
        label: impl Into<WidgetText>,
        target: SelectTargetType,
    ) -> Option<SelectedItem> {
        let id = ui.id();
        let is_target_for = self.is_target_for(id);
        let select_button = egui::SelectableLabel::new(is_target_for, label);
        let select_response = ui.add(select_button).on_hover_text("Select expression");
        if select_response.clicked() {
            self.clear_selection();
            if is_target_for {
                log::debug!("clicked while targeted, clearing target");
                self.clear_target();
            } else {
                log::debug!("clicked while untargeted, setting target");
                self.set_target(id, false, target)
            }
        }

        if is_target_for {
            let selected = self.selection.pop();
            if selected.is_some() {
                log::debug!("target selected, clearing selection");
                self.target = None;
                self.selection.clear();
            }
            selected
        } else {
            None
        }
    }

    pub fn select_label(
        &mut self,
        ui: &mut egui::Ui,
        label: impl Into<WidgetText>,
        predicate: impl FnOnce(&SelectTarget) -> bool,
        selected_item: SelectedItem,
    ) -> egui::Response {
        let targeted = self.target().is_some_and(predicate);

        let selected = self
            .selection_iter()
            .find(|selection| **selection == selected_item)
            .is_some();

        let fn_button = egui::SelectableLabel::new(selected, label);
        let fn_button_response = ui
            .add_enabled(targeted, fn_button)
            .on_hover_text("Insert Expression");

        if fn_button_response.clicked() {
            if selected {
                self.unselect(selected_item);
            } else {
                self.add_selection(selected_item);
            }
        }

        fn_button_response
    }

    pub fn select_button_output_filter(
        &mut self,
        ui: &mut egui::Ui,
        output_type: Option<&ItemPath>,
        selected_item: SelectedItem,
    ) -> egui::Response {
        self.select_label(
            ui,
            egui_phosphor::regular::CARET_LINE_LEFT,
            |target| match target.expression_select_target() {
                Some((target_call_stack, target_type)) => match (target_type, output_type) {
                    (Some(target_type), Some(output_type)) => target_type == output_type,
                    (None, _) => true,
                    (_, None) => false,
                },
                None => false,
            },
            selected_item,
        )
    }
}

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub enum SelectedItem {
    FunctionCall(Key<FunctionCall>),
    NewLiteral(ItemPath),
    RelativeValue(CallStack),
    NewFunctionCall(ItemPath),
    NewBlock,
}

impl SelectedItem {
    pub fn function_call(&self) -> Option<Key<FunctionCall>> {
        if let SelectedItem::FunctionCall(key) = self {
            Some(*key)
        } else {
            None
        }
    }

    pub fn relative_value(&self) -> Option<&CallStack> {
        if let SelectedItem::RelativeValue(key) = self {
            Some(key)
        } else {
            None
        }
    }

    pub fn new_literal(&self) -> Option<&ItemPath> {
        if let SelectedItem::NewLiteral(item) = self {
            Some(item)
        } else {
            None
        }
    }

    pub fn new_function_call(&self) -> Option<&ItemPath> {
        if let SelectedItem::NewFunctionCall(item) = self {
            Some(item)
        } else {
            None
        }
    }

    pub fn new_block(&self) -> Option<()> {
        if let SelectedItem::NewBlock = self {
            Some(())
        } else {
            None
        }
    }
}

impl From<Key<FunctionCall>> for SelectedItem {
    fn from(value: Key<FunctionCall>) -> SelectedItem {
        SelectedItem::FunctionCall(value)
    }
}

impl From<CallStack> for SelectedItem {
    fn from(call_stack: CallStack) -> Self {
        SelectedItem::RelativeValue(call_stack)
    }
}

impl From<&CallStack> for SelectedItem {
    fn from(call_stack: &CallStack) -> Self {
        SelectedItem::RelativeValue(call_stack.clone())
    }
}

#[derive(Clone, Debug)]
pub struct SelectTarget {
    id: egui::Id,
    allow_multiple: bool,
    t: SelectTargetType,
}

impl SelectTarget {
    pub fn expression_select_target(&self) -> Option<(&CallStack, &Option<ItemPath>)> {
        if let SelectTargetType::Expression(callstack, expression_type) = &self.t {
            Some((callstack, expression_type))
        } else {
            None
        }
    }

    pub fn allow_multiple(&self) -> bool {
        self.allow_multiple
    }
}

#[derive(Debug, Clone)]
pub enum SelectTargetType {
    Expression(CallStack, Option<ItemPath>),
}

pub trait TargetIdSource {
    fn as_id(&self) -> TargetId;
}

impl<T> TargetIdSource for T
where
    T: Hash,
{
    fn as_id(&self) -> TargetId {
        let mut s = DefaultHasher::new();
        self.hash(&mut s);
        TargetId(s.finish())
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq)]
pub struct TargetId(u64);

impl Display for TargetId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "ID({})", self.0)
    }
}

impl TargetIdSource for TargetId {
    fn as_id(&self) -> TargetId {
        *self
    }
}
