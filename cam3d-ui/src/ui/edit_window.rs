use crate::ui::widgets::justified;
use egui;
use egui::{Button, Ui, WidgetText};

#[derive(Copy, Clone, Debug)]
pub struct EditWindowOutput<R> {
    pub close: bool,
    pub save: bool,
    pub inner: Option<R>,
}

impl<R> Default for EditWindowOutput<R> {
    fn default() -> Self {
        EditWindowOutput {
            close: false,
            save: false,
            inner: None,
        }
    }
}

pub fn edit_window<R>(
    gui_context: &egui::Context,
    title: impl Into<WidgetText>,
    can_save: bool,
    add_contents: impl FnOnce(&mut Ui) -> R,
) -> EditWindowOutput<R> {
    egui::Window::new(title)
        .id(egui::Id::new("edit-window"))
        .vscroll(true)
        .show(gui_context, |ui| {
            let inner = (add_contents)(ui);

            let ((close_left, save_left), (close_right, save_right)) = justified(
                ui,
                |ui| (ui.button("Cancel").clicked(), false),
                |ui| {
                    ui.horizontal(|ui| {
                        let mut close = false;
                        let mut save = false;

                        if ui.add_enabled(can_save, Button::new("Ok")).clicked() {
                            close = true;
                            save = true;
                        }

                        if ui.add_enabled(can_save, Button::new("Apply")).clicked() {
                            save = true;
                        }

                        (close, save)
                    })
                    .inner
                },
            );

            let (close, save) = (close_left || close_right, save_left || save_right);

            EditWindowOutput {
                close,
                save,
                inner: Some(inner),
            }
        })
        .map(|r| r.inner)
        .flatten()
        .unwrap_or_default()
}
