use crate::cam::geometry2d::{LineString, Point2, Polygon};
use crate::cam::geometry3d::Vector3;
use crate::render3d::render3d_wgpu::{ambient_lights, camera_pick, OrbitControls, RenderContext};
use egui_dock::egui;
use egui::Vec2;
use three_d_asset::{
    Angle, CpuMaterial, Gm, InnerSpace, InstancedMesh, Instances, MetricSpace, PhysicalMaterial,
    Rad, SquareMatrix, Zero,
};
use three_d_asset::{Camera, Srgba, Mat4, Vec3};

pub fn view_cube(
    ui: &mut egui::Ui,
    render_context: &mut RenderContext,
    orbit_controls: &mut OrbitControls,
    scroll_delta: Vec2,
) {
    let visuals = ui.visuals().clone();

    render_context.add_scene(ui, |response, scene_context, gl_context| {
        let size = 1.0;
        let diagonal = size * f32::sqrt(3.0);
        let cube_stroke = 0.1;

        orbit_controls.update(scroll_delta, &response);

        let color = visuals.widgets.active.bg_fill;

        for light in ambient_lights(gl_context) {
            scene_context.add_light(light);
        }

        let mut camera_position =
            orbit_controls.camera().position().clone() - orbit_controls.camera().target().clone();
        camera_position /= camera_position.distance(Vec3::zero());
        camera_position *= size * f32::sqrt(3.0) / 2.0;

        let camera = Camera::new_orthographic(
            scene_context.viewport,
            camera_position,
            Vec3::zero(),
            Vec3::unit_z(),
            diagonal,
            0.001,
            diagonal,
        );

        let face = (
            Polygon::from(LineString::rectangle(
                Point2::from_xy(-size / 2.0 + cube_stroke),
                Point2::from_xy(size / 2.0 - cube_stroke),
            ))
            .tessillate()
            .unwrap(),
            Instances {
                transformations: vec![
                    Mat4::from_translation(Vector3::from_x(size / 2.0).into())
                        * Mat4::from_angle_y(Rad::turn_div_4()),
                ],
                texture_transformations: None,
                colors: None,
            },
            Srgba::from_rgba_slice(&color.to_array().map(|e| e as f32 / 255.0)),
        );

        let _edge = (
            Polygon::from(LineString::rectangle(
                Point2::new(-cube_stroke, -size / 2.0 + cube_stroke),
                Point2::new(0.0, size / 2.0 - cube_stroke),
            ))
            .tessillate()
            .unwrap(),
            Instances {
                transformations: vec![
                    Mat4::from_translation(Vector3::from_x(size / 2.0 * f32::sqrt(2.0)).into())
                        * Mat4::from_angle_y(Rad::turn_div_4() / -2.0),
                    Mat4::from_translation(Vector3::from_x(size / 2.0 * f32::sqrt(2.0)).into())
                        * Mat4::from_angle_y(Rad::turn_div_4() / 2.0),
                ],
                texture_transformations: None,
                colors: None,
            },
            Srgba::from_rgba_slice(
                &color
                    .gamma_multiply(2.0)
                    .to_array()
                    .map(|e| e as f32 / 255.0),
            ),
        );

        let _corner = (
            Polygon::from(LineString::rectangle(
                Point2::from_xy(cube_stroke),
                Point2::zero(),
            ))
            .tessillate()
            .unwrap(),
            Instances {
                transformations: vec![
                    Mat4::identity(),
                    Mat4::from_angle_x(Rad::turn_div_4()),
                    Mat4::from_angle_y(-Rad::turn_div_4()),
                ],
                texture_transformations: None,
                colors: None,
            },
            Srgba::from_rgba_slice(
                &color
                    .gamma_multiply(3.0)
                    .to_array()
                    .map(|e| e as f32 / 255.0),
            ),
        );

        let zero = Rad::zero();
        let div_2 = Rad::turn_div_2();
        let div_4 = Rad::turn_div_4();
        let _div_8 = Rad::turn_div_4() / 2.0;

        let transformations = [
            ("Front", zero, zero, zero, &face),
            ("Right", div_4, zero, zero, &face),
            ("Left", -div_4, zero, zero, &face),
            ("Back", div_2, zero, zero, &face),
            ("Top", zero, div_4, zero, &face),
            ("Bottom", zero, -div_4, zero, &face),
            //("TF", zero, div_8 * 1.0, zero, &edge),
            //("TB", zero, div_8 * 3.0, zero, &edge),
            //("BB", zero, div_8 * 5.0, zero, &edge),
            //("TF", zero, div_8 * 7.0, zero, &edge),
            //("TR", div_4, div_8 * 1.0, zero, &edge),
            //("TL", div_4, div_8 * 3.0, zero, &edge),
            //("BL", div_4, div_8 * 5.0, zero, &edge),
            //("BR", div_4, div_8 * 7.0, zero, &edge),
            //("FR", zero, div_8 * 1.0, div_4, &edge),
            //("BR", zero, div_8 * 3.0, div_4, &edge),
            //("BL", zero, div_8 * 5.0, div_4, &edge),
            //("FL", zero, div_8 * 7.0, div_4, &edge),
        ];

        for (name, yaw, pitch, _roll, (cpu_mesh, instances, color)) in transformations {
            let camera_normal = Vec3::new(
                f32::cos(orbit_controls.yaw()) * f32::cos(orbit_controls.pitch()),
                f32::sin(orbit_controls.yaw()) * f32::cos(orbit_controls.pitch()),
                f32::sin(orbit_controls.pitch()),
            )
            .normalize();

            let face_normal = Vec3::new(
                f32::cos(yaw.0) * f32::cos(pitch.0),
                f32::sin(yaw.0) * f32::cos(pitch.0),
                f32::sin(pitch.0),
            )
            .normalize();

            let dot = camera_normal.dot(face_normal);

            if dot > 0.0 {
                let transformation = Mat4::from_angle_z(yaw) * Mat4::from_angle_y(-pitch);

                let mut mesh = InstancedMesh::new(gl_context, instances, cpu_mesh);
                mesh.set_transformation(transformation);

                let pick = if let Some(hover) = response.hover_pos() {
                    camera_pick(gl_context, &camera, response.rect, hover, [&mesh])
                } else {
                    None
                };

                let color = if pick.is_some() {
                    if response.clicked() {
                        println!("{}", name);
                        orbit_controls.animate_pitch(pitch.0);
                        orbit_controls.animate_yaw(yaw.0);
                    }

                    Srgba::BLUE
                } else {
                    *color
                };

                let cube_material = PhysicalMaterial::new_opaque(
                    gl_context,
                    &CpuMaterial {
                        albedo: color,
                        emissive: color,
                        ..Default::default()
                    },
                );

                scene_context.add_object(Gm::new(mesh, cube_material.clone()));
            }
        }

        let edge_mesh = Polygon::from(LineString::rectangle(
            Point2::new(-size / 2.0 + cube_stroke, 0.0),
            Point2::new(size / 2.0 - cube_stroke, cube_stroke),
        ))
        .tessillate()
        .unwrap();

        let edge_instances = Instances {
            transformations: vec![
                Mat4::from_angle_x(Rad::turn_div_4() / -2.0),
                Mat4::from_angle_x(Rad::turn_div_4() / 2.0),
            ],
            texture_transformations: None,
            colors: None,
        };

        let edge_transformations = [
            Mat4::from_angle_x(Rad::turn_div_4() * 1.0 / 2.0),
            Mat4::from_angle_x(Rad::turn_div_4() * 3.0 / 2.0),
            Mat4::from_angle_x(Rad::turn_div_4() * 5.0 / 2.0),
            Mat4::from_angle_x(Rad::turn_div_4() * 7.0 / 2.0),
            Mat4::from_angle_y(Rad::turn_div_4() * 1.0 / 2.0)
                * Mat4::from_angle_z(Rad::turn_div_4()),
            Mat4::from_angle_y(Rad::turn_div_4() * 3.0 / 2.0)
                * Mat4::from_angle_z(Rad::turn_div_4()),
            Mat4::from_angle_y(Rad::turn_div_4() * 5.0 / 2.0)
                * Mat4::from_angle_z(Rad::turn_div_4()),
            Mat4::from_angle_y(Rad::turn_div_4() * 7.0 / 2.0)
                * Mat4::from_angle_z(Rad::turn_div_4()),
            Mat4::from_angle_z(Rad::turn_div_4() * 1.0 / 2.0)
                * Mat4::from_angle_y(Rad::turn_div_4()),
            Mat4::from_angle_z(Rad::turn_div_4() * 3.0 / 2.0)
                * Mat4::from_angle_y(Rad::turn_div_4()),
            Mat4::from_angle_z(Rad::turn_div_4() * 5.0 / 2.0)
                * Mat4::from_angle_y(Rad::turn_div_4()),
            Mat4::from_angle_z(Rad::turn_div_4() * 7.0 / 2.0)
                * Mat4::from_angle_y(Rad::turn_div_4()),
        ];

        for transform in edge_transformations {
            let mut mesh = InstancedMesh::new(gl_context, &edge_instances, &edge_mesh);

            mesh.set_transformation(
                transform
                    * Mat4::from_translation(Vector3::from_y(-size / 2.0 * f32::sqrt(2.0)).into()),
            );

            let pick = if let Some(hover) = response.hover_pos() {
                camera_pick(gl_context, &camera, response.rect, hover, [&mesh])
            } else {
                None
            };

            let color = if pick.is_some() {
                Srgba::BLUE
            } else {
                Srgba::from_rgba_slice(
                    &color
                        .gamma_multiply(2.0)
                        .to_array()
                        .map(|e| e as f32 / 255.0),
                )
            };

            let edge_material = PhysicalMaterial::new_opaque(
                gl_context,
                &CpuMaterial {
                    albedo: color,
                    emissive: color,
                    ..Default::default()
                },
            );

            scene_context.add_object(Gm::new(mesh, edge_material.clone()));
        }

        let corner_mesh = Polygon::from(LineString::rectangle(
            Point2::from_xy(cube_stroke),
            Point2::zero(),
        ))
        .tessillate()
        .unwrap();

        let corner_instances = Instances {
            transformations: vec![
                Mat4::identity(),
                Mat4::from_angle_x(Rad::turn_div_4()),
                Mat4::from_angle_y(-Rad::turn_div_4()),
            ],
            texture_transformations: None,
            colors: None,
        };

        let corner_transformations = [
            Mat4::from_translation(Vector3::new(-size / 2.0, size / 2.0, size / 2.0).into())
                * Mat4::from_nonuniform_scale(1.0, -1.0, -1.0),
            Mat4::from_translation(Vector3::new(-size / 2.0, -size / 2.0, size / 2.0).into())
                * Mat4::from_nonuniform_scale(1.0, 1.0, -1.0),
            Mat4::from_translation(Vector3::new(-size / 2.0, size / 2.0, -size / 2.0).into())
                * Mat4::from_nonuniform_scale(1.0, -1.0, 1.0),
            Mat4::from_translation(Vector3::new(-size / 2.0, -size / 2.0, -size / 2.0).into())
                * Mat4::from_nonuniform_scale(1.0, 1.0, 1.0),
            Mat4::from_translation(Vector3::new(size / 2.0, size / 2.0, size / 2.0).into())
                * Mat4::from_nonuniform_scale(-1.0, -1.0, -1.0),
            Mat4::from_translation(Vector3::new(size / 2.0, -size / 2.0, size / 2.0).into())
                * Mat4::from_nonuniform_scale(-1.0, 1.0, -1.0),
            Mat4::from_translation(Vector3::new(size / 2.0, size / 2.0, -size / 2.0).into())
                * Mat4::from_nonuniform_scale(-1.0, -1.0, 1.0),
            Mat4::from_translation(Vector3::new(size / 2.0, -size / 2.0, -size / 2.0).into())
                * Mat4::from_nonuniform_scale(-1.0, 1.0, 1.0),
        ];

        for transformation in corner_transformations {
            let mut mesh = InstancedMesh::new(gl_context, &corner_instances, &corner_mesh);
            mesh.set_transformation(transformation);

            let pick = if let Some(hover) = response.hover_pos() {
                camera_pick(gl_context, &camera, response.rect, hover, [&mesh])
            } else {
                None
            };

            let color = if pick.is_some() {
                Srgba::BLUE
            } else {
                Srgba::from_rgba_slice(
                    &color
                        .gamma_multiply(3.0)
                        .to_array()
                        .map(|e| e as f32 / 255.0),
                )
            };

            let corner_material = PhysicalMaterial::new_opaque(
                gl_context,
                &CpuMaterial {
                    albedo: color,
                    emissive: color,
                    ..Default::default()
                },
            );
            scene_context.add_object(Gm::new(mesh, corner_material.clone()));
        }

        (camera, None)
    });
}
