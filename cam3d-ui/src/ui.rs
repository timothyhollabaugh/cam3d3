use std::collections::{HashMap, HashSet};
use std::fmt::{Display, Formatter, Write};
use std::hash::Hash;

use cam3d_interpreter::item_path::ItemPath;
use cam3d_interpreter::library::native::NativeLibrary;
use cam3d_interpreter::library::Libraries;
use egui::{Rgba, Sense, Stroke, Vec2, Window};
use egui_dock::egui::{Ui, WidgetText};
use egui_dock::{DockArea, DockState, Node, NodeIndex, Style, SurfaceIndex, TabViewer, Tree};
use gpu::main_view::MainView;
use itertools::Itertools;
use log::error;
use selection::{SelectTarget, SelectedItem};
use serde::{Deserialize, Serialize};
use widgets::DropdownOptions;

use crate::app::{CamContext, InternalError, RenderContext};
use crate::task::OptionalRunningTaskHandle;
use crate::ui::gpu::main_view::MainViewStats;
use crate::ui::interpreter::function_3d::Function3dVeiw;
use crate::ui::interpreter::library_tree::library_tree;
use crate::ui::interpreter::toolbar::{ToolbarItem, ToolbarItemSize, ToolbarSection, ToolbarUi};
use crate::ui::interpreter::value_detail::ValueDetailUi;
use crate::ui::interpreter::{literals_ui, InterpreterUi, UserFunctionUi};
use crate::ui::notification::Notification;
use crate::ui::selection::Selection;
use crate::ui::styles::styles_ui;

pub mod edit_window;
mod gpu;
pub mod interpreter;
pub mod notification;
mod selection;
mod styles;
pub mod widgets;

#[derive(Debug)]
pub enum ActiveEdit {
    None,
}

impl ActiveEdit {
    pub fn is_none(&self) -> bool {
        matches!(self, ActiveEdit::None)
    }
}

#[derive(Debug, PartialEq)]
pub enum UiTabs {
    Interpreter(InterpreterUi),
    UserFunction(UserFunctionUi),
    UserFunction3d(Function3dVeiw),
    ValueDetail(ValueDetailUi),
    Toolbar(ToolbarUi),
    Literals,
    Libraries,
    Notifications,
    Tasks,
    Debug,
    Styles,
}

#[derive(Clone, Debug)]
pub struct GpuInfo {
    pub adapter: wgpu::AdapterInfo,
}

enum NewTabLocation {
    Tab,
    Window,
    Center,
    Split(egui_dock::Split, f32),
}

pub struct CamUiState {
    pub active_edit: ActiveEdit,
    pub selection: Selection,
    pub notifications: Vec<Notification>,
    pub show_profiler: bool,
    pub main_view: MainView,
    pub gpu_info: GpuInfo,
    pub stats: MainViewStats,
    new_tabs: Vec<(UiTabs, NewTabLocation, bool)>,
}

impl CamUiState {
    pub fn new(cc: &eframe::CreationContext<'_>) -> CamUiState {
        let adapter_info = cc.wgpu_render_state.as_ref().unwrap().adapter.get_info();

        let gpu_info = GpuInfo {
            adapter: adapter_info,
        };

        CamUiState {
            active_edit: ActiveEdit::None,
            selection: Selection::new(),
            notifications: vec![],
            show_profiler: false,
            main_view: MainView::new(cc.wgpu_render_state.as_ref()),
            gpu_info,
            stats: MainViewStats::default(),
            new_tabs: Vec::new(),
        }
    }

    pub fn report_internal_error(&mut self, e: InternalError) {
        log::error!("Internal Error: {e}");

        self.notifications.push(Notification::error(
            "Internal Error".to_string(),
            format!("{e}"),
        ))
    }

    pub fn new_tab(&mut self, new_tab: UiTabs, focus_if_existing: bool) {
        self.new_tabs
            .push((new_tab, NewTabLocation::Tab, focus_if_existing));
    }

    pub fn new_split(
        &mut self,
        new_tab: UiTabs,
        split: egui_dock::Split,
        fraction: f32,
        focus_if_existing: bool,
    ) {
        self.new_tabs.push((
            new_tab,
            NewTabLocation::Split(split, fraction),
            focus_if_existing,
        ));
    }

    pub fn new_window(&mut self, new_tab: UiTabs, focus_if_existing: bool) {
        self.new_tabs
            .push((new_tab, NewTabLocation::Window, focus_if_existing));
    }

    pub fn new_center(&mut self, new_tab: UiTabs, focus_if_existing: bool) {
        self.new_tabs
            .push((new_tab, NewTabLocation::Center, focus_if_existing));
    }

    fn tasks_ui(&mut self, ui: &mut egui::Ui, cam_context: &mut CamContext) {
        puffin::profile_function!();
        ui.vertical(|ui| {
            for (_key, task) in cam_context.tasks.tasks() {
                ui.label(task.name.as_ref());
                let progress = egui::ProgressBar::new(task.progress)
                    .text(&task.status)
                    .animate(true);
                ui.add(progress);
            }
        });
    }

    fn debug_ui(&mut self, ui: &mut egui::Ui, cam_context: &mut CamContext) {
        puffin::profile_function!();
        egui::widgets::global_dark_light_mode_switch(ui);

        if let Some(frame) = cam_context.profiler_frame.clone() {
            let frame_time = frame.duration_ns();
            ui.label(format!("ms per frame: {}", frame_time as f64 / 1000000.0));
            ui.label(format!(
                "frames per s: {}",
                1000000000.0 / frame_time as f64
            ));
        }

        ui.label(format!(
            "meshes: {} triangles: {}",
            self.stats.number_meshes, self.stats.number_triangles,
        ));

        ui.checkbox(&mut self.show_profiler, "Profiler");

        ui.label(format!("{:#?}", self.selection));
        ui.label(format!("{:#?}", self.gpu_info));

        self.main_view.debug_ui(ui);
    }

    fn styles_ui(&mut self, ui: &mut Ui, cam_context: &mut CamContext) {
        styles_ui(ui);
    }
}

pub struct UiContext<'ui, 'cam, 'render> {
    pub state: &'ui mut CamUiState,
    pub cam_context: &'ui mut CamContext<'cam>,
    pub render_context: &'ui mut RenderContext<'render>,
}

impl<'ui, 'cam, 'render> TabViewer for UiContext<'ui, 'cam, 'render> {
    type Tab = UiTabs;

    fn ui(&mut self, ui: &mut Ui, tab: &mut UiTabs) {
        puffin::profile_function!();
        let result = match tab {
            UiTabs::Interpreter(state) => Ok(state.show(ui, self)),
            UiTabs::UserFunction(function_ui) => function_ui.show(ui, self),
            UiTabs::UserFunction3d(function_ui) => function_ui.show(ui, self),
            UiTabs::ValueDetail(value_detail_ui) => value_detail_ui.show(ui, self),
            UiTabs::Libraries => Ok(library_tree(ui, self)),
            UiTabs::Literals => Ok(literals_ui(ui, self)),
            UiTabs::Toolbar(toolbar_ui) => toolbar_ui.show(ui, self),
            UiTabs::Notifications => Ok(self.state.notifications_ui(ui, self.cam_context)),
            UiTabs::Tasks => Ok(self.state.tasks_ui(ui, self.cam_context)),
            UiTabs::Debug => Ok(self.state.debug_ui(ui, self.cam_context)),
            UiTabs::Styles => Ok(self.state.styles_ui(ui, self.cam_context)),
        };

        if let Err(e) = result {
            self.state.report_internal_error(e);
        }
    }

    fn title(&mut self, tab: &mut Self::Tab) -> WidgetText {
        puffin::profile_function!();
        match tab {
            UiTabs::Interpreter(_) => "Interpreter".into(),
            UiTabs::UserFunction(user_function) => user_function.title(self),
            UiTabs::UserFunction3d(user_function) => user_function.title(self),
            UiTabs::ValueDetail(value_detail_ui) => value_detail_ui.title(),
            UiTabs::Libraries => "Libraries".into(),
            UiTabs::Literals => "Literals".into(),
            UiTabs::Toolbar(toolbar_ui) => toolbar_ui.title(),
            UiTabs::Notifications => "Notifications".into(),
            UiTabs::Tasks => "Tasks".into(),
            UiTabs::Debug => "Debug".into(),
            UiTabs::Styles => "Styles".into(),
        }
    }
}

pub struct CamUi {
    state: CamUiState,
    dock_state: DockState<UiTabs>,
    center_tab: NodeIndex,
}

impl CamUi {
    pub fn new(cc: &eframe::CreationContext<'_>, libraries: &Libraries) -> CamUi {
        puffin::profile_function!();

        let mut dock_state = DockState::new(vec![]);
        let tab_tree = &mut dock_state[SurfaceIndex::main()];
        let [center_tab, interpreter_tab] = tab_tree.split_left(
            NodeIndex::root(),
            0.3,
            vec![UiTabs::Interpreter(InterpreterUi::default())],
        );
        //let [main_view_tab, toolbar_tab] =
        //tab_tree.split_above(main_view_tab, 0.1, toolbar_tabs2(libraries));
        let [center_tab, _tasks_tab] = tab_tree.split_below(center_tab, 0.9, vec![UiTabs::Tasks]);
        let [center_tab, libraries_tab] = tab_tree.split_right(
            center_tab,
            0.7,
            vec![
                UiTabs::Libraries,
                UiTabs::Debug,
                UiTabs::Styles,
                UiTabs::Notifications,
            ],
        );

        let [libraries_tab, literals_tab] =
            tab_tree.split_above(libraries_tab, 0.2, vec![UiTabs::Literals]);

        /*
        let mut dock_state = DockState::new(vec![]);
        let tab_tree = &mut dock_state[SurfaceIndex::main()];
        let [main_view_tab, project_tab] =
            tab_tree.split_left(main_view_tab, 0.3, vec![UiTabs::Project]);
        let [_project_tab, _values_tab] =
            tab_tree.split_below(project_tab, 0.5, vec![UiTabs::Values]);
        let [_main_view_tab, interpreter_tab] = tab_tree.split_right(
            main_view_tab,
            0.7,
            vec![
                UiTabs::Interpreter(InterpreterUi::default()),
                UiTabs::Libraries,
            ],
        );
        tab_tree.split_below(
            interpreter_tab,
            0.7,
            vec![UiTabs::Debug, UiTabs::Styles, UiTabs::Notifications],
        );
         */

        //tab_tree.split_above(main_view_tab, 0.1, toolbar_tabs());

        CamUi {
            state: CamUiState::new(cc),
            dock_state,
            center_tab,
        }
    }

    pub fn state(&mut self) -> &mut CamUiState {
        &mut self.state
    }

    fn edit_ui<'g>(&mut self, cam_context: &mut CamContext, render_context: &mut RenderContext) {
        puffin::profile_function!();
        match &mut self.state.active_edit {
            ActiveEdit::None => {}
        };
    }

    pub fn update<'app>(
        &mut self,
        cam_context: &mut CamContext<'app>,
        render_context: &mut RenderContext<'app>,
    ) {
        puffin::profile_function!();

        let mut ui_context = UiContext {
            state: &mut self.state,
            cam_context,
            render_context,
        };

        egui::CentralPanel::default().show(ui_context.render_context.gui, |ui| {
            let mut tab_style = Style::from_egui(ui.style());

            DockArea::new(&mut self.dock_state)
                .style(tab_style)
                .show_add_buttons(false)
                .show_close_buttons(false)
                .draggable_tabs(true)
                .show_tab_name_on_hover(false)
                .show_inside(ui, &mut ui_context);
        });

        self.edit_ui(cam_context, render_context);

        if self.state.show_profiler {
            //puffin_egui::profiler_window(render_context.gui);
        }

        for (new_tab, location, focus_if_existing) in self.state.new_tabs.drain(..) {
            if focus_if_existing {
                if let Some((surface_index, node_index, tab_index)) =
                    self.dock_state.find_tab(&new_tab)
                {
                    self.dock_state
                        .set_focused_node_and_surface((surface_index, node_index));
                    self.dock_state
                        .set_active_tab((surface_index, node_index, tab_index));
                    break;
                }
            }

            match location {
                NewTabLocation::Tab => {
                    self.dock_state.push_to_focused_leaf(new_tab);
                }
                NewTabLocation::Window => {
                    self.dock_state.add_window(vec![new_tab]);
                }
                NewTabLocation::Center => {
                    let tree = self.dock_state.main_surface_mut();
                    let node = &mut tree[self.center_tab];
                    node.append_tab(new_tab);
                }
                NewTabLocation::Split(split, fraction) => {
                    if let Some(focused) = self.dock_state.focused_leaf() {
                        self.dock_state
                            .split(focused, split, fraction, Node::leaf(new_tab));
                    } else {
                        self.dock_state.push_to_focused_leaf(new_tab);
                    }
                }
            }
        }
    }

    pub fn notify(&mut self, notification: Notification) {
        self.state.notifications.push(notification)
    }
}

fn toolbar_tabs2(libraries: &Libraries) -> Vec<UiTabs> {
    libraries
        .native_libraries()
        .map(|(name, library)| {
            let functions_section_items = library
                .root
                .functions
                .iter()
                .map(|(function_id, function)| ToolbarItem {
                    name: function.name().to_string(),
                    icon: None,
                    size: ToolbarItemSize::Small,
                    function_path: ItemPath::root_item(
                        library.name().to_string(),
                        function_id.clone(),
                    ),
                })
                .collect();

            let functions_section = ToolbarSection {
                name: "Functions".to_string(),
                items: functions_section_items,
            };

            let mut sections = vec![functions_section];
            sections.extend(library.root.modules.iter().map(|(module_id, module)| {
                let module_path =
                    ItemPath::root_item(library.name().to_string(), module_id.clone());

                let items = library
                    .lookup_module(&module_path)
                    .unwrap()
                    .functions
                    .iter()
                    .map(|(function_id, function)| ToolbarItem {
                        name: function.name().to_string(),
                        icon: None,
                        size: ToolbarItemSize::Small,
                        function_path: module_path.with_item(function_id.clone()),
                    })
                    .collect();

                ToolbarSection {
                    name: module.name.clone(),
                    items,
                }
            }));

            UiTabs::Toolbar(ToolbarUi {
                name: library.name().to_string(),
                sections,
            })
        })
        .collect()
}

fn toolbar_tabs() -> Vec<UiTabs> {
    vec![
        UiTabs::Toolbar(ToolbarUi::new(
            "Core",
            vec![
                (
                    "Boolean",
                    vec![
                        (
                            "Not",
                            None,
                            ToolbarItemSize::Small,
                            "core::bool::Bool::not".parse().unwrap(),
                        ),
                        (
                            "And",
                            None,
                            ToolbarItemSize::Small,
                            "core::bool::Bool::and".parse().unwrap(),
                        ),
                        (
                            "Or",
                            None,
                            ToolbarItemSize::Small,
                            "core::bool::Bool::or".parse().unwrap(),
                        ),
                    ],
                ),
                (
                    "Integer",
                    vec![
                        (
                            "Add",
                            None,
                            ToolbarItemSize::Small,
                            "core::int::Integer::add".parse().unwrap(),
                        ),
                        (
                            "Subtract",
                            None,
                            ToolbarItemSize::Small,
                            "core::int::Integer::sub".parse().unwrap(),
                        ),
                        (
                            "Multiply",
                            None,
                            ToolbarItemSize::Small,
                            "core::int::Integer::mul".parse().unwrap(),
                        ),
                        (
                            "Divide",
                            None,
                            ToolbarItemSize::Small,
                            "core::int::Integer::div".parse().unwrap(),
                        ),
                    ],
                ),
                (
                    "Float",
                    vec![
                        (
                            "Add",
                            None,
                            ToolbarItemSize::Small,
                            "core::float::Float::add".parse().unwrap(),
                        ),
                        (
                            "Subtract",
                            None,
                            ToolbarItemSize::Small,
                            "core::float::Float::sub".parse().unwrap(),
                        ),
                        (
                            "Multiply",
                            None,
                            ToolbarItemSize::Small,
                            "core::float::Float::mul".parse().unwrap(),
                        ),
                        (
                            "Divide",
                            None,
                            ToolbarItemSize::Small,
                            "core::float::Float::div".parse().unwrap(),
                        ),
                    ],
                ),
                (
                    "Time",
                    vec![(
                        "Sleep",
                        None,
                        ToolbarItemSize::Small,
                        "core::time::sleep".parse().unwrap(),
                    )],
                ),
            ],
        )),
        UiTabs::Toolbar(ToolbarUi::new(
            "2D Geometry",
            vec![
                (
                    "Point 2d",
                    vec![
                        (
                            "New Point",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::new_point2".parse().unwrap(),
                        ),
                        (
                            "X",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point2d::x".parse().unwrap(),
                        ),
                        (
                            "Y",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point2d::y".parse().unwrap(),
                        ),
                        (
                            "Add Vector",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point2d::add".parse().unwrap(),
                        ),
                        (
                            "Subtract Vector",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point2d::sub".parse().unwrap(),
                        ),
                        (
                            "Multiply Scalar",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point2d::mul".parse().unwrap(),
                        ),
                        (
                            "Divide Scalar",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point2d::div".parse().unwrap(),
                        ),
                        (
                            "Distance",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point2d::distance".parse().unwrap(),
                        ),
                        (
                            "Distance²",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point2d::distance_squared".parse().unwrap(),
                        ),
                        (
                            "Distance From Edge",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point2d::distance_from_edge"
                                .parse()
                                .unwrap(),
                        ),
                        (
                            "Project Onto Edge",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point2d::project_onto_edge".parse().unwrap(),
                        ),
                        (
                            "Distance Along Edge",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point2d::distance_along_edge"
                                .parse()
                                .unwrap(),
                        ),
                        (
                            "Close To",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point2d::close".parse().unwrap(),
                        ),
                    ],
                ),
                (
                    "Vector 2d",
                    vec![
                        (
                            "New Vector",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::new_vector2".parse().unwrap(),
                        ),
                        (
                            "X",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector2d::x".parse().unwrap(),
                        ),
                        (
                            "Y",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector2d::y".parse().unwrap(),
                        ),
                        (
                            "Add Vector",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector2d::add".parse().unwrap(),
                        ),
                        (
                            "Subtract Vector",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector2d::sub".parse().unwrap(),
                        ),
                        (
                            "Multiply Scalar",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector2d::mul".parse().unwrap(),
                        ),
                        (
                            "Divide Scalar",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector2d::div".parse().unwrap(),
                        ),
                        (
                            "Magnitude",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector2d::magnitude".parse().unwrap(),
                        ),
                        (
                            "Magnitude²",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector2d::magnitude_squared"
                                .parse()
                                .unwrap(),
                        ),
                        (
                            "Normalized",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector2d::normalized".parse().unwrap(),
                        ),
                        (
                            "Dot Product",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector2d::dot".parse().unwrap(),
                        ),
                        (
                            "Project Onto²",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector2d::project_onto".parse().unwrap(),
                        ),
                    ],
                ),
            ],
        )),
        UiTabs::Toolbar(ToolbarUi::new(
            "3D Geometry",
            vec![
                (
                    "Point 3d",
                    vec![
                        (
                            "New Point",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::new_point3".parse().unwrap(),
                        ),
                        (
                            "X",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point3d::x".parse().unwrap(),
                        ),
                        (
                            "Y",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point3d::y".parse().unwrap(),
                        ),
                        (
                            "Add Vector",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point3d::add".parse().unwrap(),
                        ),
                        (
                            "Subtract Vector",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point3d::sub".parse().unwrap(),
                        ),
                        (
                            "Multiply Scalar",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point3d::mul".parse().unwrap(),
                        ),
                        (
                            "Divide Scalar",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point3d::div".parse().unwrap(),
                        ),
                        (
                            "Distance",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point3d::distance".parse().unwrap(),
                        ),
                        (
                            "Distance²",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Point3d::distance_squared".parse().unwrap(),
                        ),
                    ],
                ),
                (
                    "Vector 3d",
                    vec![
                        (
                            "New Vector",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::new_vector3".parse().unwrap(),
                        ),
                        (
                            "X",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector3d::x".parse().unwrap(),
                        ),
                        (
                            "Y",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector3d::y".parse().unwrap(),
                        ),
                        (
                            "Add Vector",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector3d::add".parse().unwrap(),
                        ),
                        (
                            "Subtract Vector",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector3d::sub".parse().unwrap(),
                        ),
                        (
                            "Multiply Scalar",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector3d::mul".parse().unwrap(),
                        ),
                        (
                            "Divide Scalar",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Vector3d::div".parse().unwrap(),
                        ),
                    ],
                ),
                (
                    "Bounding Box 2d",
                    vec![
                        (
                            "Range",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::BoundingBox2d::range".parse().unwrap(),
                        ),
                        (
                            "Min X",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::BoundingBox2d::min_x".parse().unwrap(),
                        ),
                        (
                            "Max X",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::BoundingBox2d::max_x".parse().unwrap(),
                        ),
                        (
                            "Range X",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::BoundingBox2d::range_x".parse().unwrap(),
                        ),
                        (
                            "Min Y",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::BoundingBox2d::min_y".parse().unwrap(),
                        ),
                        (
                            "Max Y",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::BoundingBox2d::max_y".parse().unwrap(),
                        ),
                        (
                            "Range Y",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::BoundingBox2d::range_y".parse().unwrap(),
                        ),
                    ],
                ),
                (
                    "Plane",
                    vec![
                        (
                            "New Plane",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::new_plane".parse().unwrap(),
                        ),
                        (
                            "Point",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Plane::point".parse().unwrap(),
                        ),
                        (
                            "Normal",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Plane::normal".parse().unwrap(),
                        ),
                        (
                            "Offset",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Plane::offset".parse().unwrap(),
                        ),
                    ],
                ),
                (
                    "Solid 3d",
                    vec![
                        (
                            "Load STL",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::load_stl".parse().unwrap(),
                        ),
                        (
                            "Transform",
                            None,
                            ToolbarItemSize::Small,
                            "cam::geometry::Solid::transform".parse().unwrap(),
                        ),
                    ],
                ),
            ],
        )),
        UiTabs::Toolbar(ToolbarUi::new(
            "CAM",
            vec![
                (
                    "Command",
                    vec![
                        (
                            "Move To",
                            None,
                            ToolbarItemSize::Small,
                            "cam::cam::move_to_command".parse().unwrap(),
                        ),
                        (
                            "Rapid To",
                            None,
                            ToolbarItemSize::Small,
                            "cam::cam::rapid_to_command".parse().unwrap(),
                        ),
                        (
                            "Rapid To Z",
                            None,
                            ToolbarItemSize::Small,
                            "cam::cam::rapid_to_z_command".parse().unwrap(),
                        ),
                    ],
                ),
                (
                    "Toolpath",
                    vec![
                        (
                            "New Toolpath",
                            None,
                            ToolbarItemSize::Small,
                            "cam::cam::new_toolpath".parse().unwrap(),
                        ),
                        (
                            "Add Command",
                            None,
                            ToolbarItemSize::Small,
                            "cam::cam::Toolpath::add_command".parse().unwrap(),
                        ),
                        (
                            "Get Command",
                            None,
                            ToolbarItemSize::Small,
                            "cam::cam::Toolpath::get_command".parse().unwrap(),
                        ),
                        (
                            "Generate Gcode",
                            None,
                            ToolbarItemSize::Small,
                            "cam::cam::Toolpath::generate_gcode".parse().unwrap(),
                        ),
                        (
                            "Write Gcode",
                            None,
                            ToolbarItemSize::Small,
                            "cam::cam::write_gcode".parse().unwrap(),
                        ),
                    ],
                ),
                (
                    "Cut",
                    vec![(
                        "Clear 3D Adaptive",
                        None,
                        ToolbarItemSize::Small,
                        "cam::cam::clear_3d_adaptive".parse().unwrap(),
                    )],
                ),
            ],
        )),
    ]
}
