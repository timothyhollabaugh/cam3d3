//use three_d_asset::{CpuMaterial, Gm, Matrix4, Mesh, PhysicalMaterial};
use crate::cam::geometry2d::{LineString, MultiLineString, MultiPolygon, Point2};
use crate::cam::geometry3d::{Plane, Point3, Vector3};
//use crate::render3d::render3d_wgpu::{RenderApp, RenderContext, SceneContext, Sender};
use crate::cam_module::cam_lib;
use crate::task::interpreter_task::InterpreterDone;
use crate::task::interpreter_task::InterpreterTask;
use crate::task::load_interpreter_project::LoadInterpreterProjectTask;
use crate::task::save_interpreter_project::{
    SaveInterpreterProjectProgress, SaveInterpreterProjectTask,
};
use crate::task::RunningTaskHandleDoneProgress;
use crate::task::{OptionalRunningTaskHandle, RunningTaskHandle, TaskKey, Tasks};
use crate::ui::interpreter::UserFunctionUi;
use crate::ui::notification::Notification;
use crate::ui::{CamUi, UiTabs};
use cam3d_interpreter::call_stack::CallStack;
use cam3d_interpreter::core_module::{core_lib, UnitValue};
use cam3d_interpreter::expression::{Expression, ExpressionBlock, FunctionCall};
use cam3d_interpreter::function::user::UserFunction;
use cam3d_interpreter::inc_map::IncMap;
use cam3d_interpreter::inc_map::Key;
use cam3d_interpreter::interpreter::RuntimeError;
use cam3d_interpreter::item_path::{ItemId, ItemPath};
use cam3d_interpreter::library::native::NativeLibrary;
use cam3d_interpreter::library::user::UserLibrary;
use cam3d_interpreter::library::{Libraries, Library, LibraryRef, LookupError, LookupHandleError};
use cam3d_interpreter::module::Module;
use cam3d_interpreter::value::LiteralValue;
use cam3d_interpreter::values;
use directories_next::ProjectDirs;
use eframe::Frame;
use egui::Context;
use egui_dock::{Split, Tree};
use indexmap::IndexMap;
use log::{debug, error};
use puffin::FrameData;
use serde::{Deserialize, Serialize};
use std::backtrace::Backtrace;
use std::cell::RefCell;
use std::collections::HashMap;
use std::path::PathBuf;
use std::sync::{Arc, Mutex};
use std::time::Instant;
use three_d_asset::Srgba;
use wgpu::naga::FastIndexMap;

#[derive(thiserror::Error, Debug)]
pub enum InternalError {
    #[error("{0}")]
    Lookup(
        #[from]
        #[backtrace]
        LookupError,
    ),

    #[error("{0}")]
    LookupHandle(
        #[from]
        #[backtrace]
        LookupHandleError,
    ),

    #[error("Missing function call {key:?} in {function_path}")]
    MissingExpressionKey {
        key: Key<Expression>,
        function_path: ItemPath,
        backtrace: Backtrace,
    },

    #[error("{0}")]
    RuntimeError(
        #[from]
        #[backtrace]
        RuntimeError,
    ),
}

impl InternalError {
    pub fn missing_expression_key(key: Key<Expression>, function_path: &ItemPath) -> InternalError {
        InternalError::MissingExpressionKey {
            key,
            function_path: function_path.clone(),
            backtrace: Backtrace::capture(),
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LibraryProject {
    pub library: UserLibrary,
}

#[derive(Debug, Clone)]
pub struct LoadedLibraryProject {
    pub name: String,
    pub file_path: Option<PathBuf>,
}

pub struct LibraryProjectCamContext<'app> {
    pub tasks: &'app mut Tasks,
    cam_tasks: &'app mut CamTasks,
    pub library_project: &'app mut LoadedLibraryProject,
    pub libraries: &'app mut Libraries,
    pub profiler_frame: Option<Arc<FrameData>>,
}

pub struct CamContext<'app> {
    pub tasks: &'app mut Tasks,
    cam_tasks: &'app mut CamTasks,
    pub library_project: &'app mut Option<LoadedLibraryProject>,
    pub libraries: &'app mut Libraries,
    pub function_run_status: &'app mut HashMap<ItemPath, UserFunctionRun>,
    pub profiler_frame: Option<Arc<FrameData>>,
}

impl<'app> CamContext<'app> {
    pub fn loaded_library_context(&mut self) -> Option<LibraryProjectCamContext> {
        if let Some(library_project) = self.library_project {
            Some(LibraryProjectCamContext {
                tasks: self.tasks,
                cam_tasks: self.cam_tasks,
                library_project,
                libraries: self.libraries,
                profiler_frame: self.profiler_frame.clone(),
            })
        } else {
            None
        }
    }

    pub fn load_interpreter_project(&mut self) {
        let handle = self.tasks.spawn(LoadInterpreterProjectTask::new());
        self.cam_tasks.load_interpreter_project = Some(handle);
    }

    pub fn save_interpreter_project(&mut self) {
        if let Some(loaded_project) = self.library_project {
            let LibraryRef::User(library) = self.libraries.get(&loaded_project.name).unwrap()
            else {
                panic!("Not a user project")
            };

            let library_project = LibraryProject {
                library: library.clone(),
            };

            let handle = self.tasks.spawn(SaveInterpreterProjectTask::new(
                library_project,
                loaded_project.file_path.clone(),
            ));

            self.cam_tasks.save_interpreter_project = Some(handle);
        }
    }

    pub fn save_as_interpreter_project(&mut self) {
        if let Some(loaded_project) = self.library_project {
            let LibraryRef::User(library) = self.libraries.get(&loaded_project.name).unwrap()
            else {
                panic!("Not a user project")
            };

            let library_project = LibraryProject {
                library: library.clone(),
            };

            let handle = self
                .tasks
                .spawn(SaveInterpreterProjectTask::new(library_project, None));

            self.cam_tasks.save_interpreter_project = Some(handle);
        }
    }

    pub fn new_library_project(&mut self) -> ItemPath {
        let name = "New Project";
        let mut library = UserLibrary::new(name.to_string());
        let main_fn = library.insert_fn(UserFunction {
            name: "Main".to_string(),
            input_types: IndexMap::new(),
            output_type: self.libraries.core_items().unit_type.clone(),
            body: Expression::Block(ExpressionBlock {
                expressions: IncMap::new(),
            }),
        });

        self.libraries.insert_user_library(library);

        let project = LoadedLibraryProject {
            name: name.to_string(),
            file_path: None,
        };

        (*self.library_project) = Some(project);

        main_fn
    }
}

#[derive(Debug, Default)]
pub struct CamTasks {
    load_interpreter_project: Option<RunningTaskHandle<LoadInterpreterProjectTask>>,
    save_interpreter_project: Option<RunningTaskHandle<SaveInterpreterProjectTask>>,
    interpret: Option<RunningTaskHandle<InterpreterTask>>,
}

pub struct CamApp {
    tasks: Tasks,
    cam_tasks: CamTasks,
    project_loading: Option<TaskKey>,
    library_project: Option<LoadedLibraryProject>,
    libraries: Libraries,
    function_run_status: HashMap<ItemPath, UserFunctionRun>,
    profiler_frame: Arc<Mutex<Option<Arc<FrameData>>>>,
    ui: CamUi,
}

impl CamApp {
    fn cam_context(&mut self) -> CamContext {
        CamContext {
            tasks: &mut self.tasks,
            cam_tasks: &mut self.cam_tasks,
            library_project: &mut self.library_project,
            libraries: &mut self.libraries,
            function_run_status: &mut self.function_run_status,
            profiler_frame: self.profiler_frame.lock().unwrap().clone(),
        }
    }
}

impl CamApp {
    pub fn new(
        cc: &eframe::CreationContext<'_>,
        sender: impl Fn() + Send + Clone + 'static,
    ) -> Self {
        puffin::profile_function!();

        let profiler_frame = Arc::new(Mutex::new(None));

        {
            let profiler_frame = profiler_frame.clone();
            puffin::GlobalProfiler::lock().add_sink(Box::new(move |frame_data: Arc<FrameData>| {
                let mut frame = profiler_frame.lock().unwrap();
                *frame = Some(frame_data.clone());
            }));
        }

        let mut fonts = egui::FontDefinitions::default();
        egui_phosphor::add_to_fonts(&mut fonts, egui_phosphor::Variant::Regular);
        cc.egui_ctx.set_fonts(fonts);

        let tasks = Tasks::new(Box::new(sender));

        let (mut core_lib, core_items) = core_lib();
        let mut cam_lib = cam_lib(&core_items);
        let mut libraries = Libraries::with_libraries(vec![core_lib, cam_lib], vec![], core_items);

        let ui = CamUi::new(cc, &libraries);

        CamApp {
            tasks,
            cam_tasks: CamTasks::default(),
            project_loading: None,
            library_project: None,
            libraries,
            function_run_status: HashMap::new(),
            ui,
            profiler_frame,
        }
    }

    fn update_tasks(&mut self) {
        puffin::profile_function!();
        self.tasks.update();

        if let Some(result) = self.cam_tasks.load_interpreter_project.take_done() {
            match result {
                Ok(done) => {
                    self.library_project = Some(LoadedLibraryProject {
                        name: done.project.library.name().to_string(),
                        file_path: Some(done.file_path),
                    });

                    let main_fn = ItemPath::root_item(
                        done.project.library.name().to_string(),
                        ItemId::new("Main"),
                    );

                    self.libraries.insert_user_library(done.project.library);

                    self.ui.state().new_split(
                        UiTabs::UserFunction(UserFunctionUi::new(main_fn)),
                        Split::Below,
                        0.2,
                        true,
                    );
                }
                Err(e) => {
                    debug!("{:?}", e);
                    self.library_project = None;
                    self.ui.notify(Notification::error(
                        "Failed to load project".into(),
                        e.to_string(),
                    ));
                }
            }
        }

        if let Some(result) = self.cam_tasks.save_interpreter_project.take_done() {
            match result {
                Ok(done) => {
                    if let Some(project) = &mut self.library_project {
                        project.file_path = Some(done.file)
                    }
                }
                Err(e) => {
                    debug!("{:?}", e);
                    self.ui.notify(Notification::error(
                        "Failed to save project".into(),
                        e.to_string(),
                    ));
                }
            }
        }

        self.function_run_status = self
            .function_run_status
            .drain()
            .map(|(function_path, status)| (function_path, status.update()))
            .collect();
    }
}

pub struct RenderContext<'a> {
    pub gui: &'a egui::Context,
    pub frame: &'a eframe::Frame,
}

impl eframe::App for CamApp {
    fn update(&mut self, gui: &egui::Context, frame: &mut eframe::Frame) {
        puffin::profile_function!();
        puffin::GlobalProfiler::lock().new_frame();

        self.update_tasks();

        let mut cam_context = CamContext {
            tasks: &mut self.tasks,
            cam_tasks: &mut self.cam_tasks,
            library_project: &mut self.library_project,
            libraries: &mut self.libraries,
            function_run_status: &mut self.function_run_status,
            profiler_frame: {
                puffin::profile_scope!("profiler frame");
                self.profiler_frame.lock().unwrap().clone()
            },
        };

        let mut render_context = RenderContext { gui, frame };

        self.ui.update(&mut cam_context, &mut render_context);
    }
}

pub struct UserFunctionRun {
    pub last_update: Instant,
    pub value_history: IndexMap<CallStack, values::Values>,
    pub status: UserFunctionRunStatus,
}

#[derive(Debug)]
pub enum UserFunctionRunStatus {
    Running {
        running_call_stack: Option<CallStack>,
        task_handle: RunningTaskHandle<InterpreterTask>,
    },
    Done {
        result: Result<InterpreterDone, RuntimeError>,
    },
    Panicked,
}

impl UserFunctionRun {
    pub fn running(&self) -> bool {
        match self.status {
            UserFunctionRunStatus::Running { .. } => true,
            UserFunctionRunStatus::Done { .. } => false,
            UserFunctionRunStatus::Panicked => false,
        }
    }

    pub fn value_history(&self) -> &IndexMap<CallStack, values::Values> {
        &self.value_history
    }

    pub fn running_call_stack(&self) -> Option<&CallStack> {
        match &self.status {
            UserFunctionRunStatus::Running {
                running_call_stack, ..
            } => running_call_stack.as_ref(),
            UserFunctionRunStatus::Done { .. } => None,
            UserFunctionRunStatus::Panicked => None,
        }
    }

    pub fn last_update(&self) -> Instant {
        self.last_update
    }

    pub fn value(
        &self,
        call_stack: &CallStack,
        value_key: &cam3d_interpreter::value_key::ValueKey,
    ) -> Option<&cam3d_interpreter::value::Value> {
        self.value_history()
            .get(call_stack)
            .and_then(|values| values.get(value_key))
    }

    pub fn update(self) -> UserFunctionRun {
        let mut last_update = self.last_update;
        let mut value_history = self.value_history;

        let status = match self.status {
            UserFunctionRunStatus::Running {
                mut running_call_stack,
                mut task_handle,
            } => match task_handle.take_done_progress() {
                RunningTaskHandleDoneProgress::Done(result, progresses) => {
                    last_update = Instant::now();

                    for progress in progresses {
                        log::trace!("Obtained progress call stack {}", progress.event.call_stack);
                        value_history
                            .insert(progress.event.call_stack.clone(), progress.event.values);
                        running_call_stack = Some(progress.event.call_stack);
                    }

                    UserFunctionRunStatus::Done { result }
                }
                RunningTaskHandleDoneProgress::NotDone(mut task_handle) => {
                    while let Some(progress) = task_handle.take_progress() {
                        last_update = Instant::now();
                        value_history
                            .insert(progress.event.call_stack.clone(), progress.event.values);
                        running_call_stack = Some(progress.event.call_stack);
                    }

                    UserFunctionRunStatus::Running {
                        running_call_stack,
                        task_handle,
                    }
                }
                RunningTaskHandleDoneProgress::Panicked(progresses) => {
                    if let Some(running_call_stack) = &running_call_stack {
                        error!("Running function panicked at {running_call_stack}");
                    } else {
                        error!("Running function panicked before a status could be received");
                    }

                    last_update = Instant::now();

                    for progress in progresses {
                        log::trace!("Obtained progress call stack {}", progress.event.call_stack);
                        value_history
                            .insert(progress.event.call_stack.clone(), progress.event.values);
                        running_call_stack = Some(progress.event.call_stack);
                    }

                    UserFunctionRunStatus::Panicked
                }
            },

            UserFunctionRunStatus::Done { result } => UserFunctionRunStatus::Done { result },
            UserFunctionRunStatus::Panicked => UserFunctionRunStatus::Panicked,
        };

        UserFunctionRun {
            last_update,
            value_history,
            status,
        }
    }
}
