use crate::task::{Task, TaskHandle, TaskProgress};
use rfd::FileDialog;
use std::borrow::Cow;
use std::path::PathBuf;

#[derive(Debug, Clone)]
pub struct PickFileTask {
    pub name: String,
    pub file_types: &'static [(&'static str, &'static [&'static str])],
    pub allow_new: bool,
}

#[derive(thiserror::Error, Debug, Clone)]
pub enum PickFileError {
    #[error("File picker closed")]
    PickerClosed,
}

#[derive(Debug)]
pub enum PickFileProgress {
    Choosing(String),
}

impl TaskProgress for PickFileProgress {
    fn progress(&self) -> f32 {
        match self {
            PickFileProgress::Choosing(_) => 0.5,
        }
    }

    fn status(&self) -> String {
        match self {
            PickFileProgress::Choosing(name) => format!("Choosing {}", name).into(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct PickFileDone {
    pub file_path: PathBuf,
}

impl Task for PickFileTask {
    type Progress = PickFileProgress;
    type Done = Result<PickFileDone, PickFileError>;

    fn name(&self) -> Cow<'static, str> {
        "Choose File".into()
    }

    fn run(self, handle: TaskHandle<Self>) -> Self::Done {
        handle.send_progress(PickFileProgress::Choosing(self.name.clone()));

        let mut file_dialog = FileDialog::new();
        for (filter, extensions) in self.file_types.iter() {
            file_dialog = file_dialog.add_filter(*filter, extensions);
        }

        let file_path = if self.allow_new {
            file_dialog.save_file().ok_or(PickFileError::PickerClosed)?
        } else {
            file_dialog.pick_file().ok_or(PickFileError::PickerClosed)?
        };

        Ok(PickFileDone { file_path })
    }
}
