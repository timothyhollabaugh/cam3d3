use crate::app::LibraryProject;
use crate::task::{Task, TaskHandle, TaskProgress};
use rfd::FileDialog;
use ron::ser::PrettyConfig;
use std::borrow::Cow;
use std::fs::File;
use std::io::Write;
use std::path::PathBuf;
use std::sync::Arc;
use thiserror::Error;

#[derive(Debug)]
pub struct SaveInterpreterProjectTask {
    file: Option<PathBuf>,
    project: LibraryProject,
}

impl SaveInterpreterProjectTask {
    pub fn new(project: LibraryProject, file: Option<PathBuf>) -> SaveInterpreterProjectTask {
        SaveInterpreterProjectTask { project, file }
    }
}

#[derive(Error, Debug, Clone)]
pub enum SaveInterpreterProjectError {
    #[error("File picker closed")]
    PickerClosed,

    #[error("Could not read project file from disk: {source}")]
    Io {
        #[from]
        source: Arc<std::io::Error>,
    },

    #[error("Could not serialize project file: {source}")]
    Serialize {
        #[from]
        source: ron::Error,
    },
}

#[derive(Clone, Debug)]
pub enum SaveInterpreterProjectProgress {
    Choosing,
    Serializing,
    Writing,
}

impl TaskProgress for SaveInterpreterProjectProgress {
    fn progress(&self) -> f32 {
        let step = match self {
            SaveInterpreterProjectProgress::Choosing => 0,
            SaveInterpreterProjectProgress::Serializing => 1,
            SaveInterpreterProjectProgress::Writing => 2,
        };

        step as f32 / 3.0
    }

    fn status(&self) -> String {
        match self {
            SaveInterpreterProjectProgress::Choosing => "Choosing File".into(),
            SaveInterpreterProjectProgress::Serializing => "Serializing".into(),
            SaveInterpreterProjectProgress::Writing => "Writing File".into(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct SaveInterpreterProjectDone {
    pub file: PathBuf,
}

impl Task for SaveInterpreterProjectTask {
    type Progress = SaveInterpreterProjectProgress;
    type Done = Result<SaveInterpreterProjectDone, SaveInterpreterProjectError>;

    fn name(&self) -> Cow<'static, str> {
        match &self.file {
            None => format!("Saving Project {}", self.project.library.name()).into(),
            Some(file) => format!(
                "Saving Project {} to {}",
                self.project.library.name(),
                file.file_name()
                    .map(|f| f.to_string_lossy())
                    .unwrap_or(file.to_string_lossy())
            )
            .into(),
        }
    }

    fn run(mut self, handle: TaskHandle<Self>) -> Self::Done {
        if self.file.is_none() {
            handle.send_progress(SaveInterpreterProjectProgress::Choosing);

            let file_dialog = FileDialog::new().add_filter("Project Files", &["ron"]);

            let file_path = file_dialog
                .save_file()
                .ok_or(SaveInterpreterProjectError::PickerClosed)?;

            self.file = Some(file_path);
        };

        handle.send_progress(SaveInterpreterProjectProgress::Serializing);

        let file_str = ron::ser::to_string_pretty(&self.project, PrettyConfig::default())?;

        handle.send_progress(SaveInterpreterProjectProgress::Writing);

        let mut file = File::create(self.file.clone().unwrap()).map_err(Arc::new)?;
        file.write_all(file_str.as_bytes()).map_err(Arc::new)?;

        Ok(SaveInterpreterProjectDone {
            file: self.file.unwrap(),
        })
    }
}
