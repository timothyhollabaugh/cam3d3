use std::borrow::Cow;
use std::collections::HashMap;
use std::sync::Arc;

use crate::cam::geometry2d::{
    BoundingBox2, LineString, MultiLineString, MultiPolygon, Point2, Polygon,
};
use crate::cam::geometry3d::{Matrix4, Plane, Point3, Vector3};
use crate::cam::toolpath::{Toolpath, ToolpathCommand};
use crate::cam_module::Solid3dValue;
use crate::task::{Task, TaskHandle, TaskProgress};
use cam3d_interpreter::value_key::ValueKey;
use cam3d_interpreter::values::Values;
use three_d_asset::{InnerSpace, Srgba, TriMesh};

#[derive(Clone, Debug)]
pub struct RenderMesh {
    pub triangles: Vec<(TriMesh, Srgba)>,
    pub lines: Vec<(Point3, Point3, Srgba)>,
}

impl RenderMesh {
    pub fn new() -> RenderMesh {
        RenderMesh {
            triangles: Vec::new(),
            lines: Vec::new(),
        }
    }

    pub fn from_trimesh(mesh: TriMesh, color: Srgba) -> RenderMesh {
        RenderMesh {
            triangles: vec![(mesh, color)],
            lines: Vec::new(),
        }
    }

    pub fn from_trimeshes(meshes: Vec<(TriMesh, Srgba)>) -> RenderMesh {
        RenderMesh {
            triangles: meshes,
            lines: Vec::new(),
        }
    }

    pub fn from_lines(lines: Vec<(Point3, Point3, Srgba)>) -> RenderMesh {
        RenderMesh {
            triangles: Vec::new(),
            lines,
        }
    }
}

#[derive(Clone, Debug)]
pub struct RenderInterpreterTask {
    pub values: Values,
}

impl RenderInterpreterTask {
    pub fn new(values: Values) -> RenderInterpreterTask {
        RenderInterpreterTask { values }
    }
}

#[derive(Clone, Debug)]
pub struct RenderInterpreterProgress {
    pub value_key: ValueKey,
    pub values_index: usize,
    pub values_len: usize,
}

impl TaskProgress for RenderInterpreterProgress {
    fn progress(&self) -> f32 {
        self.values_index as f32 / self.values_len as f32
    }

    fn status(&self) -> String {
        format!(
            "Rendering value {}/{}: {}",
            self.values_index + 1,
            self.values_len,
            self.value_key
        )
    }
}

#[derive(thiserror::Error, Clone, Debug)]
pub enum RenderInterpreterError {
    #[error("Transform Error: {source}")]
    Transform {
        #[source]
        #[from]
        source: Arc<three_d_asset::Error>,
    },

    #[error("Tessellation Error: {source}")]
    Tessellation {
        #[source]
        #[from]
        source: lyon::tessellation::TessellationError,
    },
}

#[derive(Clone, Debug)]
pub struct RenderInterpreterDone {
    pub meshes: HashMap<ValueKey, Result<RenderMesh, RenderInterpreterError>>,
}

impl Task for RenderInterpreterTask {
    type Progress = RenderInterpreterProgress;
    type Done = RenderInterpreterDone;

    fn name(&self) -> Cow<'static, str> {
        "Rendering Values".into()
    }

    fn run(self, handle: TaskHandle<Self>) -> Self::Done {
        let mut meshes = HashMap::new();

        let values_len = self.values.len();

        for (values_index, (value_key, value)) in self.values.iter().enumerate() {
            handle.send_progress(RenderInterpreterProgress {
                value_key: value_key.clone(),
                values_index,
                values_len,
            });

            let mesh = if let Ok(plane) = value.downcast() {
                Some(render_plane(plane))
            } else if let Ok(solid) = value.downcast() {
                Some(render_solid_3d(solid))
            } else if let Ok(toolpath) = value.downcast() {
                Some(render_toolpath(toolpath))
            } else if let Ok(point2d) = value.downcast() {
                Some(render_point2d(*point2d))
            } else if let Ok(point3d) = value.downcast() {
                Some(render_point3d(*point3d))
            } else if let Ok(linestring) = value.downcast() {
                Some(render_linestring(linestring))
            } else if let Ok(multilinestring) = value.downcast() {
                Some(render_multilinestring(multilinestring))
            } else if let Ok(polygon) = value.downcast() {
                Some(render_polygon(polygon))
            } else if let Ok(multipolygon) = value.downcast() {
                Some(render_multipolygon(multipolygon))
            } else if let Ok(boundingbox) = value.downcast() {
                Some(render_boundingbox2d(boundingbox))
            } else {
                None
            };

            if let Some(mesh) = mesh {
                meshes.insert(value_key.clone(), mesh);
            }
        }

        RenderInterpreterDone { meshes }
    }
}

pub fn render_plane(plane: &Plane) -> Result<RenderMesh, RenderInterpreterError> {
    Ok(RenderMesh::from_trimeshes(vec![
        (
            TriMesh::cube()
                * Matrix4::scale(Vector3::new(10.0, 0.5, 0.5))
                * Matrix4::translate(Vector3::from_x(10.5))
                * Matrix4::rotate(plane.normal)
                * Matrix4::translate(plane.point.as_vector()),
            Srgba::RED,
        ),
        (
            TriMesh::cube()
                * Matrix4::scale(Vector3::new(0.5, 10.0, 0.5))
                * Matrix4::translate(Vector3::from_y(10.5))
                * Matrix4::rotate(plane.normal)
                * Matrix4::translate(plane.point.as_vector()),
            Srgba::GREEN,
        ),
        (
            TriMesh::cube()
                * Matrix4::scale(Vector3::new(0.5, 0.5, 10.0))
                * Matrix4::translate(Vector3::from_z(10.5))
                * Matrix4::rotate(plane.normal)
                * Matrix4::translate(plane.point.as_vector()),
            Srgba::BLUE,
        ),
        (
            TriMesh::cube()
                * Matrix4::scale(Vector3::new(0.5, 0.5, 0.5))
                * Matrix4::rotate(plane.normal)
                * Matrix4::translate(plane.point.as_vector()),
            Srgba::WHITE,
        ),
    ]))
}

pub fn render_solid_3d(value: &Solid3dValue) -> Result<RenderMesh, RenderInterpreterError> {
    let mesh = &value.0;

    let wireframe_threshold = 0.5;

    let lines = mesh
        .edge_iter()
        .filter_map(|edge_index| {
            let edge_walker = mesh.walker_from_halfedge(edge_index);
            let face1_index = edge_walker.face_id()?;
            let face1_normal = mesh.face_normal(face1_index);
            let vertex1_index = edge_walker.vertex_id()?;
            let vertex1_position = mesh.vertex_position(vertex1_index);

            let edge_walker = edge_walker.into_twin();
            let face2_index = edge_walker.face_id()?;
            let face2_normal = mesh.face_normal(face2_index);
            let vertex2_index = edge_walker.vertex_id()?;
            let vertex2_position = mesh.vertex_position(vertex2_index);

            let dot = face1_normal.dot(face2_normal);
            let render_edge = dot <= wireframe_threshold;

            render_edge.then_some((
                Point3::from(vertex1_position),
                Point3::from(vertex2_position),
                Srgba::BLACK,
            ))
        })
        .collect();

    let render_mesh = RenderMesh {
        triangles: vec![(mesh.export(), Srgba::WHITE)],
        lines,
    };

    Ok(render_mesh)
}
pub fn render_toolpath(toolpath: &Toolpath) -> Result<RenderMesh, RenderInterpreterError> {
    let (_final_point, lines) = toolpath.commands.iter().fold(
        (None::<Point3>, Vec::new()),
        |(last_point, mut lines), command| {
            let point = match (last_point, command) {
                (_, ToolpathCommand::MoveTo(p, _f)) => Some((*p, Srgba::new(128, 128, 128, 255))),
                (_, ToolpathCommand::RapidTo(p)) => Some((*p, Srgba::new(240, 240, 240, 255))),
                (Some(last_point), ToolpathCommand::RapidToZ(z)) => Some((
                    Point3::new(last_point.x, last_point.y, *z),
                    Srgba::new(240, 240, 240, 255),
                )),
                _ => None,
            };

            if let Some((point, color)) = point {
                if let Some(last_point) = last_point {
                    lines.push((last_point, point, color));
                }

                (Some(point), lines)
            } else {
                (None, lines)
            }
        },
    );

    Ok(RenderMesh::from_lines(lines))
}

pub fn render_point2d(point: Point2) -> Result<RenderMesh, RenderInterpreterError> {
    Ok(RenderMesh::from_trimesh(
        TriMesh::cube()
            * Matrix4::scale(Vector3::new(0.5, 0.5, 0.5))
            * Matrix4::translate(point.into_3d(&Plane::xy(0.0)).as_vector()),
        Srgba::WHITE,
    ))
}

pub fn render_point3d(point: Point3) -> Result<RenderMesh, RenderInterpreterError> {
    Ok(RenderMesh::from_trimesh(
        TriMesh::cube()
            * Matrix4::scale(Vector3::new(0.5, 0.5, 0.5))
            * Matrix4::translate(point.as_vector()),
        Srgba::WHITE,
    ))
}

pub fn render_linestring(linestring: &LineString) -> Result<RenderMesh, RenderInterpreterError> {
    let lines = linestring
        .points
        .iter()
        .map_windows(|[p1, p2]| {
            (
                p1.into_3d(&Plane::xy(0.0)),
                p2.into_3d(&Plane::xy(0.0)),
                Srgba::new(128, 128, 128, 255),
            )
        })
        .collect();

    Ok(RenderMesh::from_lines(lines))
}

pub fn render_multilinestring(
    multilinestring: &MultiLineString,
) -> Result<RenderMesh, RenderInterpreterError> {
    let lines = multilinestring
        .linestrings
        .iter()
        .flat_map(|linestring| {
            linestring.points.iter().map_windows(|[p1, p2]| {
                (
                    p1.into_3d(&Plane::xy(0.0)),
                    p2.into_3d(&Plane::xy(0.0)),
                    Srgba::new(128, 128, 128, 255),
                )
            })
        })
        .collect();

    Ok(RenderMesh::from_lines(lines))
}

pub fn render_polygon(polygon: &Polygon) -> Result<RenderMesh, RenderInterpreterError> {
    let lines = polygon
        .interiors
        .iter()
        .chain(std::iter::once(&polygon.exterior))
        .flat_map(|linestring| {
            linestring.points.iter().map_windows(|[p1, p2]| {
                (
                    p1.into_3d(&Plane::xy(0.0)),
                    p2.into_3d(&Plane::xy(0.0)),
                    Srgba::new(128, 128, 128, 255),
                )
            })
        })
        .collect();

    let meshes = vec![(polygon.tessillate()?, Srgba::new(240, 240, 240, 255))];

    Ok(RenderMesh {
        triangles: meshes,
        lines,
    })
}

pub fn render_multipolygon(
    multipolygon: &MultiPolygon,
) -> Result<RenderMesh, RenderInterpreterError> {
    let lines = multipolygon
        .polygons
        .iter()
        .flat_map(|polygon| {
            polygon
                .interiors
                .iter()
                .chain(std::iter::once(&polygon.exterior))
                .flat_map(|linestring| {
                    linestring.points.iter().map_windows(|[p1, p2]| {
                        (
                            p1.into_3d(&Plane::xy(0.0)),
                            p2.into_3d(&Plane::xy(0.0)),
                            Srgba::new(128, 128, 128, 255),
                        )
                    })
                })
        })
        .collect();

    let meshes = multipolygon
        .polygons
        .iter()
        .map(|polygon| {
            polygon
                .tessillate()
                .map(|mesh| (mesh, Srgba::new(240, 240, 240, 255)))
        })
        .collect::<Result<_, _>>()?;

    Ok(RenderMesh {
        triangles: meshes,
        lines,
    })
}

pub fn render_boundingbox2d(
    boundingbox: &BoundingBox2,
) -> Result<RenderMesh, RenderInterpreterError> {
    let lines = LineString::rectangle(boundingbox.min_xy(), boundingbox.max_xy());
    render_linestring(&lines)
}
