use crate::app::LibraryProject;
use crate::task::{Task, TaskHandle, TaskProgress};
use rfd::FileDialog;
use std::borrow::Cow;
use std::fs::File;
use std::io::Read;
use std::path::PathBuf;
use std::sync::Arc;
use thiserror::Error;

#[derive(Debug)]
pub struct LoadInterpreterProjectTask {
    file: Option<String>,
    name: Option<String>,
}

impl LoadInterpreterProjectTask {
    pub fn new() -> LoadInterpreterProjectTask {
        LoadInterpreterProjectTask {
            file: None,
            name: None,
        }
    }
}

#[derive(Error, Debug, Clone)]
pub enum LoadInterpreterProjectError {
    #[error("File picker closed")]
    PickerClosed,

    #[error("Could not read project file from disk: {source}")]
    Io {
        #[from]
        source: Arc<std::io::Error>,
    },

    #[error("Could not parse project file: {source}")]
    Parse {
        #[from]
        source: ron::error::SpannedError,
    },
}

#[derive(Clone, Debug)]
pub enum LoadInterpreterProjectProgress {
    Choosing,
    Reading,
    Parsing,
}

impl TaskProgress for LoadInterpreterProjectProgress {
    fn progress(&self) -> f32 {
        let step = match self {
            LoadInterpreterProjectProgress::Choosing => 0,
            LoadInterpreterProjectProgress::Reading => 1,
            LoadInterpreterProjectProgress::Parsing => 2,
        };

        step as f32 / 5.0
    }

    fn status(&self) -> String {
        match self {
            LoadInterpreterProjectProgress::Choosing => "Choosing File".into(),
            LoadInterpreterProjectProgress::Reading => "Reading File".into(),
            LoadInterpreterProjectProgress::Parsing => "Parsing Project".into(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct LoadInterpreterProjectDone {
    pub project: LibraryProject,
    pub file_path: PathBuf,
}

impl Task for LoadInterpreterProjectTask {
    type Progress = LoadInterpreterProjectProgress;
    type Done = Result<LoadInterpreterProjectDone, LoadInterpreterProjectError>;

    fn name(&self) -> Cow<'static, str> {
        match (&self.file, &self.name) {
            (None, None) => "Loading Project".into(),
            (Some(file), None) => format!("Loading Project from {}", file).into(),
            (None, Some(name)) => format!("Loading Project {}", name).into(),
            (Some(file), Some(name)) => format!("Loading Project {} from {}", name, file).into(),
        }
    }

    fn run(mut self, handle: TaskHandle<Self>) -> Self::Done {
        handle.send_progress(LoadInterpreterProjectProgress::Choosing);

        let file_dialog = FileDialog::new().add_filter("Project Files", &["ron"]);

        let file_path = file_dialog
            .pick_file()
            .ok_or(LoadInterpreterProjectError::PickerClosed)?;

        self.file = Some(
            file_path
                .file_name()
                .map(|f| f.to_string_lossy())
                .unwrap_or(file_path.to_string_lossy())
                .to_string(),
        );

        handle.send_progress(LoadInterpreterProjectProgress::Reading);

        let mut file = File::open(&file_path).map_err(Arc::new)?;

        let mut file_str = String::new();
        file.read_to_string(&mut file_str).map_err(Arc::new)?;

        handle.send_progress(LoadInterpreterProjectProgress::Parsing);

        let project: LibraryProject = ron::from_str(&file_str)?;

        Ok(LoadInterpreterProjectDone { project, file_path })
    }
}
