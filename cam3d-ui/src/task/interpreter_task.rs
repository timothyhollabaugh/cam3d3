use crate::task::{Task, TaskHandle, TaskProgress};
use cam3d_interpreter::call_stack::{CallStack, UserFunctionProgressError};
use cam3d_interpreter::function::{Function, FunctionRef};
use cam3d_interpreter::inc_map::IncMap;
use cam3d_interpreter::interpreter::{Interpreter, InterpreterEvent, RuntimeError};
use cam3d_interpreter::item_path::ItemPath;
use cam3d_interpreter::library::Libraries;
use cam3d_interpreter::value::Value;
use cam3d_interpreter::values::Values;
use std::borrow::Cow;

#[derive(Debug, Clone)]
pub struct InterpreterTask {
    pub libraries: Libraries,
    pub main_fn: ItemPath,
}

#[derive(Debug, Clone)]
pub struct InterpreterProgress {
    pub event: InterpreterEvent,
    pub progress: f32,
    pub status: String,
}

impl InterpreterProgress {
    fn new(event: InterpreterEvent, libraries: &Libraries) -> InterpreterProgress {
        match event.call_stack.progress(libraries) {
            Ok((position, length, item_path)) => InterpreterProgress {
                event,
                progress: position as f32 / length as f32,
                status: format!("{position}/{length}: {item_path}"),
            },
            Err(e) => InterpreterProgress {
                event,
                progress: 0.0,
                status: format!("Error calculating progress: {e}"),
            },
        }
    }

    pub fn event(&self) -> &InterpreterEvent {
        &self.event
    }

    pub fn values(&self) -> &Values {
        &self.event.values
    }

    pub fn call_stack(&self) -> &CallStack {
        &self.event.call_stack
    }
}

impl TaskProgress for InterpreterProgress {
    fn progress(&self) -> f32 {
        self.progress
    }

    fn status(&self) -> String {
        self.status.clone()
    }
}

#[derive(Debug, Clone)]
pub struct InterpreterDone {
    pub values: Values,
}

impl Task for InterpreterTask {
    type Progress = InterpreterProgress;
    type Done = Result<InterpreterDone, RuntimeError>;

    fn name(&self) -> Cow<'static, str> {
        "Interpreting".into()
    }

    fn run(self, handle: TaskHandle<Self>) -> Self::Done {
        log::info!("Starting interpreter");

        let interpreter = Interpreter::new(&self.libraries, self.main_fn, Vec::new());
        let result = interpreter.run(|event| {
            match &event {
                InterpreterEvent { call_stack, values } => {
                    log::trace!("Interpreter Event");
                    log::trace!("Call Stack: {}", call_stack);
                    log::trace!("Values:");
                    for (value_key, value) in values.iter() {
                        log::trace!("  {value_key}: {value}");
                    }
                }
            }
            handle.send_progress(InterpreterProgress::new(event, &self.libraries))
        });

        match result {
            Ok(values) => {
                log::info!("Done running interpreter");
                log::trace!("Values:");
                for (value_key, value) in values.iter() {
                    log::trace!("  {value_key}: {value}");
                }
                Ok(InterpreterDone { values })
            }
            Err(e) => {
                log::error!("Error running interpreter: {}", e.error_type);
                log::error!("Backtrace:\n{}", e.backtrace);
                if let Some(callstack) = &e.callstack {
                    log::error!("Call Stack:\n{}", callstack.format(None, &self.libraries));
                }
                if let Some(values) = &e.values {
                    log::error!("Values:\n{}", values.format(&self.libraries));
                }
                Err(e)
            }
        }
    }
}
