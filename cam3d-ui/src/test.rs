const TOLERANCE: f32 = 1e-3;

pub fn assert_close(a: f32, b: f32) {
    if f32::abs(a - b) > TOLERANCE {
        panic!(
            "{} and {} differ by {} which is more than {}",
            a,
            b,
            f32::abs(a - b),
            TOLERANCE
        )
    }
}
