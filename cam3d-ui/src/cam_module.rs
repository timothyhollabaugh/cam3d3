use crate::cam::geometry2d::{
    load_svg_multilinestring, BoundingBox2, LineString, MultiLineString, MultiPolygon, Origin2,
    Point2, Polygon, Vector2,
};
use crate::cam::geometry3d::{intersect_mesh_z, load_stl, Matrix4, Plane, Point3, Vector3};
use crate::cam::toolpath::adaptive_2d::{clear_polygons_adaptive, AdaptiveClearConfig};
use crate::cam::toolpath::adaptive_3d::{clear_3d_adaptive, Clear3dAdaptiveConfig};
use crate::cam::toolpath::drag_knife::generate_drag_knife_gcode;
use crate::cam::toolpath::engrave::engrave_multilinestring;
use crate::cam::toolpath::{Toolpath, ToolpathCommand};
use cam3d_interpreter::core_module::{FloatOptionValue, PathValue, UnitValue};
use cam3d_interpreter::function::native::NativeFunction;
use cam3d_interpreter::item_path::ItemPath;
use cam3d_interpreter::library::native::NativeLibrary;
use cam3d_interpreter::library::CoreItems;
use cam3d_interpreter::module::native::NativeModule;
use cam3d_interpreter::type_::NativeType;
use cam3d_interpreter::value::{LiteralValueBox, Value, ValueBox, ValueUnboxed};
use cam3d_interpreter::value_key::{AbsoluteRuntimeKey, FunctionCallInputKey};
use clipper_rs::PointPolygon;
use indexmap::IndexMap;
use serde::Deserialize;
use std::any::Any;
use std::fmt::{Display, Formatter};
use std::ops::{Add, Div, Mul, Sub};
use std::sync::Arc;

#[derive(Copy, Clone, Debug)]
struct CamPaths<'a> {
    unit: &'a ItemPath,
    bool: &'a ItemPath,
    int: &'a ItemPath,
    float: &'a ItemPath,
    float_optional: &'a ItemPath,
    path_open: &'a ItemPath,
    path_save: &'a ItemPath,
    string: &'a ItemPath,
    newlinestring: &'a ItemPath,
    geometry_module: &'a ItemPath,
    point2: &'a ItemPath,
    vector2: &'a ItemPath,
    point3: &'a ItemPath,
    vector3: &'a ItemPath,
    boundingbox2d: &'a ItemPath,
    linestring: &'a ItemPath,
    multilinestring: &'a ItemPath,
    polygon: &'a ItemPath,
    multipolygon: &'a ItemPath,
    plane: &'a ItemPath,
    solid: &'a ItemPath,
    cam_module: &'a ItemPath,
    toolpath_command: &'a ItemPath,
    toolpath: &'a ItemPath,
}

pub fn cam_lib(core_items: &CoreItems) -> NativeLibrary {
    let mut library = NativeLibrary::new("cam");
    let geometry_module = library.insert_module(NativeModule::new("geometry"));
    let cam_module = library.insert_module(NativeModule::new("cam"));

    let cam_paths = CamPaths {
        unit: &core_items.unit_type,
        bool: &core_items.bool_type,
        int: &core_items.integer_type,
        float: &core_items.float_type,
        float_optional: &core_items.float_option_type,
        path_open: &core_items.path_open_type,
        path_save: &core_items.path_save_type,
        string: &core_items.string_type,
        newlinestring: &core_items.newlinestring,
        geometry_module: &geometry_module,
        point2: &library.insert_type_to(&geometry_module, NativeType::new("Point2d")),
        vector2: &library.insert_type_to(&geometry_module, NativeType::new("Vector2d")),
        point3: &library.insert_type_to(&geometry_module, NativeType::new("Point3d")),
        vector3: &library.insert_type_to(&geometry_module, NativeType::new("Vector3d")),
        boundingbox2d: &library.insert_type_to(&geometry_module, NativeType::new("BoundingBox2d")),
        linestring: &library.insert_type_to(&geometry_module, NativeType::new("LineString")),
        multilinestring: &library
            .insert_type_to(&geometry_module, NativeType::new("MultilineString")),
        polygon: &library.insert_type_to(&geometry_module, NativeType::new("Polygon")),
        multipolygon: &library.insert_type_to(&geometry_module, NativeType::new("Multipolygon")),
        plane: &library.insert_type_to(&geometry_module, NativeType::new("Plane")),
        solid: &library.insert_type_to(&geometry_module, NativeType::new("Solid")),
        cam_module: &cam_module,
        toolpath_command: &library.insert_type_to(&cam_module, NativeType::new("ToolpathCommand")),
        toolpath: &library.insert_type_to(&cam_module, NativeType::new("Toolpath")),
    };

    point_2d_functions(&mut library, cam_paths);
    vector_2d_functions(&mut library, cam_paths);
    vector_3d_functions(&mut library, cam_paths);
    boundingbox_2d_functions(&mut library, cam_paths);
    linestring_functions(&mut library, cam_paths);
    multilinestring_functions(&mut library, cam_paths);
    polygon_functions(&mut library, cam_paths);
    multipolygon_functions(&mut library, cam_paths);
    plane_functions(&mut library, cam_paths);
    solid_functions(&mut library, cam_paths);
    toolpath_functions(&mut library, cam_paths);
    cam_functions(&mut library, cam_paths);

    library
}

fn point_2d_functions(library: &mut NativeLibrary, p: CamPaths) {
    // Point 2d functions
    library.insert_fn2_to(
        (&p.geometry_module, "new_point2"),
        ("x", p.float),
        ("y", p.float),
        p.point2,
        |x, y| Point2::new(*x, *y),
    );

    library.insert_method1_to((p.point2, "x"), p.float, |point: &Point2| point.x);
    library.insert_method1_to((p.point2, "y"), p.float, |point: &Point2| point.y);

    library.insert_method2_to(
        (p.point2, "add"),
        ("point", p.vector2),
        p.point2,
        |a: &Point2, b: &Vector2| Point2::add(*a, *b),
    );

    library.insert_method2_to(
        (p.point2, "sub"),
        ("point", p.vector2),
        p.point2,
        |a: &Point2, b: &Vector2| Point2::sub(*a, *b),
    );

    library.insert_method2_to(
        (p.point2, "mul"),
        ("scalar", p.float),
        p.point2,
        |a: &Point2, b: &f32| Point2::mul(*a, *b),
    );

    library.insert_method2_to(
        (p.point2, "div"),
        ("point", p.float),
        p.point2,
        |a: &Point2, b: &f32| Point2::div(*a, *b),
    );

    library.insert_method1_to((p.point2, "distance"), p.float, |point: &Point2| {
        point.distance()
    });

    library.insert_method1_to((p.point2, "distance_squared"), p.float, |point: &Point2| {
        point.distance_squared()
    });

    library.insert_method3_to(
        (p.point2, "distance_from_edge"),
        ("start", p.point2),
        ("end", p.point2),
        p.float,
        |self_: &Point2, start: &Point2, end: &Point2| self_.distance_from_edge(*start, *end),
    );

    library.insert_method3_to(
        (p.point2, "project_onto_edge"),
        ("start", p.point2),
        ("end", p.point2),
        p.point2,
        |self_: &Point2, start: &Point2, end: &Point2| self_.project_onto_edge(*start, *end),
    );

    library.insert_method3_to(
        (p.point2, "distance_along_edge"),
        ("start", p.point2),
        ("end", p.point2),
        p.float,
        |self_: &Point2, start: &Point2, end: &Point2| self_.distance_along_edge(*start, *end),
    );

    library.insert_method2_to(
        (p.point2, "close"),
        ("other", p.point2),
        &p.bool,
        |self_: &Point2, other: &Point2| self_.close(other),
    );
}

fn vector_2d_functions(library: &mut NativeLibrary, p: CamPaths) {
    // Vector 2d functions

    library.insert_fn2_to(
        (&p.geometry_module, "new_vector2"),
        ("x", p.float),
        ("y", p.float),
        p.vector2,
        |x, y| Vector2::new(*x, *y),
    );

    library.insert_method1_to((p.vector2, "x"), p.float, |vector: &Vector2| vector.x);
    library.insert_method1_to((p.vector2, "y"), p.float, |vector: &Vector2| vector.y);

    library.insert_method2_to(
        (p.vector2, "add"),
        ("vector", p.vector2),
        p.vector2,
        |a: &Vector2, b: &Vector2| Vector2::add(*a, *b),
    );

    library.insert_method2_to(
        (p.vector2, "sub"),
        ("vector", p.vector2),
        p.vector2,
        |a: &Vector2, b: &Vector2| Vector2::sub(*a, *b),
    );

    library.insert_method2_to(
        (p.vector2, "mul"),
        ("scalar", p.float),
        p.vector2,
        |a: &Vector2, b: &f32| Vector2::mul(*a, *b),
    );

    library.insert_method2_to(
        (p.vector2, "div"),
        ("vector", p.float),
        p.vector2,
        |a: &Vector2, b: &f32| Vector2::div(*a, *b),
    );

    library.insert_method1_to((p.vector2, "magnitude"), p.float, |vector: &Vector2| {
        vector.magnitude()
    });

    library.insert_method1_to(
        (p.vector2, "magnitude_squared"),
        p.float,
        |vector: &Vector2| vector.magnitude_squared(),
    );

    library.insert_method1_to((p.vector2, "normalized"), p.vector2, |vector: &Vector2| {
        vector.normalized()
    });

    library.insert_method2_to(
        (p.vector2, "dot"),
        ("other", p.vector2),
        p.vector2,
        |vector: &Vector2, other: &Vector2| vector.dot(*other),
    );

    library.insert_method2_to(
        (p.vector2, "project_onto"),
        ("other", p.vector2),
        p.vector2,
        |vector: &Vector2, other: &Vector2| vector.project_onto(*other),
    );

    // Point 3d functions
    library.insert_fn3_to(
        (&p.geometry_module, "new_point3"),
        ("x", p.float),
        ("y", p.float),
        ("z", p.float),
        p.point3,
        |x, y, z| Point3::new(*x, *y, *z),
    );

    library.insert_method1_to((p.point3, "x"), p.float, |point: &Point3| point.x);
    library.insert_method1_to((p.point3, "y"), p.float, |point: &Point3| point.y);

    library.insert_method2_to(
        (p.point3, "add"),
        ("point", p.vector3),
        p.point3,
        |a: &Point3, b: &Vector3| Point3::add(*a, *b),
    );

    library.insert_method2_to(
        (p.point3, "sub"),
        ("point", p.vector3),
        p.point3,
        |a: &Point3, b: &Vector3| Point3::sub(*a, *b),
    );

    library.insert_method2_to(
        (p.point3, "mul"),
        ("scalar", p.float),
        p.point3,
        |a: &Point3, b: &f32| Point3::mul(*a, *b),
    );

    library.insert_method2_to(
        (p.point3, "div"),
        ("point", p.float),
        p.point3,
        |a: &Point3, b: &f32| Point3::div(*a, *b),
    );

    library.insert_method1_to((p.point3, "distance"), p.float, |point: &Point3| {
        point.distance()
    });

    library.insert_method1_to((p.point3, "distance_squared"), p.float, |point: &Point3| {
        point.distance_squared()
    });
}

fn vector_3d_functions(library: &mut NativeLibrary, p: CamPaths) {
    // Vector 3d functions
    library.insert_fn3_to(
        (&p.geometry_module, "new_vector3"),
        ("x", p.float),
        ("y", p.float),
        ("z", p.float),
        p.vector3,
        |x, y, z| Vector3::new(*x, *y, *z),
    );

    library.insert_method1_to((p.vector3, "x"), p.float, |vector: &Vector3| vector.x);
    library.insert_method1_to((p.vector3, "y"), p.float, |vector: &Vector3| vector.y);

    library.insert_method2_to(
        (p.vector3, "add"),
        ("vector", p.vector3),
        p.vector3,
        |a: &Vector3, b: &Vector3| Vector3::add(*a, *b),
    );

    library.insert_method2_to(
        (p.vector3, "sub"),
        ("vector", p.vector3),
        p.vector3,
        |a: &Vector3, b: &Vector3| Vector3::sub(*a, *b),
    );

    library.insert_method2_to(
        (p.vector3, "mul"),
        ("scalar", p.float),
        p.vector3,
        |a: &Vector3, b: &f32| Vector3::mul(*a, *b),
    );

    library.insert_method2_to(
        (p.vector3, "div"),
        ("vector", p.float),
        p.vector3,
        |a: &Vector3, b: &f32| Vector3::div(*a, *b),
    );
}

fn boundingbox_2d_functions(library: &mut NativeLibrary, p: CamPaths) {
    // Bounding box 2d functions

    library.insert_method1_to(
        (p.boundingbox2d, "range"),
        p.vector2,
        |bounds: &BoundingBox2| bounds.range(),
    );

    library.insert_method1_to(
        (p.boundingbox2d, "min_x"),
        p.float,
        |bounds: &BoundingBox2| bounds.min_x(),
    );

    library.insert_method1_to(
        (p.boundingbox2d, "max_x"),
        p.float,
        |bounds: &BoundingBox2| bounds.max_x(),
    );

    library.insert_method1_to(
        (p.boundingbox2d, "range_x"),
        p.float,
        |bounds: &BoundingBox2| bounds.range_x(),
    );

    library.insert_method1_to(
        (p.boundingbox2d, "min_y"),
        p.float,
        |bounds: &BoundingBox2| bounds.min_y(),
    );

    library.insert_method1_to(
        (p.boundingbox2d, "max_y"),
        p.float,
        |bounds: &BoundingBox2| bounds.max_y(),
    );

    library.insert_method1_to(
        (p.boundingbox2d, "range_y"),
        p.float,
        |bounds: &BoundingBox2| bounds.range_y(),
    );

    library.insert_method1_to(
        (p.boundingbox2d, "min_xy"),
        p.point2,
        |bounds: &BoundingBox2| bounds.min_xy(),
    );

    library.insert_method1_to(
        (p.boundingbox2d, "max_xy"),
        p.point2,
        |bounds: &BoundingBox2| bounds.max_xy(),
    );

    library.insert_method1_to(
        (p.boundingbox2d, "min_x_max_y"),
        p.point2,
        |bounds: &BoundingBox2| bounds.min_x_max_y(),
    );

    library.insert_method1_to(
        (p.boundingbox2d, "max_x_min_y"),
        p.point2,
        |bounds: &BoundingBox2| bounds.max_x_min_y(),
    );

    library.insert_method2_mut_to(
        (p.boundingbox2d, "expand"),
        ("other", p.boundingbox2d),
        p.unit,
        |bounds: &mut BoundingBox2, other: &BoundingBox2| {
            bounds.expand(*other);
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.boundingbox2d, "expand_to_point"),
        ("point", p.boundingbox2d),
        p.unit,
        |bounds: &mut BoundingBox2, other: &Point2| {
            bounds.expand_to_point(*other);
            UnitValue
        },
    );
}

fn linestring_functions(library: &mut NativeLibrary, p: CamPaths) {
    // Linestring Functions

    library.insert_fn0_to((&p.geometry_module, "new_linestring"), p.linestring, || {
        LineString::new()
    });

    library.insert_fn2_to(
        (&p.geometry_module, "rectangle"),
        ("point1", p.point2),
        ("point2", p.point2),
        p.linestring,
        |p1, p2| LineString::rectangle(*p1, *p2),
    );

    library.insert_fn2_to(
        (&p.geometry_module, "circle"),
        ("radius", p.float),
        ("number_segments", p.int),
        p.linestring,
        |radius: &f32, number_segments: &i32| {
            LineString::circle(*radius, *number_segments as usize)
        },
    );

    library.insert_method2_mut_to(
        (p.linestring, "add_point"),
        ("point", p.point2),
        p.unit,
        |linestring: &mut LineString, point: &Point2| {
            linestring.points.push(*point);
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.linestring, "remove_point"),
        ("index", p.int),
        p.unit,
        |linestring: &mut LineString, index: &i32| {
            linestring.points.remove(*index as usize);
            UnitValue
        },
    );

    library.insert_method2_ranged_to(
        (p.linestring, "get_point"),
        (
            "index",
            p.int,
            Some(|linestring: &LineString| 0..(linestring.points.len() as i32)),
        ),
        p.point2,
        |linestring: &LineString, index: &i32| {
            linestring.points.get(*index as usize).unwrap().clone()
        },
    );

    library.insert_method1_to(
        (p.linestring, "number_points"),
        p.int,
        |linestring: &LineString| linestring.points.len() as i32,
    );

    library.insert_method1_to(
        (p.linestring, "is_empty"),
        p.bool,
        |linestring: &LineString| linestring.is_empty(),
    );

    library.insert_method1_to(
        (p.linestring, "is_closed"),
        p.bool,
        |linestring: &LineString| linestring.is_closed(),
    );

    library.insert_method1_mut_to(
        (p.linestring, "close"),
        p.unit,
        |linestring: &mut LineString| {
            linestring.close();
            UnitValue
        },
    );

    library.insert_method1_mut_to(
        (p.linestring, "open"),
        p.unit,
        |linestring: &mut LineString| {
            linestring.open();
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.linestring, "translate"),
        ("offset", p.vector2),
        p.unit,
        |linestring: &mut LineString, offset: &Vector2| {
            *linestring += *offset;
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.linestring, "rotate"),
        ("angle", p.float),
        p.unit,
        |linestring: &mut LineString, angle: &f32| {
            linestring.transform_iso(&Origin2::from_point_angle(Point2::zero(), *angle));
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.linestring, "scale"),
        ("scale", p.float),
        p.unit,
        |linestring: &mut LineString, scale: &f32| {
            *linestring *= *scale;
            UnitValue
        },
    );

    library.insert_method2_to(
        (p.linestring, "offset"),
        ("offset", p.float),
        p.polygon,
        |linestring: &LineString, offset: &f32| linestring.clone().offset(*offset),
    );

    library.insert_method2_to(
        (p.linestring, "is_point_inside"),
        ("point", p.point2),
        p.polygon,
        |linestring: &LineString, point: &Point2| {
            linestring.point_inside(*point) == PointPolygon::In
        },
    );

    library.insert_method2_to(
        (p.linestring, "is_point_inside_or_on"),
        ("point", p.point2),
        p.polygon,
        |linestring: &LineString, point: &Point2| linestring.point_inside(*point).in_or_on(),
    );

    library.insert_method1_to(
        (p.linestring, "bounds"),
        p.boundingbox2d,
        |linestring: &LineString| linestring.bounds(),
    );

    library.insert_method2_mut_to(
        (p.linestring, "clean"),
        ("distance", p.float),
        p.unit,
        |linestring: &mut LineString, distance| {
            linestring.clean(*distance);
            UnitValue
        },
    );
}

fn multilinestring_functions(library: &mut NativeLibrary, p: CamPaths) {
    library.insert_fn0_to(
        (&p.geometry_module, "new_multilinestring"),
        p.multilinestring,
        || MultiLineString::new(),
    );

    library.insert_fn2_to(
        (p.geometry_module, "load_svg_multilinestring"),
        ("svg_path", p.path_open),
        ("dpi", p.float),
        p.multilinestring,
        |svg_path: &PathValue, dpi: &f32| {
            load_svg_multilinestring(svg_path.0.clone(), *dpi).unwrap()
        },
    );

    library.insert_method2_mut_to(
        (p.multilinestring, "add_linestring"),
        ("linestring", p.linestring),
        p.unit,
        |multilinestring: &mut MultiLineString, linestring: &LineString| {
            multilinestring.linestrings.push(linestring.clone());
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.multilinestring, "remove_linestring"),
        ("index", p.int),
        p.unit,
        |multilinestring: &mut MultiLineString, index: &i32| {
            multilinestring.linestrings.remove(*index as usize);
            UnitValue
        },
    );

    library.insert_method2_ranged_to(
        (p.multilinestring, "get_linestring"),
        (
            "index",
            p.int,
            Some(|multilinestring: &MultiLineString| 0..(multilinestring.linestrings.len() as i32)),
        ),
        p.linestring,
        |multilinestring: &MultiLineString, index: &i32| {
            multilinestring
                .linestrings
                .get(*index as usize)
                .unwrap()
                .clone()
        },
    );

    library.insert_method1_to(
        (p.multilinestring, "number_linestrings"),
        p.int,
        |multilinestring: &MultiLineString| multilinestring.linestrings.len() as i32,
    );

    library.insert_method1_to(
        (p.multilinestring, "is_empty"),
        p.bool,
        |multilinestring: &MultiLineString| multilinestring.is_empty(),
    );

    library.insert_method2_mut_to(
        (p.multilinestring, "translate"),
        ("offset", p.vector2),
        p.unit,
        |multilinestring: &mut MultiLineString, offset: &Vector2| {
            *multilinestring += *offset;
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.multilinestring, "rotate"),
        ("angle", p.float),
        p.unit,
        |multilinestring: &mut MultiLineString, angle: &f32| {
            multilinestring.transform_iso(&Origin2::from_point_angle(Point2::zero(), *angle));
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.multilinestring, "scale"),
        ("scale", p.float),
        p.unit,
        |multilinestring: &mut MultiLineString, scale: &f32| {
            *multilinestring *= *scale;
            UnitValue
        },
    );

    library.insert_method2_to(
        (p.multilinestring, "offset"),
        ("offset", p.float),
        p.polygon,
        |multilinestring: &MultiLineString, offset: &f32| multilinestring.clone().offset(*offset),
    );

    library.insert_method1_to(
        (p.multilinestring, "bounds"),
        p.boundingbox2d,
        |multilinestring: &MultiLineString| multilinestring.bounds(),
    );

    library.insert_method2_mut_to(
        (p.multilinestring, "clean"),
        ("distance", p.float),
        p.unit,
        |multilinestring: &mut MultiLineString, distance| {
            multilinestring.clean(*distance);
            UnitValue
        },
    );
}

fn polygon_functions(library: &mut NativeLibrary, p: CamPaths) {
    library.insert_fn1_to(
        (&p.geometry_module, "new_polygon"),
        ("exterior", p.linestring),
        p.polygon,
        |exterior: &LineString| Polygon {
            exterior: exterior.clone(),
            interiors: Vec::new(),
        },
    );

    library.insert_method2_mut_to(
        (p.polygon, "add_interior"),
        ("interior", p.linestring),
        p.unit,
        |polygon: &mut Polygon, linestring: &LineString| {
            polygon.interiors.push(linestring.clone());
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.polygon, "remove_interior"),
        ("index", p.int),
        p.unit,
        |polygon: &mut Polygon, index: &i32| {
            polygon.interiors.remove(*index as usize);
            UnitValue
        },
    );

    library.insert_method2_ranged_to(
        (p.polygon, "get_interior"),
        (
            "index",
            p.int,
            Some(|polygon: &Polygon| 0..(polygon.interiors.len() as i32)),
        ),
        p.linestring,
        |polygon: &Polygon, index: &i32| polygon.interiors.get(*index as usize).unwrap().clone(),
    );

    library.insert_method1_to(
        (p.polygon, "get_interiors"),
        p.multilinestring,
        |polygon: &Polygon| MultiLineString {
            linestrings: polygon.interiors.clone(),
        },
    );

    library.insert_method1_to(
        (p.polygon, "number_interiors"),
        p.int,
        |polygon: &Polygon| polygon.interiors.len() as i32,
    );

    library.insert_method1_to(
        (p.polygon, "empty_interiors"),
        p.bool,
        |polygon: &Polygon| polygon.interiors.is_empty(),
    );

    library.insert_method1_to(
        (p.polygon, "get_exterior"),
        p.linestring,
        |polygon: &Polygon| polygon.exterior.clone(),
    );

    library.insert_method1_to(
        (p.polygon, "get_boundaries"),
        p.multilinestring,
        |polygon: &Polygon| polygon.clone().into_boundary(),
    );

    library.insert_method2_mut_to(
        (p.polygon, "translate"),
        ("offset", p.vector2),
        p.unit,
        |polygon: &mut Polygon, offset: &Vector2| {
            *polygon += *offset;
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.polygon, "rotate"),
        ("angle", p.float),
        p.unit,
        |polygon: &mut Polygon, angle: &f32| {
            polygon.transform_iso(&Origin2::from_point_angle(Point2::zero(), *angle));
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.polygon, "scale"),
        ("scale", p.float),
        p.unit,
        |polygon: &mut Polygon, scale: &f32| {
            *polygon *= *scale;
            UnitValue
        },
    );

    library.insert_method2_to(
        (p.polygon, "offset"),
        ("offset", p.float),
        p.multipolygon,
        |polygon: &Polygon, offset: &f32| polygon.clone().offset(*offset),
    );

    library.insert_method1_to(
        (p.polygon, "bounds"),
        p.boundingbox2d,
        |polygon: &Polygon| polygon.bounds(),
    );

    library.insert_method2_mut_to(
        (p.polygon, "clean"),
        ("distance", p.float),
        p.unit,
        |polygon: &mut Polygon, distance| {
            polygon.clean(*distance);
            UnitValue
        },
    );
}

fn multipolygon_functions(library: &mut NativeLibrary, p: CamPaths) {
    library.insert_fn0_to(
        (&p.geometry_module, "new_multipolygon"),
        p.multipolygon,
        || MultiPolygon::new(),
    );

    library.insert_method2_mut_to(
        (p.multipolygon, "add_polygon"),
        ("polygon", p.polygon),
        p.unit,
        |multipolygon: &mut MultiPolygon, polygon: &Polygon| {
            multipolygon.polygons.push(polygon.clone());
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.multipolygon, "remove_polygon"),
        ("index", p.int),
        p.unit,
        |multipolygon: &mut MultiPolygon, index: &i32| {
            multipolygon.polygons.remove(*index as usize);
            UnitValue
        },
    );

    library.insert_method2_ranged_to(
        (p.multipolygon, "get_polygon"),
        (
            "index",
            p.int,
            Some(|multipolygon: &MultiPolygon| 0..(multipolygon.polygons.len() as i32)),
        ),
        p.polygon,
        |multipolygon: &MultiPolygon, index: &i32| {
            multipolygon.polygons.get(*index as usize).unwrap().clone()
        },
    );

    library.insert_method1_to(
        (p.multipolygon, "number_polygons"),
        p.int,
        |multipolygon: &MultiPolygon| multipolygon.polygons.len() as i32,
    );

    library.insert_method1_to(
        (p.multipolygon, "is_empty"),
        p.bool,
        |multipolygon: &MultiPolygon| multipolygon.is_empty(),
    );

    library.insert_method1_to(
        (p.multipolygon, "get_boundaries"),
        p.multilinestring,
        |multipolygon: &MultiPolygon| multipolygon.clone().into_boundary(),
    );

    library.insert_method2_mut_to(
        (p.multipolygon, "translate"),
        ("offset", p.vector2),
        p.unit,
        |multipolygon: &mut MultiPolygon, offset: &Vector2| {
            *multipolygon += *offset;
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.multipolygon, "rotate"),
        ("angle", p.float),
        p.unit,
        |multipolygon: &mut MultiPolygon, angle: &f32| {
            multipolygon.transform_iso(&Origin2::from_point_angle(Point2::zero(), *angle));
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.multipolygon, "scale"),
        ("scale", p.float),
        p.unit,
        |multipolygon: &mut MultiPolygon, scale: &f32| {
            *multipolygon *= *scale;
            UnitValue
        },
    );

    library.insert_method2_to(
        (p.multipolygon, "offset"),
        ("offset", p.float),
        p.multipolygon,
        |multipolygon: &MultiPolygon, offset: &f32| multipolygon.clone().offset(*offset),
    );

    library.insert_method1_to(
        (p.multipolygon, "bounds"),
        p.boundingbox2d,
        |multipolygon: &MultiPolygon| multipolygon.bounds(),
    );

    library.insert_method2_mut_to(
        (p.multipolygon, "clean"),
        ("distance", p.float),
        p.unit,
        |multipolygon: &mut MultiPolygon, distance| {
            multipolygon.clean(*distance);
            UnitValue
        },
    );
}

fn plane_functions(library: &mut NativeLibrary, p: CamPaths) {
    // Plane Functions

    library.insert_fn2_to(
        (&p.geometry_module, "new_plane"),
        ("point", p.point3),
        ("normal", p.vector3),
        p.plane,
        |point, normal: &Vector3| Plane::from_point_normal(*point, normal.normalized()),
    );

    library.insert_method1_to((p.plane, "point"), p.point3, |plane: &Plane| plane.point);
    library.insert_method1_to((p.plane, "normal"), p.vector3, |plane: &Plane| plane.normal);

    library.insert_method2_to(
        (p.plane, "offset"),
        ("distance", p.float),
        p.plane,
        |plane: &Plane, distance: &f32| plane.offset(*distance),
    );
}

fn solid_functions(library: &mut NativeLibrary, p: CamPaths) {
    // Solid functions
    library.insert_fn1_to(
        (&p.geometry_module, "load_stl"),
        ("path", p.path_open),
        p.solid,
        |path: &PathValue| Solid3dValue(load_stl(path.0.as_ref()).unwrap()),
    );

    library.insert_method2_mut_to(
        (p.solid, "transform"),
        ("offset", p.vector3),
        p.unit,
        |solid: &mut Solid3dValue, offset: &Vector3| {
            let transform: Matrix4 =
                nalgebra::Isometry3::<f32>::translation(offset.x, offset.y, offset.z).into();
            solid.0.apply_transformation(transform.into());
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.solid, "scale"),
        ("scale", p.float),
        p.unit,
        |solid: &mut Solid3dValue, scale: &f32| {
            let transform: Matrix4 = Matrix4::scale(Vector3::new(*scale, *scale, *scale));
            solid.0.apply_transformation(transform.into());
            UnitValue
        },
    );

    library.insert_method2_mut_to(
        (p.solid, "rotate"),
        ("rotate", p.vector3),
        p.unit,
        |solid: &mut Solid3dValue, rotate: &Vector3| {
            let transform: Matrix4 = nalgebra::Isometry3::<f32>::rotation((*rotate).into()).into();
            solid.0.apply_transformation(transform.into());
            UnitValue
        },
    );

    library.insert_method2_to(
        (p.solid, "intersect_z"),
        ("z", p.float),
        p.multipolygon,
        |solid: &Solid3dValue, z: &f32| intersect_mesh_z(&solid.0, *z as f64).unwrap(),
    );
}

fn toolpath_functions(library: &mut NativeLibrary, p: CamPaths) {
    library.insert_fn2_to(
        (&p.cam_module, "move_to_command"),
        ("point", p.point3),
        ("feedrate", p.float),
        p.toolpath_command,
        |point: &Point3, feedrate: &f32| ToolpathCommand::MoveTo(*point, *feedrate),
    );

    library.insert_fn1_to(
        (&p.cam_module, "rapid_to_command"),
        ("point", p.point3),
        p.toolpath_command,
        |point: &Point3| ToolpathCommand::RapidTo(*point),
    );

    library.insert_fn1_to(
        (&p.cam_module, "rapid_to_z_command"),
        ("z", p.float),
        p.toolpath_command,
        |z: &f32| ToolpathCommand::RapidToZ(*z),
    );

    library.insert_fn1_to(
        (&p.cam_module, "new_toolpath"),
        ("tool", p.string),
        p.toolpath,
        |tool: &String| Toolpath {
            commands: Vec::new(),
            tool: tool.clone(),
        },
    );

    library.insert_method2_mut_to(
        (p.toolpath, "add_command"),
        ("command", p.toolpath_command),
        p.unit,
        |toolpath: &mut Toolpath, command: &ToolpathCommand| {
            toolpath.commands.push(command.clone());
            UnitValue
        },
    );

    library.insert_method2_ranged_to(
        (p.toolpath, "get_command"),
        (
            "index",
            p.int,
            Some(|t: &Toolpath| (0..t.commands.len() as i32)),
        ),
        p.toolpath_command,
        |toolpath: &Toolpath, index: &i32| toolpath.commands.get(*index as usize).unwrap().clone(),
    );
}

fn cam_functions(library: &mut NativeLibrary, p: CamPaths) {
    let input = |s| FunctionCallInputKey::new(s);

    library.insert_fn_to(
        &p.cam_module,
        NativeFunction {
            name: "engrave_path".to_string(),
            input_types: IndexMap::from([
                (input("path"), p.multilinestring.clone()),
                (input("tool"), p.string.clone()),
                (input("cut_feed"), p.float.clone()),
                (input("travel_z"), p.float.clone()),
                (input("start_z"), p.float.clone()),
                (input("end_z"), p.float.clone()),
                (input("step_z"), p.float.clone()),
            ]),
            output_type: p.toolpath.clone(),
            f: Arc::new(|call_stack, values, libraries| {
                let toolpath = engrave_multilinestring(
                    values.get_input(call_stack, "path")?,
                    values
                        .get_input::<String, _>(call_stack, "tool")?
                        .to_string(),
                    *values.get_input(call_stack, "cut_feed")?,
                    *values.get_input(call_stack, "travel_z")?,
                    *values.get_input(call_stack, "start_z")?,
                    *values.get_input(call_stack, "end_z")?,
                    *values.get_input(call_stack, "step_z")?,
                );

                values.insert_value(
                    AbsoluteRuntimeKey::new(call_stack.clone()),
                    Value::from_value(toolpath),
                );
                Ok(())
            }),
        },
    );

    library.insert_fn_to(
        &p.cam_module,
        NativeFunction {
            name: "drag_knife".to_string(),
            input_types: IndexMap::from([
                (input("path"), p.multilinestring.clone()),
                (input("tool"), p.string.clone()),
                (input("knife_radius"), p.float.clone()),
                (input("start_z"), p.float.clone()),
                (input("end_z"), p.float.clone()),
                (input("step_z"), p.float.clone()),
                (input("travel_z"), p.float.clone()),
                (input("cut_feed"), p.float.clone()),
            ]),
            output_type: p.toolpath.clone(),
            f: Arc::new(|call_stack, values, libraries| {
                let toolpath = generate_drag_knife_gcode(
                    values.get_input(call_stack, "path")?,
                    values
                        .get_input::<String, _>(call_stack, "tool")?
                        .to_string(),
                    *values.get_input(call_stack, "knife_radius")?,
                    *values.get_input(call_stack, "start_z")?,
                    *values.get_input(call_stack, "end_z")?,
                    *values.get_input(call_stack, "step_z")?,
                    *values.get_input(call_stack, "travel_z")?,
                    *values.get_input(call_stack, "cut_feed")?,
                );

                values.insert_value(
                    AbsoluteRuntimeKey::new(call_stack.clone()),
                    Value::from_value(toolpath),
                );
                Ok(())
            }),
        },
    );

    library.insert_fn_to(
        &p.cam_module,
        NativeFunction {
            name: "clear_2d_adaptive".to_string(),
            input_types: IndexMap::from([
                (input("clear_polygons"), p.multipolygon.clone()),
                (input("tool"), p.string.clone()),
                (input("tool_diameter"), p.float.clone()),
                (input("min_radius"), p.float.clone()),
                (input("helix_start_z"), p.float.clone()),
                (input("helix_radius"), p.float.clone()),
                (input("helix_step"), p.float.clone()),
                (input("helix_feed"), p.float.clone()),
                (input("cut_stepover"), p.float.clone()),
                (input("cut_lead_in"), p.float.clone()),
                (input("cut_lead_out"), p.float.clone()),
                (input("cut_feed"), p.float.clone()),
                (input("cut_z"), p.float.clone()),
                (input("finish_lead_in"), p.float.clone()),
                (input("finish_lead_out"), p.float.clone()),
                (input("finish_xy"), p.float.clone()),
                (input("finish_feed"), p.float.clone()),
                (input("travel_feed_xy"), p.float.clone()),
                (input("travel_feed_z"), p.float.clone()),
                (input("travel_close_z"), p.float.clone()),
                (input("travel_far_z"), p.float.clone()),
                (input("points_per_turn"), p.float.clone()),
            ]),
            output_type: p.toolpath.clone(),
            f: Arc::new(|call_stack, values, libraries| {
                let clear_polygons = values
                    .get_input::<MultiPolygon, _>(call_stack, "clear_polygons")?
                    .clone();

                let tool = values
                    .get_input::<String, _>(call_stack, "tool")?
                    .to_string();

                let config = AdaptiveClearConfig {
                    tool_diameter: *values.get_input(call_stack, "tool_diameter")?,
                    min_radius: *values.get_input(call_stack, "min_radius")?,
                    helix_start_z: *values.get_input(call_stack, "helix_start_z")?,
                    helix_radius: *values.get_input(call_stack, "helix_radius")?,
                    helix_step: *values.get_input(call_stack, "helix_step")?,
                    helix_feed: *values.get_input(call_stack, "helix_feed")?,
                    cut_stepover: *values.get_input(call_stack, "cut_stepover")?,
                    cut_lead_in: *values.get_input(call_stack, "cut_lead_in")?,
                    cut_lead_out: *values.get_input(call_stack, "cut_lead_out")?,
                    cut_feed: *values.get_input(call_stack, "cut_feed")?,
                    cut_z: *values.get_input(call_stack, "cut_z")?,
                    finish_lead_in: *values.get_input(call_stack, "finish_lead_in")?,
                    finish_lead_out: *values.get_input(call_stack, "finish_lead_out")?,
                    finish_xy: *values.get_input(call_stack, "finish_xy")?,
                    finish_feed: *values.get_input(call_stack, "finish_feed")?,
                    travel_feed_xy: *values.get_input(call_stack, "travel_feed_xy")?,
                    travel_feed_z: *values.get_input(call_stack, "travel_feed_z")?,
                    travel_close_z: *values.get_input(call_stack, "travel_close_z")?,
                    travel_far_z: *values.get_input(call_stack, "travel_far_z")?,
                    points_per_turn: *values.get_input(call_stack, "points_per_turn")?,
                };

                let (commands, _) = clear_polygons_adaptive(clear_polygons, config);

                let toolpath = Toolpath { commands, tool };

                values.insert_value(
                    AbsoluteRuntimeKey::new(call_stack.clone()),
                    Value::from_value(toolpath),
                );

                Ok(())
            }),
        },
    );

    library.insert_fn_to(
        &p.cam_module,
        NativeFunction {
            name: "clear_3d_adaptive".to_string(),
            input_types: IndexMap::from([
                (input("solid"), p.solid.clone()),
                (input("start_z"), p.float.clone()),
                (input("end_z"), p.float.clone()),
                (input("travel_height"), p.float.clone()),
                (input("step_depth"), p.float.clone()),
                (input("exterior"), p.float_optional.clone()),
                (input("tool"), p.string.clone()),
                (input("tool_diameter"), p.float.clone()),
                (input("min_radius"), p.float.clone()),
                (input("helix_radius"), p.float.clone()),
                (input("helix_step"), p.float.clone()),
                (input("helix_feed"), p.float.clone()),
                (input("cut_stepover"), p.float.clone()),
                (input("cut_lead_in"), p.float.clone()),
                (input("cut_lead_out"), p.float.clone()),
                (input("cut_feed"), p.float.clone()),
                (input("finish_lead_in"), p.float.clone()),
                (input("finish_lead_out"), p.float.clone()),
                (input("finish_xy"), p.float.clone()),
                (input("finish_feed"), p.float.clone()),
                (input("travel_feed_xy"), p.float.clone()),
                (input("travel_feed_z"), p.float.clone()),
                (input("tabs_length"), p.float.clone()),
                (input("tabs_height"), p.float.clone()),
                (input("points_per_turn"), p.float.clone()),
            ]),
            output_type: p.toolpath.clone(),
            f: Arc::new(|call_stack, values, libraries| {
                let config = Clear3dAdaptiveConfig {
                    mesh: &values.get_input::<Solid3dValue, _>(call_stack, "solid")?.0,
                    start_z: *values.get_input(call_stack, "start_z")?,
                    end_z: *values.get_input(call_stack, "end_z")?,
                    travel_height: *values.get_input(call_stack, "travel_height")?,
                    step_depth: *values.get_input(call_stack, "step_depth")?,
                    exterior: values
                        .get_input::<FloatOptionValue, _>(call_stack, "exterior")?
                        .0,
                    tool: values
                        .get_input::<String, _>(call_stack, "tool")?
                        .to_string(),
                    tool_diameter: *values.get_input(call_stack, "tool_diameter")?,
                    min_radius: *values.get_input(call_stack, "min_radius")?,
                    helix_radius: *values.get_input(call_stack, "helix_radius")?,
                    helix_step: *values.get_input(call_stack, "helix_step")?,
                    helix_feed: *values.get_input(call_stack, "helix_feed")?,
                    cut_stepover: *values.get_input(call_stack, "cut_stepover")?,
                    cut_lead_in: *values.get_input(call_stack, "cut_lead_in")?,
                    cut_lead_out: *values.get_input(call_stack, "cut_lead_out")?,
                    cut_feed: *values.get_input(call_stack, "cut_feed")?,
                    finish_lead_in: *values.get_input(call_stack, "finish_lead_in")?,
                    finish_lead_out: *values.get_input(call_stack, "finish_lead_out")?,
                    finish_xy: *values.get_input(call_stack, "finish_xy")?,
                    finish_feed: *values.get_input(call_stack, "finish_feed")?,
                    travel_feed_xy: *values.get_input(call_stack, "travel_feed_xy")?,
                    travel_feed_z: *values.get_input(call_stack, "travel_feed_z")?,
                    tabs_length: *values.get_input(call_stack, "tabs_length")?,
                    tabs_height: *values.get_input(call_stack, "tabs_height")?,
                    points_per_turn: *values.get_input(call_stack, "points_per_turn")?,
                };

                let toolpath = clear_3d_adaptive(config).unwrap();

                values.insert_value(
                    AbsoluteRuntimeKey::new(call_stack.clone()),
                    Value::from_value(toolpath),
                );
                Ok(())
            }),
        },
    );

    library.insert_method5_to(
        (&p.toolpath, "generate_gcode"),
        ("rapid_feed", p.float),
        ("start_gcode", p.newlinestring),
        ("end_gcode", p.newlinestring),
        ("toolchange_gcode", p.newlinestring),
        p.newlinestring,
        |toolpath: &Toolpath,
         rapid_feed,
         start_gcode: &String,
         end_gcode: &String,
         toolchange_gcode: &String| {
            toolpath
                .generate_gcode(*rapid_feed, start_gcode, end_gcode, toolchange_gcode)
                .unwrap()
        },
    );

    library.insert_fn2_to(
        (&p.cam_module, "write_gcode"),
        ("program", p.newlinestring),
        ("path", p.path_save),
        p.unit,
        |program: &String, path: &PathValue| {
            std::fs::write(&path.0, program.as_bytes()).unwrap();
            UnitValue
        },
    );
}

#[typetag::serde]
impl LiteralValueBox for Point2 {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox> {
        Box::new(*self) as Box<dyn ValueBox>
    }
}

impl ValueBox for Point2 {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for Point2 {
    fn name() -> &'static str {
        "point2"
    }
}

#[typetag::serde]
impl LiteralValueBox for Vector2 {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox> {
        Box::new(*self) as Box<dyn ValueBox>
    }
}

impl ValueBox for Vector2 {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for Vector2 {
    fn name() -> &'static str {
        "vector2"
    }
}

#[typetag::serde]
impl LiteralValueBox for Point3 {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox> {
        Box::new(*self) as Box<dyn ValueBox>
    }
}

impl ValueBox for Point3 {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for Point3 {
    fn name() -> &'static str {
        "point3"
    }
}

#[typetag::serde]
impl LiteralValueBox for Vector3 {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox> {
        Box::new(*self) as Box<dyn ValueBox>
    }
}

impl ValueBox for Vector3 {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for Vector3 {
    fn name() -> &'static str {
        "vector3"
    }
}

#[typetag::serde]
impl LiteralValueBox for Plane {
    fn into_value(self: Box<Self>) -> Box<dyn ValueBox> {
        Box::new(*self) as Box<dyn ValueBox>
    }
}

impl ValueBox for Plane {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for Plane {
    fn name() -> &'static str {
        "vector3"
    }
}

impl ValueBox for BoundingBox2 {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for BoundingBox2 {
    fn name() -> &'static str {
        "boundingbox2"
    }
}

impl ValueBox for LineString {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for LineString {
    fn name() -> &'static str {
        "linestring"
    }
}

impl ValueBox for MultiLineString {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for MultiLineString {
    fn name() -> &'static str {
        "multilinestring"
    }
}

impl ValueBox for Polygon {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for Polygon {
    fn name() -> &'static str {
        "polygon"
    }
}

impl ValueBox for MultiPolygon {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for MultiPolygon {
    fn name() -> &'static str {
        "multipolygon"
    }
}

#[derive(Debug, Clone)]
pub struct Solid3dValue(pub tri_mesh::Mesh);

impl Display for Solid3dValue {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let bb = self.0.axis_aligned_bounding_box();
        write!(f, "({} x {} x {})", bb.size().x, bb.size().y, bb.size().z,)
    }
}

impl ValueBox for Solid3dValue {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for Solid3dValue {
    fn name() -> &'static str {
        "Solid 3D Value"
    }
}
impl ValueBox for Toolpath {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for Toolpath {
    fn name() -> &'static str {
        "Solid 3D Value"
    }
}
impl ValueBox for ToolpathCommand {
    fn as_any(&self) -> &dyn Any {
        self as &dyn Any
    }

    fn as_any_mut(&mut self) -> &mut dyn Any {
        self as &mut dyn Any
    }

    fn into_any(self: Box<Self>) -> Box<dyn Any> {
        Box::new(self) as Box<dyn Any>
    }

    fn name(&self) -> &'static str {
        <Self as ValueUnboxed>::name()
    }
}

impl ValueUnboxed for ToolpathCommand {
    fn name() -> &'static str {
        "Solid 3D Value"
    }
}
