pub mod cut;
pub mod geometry2d;
pub mod geometry3d;
mod tool;
pub mod toolpath;

pub enum CutType {
    Inside,
    Outside,
    On,
}

pub enum OpType {
    Clear,
    Cut(CutType),
}
