#![feature(error_generic_member_access)]
#![feature(option_get_or_insert_default)]
#![feature(associated_type_defaults)]
#![feature(array_windows)]
#![feature(extract_if)]
#![feature(trait_upcasting)]
#![feature(iter_map_windows)]
#![feature(try_blocks)]
#![allow(dead_code)]
#![allow(unused)]

pub mod app;
pub mod cam;
pub mod cam_module;
pub mod task;
pub mod test;
pub mod ui;
