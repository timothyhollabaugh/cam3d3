use dyn_clone::DynClone;
use slotmap::{new_key_type, SlotMap};
use std::collections::VecDeque;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::{mpsc, Arc};
use std::thread::JoinHandle;
use std::{borrow::Cow, thread};

pub mod interpreter_task;
pub mod load_interpreter_project;
pub mod pick_file;
pub mod render_interpreter_task;
pub mod save_interpreter_project;

pub trait TaskNotify: Send + DynClone {
    fn notify(&self);
}

dyn_clone::clone_trait_object!(TaskNotify);

impl<N> TaskNotify for N
where
    N: Fn() + Send + Clone,
{
    fn notify(&self) {
        (self)()
    }
}

pub struct Tasks {
    tasks: SlotMap<TaskKey, RunningTask>,
    notify: Box<dyn TaskNotify>,
    general_sender: mpsc::Sender<GeneralTaskEvent>,
    general_receiver: mpsc::Receiver<GeneralTaskEvent>,
}

impl Tasks {
    pub fn new(notify: Box<dyn TaskNotify>) -> Tasks {
        puffin::profile_function!();
        let (general_sender, general_receiver) = mpsc::channel();
        Tasks {
            tasks: SlotMap::with_key(),
            notify,
            general_sender,
            general_receiver,
        }
    }

    pub fn update(&mut self) {
        puffin::profile_function!();
        for event in self.general_receiver.try_iter() {
            if let Some(task) = self.tasks.get_mut(event.key) {
                match event.event_type {
                    GeneralTaskEventType::Progress(progress, status) => {
                        task.progress = progress;
                        task.status = status;
                    }
                    GeneralTaskEventType::Done => {
                        self.tasks.remove(event.key);
                    }
                }
            }
        }

        let mut remove_tasks = Vec::new();

        for (key, task) in self.tasks.iter() {
            if let Some(join_handle) = &task.join_handle {
                if join_handle.is_finished() {
                    remove_tasks.push(key);
                    task.panicked.store(true, Ordering::SeqCst);
                }
            }
        }

        for key in remove_tasks {
            self.tasks.remove(key);
        }
    }

    pub fn spawn<T: Task + Send + 'static>(&mut self, task: T) -> RunningTaskHandle<T> {
        puffin::profile_function!();
        let panicked = Arc::new(AtomicBool::new(false));

        let key = self.tasks.insert(RunningTask {
            name: task.name(),
            progress: 0.0,
            status: "".to_string(),
            join_handle: None,
            panicked: panicked.clone(),
        });

        let (running_sender, running_receiver) = mpsc::channel();

        let running_handle = RunningTaskHandle {
            key,
            receiver: running_receiver,
            progresses: VecDeque::new(),
            last_done: None,
            panicked,
        };

        let handle = TaskHandle::<T> {
            notify: self.notify.clone(),
            running_sender: running_sender.clone(),
            general_sender: self.general_sender.clone(),
            key,
        };

        let general_sender = self.general_sender.clone();
        let notify = self.notify.clone();

        let join_handle = thread::spawn(move || {
            let done = task.run(handle);

            general_sender.send(GeneralTaskEvent::done(key)).ok();
            running_sender.send(RunningTaskEvent::Done(done)).ok();
            notify.notify();
        });

        if let Some(running_task) = self.tasks.get_mut(key) {
            running_task.join_handle = Some(join_handle);
        }

        running_handle
    }

    pub fn task(&self, key: TaskKey) -> Option<&RunningTask> {
        self.tasks.get(key)
    }

    pub fn tasks(&self) -> impl Iterator<Item = (TaskKey, &RunningTask)> {
        self.tasks.iter()
    }
}

new_key_type! {
    pub struct TaskKey;
}

pub struct RunningTask {
    pub name: Cow<'static, str>,
    pub progress: f32,
    pub status: String,
    join_handle: Option<JoinHandle<()>>,
    panicked: Arc<AtomicBool>,
}

#[derive(Debug, Clone)]
pub enum RunningTaskEvent<T: Task> {
    Progress(T::Progress),
    Done(T::Done),
    Panicked,
}

#[derive(Debug)]
pub struct RunningTaskHandle<T: Task> {
    key: TaskKey,
    receiver: std::sync::mpsc::Receiver<RunningTaskEvent<T>>,
    progresses: VecDeque<T::Progress>,
    last_done: Option<T::Done>,
    panicked: Arc<AtomicBool>,
}

impl<T: Task> RunningTaskHandle<T> {
    fn update(&mut self) {
        for event in self.receiver.try_iter() {
            match event {
                RunningTaskEvent::Progress(progress) => {
                    self.progresses.push_back(progress);
                    self.progresses.len();
                }
                RunningTaskEvent::Done(done) => self.last_done = Some(done),
                RunningTaskEvent::Panicked => {}
            }
        }
    }

    pub fn progress(&mut self) -> Option<&T::Progress> {
        self.update();
        self.progresses.back()
    }

    pub fn done(&mut self) -> Option<&T::Done> {
        self.update();
        self.last_done.as_ref()
    }

    pub fn take_done(mut self) -> RunningTaskHandleDone<T> {
        self.update();

        if let Some(done) = self.last_done {
            RunningTaskHandleDone::Done(done)
        } else if self.panicked.load(Ordering::SeqCst) {
            RunningTaskHandleDone::Panicked
        } else {
            RunningTaskHandleDone::NotDone(self)
        }
    }

    pub fn take_progress(&mut self) -> Option<T::Progress> {
        self.progresses.len();
        self.progresses.pop_front()
    }

    pub fn take_done_progress(mut self) -> RunningTaskHandleDoneProgress<T> {
        self.update();

        if let RunningTaskHandle {
            last_done: Some(done),
            progresses,
            ..
        } = self
        {
            RunningTaskHandleDoneProgress::Done(done, progresses)
        } else if self.panicked.load(Ordering::SeqCst) {
            RunningTaskHandleDoneProgress::Panicked(self.progresses)
        } else {
            RunningTaskHandleDoneProgress::NotDone(self)
        }
    }

    pub fn key(&self) -> TaskKey {
        self.key
    }
}

pub enum RunningTaskHandleDone<T: Task> {
    Done(T::Done),
    NotDone(RunningTaskHandle<T>),
    Panicked,
}

pub enum RunningTaskHandleDoneProgress<T: Task> {
    Done(T::Done, VecDeque<T::Progress>),
    NotDone(RunningTaskHandle<T>),
    Panicked(VecDeque<T::Progress>),
}

pub trait OptionalRunningTaskHandle<T: Task> {
    fn progress(&mut self) -> Option<&T::Progress>;
    fn done(&mut self) -> Option<&T::Done>;
    fn take_done(&mut self) -> Option<T::Done>;
}

impl<T: Task> OptionalRunningTaskHandle<T> for Option<RunningTaskHandle<T>> {
    fn progress(&mut self) -> Option<&T::Progress> {
        self.as_mut().and_then(|h| h.progress())
    }

    fn done(&mut self) -> Option<&T::Done> {
        self.as_mut().and_then(|h| h.done())
    }

    fn take_done(&mut self) -> Option<T::Done> {
        if let Some(handle) = self.take() {
            match handle.take_done() {
                RunningTaskHandleDone::Done(done) => Some(done),
                RunningTaskHandleDone::NotDone(handle) => {
                    *self = Some(handle);
                    None
                }
                RunningTaskHandleDone::Panicked => None,
            }
        } else {
            None
        }
    }
}

#[derive(Clone, Debug)]
enum GeneralTaskEventType {
    Progress(f32, String),
    Done,
}

#[derive(Clone, Debug)]
struct GeneralTaskEvent {
    key: TaskKey,
    event_type: GeneralTaskEventType,
}

impl GeneralTaskEvent {
    fn done(key: TaskKey) -> GeneralTaskEvent {
        GeneralTaskEvent {
            key,
            event_type: GeneralTaskEventType::Done,
        }
    }

    fn progress(key: TaskKey, progress: &impl TaskProgress) -> GeneralTaskEvent {
        GeneralTaskEvent {
            key,
            event_type: GeneralTaskEventType::Progress(progress.progress(), progress.status()),
        }
    }
}

pub struct TaskHandle<T: Task> {
    notify: Box<dyn TaskNotify>,
    running_sender: mpsc::Sender<RunningTaskEvent<T>>,
    general_sender: mpsc::Sender<GeneralTaskEvent>,
    key: TaskKey,
}

impl<T: Task> TaskHandle<T> {
    pub fn send_progress(&self, progress: T::Progress) {
        self.general_sender
            .send(GeneralTaskEvent::progress(self.key, &progress))
            .ok();

        self.running_sender
            .send(RunningTaskEvent::Progress(progress))
            .ok();

        self.notify.notify();
    }
}

pub trait TaskProgress {
    fn progress(&self) -> f32;
    fn status(&self) -> String;
}

/// Something that runs on a background thread
/// and reports back status as it's working and
/// a result when it's done
pub trait Task: Sized {
    type Progress: TaskProgress + Send + 'static;
    type Done: Send + 'static;

    fn name(&self) -> Cow<'static, str>;
    fn run(self, handle: TaskHandle<Self>) -> Self::Done;
}
